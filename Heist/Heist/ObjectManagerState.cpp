#include "stdafx.h"
#include "ObjectManagerState.h"

ObjectManagerState::ObjectManagerState(void) :
	_object_manager(nullptr)
{
}

ObjectManagerState::~ObjectManagerState(void)
{
}

void ObjectManagerState::Initialize(ObjectManager* object_manager, Renderer* renderer)
{
	_object_manager = object_manager;
}

void ObjectManagerState::Enter()
{
}

void ObjectManagerState::Exit()
{
}