#include "stdafx.h"
#include "FeatureCollisionClickable.h"
#include "ComponentCollision.h"

FeatureCollisionClickable::FeatureCollisionClickable()
{
}


FeatureCollisionClickable::~FeatureCollisionClickable()
{
}

IFeature::FEATURE_TYPE FeatureCollisionClickable::GetType() const
{
	return IFeature::FEATURE_COLLISION_CLICKABLE;
}

unsigned int FeatureCollisionClickable::GetComponentType() const
{
	return IComponent::COMPONENT_COLLISION;
}

void FeatureCollisionClickable::OnAdded(IComponent& component)
{
	if (component.GetComponentType() == GetComponentType())
	{
		auto& collision_component = (ComponentCollision&)component;

		auto& collider = collision_component._collider;

		collider.EnableCollisionFlag(COLLISION_FLAG::CLICKABLE);
	}
}

void FeatureCollisionClickable::OnRemoved(IComponent& component)
{
	if (component.GetComponentType() == GetComponentType())
	{
		auto& collision_component = (ComponentCollision&)component;

		auto& collider = collision_component._collider;

		collider.DisableCollisionFlag(COLLISION_FLAG::CLICKABLE);
	}
}