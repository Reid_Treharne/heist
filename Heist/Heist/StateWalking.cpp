#include "stdafx.h"
#include "StateWalking.h"
#include "BaseObject.h"

#include "../TileManager/Tile.h"
#include "../CollisionManager/CollisionLibrary.h"
#include "../TileManager/PathFinder.h"

using namespace CollisionLibrary;

StateWalking::StateWalking(BaseObject* base_object, const Tile* destination_tile) :
	_move_speed(5.0f)
{
	// Find a path to the destination tile and save it
	XMFLOAT3 start_position;
	base_object->GetWorldPosition(start_position);
	PathFinder::GetInstance().FindPath(start_position.x,
		start_position.z,
		destination_tile->GetWorldX(),
		destination_tile->GetWorldZ(),
		_destination_path);
}


StateWalking::~StateWalking(void)
{
}

void StateWalking::Enter()
{
}

bool StateWalking::Update(std::shared_ptr<BaseObject> base_object, float delta_time)
{
	bool state_finished = false;

	// Check if the player's destination path vector has any tiles,
	// if so, that means the player needs to move to the next one.
	if (_destination_path.empty() == false)
	{
		do
		{
			// Get the next tile in the vector
			const Tile* next_tile  = _destination_path[_destination_path.size() - 1];

			// Store the tile's position
			XMFLOAT3 tile_position(next_tile->GetWorldX(), 0.0f, next_tile->GetWorldZ());
			XMFLOAT3 player_position;

			base_object->GetWorldPosition(player_position);

			// Zero out the player's y position since we are not using the tile's y position
			player_position.y = 0.0f;

			// Get a vector from the player's position to the tile's position
			XMFLOAT3 to_tile = tile_position - player_position;

			// Check if the player has reached this tile, and if so pop a tile and
			// restart the algorithm
			if (Magnitude(to_tile) < 0.1f)
			{
				_destination_path.pop_back();
			}
			else
			{
				// Look at the tile's position
				base_object->LookAt2D(tile_position);

				// Move forward towards the tile
				base_object->Translate(XMFLOAT3(0.0f, 0.0f, 1.0f) * _move_speed * delta_time);

				break;
			}
		} while (_destination_path.empty() == false);
	}
	else
	{
		state_finished = true;
	}

	return state_finished;
}
