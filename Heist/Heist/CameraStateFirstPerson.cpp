#include "stdafx.h"
#include "CameraStateFirstPerson.h"
#include "Camera.h"
#include "ComponentFactory.h"
#include "FeatureFactory.h"
#include "../InputManager/InputManager.h"

CameraStateFirstPerson::CameraStateFirstPerson(void)
{
}


CameraStateFirstPerson::~CameraStateFirstPerson(void)
{
}

void CameraStateFirstPerson::Enter(std::shared_ptr<BaseObject> object_attached_to)
{
	if (object_attached_to)
	{
		// Attach the camera to the object
		object_attached_to->AttachObject(_camera, BaseObject::NORMAL, BaseObject::ATTACHMENT_TYPE::ATTACH, XMFLOAT3(0.0f, 2.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f));

		// Remove and save the old render component if it exists. It will be added back on exit. 
		attached_object_render_component = object_attached_to->RemoveComponent(IComponent::COMPONENT_RENDER);
		
		// Add the walk component and collision component
		object_attached_to->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_WALK));
		
		CollisionComponentParams collision_params(object_attached_to);
		object_attached_to->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_COLLISION, collision_params));
		FeatureCollisionTriggerParams collision_trigger_params('E');
		object_attached_to->AddComponentFeature(FeatureFactory::CreateFeature(IFeature::FEATURE_COLLISION_TRIGGER, collision_trigger_params));
		object_attached_to->AddComponentFeature(FeatureFactory::CreateFeature(IFeature::FEATURE_COLLISION_CLICKABLE));
		object_attached_to->AddComponentFeature(FeatureFactory::CreateFeature(IFeature::FEATURE_COLLISION_COLLIDABLE));


		// Clear the object's states to idle since we're taking control of this object
		object_attached_to->ClearStatesToIdle();
	}

	CameraState::Enter(object_attached_to);
}

void CameraStateFirstPerson::Exit()
{
	if (_object_attached_to)
	{
		// Detach the camera from the object
		_object_attached_to->DetachObject(BaseObject::NORMAL, _camera);

		// If the object was rendered when we attached to it, add back the render component
		if (attached_object_render_component)
		{
			_object_attached_to->AddComponent(std::move(attached_object_render_component));
		
			attached_object_render_component = nullptr;
		}
		
		_object_attached_to->RemoveComponent(IComponent::COMPONENT_WALK);
		_object_attached_to->RemoveComponent(IComponent::COMPONENT_COLLISION);
	}

	CameraState::Exit();
}


void CameraStateFirstPerson::Input(InputManager* input_manager)
{
	InputManager& input = *input_manager;
}



