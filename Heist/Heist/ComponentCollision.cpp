#include "stdafx.h"
#include "ComponentCollision.h"
#include "BaseObject.h"
#include "FeatureFactory.h"
#include "DirtyFlagsManager.h"
#include "../CollisionManager/ObjectTree.h"
#include "../CollisionManager/CollisionManager.h"

using namespace std::placeholders;

ComponentCollision::ComponentCollision(
	ObjectTree<std::shared_ptr<BaseObject>>* object_tree,
	CollisionManager* collision_manager,
	std::shared_ptr<BaseObject> base_object) :
	_object_tree(object_tree),
	_collision_manager(collision_manager),
	_current_attempts_to_add_to_tree(0),
	_max_attempts_to_add_to_tree(3),
	_object_added_to_tree(false)
{
	// The object will be added to the object tree in the Update function. See Update function for more details.
	_collider._object = base_object;

	UpdateCollider();
}


ComponentCollision::~ComponentCollision(void)
{
	_object_tree->RemoveObject(_collider);
}

void ComponentCollision::Update(BaseObject* base_object, float delta_time)
{
	IComponent::Update(base_object, delta_time);

	// Since the collision component adds the object it belongs to to the object tree,
	// it needs to be a shared_ptr. Because of this, the object must be added when the
	// component is contructed since the 'this' pointer can not be passed as a shared_ptr.
	// So we will just ignore this parameter.
	(void)base_object;

	if (_object_added_to_tree == false)
	{
		UpdateCollider();

		if (_object_tree->AddObject(_collider))
		{
			_object_added_to_tree = true;
		}
		else
		{
			// If the object failed to be added to the tree, it probably means the object was outside the tree's AABB.
			// Sometimes, newly created objects are created outside of this bounds and need to first be ground clamped
			// in order to be placed inside the AABB. To account for this, if the object fails to be added, we will keep
			// trying to add it until we reach our _max_attempts_to_add_to_tree, giving it time to be ground clamped.
			_current_attempts_to_add_to_tree++;

			if (_current_attempts_to_add_to_tree >= _max_attempts_to_add_to_tree)
			{
				// If this gets hit, it probably means the object is outside the object tree's AABB
				Log::Error("ComponentCollision::Update - Could not add object to tree\n");
			}
		}
	}
	else
	{
			// If the collision flags changed then the object needs to be updated
		if (_collider.CollisionFlagsChanged() ||
			// If this is a triggerable object, always check collision with triggers for simplicity sake
			_collider.IsCollisionFlagEnabled(COLLISION_FLAG::TRIGGERABLE) ||
			// If the position, orientation, or geometry of the object has changed
			_collider._object->GetDirty(DirtyFlagsManager::DIRTY_FLAGS::DIRTY_POSITION) ||
			_collider._object->GetDirty(DirtyFlagsManager::DIRTY_FLAGS::DIRTY_ORIENTATION) ||
			_collider._object->GetDirty(DirtyFlagsManager::DIRTY_FLAGS::DIRTY_GEOMETRY))
		{
			// Update the object's collider to reflect these changes
			UpdateCollider();

			// Check collision
			_object_tree->UpdateObject(_collider);
		}
	}
}

IComponent::COMPONENT_TYPE ComponentCollision::GetComponentType() const
{
	return COMPONENT_COLLISION;
}

void ComponentCollision::OnCollide(std::shared_ptr<BaseObject> collider1, std::shared_ptr<BaseObject> collider2, bool collided_xyz[3])
{
	auto& old_position = collider1->GetWorldOldPosition();

	auto resolved_position = collider1->GetWorldPosition();

	// Revert the position of any axis that is causing a collision
	if (collided_xyz[0])
	{
		resolved_position.x = old_position.x;
		_collider._aabb.min.x = _collider._previous_aabb.min.x;
		_collider._aabb.max.x = _collider._previous_aabb.max.x;
	}
	if (collided_xyz[1])
	{
		resolved_position.y = old_position.y;
		_collider._aabb.min.y = _collider._previous_aabb.min.y;
		_collider._aabb.max.y = _collider._previous_aabb.max.y;
	}
	if (collided_xyz[2])
	{
		resolved_position.z = old_position.z;
		_collider._aabb.min.z = _collider._previous_aabb.min.z;
		_collider._aabb.max.z = _collider._previous_aabb.max.z;
	}

	collider1->SetPosition(resolved_position);
}

void ComponentCollision::UpdateCollider()
{
	auto& object_name = _collider._object->GetName();

	_collider._previous_aabb = _collider._aabb;

	if (_collision_manager->GetModelBounds(object_name, _collider._aabb) && _collision_manager->GetModelTriangles(object_name, _collider._triangles))
	{
		XMFLOAT3 object_world_position;
		XMFLOAT4X4 object_world_matrix;

		_collider._object->GetWorldPosition(object_world_position);
		_collider._object->GetWorldMatrix(object_world_matrix);

		_collider._aabb.min = _collider._aabb.min + object_world_position;
		_collider._aabb.max = _collider._aabb.max + object_world_position;

		_collider._world_matrix = object_world_matrix;
	}
	else
	{
		Log::Warning("ComponentCollision::ComponentCollision - Failed to get object bounds for: %s\n", object_name.c_str());
	}
}
