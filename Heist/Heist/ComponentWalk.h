#pragma once
#include "IComponent.h"

class InputManager;
class Camera;

class ComponentWalk : public IComponent
{
public:
	ComponentWalk(const InputManager* input_manager);
	virtual ~ComponentWalk(void);

	virtual void Update(BaseObject* base_object, float delta_time) override;
	virtual IComponent::COMPONENT_TYPE GetComponentType() const override;

private:
	const InputManager* _input_manager;
};

