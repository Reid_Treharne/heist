#include "stdafx.h"
#include "ObjectManager.h"
#include "Terrain.h"
#include "Service.h"
#include "DirtyFlagsManager.h"
#include "../Logger/Log.h"

ObjectManager::ObjectManager(void) :
	_current_state(nullptr)
{
}


ObjectManager::~ObjectManager(void)
{
}

void ObjectManager::Initialize(Renderer* renderer, ObjectFactory* object_factory, ObjectTree<std::shared_ptr<BaseObject>>* object_tree)
{
	// initialize the states
	_state_basic.Initialize(this, renderer);
	_state_debug.Initialize(this, renderer, object_factory, object_tree);

	// Set the object manager's state
	SetState(&_state_basic);
}

void ObjectManager::AddObject(std::shared_ptr<BaseObject> base_render_object, OBJECT_NAME object_name)
{
	if (base_render_object != nullptr)
	{
		// Add the object to the object list
		_object_map[object_name].push_back(base_render_object);
	}
	else
	{
		assert(false && "ObjectManager::AddObject - Trying to add null object");
	}
}

void ObjectManager::RemoveObject(std::shared_ptr<BaseObject> base_render_object, OBJECT_NAME object_name)
{
	if (base_render_object != nullptr)
	{
		if (object_name < OBJECT_NAME::NUM_NAMES && object_name >= 0)
		{
			auto& object_vector = _object_map[object_name];

			auto iter = object_vector.begin();

			for (; iter != object_vector.end(); ++iter)
			{
				if (*iter == base_render_object)
				{
					break;
				}
			}

			if (iter != object_vector.end())
			{
				object_vector.erase(iter);
			}
			else
			{
				Log::Warning("ObjectManager::RemoveObject - could not find object\n");
			}
		}
		else
		{
			Log::Warning("ObjectManager::RemoveObject - object name out of bounds\n");
		}
	}
	else
	{
		Log::Warning("ObjectManager::RemoveObject - Trying to remove null object\n");
	}
}

void ObjectManager::Update(float delta_time)
{
	// loop through the object names
	for (auto object_name_iter = _object_map.begin(); object_name_iter != _object_map.end(); ++object_name_iter)
	{
		auto& name_list = object_name_iter->second;

		// loop through the objects with this name
		for (size_t i = 0; i < name_list.size(); ++i)
		{
			auto& current_object = name_list[i];

			current_object->Update(delta_time);
		}
	}

	DirtyFlagsManager::ResetAll();
}

void ObjectManager::SetAsDebug()
{
	SetState(&_state_debug);
}

void ObjectManager::SetAsBasic()
{
	SetState(&_state_basic);
}

void ObjectManager::SetState(ObjectManagerState* state)
{
	if (state != nullptr && state != _current_state)
	{
		if (_current_state != nullptr)
		{
			_current_state->Exit();
		}

		_current_state = state;

		_current_state->Enter();
	}
	else
	{
		Log::Warning("ObjectManager::SetState - Trying to set object manager state to null\n");
	}
}
