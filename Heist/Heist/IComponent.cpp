#include "stdafx.h"
#include "IComponent.h"

void IComponent::Update(BaseObject* base_object, float delta_time)
{
	for (auto& feature : _features)
	{
		feature->Update(base_object, *this);
	}
}

void IComponent::AddFeature(std::unique_ptr<IFeature> feature)
{
	if (feature->GetComponentType() == this->GetComponentType())
	{
		auto iter = FindFeature(feature->GetType());

		auto feature_type = feature->GetType();

		feature->OnAdded(*this);

		if (iter != _features.end())
		{
			iter->swap(feature);
		}
		else
		{
			_features.push_back(std::move(feature));
		}
	}
}

void IComponent::RemoveFeature(unsigned int feature_type)
{
	auto iter = FindFeature(feature_type);

	if (iter != _features.end())
	{
		auto& feature = *iter;

		feature->OnRemoved(*this);

		_features.erase(iter);
	}
}

std::vector<std::unique_ptr<IFeature>>::iterator IComponent::FindFeature(unsigned int feature_type)
{
	auto predicate = [feature_type](const std::unique_ptr<IFeature>& f)
	{
		return f->GetType() == feature_type;
	};

	auto iter = std::find_if(_features.begin(), _features.end(), predicate);

	return iter;
}