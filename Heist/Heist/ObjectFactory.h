#pragma once

#include "ObjectNames.h"
#include <memory>
#include <functional>
#include <DirectXMath.h>

using namespace DirectX;

class ObjectManager;
class ModelManager;
class LightManager;
class BaseObject;
class Service;

class ObjectFactory
{
public:
	ObjectFactory(void);
	~ObjectFactory(void);

	void Initialize(ObjectManager* object_manager, ModelManager* model_manager, LightManager* light_manager, const Service* service);

	std::shared_ptr<BaseObject> CreateObject(const std::string& mesh_name, OBJECT_NAME object_name, const XMFLOAT3& position, const XMFLOAT3& rotation) const;

	std::shared_ptr<BaseObject> CreateAABB(const XMFLOAT3& min, const XMFLOAT3& max, const XMFLOAT4& color) const;
	std::shared_ptr<BaseObject> CreateAABB(const XMFLOAT3& max, const XMFLOAT4& color) const;

private:
	std::shared_ptr<BaseObject> CreateNull(const std::string& mesh_name);
	std::shared_ptr<BaseObject> CreateInteractable(const std::string& mesh_name);
	std::shared_ptr<BaseObject> CreateEquipment(const std::string& mesh_name);
	std::shared_ptr<BaseObject> CreateAesthetic(const std::string& mesh_name);
	std::shared_ptr<BaseObject> CreateSkybox(const std::string& mesh_name);
	std::shared_ptr<BaseObject> CreateDirectionalLight(const std::string& mesh_name);

	std::string GenerateAABBMeshObject(const XMFLOAT3& max, const XMFLOAT4 color) const;
	std::string GenerateAABBMeshObject(const XMFLOAT3& min, const XMFLOAT3& max, const XMFLOAT4 color) const;

	ObjectManager* _object_manager;
	ModelManager* _model_manager;
	LightManager* _light_manager;
	const Service* _service;

	std::function<std::shared_ptr<BaseObject>(const std::string& mesh_name)> _creator[OBJECT_NAME::NUM_NAMES];
};

