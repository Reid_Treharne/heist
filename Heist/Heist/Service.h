#pragma once
#include "../CollisionManager/Collider.h"
#include "../CollisionManager/ObjectTree.h"
#include "Camera.h"
#include "Terrain.h"

class Renderer;
class ObjectManager;
class HUDManager;
class InputManager;
class ObjectFactory;
class TileManager;
class ModelManager;
class CollisionManager;
class CameraManager;

class Service
{
public:
	Service(void);
	~Service(void);

	Renderer* GetRenderer() const;
	ObjectManager* GetObjectManager() const;
	HUDManager* GetHUDManager() const;
	std::shared_ptr<Camera> GetCamera() const;
	CameraManager* GetCameraManager() const;
	InputManager* GetInputManager() const;
	ObjectFactory* GetObjectFactory() const;
	std::shared_ptr<Terrain> GetTerrain() const;
	TileManager* GetTileManager() const;
	ObjectTree<std::shared_ptr<BaseObject>>* GetObjectTree() const;
	ModelManager* GetModelManager() const;
	CollisionManager* GetCollisionManager() const;

	void SetRenderer(Renderer* renderer);
	void SetObjectManager(ObjectManager* object_manager);
	void SetHUDManager(HUDManager* HUD_manager);
	void SetCamera(std::shared_ptr<Camera> camera);
	void SetCameraManager(CameraManager* camera_manager);
	void SetInputManager(InputManager* input_manager);
	void SetObjeectFactory(ObjectFactory* object_factory);
	void SetTerrain(std::shared_ptr<Terrain> terrain);
	void SetTileManager(TileManager* tile_manager);
	void SetObjectTree(ObjectTree<std::shared_ptr<BaseObject>>* object_tree);
	void SetModelManager(ModelManager* model_manager);
	void SetCollisionManager(CollisionManager* collision_manager);

private:
	Renderer* _renderer;
	ObjectManager* _object_manager;
	HUDManager* _HUD_manager;
	std::shared_ptr<Camera> _camera;
	CameraManager* _camera_manager;
	InputManager* _input_manager;
	ObjectFactory* _object_factory;
	std::shared_ptr<Terrain> _terrain;
	TileManager* _tile_manager;
	ObjectTree<std::shared_ptr<BaseObject>>* _object_tree;
	ModelManager* _model_manager;
	CollisionManager* _collision_manager;
};

