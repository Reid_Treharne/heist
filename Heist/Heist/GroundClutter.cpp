#include "stdafx.h"
#include "GroundClutter.h"
#include "ComponentFactory.h"
#include "ObjectNames.h"
#include "../Renderer/MeshObject.h"
#include "../Renderer/VertexDeclarations.h"

GroundClutter::GroundClutter(void) :
	_points_changed(true)
{
}


GroundClutter::~GroundClutter(void)
{
	RemoveComponent(IComponent::COMPONENT_GROUND_CLAMP);
}

void GroundClutter::Initialize(MeshObject* mesh_object, std::unordered_map<int, XMFLOAT2>&& points)
{
	GroundClampComponentParams component_params(OBJECT_NAME::NAME_GROUND_CLUTTER);
	AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_GROUND_CLAMP, component_params));

	_mesh_object = mesh_object;

	if (points.empty() == false)
	{
		for (auto& point : points)
		{
			// randomly offset the grass from the middle of the tile by +-0.300f so it looks more natural
			auto rand_x = ((rand() % 600) / 1000.0f) - 0.3f;
			auto rand_y = ((rand() % 600) / 1000.0f) - 0.3f;

			_points[point.first] = (XMFLOAT3(point.second.x + 0.5f + rand_x, 0.0f, point.second.y + 0.5f + rand_y));
		}
	}
}

void GroundClutter::Update(float delta_time)
{
	if (_points_changed && _points.empty() == false)
	{
		UpdatePoints();

		_points_changed = false;
	}

	BaseObject::Update(delta_time);
}

void GroundClutter::Render() const
{
	if (_points.empty() == false)
	{
		_mesh_object->Render();
	}
}

void GroundClutter::AddOrUpdatePoint(unsigned int index, const XMFLOAT3& point)
{
	_points[index] = point;

	_points_changed = true;
}

void GroundClutter::UpdatePoints()
{
	std::vector<VERTEX_POS> vertices;
	VERTEX_POS vertex;

	for (auto& point : _points)
	{
		vertex.position = point.second;
		vertices.push_back(vertex);
	}

	D3D11_BUFFER_DESC desc;

	// Create vertex buffer
	desc.ByteWidth = sizeof(VERTEX_POS) * vertices.size();
	desc.CPUAccessFlags = NULL;
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.Usage = D3D11_USAGE_IMMUTABLE;
	desc.MiscFlags = NULL;
	desc.StructureByteStride = sizeof(VERTEX_POS);

	_mesh_object->SetVertexBuffer(desc, &vertices[0]);
}
