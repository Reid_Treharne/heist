#include "stdafx.h"
#include "FeatureCollisionCollidable.h"
#include "ComponentCollision.h"

using namespace std::placeholders;

FeatureCollisionCollidable::FeatureCollisionCollidable()
{
}


FeatureCollisionCollidable::~FeatureCollisionCollidable()
{
}

IFeature::FEATURE_TYPE FeatureCollisionCollidable::GetType() const
{
	return IFeature::FEATURE_COLLISION_COLLIDABLE;
}

unsigned int FeatureCollisionCollidable::GetComponentType() const
{
	return IComponent::COMPONENT_COLLISION;
}

void FeatureCollisionCollidable::OnAdded(IComponent& component)
{
	if (component.GetComponentType() == GetComponentType())
	{
		auto& collision_component = (ComponentCollision&)component;

		auto& collider = collision_component._collider;

		collider.BindOnCollideFunction(std::bind(&ComponentCollision::OnCollide, &collision_component, _1, _2, _3));
	}
}

void FeatureCollisionCollidable::OnRemoved(IComponent& component)
{
	if (component.GetComponentType() == GetComponentType())
	{
		auto& collision_component = (ComponentCollision&)component;

		auto& collider = collision_component._collider;

		collider.BindOnCollideFunction(nullptr);
	}
}