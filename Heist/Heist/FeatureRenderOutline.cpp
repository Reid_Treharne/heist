#include "stdafx.h"
#include "FeatureRenderOutline.h"
#include "BaseObject.h"
#include "IComponent.h"
#include "ComponentRender.h"
#include "../Logger/Log.h"
#include "../CollisionManager/CollisionLibrary.h"

using namespace CollisionLibrary;

FeatureRenderOutline::FeatureRenderOutline(
	std::shared_ptr<BaseObject> outline_distance_base_object,
	float outline_range,
	const XMFLOAT3& outline_color) :
	_outline_distance_base_object(outline_distance_base_object),
	_outline_range(outline_range),
	_outline_color(outline_color)
{
}

FeatureRenderOutline::~FeatureRenderOutline()
{

}

FeatureRenderOutline::FEATURE_TYPE FeatureRenderOutline::GetType() const
{
	return IFeature::FEATURE_RENDER_OUTLINE_WHEN_NEAR_OBJECT;
}

unsigned int FeatureRenderOutline::GetComponentType() const
{
	return IComponent::COMPONENT_RENDER;
}

void FeatureRenderOutline::OnAdded(IComponent& component)
{
	auto& render_component = (ComponentRender&)component;

	for (auto& mesh_object : render_component._mesh_objects)
	{
		render_component.EnableRenderFlag(RENDER_FLAG::OUTLINE);
	}
}

void FeatureRenderOutline::OnRemoved(IComponent& component)
{
	auto& render_component = (ComponentRender&)component;

	for (auto& mesh_object : render_component._mesh_objects)
	{
		render_component.DisableRenderFlag(RENDER_FLAG::OUTLINE);
	}
}
void FeatureRenderOutline::Update(BaseObject* base_object, IComponent& component)
{
	if (component.GetComponentType() == IComponent::COMPONENT_RENDER)
	{
		auto& render_component = (ComponentRender&)component;

		XMFLOAT3 object_position;
		XMFLOAT3 camera_position;

		base_object->GetWorldPosition(object_position);

		_outline_distance_base_object->GetWorldPosition(camera_position);

		auto distance_to_camera = CollisionLibrary::MagnitudeSquared(object_position - camera_position);

		if (distance_to_camera < _outline_range * _outline_range)
		{
			render_component._render_flags.outline_color = _outline_color;
		}
		else
		{
			render_component._render_flags.outline_color = XMFLOAT3(0.0f, 0.0f, 0.0f);
		}
	}
	else
	{
		Log::Error("FeatureRenderOutline::Update - Unexpected component type\n");
	}
}
