#pragma once
#include "CameraState.h"

class BaseObject;

class CameraStateThirdPerson : public CameraState
{
public:
	CameraStateThirdPerson(void);
	~CameraStateThirdPerson(void);

	virtual void Initialize(std::shared_ptr<Camera> camera) override;

	virtual void Enter(std::shared_ptr<BaseObject> object_attached_to) override;
	virtual void Exit() override;

	virtual void Input(InputManager* input_manager) override;
	virtual void Update(float delta_time) override;

private:
	void Translate(const XMFLOAT3& translation);

	bool _dirty_camera_position;

	XMFLOAT3 _old_attached_object_position;
	XMFLOAT3 _default_translation;
	XMFLOAT3 _delta_translation;

	float _rotation_speed;
	float _raise_lower_speed;

	float _max_height;
	float _min_height;
};

