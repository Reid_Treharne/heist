#include "stdafx.h"
#include "Skybox.h"
#include "Camera.h"
#include "../Renderer/MeshObject.h"

Skybox::Skybox()
	//_mesh_object(nullptr)
{
}


Skybox::~Skybox()
{
}

void Skybox::Initialize(/*MeshObject* mesh_object, */const std::shared_ptr<Camera> camera)
{
	//_mesh_object = mesh_object;
	_camera = camera;
}

void Skybox::Update(float delta_time)
{
	// Set the position of the skybox to that it's centered around the camera
	//auto camera_position = _camera->GetWorldPosition();

	//SetPosition(camera_position);

	BaseObject::Update(delta_time);
}

