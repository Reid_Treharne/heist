#include "stdafx.h"
#include "ComponentFactory.h"
#include "Service.h"
#include "ComponentInteract.h"
#include "ComponentMouseOver.h"
#include "ComponentWalk.h"
#include "ComponentCollision.h"
#include "ComponentGroundClamp.h"
#include "ComponentForge.h"
#include "ComponentRender.h"
#include "../Logger/Log.h"

Service* ComponentFactory::_service = nullptr;
std::vector<std::function<std::unique_ptr<IComponent>(const IComponentParams& parameters)>> ComponentFactory::_create_functions;

void ComponentFactory::Initialize(Service* service)
{
	_service = service;

	_create_functions.resize(IComponent::COMPONENT_COUNT);

	_create_functions[IComponent::COMPONENT_INTERACT] = std::bind(&ComponentFactory::CreateInteractComponent, std::placeholders::_1);
	_create_functions[IComponent::COMPONENT_MOUSE_OVER] = std::bind(&ComponentFactory::CreateMouseOverComponent, std::placeholders::_1);
	_create_functions[IComponent::COMPONENT_WALK] = std::bind(&ComponentFactory::CreateWalkComponent, std::placeholders::_1);
	_create_functions[IComponent::COMPONENT_COLLISION] = std::bind(&ComponentFactory::CreateCollisionComponent, std::placeholders::_1);
	_create_functions[IComponent::COMPONENT_GROUND_CLAMP] = std::bind(&ComponentFactory::CreateGroundClampComponent, std::placeholders::_1);
	_create_functions[IComponent::COMPONENT_FORGE] = std::bind(&ComponentFactory::CreateForgeComponent, std::placeholders::_1);
	_create_functions[IComponent::COMPONENT_RENDER] = std::bind(&ComponentFactory::CreateRenderComponent, std::placeholders::_1);
}

// Move all of the components' constructors to private section and make ComponentFactory their friend so they can only be created through this class
std::unique_ptr<IComponent> ComponentFactory::CreateComponent(IComponent::COMPONENT_TYPE component_type, const IComponentParams& parameters)
{
	std::unique_ptr<IComponent> component = nullptr;

	auto number_of_components = (int)_create_functions.size();	// Initialize the component factory

	if (component_type >= 0 && component_type < number_of_components)
	{
		component = _create_functions[component_type](parameters);
	}
	else
	{
		Log::Error("ComponentFactory::CreateComponent - Component type out of valid range. Value: %d\n", component_type);
	}

	return component;
}

std::unique_ptr<IComponent> ComponentFactory::CreateInteractComponent(const IComponentParams& parameters)
{
	(void)parameters; // unused

	std::unique_ptr<IComponent> component(new ComponentInteract(
		_service->GetInputManager(),
		_service->GetCamera(),
		_service->GetObjectTree(),
		_service->GetTileManager(),
		_service->GetTerrain()));

	return component;
}

std::unique_ptr<IComponent> ComponentFactory::CreateMouseOverComponent(const IComponentParams& parameters)
{
	(void)parameters; // unused

	std::unique_ptr<IComponent> component(new ComponentMouseOver(
		_service->GetInputManager(),
		_service->GetObjectFactory(),
		_service->GetObjectManager(),
		_service->GetCamera(),
		_service->GetObjectTree()));

	return component;
}

std::unique_ptr<IComponent> ComponentFactory::CreateWalkComponent(const IComponentParams& parameters)
{
	(void)parameters; // unused

	std::unique_ptr<IComponent> component(new ComponentWalk(
		_service->GetInputManager()));

	return component;
}

std::unique_ptr<IComponent> ComponentFactory::CreateCollisionComponent(const IComponentParams& parameters)
{
	auto& component_parameters = (const CollisionComponentParams&)parameters;

	std::unique_ptr<IComponent> component(new ComponentCollision(
		_service->GetObjectTree(),
		_service->GetCollisionManager(),
		component_parameters._base_object));

	return component;
}

std::unique_ptr<IComponent> ComponentFactory::CreateGroundClampComponent(const IComponentParams& parameters)
{
	auto& ground_clamp_params = (const GroundClampComponentParams&)parameters;

	std::unique_ptr<IComponent> component(new ComponentGroundClamp(
		ground_clamp_params._object_name,
		ground_clamp_params._offset_y,
		_service->GetTerrain(),
		_service->GetCollisionManager(),
		_service->GetTileManager()));

	return component;
}

std::unique_ptr<IComponent> ComponentFactory::CreateForgeComponent(const IComponentParams& parameters)
{
	(void)parameters; // unused

	std::unique_ptr<IComponent> component(new ComponentForge(
		_service->GetInputManager(),
		_service->GetCamera(),
		_service->GetTerrain(),
		_service->GetTileManager()));

	return component;
}

std::unique_ptr<IComponent> ComponentFactory::CreateRenderComponent(const IComponentParams& parameters)
{
	auto& render_parameters = (const RenderComponentParams&)parameters;

	std::unique_ptr<IComponent> component(new ComponentRender(
		render_parameters._mesh_objects));

	return component;
}
