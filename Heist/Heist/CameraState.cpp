#include "stdafx.h"
#include "CameraState.h"
#include "Camera.h"

CameraState::CameraState(void) :
	_camera(nullptr),
	_object_attached_to(nullptr)
{

}


CameraState::~CameraState(void)
{
}

void CameraState::Initialize(std::shared_ptr<Camera> camera)
{
	_camera = camera;
}

void CameraState::Enter(std::shared_ptr<BaseObject> object_attached_to)
{
	SetObjectAttachedTo(object_attached_to);
}

void CameraState::Exit()
{
	_object_attached_to = nullptr;
}

void CameraState::SetObjectAttachedTo(std::shared_ptr<BaseObject> object_attached_to)
{
	_object_attached_to = object_attached_to;
}