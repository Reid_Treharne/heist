#pragma once

#include "../EventSystem/IEvent.h"
#include "EventIDs.h"
#include <unordered_set>

class EventTerrainHeightChanged : public IEvent
{
public:
	EventTerrainHeightChanged(std::unordered_set<int>&& moved_triangle_indices);
	virtual ~EventTerrainHeightChanged(void);

	inline virtual unsigned int GetID() const override { return EVENT_TERRAIN_HEIGHT_CHANGED; }
	inline const std::unordered_set<int>& GetMovedTriangleIndices() const { return _moved_triangle_indices; }
private:
	std::unordered_set<int> _moved_triangle_indices;
};

