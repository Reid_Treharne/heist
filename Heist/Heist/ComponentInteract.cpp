#include "stdafx.h"
#include "ComponentInteract.h"
#include "BaseObject.h"
#include "Camera.h"
#include "Terrain.h"
#include "StateWalking.h"
#include "StateInteract.h"
#include "BaseObjectUtil.h"
#include "../TileManager/TileManager.h"
#include "../TileManager/Tile.h"
#include "../InputManager/InputManager.h"
#include "../Logger/Log.h"

ComponentInteract::ComponentInteract(const InputManager* input_manager, std::shared_ptr<const Camera> camera, const ObjectTree<std::shared_ptr<BaseObject>>* object_tree,
									 const TileManager* tile_manager, std::shared_ptr<Terrain> terrain) :
_input_manager(input_manager),
	_camera(camera),
	_object_tree(object_tree),
	_tile_manager(tile_manager),
	_terrain(terrain)
{
}


ComponentInteract::~ComponentInteract(void)
{
}

void ComponentInteract::Update(BaseObject* base_object, float delta_time)
{
	const InputManager& input_manager = *_input_manager;
	
	// Check for left mouse click
	if (input_manager.GetLeftMousePressed())
	{
		int _mouse_x_client_position;
		int _mouse_y_client_position;
	
		input_manager.GetMouseClientPosition(_mouse_x_client_position, _mouse_y_client_position);
	
		XMFLOAT3 world_space_start;
		XMFLOAT3 world_space_end;
	
		_camera->Unproject(_mouse_x_client_position, _mouse_y_client_position, world_space_start, world_space_end);

		// Check collision with objects
		auto clicked_objects = _object_tree->CheckCollisionWithRay(world_space_start, world_space_end, COLLISION_FLAG::CLICKABLE);

		// If ray collided with some objects, find the closest object
		if (clicked_objects.empty() == false)
		{
			// Sort the objects from closests
			BaseObjectUtil::SortByPoint(clicked_objects, world_space_start);
	
			// The first object in the vector should now be the closest object.
			auto clicked_object = clicked_objects.front().first;
	
			auto terrain = std::dynamic_pointer_cast<Terrain>(clicked_object);

			const Tile* clicked_tile = nullptr;

			if (terrain)
			{
				clicked_tile = _tile_manager->GetTile(clicked_objects.front().second.x, clicked_objects.front().second.z);
			}
			else
			{
				XMFLOAT3 position;
				clicked_object->GetWorldPosition(position);
				clicked_tile = _tile_manager->GetTile(position.x, position.z);
			}

			if (clicked_tile != nullptr)
			{
				// Clear all the states except idle
				base_object->ClearStatesToIdle();

				if (clicked_object)
				{
					base_object->AddState(new StateInteract(clicked_object));
				}

				base_object->AddState(new StateWalking(base_object, clicked_tile));
			}
		}
	}
}

IComponent::COMPONENT_TYPE ComponentInteract::GetComponentType() const
{
	return IComponent::COMPONENT_INTERACT;
}
