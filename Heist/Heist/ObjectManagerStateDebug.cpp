#include "stdafx.h"
#include "ObjectManagerStateDebug.h"
#include "ObjectManager.h"
#include "ObjectFactory.h"
#include "BaseObject.h"
#include "../Renderer/VertexDeclarations.h"
#include "../Renderer/ShapeFactory.h"

ObjectManagerStateDebug::ObjectManagerStateDebug(void)
{
}


ObjectManagerStateDebug::~ObjectManagerStateDebug(void)
{
}

void ObjectManagerStateDebug::Initialize(ObjectManager* object_manager, Renderer* renderer, ObjectFactory* object_factory, ObjectTree<std::shared_ptr<BaseObject>>* object_tree)
{
	std::vector<AABB> aabbs;

	object_tree->GetAABBs(aabbs);

	for (auto& aabb : aabbs)
	{
		auto aabb_base_object = object_factory->CreateAABB(aabb.max - aabb.min, XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));

		if (aabb_base_object)
		{
			XMFLOAT4X4 world_matrix;

			XMStoreFloat4x4(&world_matrix, XMMatrixTranslation(aabb.min.x, aabb.min.y, aabb.min.z));

			aabb_base_object->SetPosition(aabb.min);

			_aabb_objects.push_back(aabb_base_object);
		}
	}

	ObjectManagerState::Initialize(object_manager, renderer);
}

void ObjectManagerStateDebug::Enter()
{
	for (auto& base_render_object : _aabb_objects)
	{
		_object_manager->AddObject(base_render_object, OBJECT_NAME::NAME_AESTHETIC);
	}
}

void ObjectManagerStateDebug::Exit()
{
	for (auto& base_render_object : _aabb_objects)
	{
		_object_manager->RemoveObject(base_render_object, OBJECT_NAME::NAME_AESTHETIC);
	}
}
