#pragma once
#include "BaseObject.h"
#include <unordered_map>

class MeshObject;

class GroundClutter : public BaseObject
{
public:
	GroundClutter(void);
	~GroundClutter(void);

	void Initialize(MeshObject* mesh_object, std::unordered_map<int, XMFLOAT2>&& points);

	virtual void Update(float delta_time) override;
	void Render() const;

	void AddOrUpdatePoint(unsigned int index, const XMFLOAT3& point);

	inline const std::unordered_map<int, XMFLOAT3>& GetPoints() const { return _points; }

private:

	void UpdatePoints();
	MeshObject* _mesh_object;

	// Maps tile index number to position of the piece of ground clutter.
	// Note: The position is in terrain space, not local to the tile.
	std::unordered_map<int, XMFLOAT3> _points;

	bool _points_changed;
};

