#include "stdafx.h"
#include "ComponentForge.h"
#include "Terrain.h"
#include "Camera.h"
#include "../TileManager/TileManager.h"
#include "../InputManager/InputManager.h"
#include "../Logger/Log.h"

ComponentForge::ComponentForge(const InputManager* input_manager,
							   std::shared_ptr<const Camera> camera,
							   std::shared_ptr<Terrain> terrain,
							   const TileManager* tile_manager) :
_input_manager(input_manager),
	_camera(camera),
	_terrain(terrain),
	_tile_manager(tile_manager)
{
}


ComponentForge::~ComponentForge(void)
{
}

void ComponentForge::Update(BaseObject* base_object, float delta_time)
{
	const InputManager& input_manager = *_input_manager;

	auto left_mouse_pressed = input_manager.GetLeftMouseDown();
	auto right_mouse_pressed = input_manager.GetRightMouseDown();

	// Check for left mouse click
	if (left_mouse_pressed || right_mouse_pressed)
	{
		int _mouse_x_client_position;
		int _mouse_y_client_position;

		input_manager.GetMouseClientPosition(_mouse_x_client_position, _mouse_y_client_position);

		XMFLOAT3 world_space_start;
		XMFLOAT3 world_space_end;

		_camera->Unproject(_mouse_x_client_position, _mouse_y_client_position, world_space_start, world_space_end);

		XMFLOAT3 collision_point;

		// Check collision with objects
		auto collided = _terrain->CollidesWithLine(world_space_start, world_space_end, collision_point);

		if (collided)
		{
			auto triangle_index = _terrain->GetClickedTriangleIndex();

			auto raise_amount = 0.0f;

			if (left_mouse_pressed)
			{
				raise_amount += 0.5f;
			}
			else
			{
				raise_amount -= 0.5f;
			}

			RaiseTile(triangle_index, raise_amount);
		}
	}
}

IComponent::COMPONENT_TYPE ComponentForge::GetComponentType() const
{
	return IComponent::COMPONENT_FORGE;
}

void ComponentForge::RaiseTile(unsigned int triangle_index, float raise_amount)
{
	int starting_vertex_index = triangle_index * 3;

	// If it's an odd index triangle, then its the second triangle in the tile and
	// therefor we must move the starting vertices index, back 3 to account for the first triangle
	if (triangle_index % 2 != 0)
	{
		starting_vertex_index -= 3;
	}

	auto& terrain = *_terrain;

	// keep track of the vertices that were raised
	std::vector<XMFLOAT2> raised_vertices;

	// Loop 6 times since there are 6 vertices per tile
	for (int i = 0; i < 6; ++i)
	{
		int current_index = starting_vertex_index + i;

		terrain.RaiseVertex(current_index, raise_amount);

		auto raised_vertex_ptr = terrain.GetVertex(current_index);

		if (raised_vertex_ptr)
		{
			auto raised_vertex = *raised_vertex_ptr;

			raised_vertices.push_back(XMFLOAT2(raised_vertex.position_x, raised_vertex.position_z));
		}
	}

	auto raise = [&](int starting_index) 
	{
		for (int i = 0; i < 6; ++i)
		{
			if (starting_index >= 0)
			{
				for (auto vert : raised_vertices)
				{
					auto raised_vertex_ptr = terrain.GetVertex(starting_index + i);

					if (raised_vertex_ptr)
					{
						auto raised_vertex = *raised_vertex_ptr;

						if (vert.x == raised_vertex.position_x && vert.y == raised_vertex.position_z)
						{
							terrain.RaiseVertex(starting_index + i, raise_amount);

							break;
						}
					}
				}
			}
		}
	};

	// Raise the surrounding triangles by the same amount
	raise(starting_vertex_index - 6);
	raise(starting_vertex_index + 6);
	raise(starting_vertex_index - _tile_manager->GetNumRows() * 6);
	raise(starting_vertex_index + _tile_manager->GetNumRows() * 6);
	raise(starting_vertex_index - 6 - _tile_manager->GetNumRows() * 6);
	raise(starting_vertex_index + 6 + _tile_manager->GetNumRows() * 6);
	raise(starting_vertex_index - 6 + _tile_manager->GetNumRows() * 6);
	raise(starting_vertex_index + 6 - _tile_manager->GetNumRows() * 6);
}
