#pragma once
#include "IComponent.h"
#include "../CollisionManager/ObjectTree.h"

class InputManager;
class Camera;
class ObjectFactory;
class ObjectManager;

class ComponentMouseOver : public IComponent
{
public:
	ComponentMouseOver(
		const InputManager* input_manager,
		const ObjectFactory* object_factory,
		ObjectManager* object_manager,
		std::shared_ptr<const Camera> camera,
		const ObjectTree<std::shared_ptr<BaseObject>>* object_tree);

	virtual ~ComponentMouseOver(void);

	virtual void Update(BaseObject* base_object, float delta_time) override;
	virtual IComponent::COMPONENT_TYPE GetComponentType() const override;

private:
	const InputManager* _input_manager;
	const ObjectFactory* _object_factory;
	ObjectManager* _object_manager;

	std::shared_ptr<const Camera> _camera;
	const ObjectTree<std::shared_ptr<BaseObject>>* _object_tree;

	std::shared_ptr<BaseObject> _collision_point_marker;
	std::unique_ptr<IComponent> _collision_point_marker_render_component;
};

