#pragma once
#include "IFeature.h"

class CameraManager;

class FeatureRenderOnTop : public IFeature
{
public:
	FeatureRenderOnTop(CameraManager* camera_manager);
	~FeatureRenderOnTop();

	virtual FEATURE_TYPE GetType() const override;
	virtual unsigned int GetComponentType() const override;

	virtual void Update(BaseObject* base_object, IComponent& component) override;
private:
	CameraManager* _camera_manager;

};

