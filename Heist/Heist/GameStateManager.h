#pragma once

#include "IGameState.h"

#include <stack>

class Renderer;

class GameStateManager
{
public:
	GameStateManager(void);
	~GameStateManager(void);

	void PushState(IGameState* game_state);
	void PopState();

	void ChangeState(IGameState* game_state);

	void Input();
	void Update(float delta_time);

private:
	std::stack<IGameState*> _game_states;
};

