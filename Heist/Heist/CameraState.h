#pragma once

#include <DirectXMath.h>
using namespace DirectX;

#include <memory>

class Camera;
class BaseObject;
class InputManager;

class CameraState
{
public:
	CameraState(void);
	virtual ~CameraState(void) = 0;

	virtual void Initialize(std::shared_ptr<Camera> camera);

	virtual void Enter(std::shared_ptr<BaseObject> object_attached_to);
	virtual void Exit();

	virtual void Input(InputManager* input_manager) {}
	virtual void Update(float delta_time) {}

	virtual void SetObjectAttachedTo(std::shared_ptr<BaseObject> object_attached_to);

protected:
	std::shared_ptr<Camera> _camera;

	std::shared_ptr<BaseObject> _object_attached_to;
};

