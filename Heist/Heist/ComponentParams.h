#pragma once

#include "ObjectNames.h"
#include "../CollisionManager/CollisionFlags.h"
#include <vector>
#include <memory>

using namespace CollisionLibrary;

class BaseObject;
class MeshObject;

struct IComponentParams
{
	virtual ~IComponentParams()
	{
	}
};

struct RenderComponentParams : public IComponentParams
{
	RenderComponentParams(const std::vector<MeshObject*>& mesh_objects) :
		_mesh_objects(mesh_objects)
	{
	}

	RenderComponentParams(MeshObject* mesh_object)
	{
		_mesh_objects.push_back(mesh_object);
	}

	std::vector<MeshObject*> _mesh_objects;
};

struct CollisionComponentParams : public IComponentParams
{
	CollisionComponentParams(std::shared_ptr<BaseObject> base_object) :
		_base_object(base_object)
	{
	}

	std::shared_ptr<BaseObject> _base_object;
};

struct GroundClampComponentParams : public IComponentParams
{
	GroundClampComponentParams(OBJECT_NAME object_name, float offset_y = 0.0f) :
		_object_name(object_name),
		_offset_y(offset_y)
	{
	}

	OBJECT_NAME _object_name;
	float _offset_y;
};

struct ComponentOutlineWhenNearCameraParam : public IComponentParams
{
	// distance_until_outline represents the distance the object has to get away from the camera before it has an outline
	ComponentOutlineWhenNearCameraParam(float distance_until_outline) :
		_distance_until_outline(distance_until_outline)
	{
	}

	float _distance_until_outline;
};
