#include "stdafx.h"
#include "Camera.h"
#include "CameraState.h"
#include "../Renderer/Renderer.h"
#include "../Renderer/ConstantBufferDeclarations.h"
#include "../Logger/Log.h"
#include "../CollisionManager/CollisionLibrary.h"

using namespace CollisionLibrary;

static const int kDefaultMoveSpeed = 21;
static const int kMaxMoveSpeed = 101;
static const int kMinMoveSpeed = 1;
static const int kDefaultRotationSpeed = 100;
static const int kMaxRotationSpeed = 200;
static const int kMinRotationSpeed = 50;

Camera::Camera() :
	_camera_buffer(nullptr),
	_move_speed(kDefaultMoveSpeed),
	_rotation_speed(kDefaultRotationSpeed)
{
	memset(&_projection_matrix, 0, sizeof(_projection_matrix));
}


Camera::~Camera(void)
{
	delete _camera_buffer;
}

void Camera::Translate(const XMFLOAT3& translation)
{
	BaseObject::Translate(translation * (float)_move_speed);
}

void Camera::TranslateWorld(const XMFLOAT3& translation)
{
	BaseObject::TranslateWorld(translation * (float)_move_speed);
}

void Camera::Rotate(const XMFLOAT3& rotation)
{
	BaseObject::Rotate(rotation * (float)_rotation_speed);
}

void Camera::Initialize(Renderer* renderer)
{
	if (_camera_buffer == nullptr)
	{
		_camera_buffer = new ConstantBuffer();

		D3D11_BUFFER_DESC desc;
		memset(&desc, 0, sizeof(desc));
		desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		desc.Usage = D3D11_USAGE_DYNAMIC;
		desc.ByteWidth = sizeof(C_BUFFER_CAMERA);
		desc.StructureByteStride = sizeof(C_BUFFER_CAMERA);

		_camera_buffer->Initialize(renderer, desc, 0, 0, 0);

		// Store a copy of the view port for easy access
		renderer->GetViewport(_viewport);
	}
	else
	{
		assert(false && "Camera::Initialize - Camera already initialized");
	}
}

void Camera::Render() const
{
	// NOTE :: The camera is not actually rendered. This function just sets the
	// view matrix, projection matrix, and window resolution constant buffers in the shaders.

	C_BUFFER_CAMERA camera_buffer;

	// projection matrix
	camera_buffer.projection_matrix = _projection_matrix;

	// To get the view matrix, take the inverse of the world matrix
	XMFLOAT4X4 world_matrix;
	GetWorldMatrix(world_matrix);

	XMMATRIX inv = XMMatrixInverse(nullptr, XMLoadFloat4x4(&world_matrix));
	XMStoreFloat4x4(&camera_buffer.view_matrix, inv);

	// Map the camera buffer
	_camera_buffer->Map(&camera_buffer, sizeof(camera_buffer));
}

void Camera::Unproject(unsigned int mouse_x_client_position,
					   unsigned int mouse_y_client_position,
					   XMFLOAT3& object_space_start,
					   XMFLOAT3& object_space_end,
					   const XMFLOAT4X4& object_world_matrix) const
{
	XMVECTOR xm_client_space_point;
	XMVECTOR xm_object_space_point;

	XMFLOAT4X4 world_matrix;

	GetWorldMatrix(world_matrix);

	// Find the start point
	xm_client_space_point = XMLoadFloat3(&XMFLOAT3((float)mouse_x_client_position, (float)mouse_y_client_position, 0.0f));

	xm_object_space_point = XMVector3Unproject(xm_client_space_point,
		_viewport.TopLeftX,
		_viewport.TopLeftY,
		_viewport.Width,
		_viewport.Height,
		_viewport.MinDepth,
		_viewport.MaxDepth,
		XMLoadFloat4x4(&_projection_matrix),
		XMMatrixInverse(nullptr,XMLoadFloat4x4(&world_matrix)),
		XMLoadFloat4x4(&object_world_matrix));

	XMStoreFloat3(&object_space_start, xm_object_space_point);

	// Find the end point
	xm_client_space_point = XMLoadFloat3(&XMFLOAT3((float)mouse_x_client_position, (float)mouse_y_client_position, 1.0f));

	xm_object_space_point = XMVector3Unproject(xm_client_space_point,
		_viewport.TopLeftX,
		_viewport.TopLeftY,
		_viewport.Width,
		_viewport.Height,
		_viewport.MinDepth,
		_viewport.MaxDepth,
		XMLoadFloat4x4(&_projection_matrix),
		XMMatrixInverse(nullptr, XMLoadFloat4x4(&world_matrix)),
		XMLoadFloat4x4(&object_world_matrix));

	XMStoreFloat3(&object_space_end, xm_object_space_point);
}

void Camera::Unproject(unsigned int mouse_x_client_position,
					   unsigned int mouse_y_client_position,
					   XMFLOAT3& object_space_start,
					   XMFLOAT3& object_space_end) const
{
	XMFLOAT4X4 identity_matrix;

	XMStoreFloat4x4(&identity_matrix, XMMatrixIdentity());

	Unproject(mouse_x_client_position,
		mouse_y_client_position,
		object_space_start,
		object_space_end,
		identity_matrix);
}

void Camera::SetProjectionMatrix(float field_of_view, float width, float height, float near_clip, float far_clip)
{
	XMMATRIX xm_projection_matrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(field_of_view), width/height, near_clip, far_clip);

	XMStoreFloat4x4(&_projection_matrix, xm_projection_matrix);
}

void Camera::ChangeMoveSpeed(int delta_speed)
{
	_move_speed += delta_speed;

	if (_move_speed > kMaxMoveSpeed)
	{
		_move_speed = kMaxMoveSpeed;
	}
	else if (_move_speed < kMinMoveSpeed)
	{
		_move_speed = kMinMoveSpeed;
	}
}

void Camera::ChangeRotationSpeed(int delta_speed)
{
	_rotation_speed += delta_speed;

	if (_rotation_speed > kMaxRotationSpeed)
	{
		_rotation_speed = kMaxRotationSpeed;
	}
	else if (_rotation_speed < kMinRotationSpeed)
	{
		_rotation_speed = kMinRotationSpeed;
	}
}

void Camera::GetViewMatrix(XMFLOAT4X4& view_matrix) const
{
	// To get the view matrix, take the inverse of the camera's world matrix
	XMFLOAT4X4 world_matrix;
	GetWorldMatrix(world_matrix);

	XMMATRIX xm_world_matrix = XMLoadFloat4x4(&world_matrix);

	XMMATRIX xm_view_matrix = XMMatrixInverse(nullptr, xm_world_matrix);

	XMStoreFloat4x4(&view_matrix, xm_view_matrix);
}

void Camera::GetProjectionMatrix(XMFLOAT4X4& projection_matrix) const
{
	memcpy_s(&projection_matrix, sizeof(projection_matrix), &_projection_matrix, sizeof(_projection_matrix));
}

void Camera::GetViewport(D3D11_VIEWPORT& viewport) const
{
	viewport = _viewport;
}

