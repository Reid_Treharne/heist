#include "stdafx.h"
#include "ComponentWalk.h"
#include "BaseObject.h"
#include "../CollisionManager/CollisionLibrary.h"
#include "../InputManager/InputManager.h"
#include "../Logger/Log.h"

using namespace CollisionLibrary;

ComponentWalk::ComponentWalk(const InputManager* input_manager) :
	_input_manager(input_manager)
{
}

ComponentWalk::~ComponentWalk(void)
{
}

void ComponentWalk::Update(BaseObject* base_object, float delta_time)
{
	if (base_object)
	{
		if (_input_manager->GetKeyDown('W'))
		{
			base_object->Translate(XMFLOAT3(0.0f, 0.0f, 10.0f) * delta_time);
		}
		else if (_input_manager->GetKeyDown('S'))
		{
			base_object->Translate(XMFLOAT3(0.0f, 0.0f, -10.0f) * delta_time);
		}
		if (_input_manager->GetKeyDown('A'))
		{
			base_object->Translate(XMFLOAT3(-10.0f, 0.0f, 0.0f) * delta_time);
		}
		else if (_input_manager->GetKeyDown('D'))
		{
			base_object->Translate(XMFLOAT3(10.0f, 0.0f, 0.0f) * delta_time);
		}

		int mouse_delta_x_position, mouse_delta_y_position;

		_input_manager->GetMouseDeltaPosition(mouse_delta_x_position, mouse_delta_y_position);

		if (mouse_delta_x_position != 0 || mouse_delta_y_position != 0)
		{
			// Don't factor in the delta time. This is important in this case since
			// we're rotating the object based off the delta mouse position which is dependent on the FPS.
			// For example, if we're running at 500 FPS the distance the mouse can travel per frame is
			// much smaller than if we're running at 30 FPS.
			base_object->Rotate(XMFLOAT3(-0.1f * mouse_delta_y_position, -0.1f * mouse_delta_x_position, 0.0f));
		}
	}
}

IComponent::COMPONENT_TYPE ComponentWalk::GetComponentType() const
{
	return IComponent::COMPONENT_WALK;
}

