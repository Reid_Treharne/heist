#pragma once
#include "IFeature.h"
#include <memory>
#include <vector>

class InputManager;

class FeatureCollisionTrigger : public IFeature
{
public:
	FeatureCollisionTrigger(const InputManager* input_manager, unsigned char key);
	~FeatureCollisionTrigger();

	virtual FEATURE_TYPE GetType() const override;
	virtual unsigned int GetComponentType() const override;

	virtual void OnAdded(IComponent& component) override;
	virtual void OnRemoved(IComponent& component) override;

	virtual void Update(BaseObject* base_object, IComponent& component) override;

private:
	void OnTrigger(std::shared_ptr<BaseObject> collider1, std::shared_ptr<BaseObject> collider2);

	std::pair<std::shared_ptr<BaseObject>,std::vector<std::shared_ptr<BaseObject>>> _current_triggers;
	const InputManager* _input_manager;
	unsigned char _key;
};

