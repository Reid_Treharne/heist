#include "stdafx.h"
#include "HeistMain.h"
#include "DirectionalLight.h"
#include "ComponentFactory.h"
#include "FeatureFactory.h"
#include "GroundClutter.h"
#include "Skybox.h"
#include "../TileManager/Tile.h"
#include "../EventSystem/EventSystem.h"

using namespace std::placeholders;

HeistMain::HeistMain(void) :
	_renderer(),
	_window_width(0),
	_window_height(0)
{
	_camera = std::make_shared<Camera>();
	_terrain = std::make_shared<Terrain>();
	_ground_clutter = std::make_shared<GroundClutter>();
}


HeistMain::~HeistMain(void)
{
	// Make sure the camera is detached from any objects on shutdown. If the camera is still
	// attached to an object, it's possible the object's destructor will never get called if
	// that object also holds a pointer to the camera. Scary stuff.
	_camera_manager.Detach();

	EventSystem::Delete();
}

void HeistMain::Initialize(unsigned width, unsigned height, InputManager* input_manager, HWND h_wnd, bool windowed)
{
	_window_width = width;
	_window_height = height;

	// Initialize the component factory
	ComponentFactory::Initialize(&_service);

	// Initialize the feature factory
	FeatureFactory::Initialize(&_service);

	// Set the input manager
	_input_manager = input_manager;

	// Initialize the renderer
	_renderer.Initialize(width, height, h_wnd, windowed);

	// Initialize the model manager and preload all the objects
	_model_manager.Initialize(&_renderer, std::bind(&CollisionManager::AddTriangle, &_collision_manager, _1, _2));
	_model_manager.PreLoad("../../Resources/Models/");

	// Initialize the light manager
	_light_manager.Initialize(&_renderer);

	// Initialize the post process manager
	_post_process_manager.Initialize(&_renderer);

	// Initialize the object factory so objects may be created
	_object_factory.Initialize(&_object_manager, &_model_manager, &_light_manager, &_service);

	// Initialize the terrain
	_terrain_mesh.Initialize(&_renderer);
	_terrain_mesh.SetPrimaryTexture("../../Resources/Textures/DDSTextures/grass.dds");
	_terrain_mesh.SetSecondaryTexture("../../Resources/Textures/DDSTextures/mountain.dds", 10.0f);

	std::vector<TileMeshData> vertices;
	std::vector<unsigned int> indices;
	MapData map_data;

	// Load the world and get the terrain objects
	_tile_manager.LoadMap("../../Resources/Maps/map.xml", vertices, indices, map_data);
	_tile_manager.ConnectNeighboringTiles();

	// TODO calculate aabb based on terrain
	AABB aabb;
	aabb.min = XMFLOAT3(0.0f, 25.0f, 0.0f);
	aabb.max = XMFLOAT3((float)_tile_manager.GetNumColumns(), 75.0f, (float)_tile_manager.GetNumRows());

	_object_tree.Resize(aabb);

	// Compute the terrain mesh object based off the world map
	_terrain->Initialize(&_terrain_mesh, &_tile_manager, &_collision_manager, vertices, indices);
	_terrain->SetPosition(XMFLOAT3(0.0f, 0.0f, 0.0f));

	// Initialize the object manager and the HUD manager
	_object_manager.Initialize(&_renderer, &_object_factory, &_object_tree);
	_HUD_manager.Initialize(&_renderer);

	// Initialize the camera
	_camera->Initialize(&_renderer);
	_camera->SetPosition(_terrain->GetWorldPosition());
	_camera->SetProjectionMatrix(75.0f, (float)width, (float)height, 0.1f, 500.0f);

	// Initialize the camera manager
	_camera_manager.Initialize(_camera, _input_manager);

	// Set up the service
	_service.SetCamera(_camera);
	_service.SetCameraManager(&_camera_manager);
	_service.SetHUDManager(&_HUD_manager);
	_service.SetInputManager(_input_manager);
	_service.SetObjectManager(&_object_manager);
	_service.SetObjeectFactory(&_object_factory);
	_service.SetRenderer(&_renderer);
	_service.SetTerrain(_terrain);
	_service.SetTileManager(&_tile_manager);
	_service.SetObjectTree(&_object_tree);
	_service.SetModelManager(&_model_manager);
	_service.SetCollisionManager(&_collision_manager);

	const auto& terrain_objects = map_data._objects;

	for (unsigned int i = 0; i < terrain_objects.size(); ++i)
	{
		const auto& object = terrain_objects[i];

		auto position = object._position + _terrain->GetWorldPosition();

		auto base_object = _object_factory.CreateObject(object._mesh_names, (OBJECT_NAME)object._object_name, position, XMFLOAT3(0.0f, 0.0f, 0.0f));

		// Add the new object to the object manager
		_object_manager.AddObject(base_object, (OBJECT_NAME)object._object_name);
	}

	auto collision_params = CollisionComponentParams(_terrain);

	_terrain->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_COLLISION, collision_params));
	_terrain->AddComponentFeature(FeatureFactory::CreateFeature(IFeature::FEATURE_COLLISION_CLICKABLE));

	// Initialize the ground clutter
	_ground_clutter_mesh.Initialize(&_renderer, RENDER_TYPE::TYPE_GROUND_CLUTTER);
	_ground_clutter_mesh.SetTexture("../../Resources/Textures/DDSTextures/grass_clutter2.dds");

	_ground_clutter->Initialize(&_ground_clutter_mesh, std::move(map_data._grass_points));
	_skybox = std::static_pointer_cast<Skybox>(_object_factory.CreateObject("Skybox", OBJECT_NAME::NAME_SKYBOX, _camera->GetWorldPosition(), XMFLOAT3(0.0f, 0.0f, 0.0f)));

	// Initialize the game states
	_game_state_debug.Initialize(&_service);
	_game_state_play.Initialize(&_service, &_game_state_debug);

	// Set up the game state stack
	_game_state_manager.PushState(&_game_state_play);

	// Add the objects to the object manager
	_object_manager.AddObject(_camera, OBJECT_NAME::NAME_CAMERA);
	_object_manager.AddObject(_terrain, OBJECT_NAME::NAME_TERRAIN);
	_object_manager.AddObject(_ground_clutter, OBJECT_NAME::NAME_GROUND_CLUTTER);
	_object_manager.AddObject(_skybox, OBJECT_NAME::NAME_SKYBOX);
}

void HeistMain::Input()
{
	// Handle input for the current game state
	_game_state_manager.Input();

	// Sets the light buffer to DEFAULT, allowing default rendering of the scene. This is used most of the time.
	if (_input_manager->GetKeyPressed(VK_F1))
	{
		_light_manager.SetBuffer(LightManager::Buffer::DEFAULT);
	}
	// Sets the light buffer to NORMAL, causing the normal buffer to be rendered. All lights are ignored.
	else if (_input_manager->GetKeyPressed(VK_F2))
	{
		_light_manager.SetBuffer(LightManager::Buffer::NORMAL);
	}
	// Sets the light buffer to DEPTH, causing the depth buffer to be rendered. All lights are ignored.
	// Note: The depth buffer renders the depth by converting the singluar depth value to a grey-scale color
	// and raising it to an arbitrary power so it's easily visible. It does not reflect the exact depth buffer.
	// See the PS_BufferDepth shader for more information.
	else if (_input_manager->GetKeyPressed(VK_F3))
	{
		_light_manager.SetBuffer(LightManager::Buffer::DEPTH);
	}
	// Sets the light buffer to COLOR, causing the color buffer to be rendered. All lights are ignored.
	else if (_input_manager->GetKeyPressed(VK_F4))
	{
		_light_manager.SetBuffer(LightManager::Buffer::COLOR);
	}
}

void HeistMain::Update(float delta_time)
{
	// Update the camera manager
	_camera_manager.Update(delta_time);

	// Update the current game state
	_game_state_manager.Update(delta_time);

	// Update the input manager to clear the current key states 
	_input_manager->Update();
}

void HeistMain::Render()
{
	// Clear the back buffer
	_renderer.ClearBackBuffer(XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	_camera->Render();

	// Render all the models
	_model_manager.Render();

	// Render the terrain
	_terrain_mesh.Render();

	// Render the ground clutter
	_ground_clutter->Render();

	// Render all the lights
	_light_manager.Render();

	// Do post process effects
	_post_process_manager.Render();

	// Render all the HUD elements
	_HUD_manager.Render();

	// Present
	_renderer.Present();
}
