#pragma once

class ObjectManager;
class Renderer;

class ObjectManagerState
{
public:
	ObjectManagerState(void);

	virtual ~ObjectManagerState(void) = 0;

	virtual void Initialize(ObjectManager* object_manager, Renderer* renderer);

	virtual void Enter();
	virtual void Exit();
protected:
	ObjectManager* _object_manager;
};

