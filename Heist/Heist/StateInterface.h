#pragma once

#include <memory>

class BaseObject;

class StateInterface
{
public:
	StateInterface(void) {}
	virtual ~StateInterface(void) = 0 {}

	virtual void Enter() = 0;

	// returns true if the state has finished
	virtual bool Update(std::shared_ptr<BaseObject> base_object, float delta_time) = 0;
};

