#pragma once
#include "IFeature.h"
class FeatureCollisionTriggerable : public IFeature
{
public:
	FeatureCollisionTriggerable();
	~FeatureCollisionTriggerable();

	virtual FEATURE_TYPE GetType() const override;
	virtual unsigned int GetComponentType() const override;

	virtual void OnAdded(IComponent& component) override;
	virtual void OnRemoved(IComponent& component) override;
};

