#include "stdafx.h"
#include "BaseObjectUtil.h"
#include "BaseObject.h"
#include "../CollisionManager/CollisionLibrary.h"

#include <algorithm>

using namespace CollisionLibrary;

void BaseObjectUtil::SortByDistance(std::vector<std::shared_ptr<BaseObject>>& base_objects, const XMFLOAT3& posistion)
{
	// Returns the object closest to the screen
	auto sort_func = [&posistion](std::shared_ptr<BaseObject> b1, std::shared_ptr<BaseObject> b2)
	{
		auto to_b1 = b1->GetWorldPosition() - posistion;
		auto to_b2 = b2->GetWorldPosition() - posistion;

		return MagnitudeSquared(to_b1) < MagnitudeSquared(to_b2);
	};

	std::sort(base_objects.begin(), base_objects.end(), sort_func);
}

void BaseObjectUtil::SortByPoint(std::vector<std::pair<std::shared_ptr<BaseObject>, XMFLOAT3>>& base_objects, const XMFLOAT3& posistion)
{
	// Returns the object closest to the screen
	auto sort_func = [&posistion](std::pair<std::shared_ptr<BaseObject>, XMFLOAT3> b1, std::pair<std::shared_ptr<BaseObject>, XMFLOAT3> b2)
	{
		auto to_b1 = b1.second - posistion;
		auto to_b2 = b2.second - posistion;

		return MagnitudeSquared(to_b1) < MagnitudeSquared(to_b2);
	};

	std::sort(base_objects.begin(), base_objects.end(), sort_func);
}
