#include "stdafx.h"
#include "CameraStateFree.h"
#include "Camera.h"
#include "IComponent.h"
#include "../CollisionManager/CollisionLibrary.h"
#include "../InputManager/InputManager.h"

using namespace CollisionLibrary;

CameraStateFree::CameraStateFree(void) :
	_delta_translation(XMFLOAT3(0.0f, 0.0f, 0.0f)),
	_delta_rotation(XMFLOAT3(0.0f, 0.0f, 0.0f))
{
}


CameraStateFree::~CameraStateFree(void)
{
}

void CameraStateFree::Initialize(std::shared_ptr<Camera> camera)
{
	CameraState::Initialize(camera);
}

void CameraStateFree::Enter(std::shared_ptr<BaseObject> object_attached_to)
{
	CameraState::Enter(object_attached_to);
}

void CameraStateFree::Input(InputManager* input_manager)
{
	InputManager& input = *input_manager;

	// Check for camera translation
	if (input.GetKeyDown('W'))
	{
		_delta_translation = _delta_translation + XMFLOAT3(0.0f, 0.0f, 1.0f);
	}
	else if (input.GetKeyDown('S'))
	{
		_delta_translation = _delta_translation + XMFLOAT3(0.0f, 0.0f, -1.0f);
	}
	if (input.GetKeyDown('D'))
	{
		_delta_translation = _delta_translation + XMFLOAT3(1.0f, 0.0f, 0.0f);
	}
	else if (input.GetKeyDown('A'))
	{
		_delta_translation = _delta_translation + XMFLOAT3(-1.0f, 0.0f, 0.0f);
	}
	if (input.GetKeyDown('Q'))
	{
		_delta_translation = _delta_translation + XMFLOAT3(0.0f, 1.0f, 0.0f);
	}
	else if (input.GetKeyDown('Z'))
	{
		_delta_translation = _delta_translation + XMFLOAT3(0.0f, -1.0f, 0.0f);
	}

	// Check for rotation change
	if (input.GetKeyDown(VK_UP))
	{
		_delta_rotation = _delta_rotation + XMFLOAT3(-1.0f, 0.0f, 0.0f);
	}
	else if (input.GetKeyDown(VK_DOWN))
	{
		_delta_rotation = _delta_rotation + XMFLOAT3(1.0f, 0.0f, 0.0f);
	}
	if (input.GetKeyDown(VK_RIGHT))
	{
		_delta_rotation = _delta_rotation + XMFLOAT3(0.0f, 1.0f, 0.0f);
	}
	else if (input.GetKeyDown(VK_LEFT))
	{
		_delta_rotation = _delta_rotation + XMFLOAT3(0.0f, -1.0f, 0.0f);
	}
}

void CameraStateFree::Update(float delta_time)
{
	static const auto zero_vector = XMFLOAT3(0.0f, 0.0f, 0.0f);

	if (_delta_translation != zero_vector)
	{
		_camera->Translate(_delta_translation * delta_time);

		_delta_translation = zero_vector;
	}
	if (_delta_rotation != zero_vector)
	{
		_camera->Rotate(_delta_rotation * delta_time);

		_delta_rotation = zero_vector;
	}
}


