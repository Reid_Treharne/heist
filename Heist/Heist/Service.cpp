#include "stdafx.h"
#include "Service.h"

Service::Service(void) :
	_renderer(nullptr),
	_object_manager(nullptr),
	_HUD_manager(nullptr),
	_camera(nullptr),
	_input_manager(nullptr),
	_object_factory(nullptr),
	_terrain(nullptr),
	_tile_manager(nullptr),
	_object_tree(nullptr)
{
}


Service::~Service(void)
{
}

Renderer* Service::GetRenderer() const
{
	return _renderer;
}

ObjectManager* Service::GetObjectManager() const
{
	return _object_manager;
}

HUDManager* Service::GetHUDManager() const
{
	return _HUD_manager;
}

std::shared_ptr<Camera> Service::GetCamera() const
{
	return _camera;
}

CameraManager* Service::GetCameraManager() const
{
	return _camera_manager;
}

InputManager* Service::GetInputManager() const
{
	return _input_manager;
}

ObjectFactory* Service::GetObjectFactory() const
{
	return _object_factory;
}

std::shared_ptr<Terrain> Service::GetTerrain() const
{
	return _terrain;
}

TileManager* Service::GetTileManager() const
{
	return _tile_manager;
}

ObjectTree<std::shared_ptr<BaseObject>>* Service::GetObjectTree() const
{
	return _object_tree;
}

ModelManager* Service::GetModelManager() const
{
	return _model_manager;
}

CollisionManager* Service::GetCollisionManager() const
{
	return _collision_manager;
}

void Service::SetRenderer(Renderer* renderer)
{
	_renderer = renderer;
}

void Service::SetObjectManager(ObjectManager* object_manager)
{
	_object_manager = object_manager;
}

void Service::SetHUDManager(HUDManager* HUD_manager)
{
	_HUD_manager = HUD_manager;
}

void Service::SetCamera(std::shared_ptr<Camera> camera)
{
	_camera = camera;
}

void Service::SetCameraManager(CameraManager* camera_manager)
{
	_camera_manager = camera_manager;
}

void Service::SetInputManager(InputManager* input_manager)
{
	_input_manager = input_manager;
}

void Service::SetObjeectFactory(ObjectFactory* object_factory)
{
	_object_factory = object_factory;
}

void Service::SetTerrain(std::shared_ptr<Terrain> terrain)
{
	_terrain = terrain;
}

void Service::SetTileManager(TileManager* tile_manager)
{
	_tile_manager = tile_manager;
}

void Service::SetObjectTree(ObjectTree<std::shared_ptr<BaseObject>>* object_tree)
{
	_object_tree = object_tree;
}

void Service::SetModelManager(ModelManager* model_manager)
{
	_model_manager = model_manager;
}

void Service::SetCollisionManager(CollisionManager* collision_manager)
{
	_collision_manager = collision_manager;
}
