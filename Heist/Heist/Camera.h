#pragma once

#include "BaseObject.h"

#include "../Renderer/ConstantBuffer.h"

class Renderer;
class InputManager;

class Camera : public BaseObject
{
public:
	Camera(void);
	virtual ~Camera(void);

	virtual void Translate(const XMFLOAT3& translation) override;
	virtual void TranslateWorld(const XMFLOAT3& translation) override;
	virtual void Rotate(const XMFLOAT3& rotation) override;

	void Initialize(Renderer* renderer);

	void Render() const;

	void Unproject(unsigned int mouse_x_client_position,
		unsigned int mouse_y_client_position,
		XMFLOAT3& object_space_start,
		XMFLOAT3& object_space_end,
		const XMFLOAT4X4& object_world_matrix) const;

	void Unproject(unsigned int mouse_x_client_position,
		unsigned int mouse_y_client_position,
		XMFLOAT3& object_space_start,
		XMFLOAT3& object_space_end) const;

	// Mutators
	void SetProjectionMatrix(float field_of_view, float width, float height, float near_clip, float far_clip);

	void ChangeMoveSpeed(int delta_speed);
	void ChangeRotationSpeed(int delta_speed);

	// Accessors
	void GetViewMatrix(XMFLOAT4X4& view_matrix) const;
	void GetProjectionMatrix(XMFLOAT4X4& projection_matrix) const;
	void GetViewport(D3D11_VIEWPORT& viewport) const;

private:
	ConstantBuffer* _camera_buffer;
	XMFLOAT4X4 _projection_matrix;

	// a copy of the current viewport for easy access
	D3D11_VIEWPORT _viewport;

	int _move_speed;
	int _rotation_speed;
};

