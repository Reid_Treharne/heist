#pragma once
#include "CameraState.h"
#include "IComponent.h"

class BaseObject;

class CameraStateFirstPerson : public CameraState
{
public:
	CameraStateFirstPerson(void);
	~CameraStateFirstPerson(void);

	virtual void Enter(std::shared_ptr<BaseObject> object_attached_to) override;
	virtual void Exit() override;

	virtual void Input(InputManager* input_manager) override;

private:
	// Whether or not the object attached was rendered. Will be restored on Exit.
	std::unique_ptr<IComponent> attached_object_render_component;
};