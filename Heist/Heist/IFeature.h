#pragma once

class IComponent;
class BaseObject;

class IFeature
{
public:
	IFeature() = default;
	IFeature(const IFeature& copy) = delete;
	IFeature(const IFeature&& copy) = delete;
	IFeature& operator=(const IFeature& copy) = delete;

	virtual ~IFeature() = 0 {}

	enum FEATURE_TYPE {
		FEATURE_RENDER_OUTLINE_WHEN_NEAR_OBJECT,
		FEATURE_RENDER_ON_TOP,
		FEATURE_COLLISION_CLICKABLE,
		FEATURE_COLLISION_COLLIDABLE,
		FEATURE_COLLISION_TRIGGERABLE,
		FEATURE_COLLISION_TRIGGER,
		NUM_FEATURE_TYPES
	};

	virtual FEATURE_TYPE GetType() const = 0;
	virtual unsigned int GetComponentType() const = 0;

	virtual void OnAdded(IComponent& component) {};
	virtual void OnRemoved(IComponent& component) {};

	virtual void Update(BaseObject* base_object, IComponent& component) {}
};

