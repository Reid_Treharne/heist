#pragma once

#include <DirectXMath.h>
using namespace DirectX;

#include "../Renderer/ConstantBufferDeclarations.h"
#include <memory>

class HUDMeshObject;

class HUDObject
{
public:
	HUDObject(void);
	~HUDObject(void);

	void Initalize(HUDMeshObject* hud_mesh_object);

	void Update(float delta_time);
	void Render();

	// Sets the position of the top left corner of the image
	// left_top		the left top corner of the image with the
	//				x component being the left and the y
	//				component being the top. Valid values range
	//				from (0, 0) - left top to
	//					 (1, 1) - right bottom
	void SetPosition(const XMFLOAT2& left_top);

	void SetColor(const XMFLOAT4& color);

private:
	XMFLOAT2 _position;
	XMFLOAT2 _UV_offset;
	XMFLOAT4 _color;

	mutable unsigned int _num_references;

	HUDMeshObject* _hud_mesh_object;
};

