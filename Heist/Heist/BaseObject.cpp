#include "stdafx.h"
#include "BaseObject.h"
#include "IComponent.h"
#include "StateInterface.h"
#include "../CollisionManager/CollisionLibrary.h"
#include "../Logger/Log.h"
#include "StateIdle.h"
#include "ComponentFactory.h"
#include "FeatureFactory.h"
#include "../Renderer/ConstantBufferDeclarations.h"

using namespace CollisionLibrary;

BaseObject::BaseObject(void) :
	_old_position(0.0f, 0.0f, 0.0f),
	_object_name(NUM_NAMES),
	_dirty_flags_handle(0)
{
	XMStoreFloat4x4(&_matrix, XMMatrixIdentity());

	_dirty_flags_handle = DirtyFlagsManager::Subscribe();

	// push back an idle state
	_states.emplace_back(std::unique_ptr<StateIdle>(new StateIdle));
}


BaseObject::~BaseObject(void)
{
	if (_parent.first)
	{
		_parent.first->DetachObject(this);
	}

	DirtyFlagsManager::Unsubscribe(_dirty_flags_handle);
}

void BaseObject::Update(float delta_time)
{
	if (_states.empty() == false)
	{
		// Update returns true if it has finished and should be popped
		if (_states.back()->Update(shared_from_this(), delta_time) == true)
		{
			_states.pop_back();

			// call enter on the new state
			if (_states.empty() == false)
			{
				_states.back()->Enter();
			}
		}
	}

	for (auto& component : _components)
	{
		component.second->Update(this, delta_time);
	}

	// Check if the position or orientation has been dirtied
	if (DirtyFlagsManager::IsDirty(_dirty_flags_handle, DirtyFlagsManager::DIRTY_POSITION) ||
		DirtyFlagsManager::IsDirty(_dirty_flags_handle, DirtyFlagsManager::DIRTY_ORIENTATION))
	{
		// save the old position
		GetWorldPosition(_old_position);
	}
}

void BaseObject::SetPosition(const XMFLOAT3& position)
{
	XMMATRIX matrix = XMLoadFloat4x4(&_matrix);

	matrix.r[3] = XMVectorSet(position.x, position.y, position.z, 1.0f);

	XMStoreFloat4x4(&_matrix, matrix);

	// Dirty the position
	DirtyFlagsManager::SetDirty(_dirty_flags_handle, DirtyFlagsManager::DIRTY_POSITION, true);
}

void BaseObject::SetOrientation(const XMFLOAT3& rotation)
{
	XMMATRIX matrix = XMLoadFloat4x4(&_matrix);

	XMVECTOR camera_position = matrix.r[3];

	XMMATRIX new_matrix = XMMatrixRotationRollPitchYaw(XMConvertToRadians(rotation.x),
		XMConvertToRadians(rotation.y),
		XMConvertToRadians(rotation.z));

	new_matrix.r[3] = camera_position;

	XMStoreFloat4x4(&_matrix, new_matrix);

	// Dirty the Position
	DirtyFlagsManager::SetDirty(_dirty_flags_handle, DirtyFlagsManager::DIRTY_ORIENTATION, true);
}

void BaseObject::Translate(const XMFLOAT3& translation)
{
	XMMATRIX matrix = XMLoadFloat4x4(&_matrix);

	matrix = XMMatrixMultiply(XMMatrixTranslation(translation.x, translation.y, translation.z), matrix);

	XMStoreFloat4x4(&_matrix, matrix);

	DirtyFlagsManager::SetDirty(_dirty_flags_handle, DirtyFlagsManager::DIRTY_POSITION, true);
}

void BaseObject::TranslateWorld(const XMFLOAT3& translation)
{
	if (_parent.first == nullptr)
	{
		_matrix._41 += translation.x;
		_matrix._42 += translation.y;
		_matrix._43 += translation.z;

		DirtyFlagsManager::SetDirty(_dirty_flags_handle, DirtyFlagsManager::DIRTY_POSITION, true);
	}
	else
	{
		Log::Warning("BaseObject::TranslateWorld - Cannot translate world of child object\n");
	}
}

void BaseObject::Rotate(const XMFLOAT3& rotation)
{
	XMMATRIX matrix = XMLoadFloat4x4(&_matrix);

	if (rotation.x != 0.0f || rotation.z != 0.0f)
	{
		matrix = XMMatrixMultiply(XMMatrixRotationRollPitchYaw(XMConvertToRadians(rotation.x), 0.0f, XMConvertToRadians(rotation.z)), matrix);
	}
	if (rotation.y != 0.0f)
	{
		XMVECTOR camera_position = matrix.r[3];

		matrix.r[3] = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);

		matrix = XMMatrixMultiply(matrix, XMMatrixRotationRollPitchYaw(0.0f, XMConvertToRadians(rotation.y), 0.0f));

		matrix.r[3] = camera_position;
	}

	XMStoreFloat4x4(&_matrix, matrix);

	DirtyFlagsManager::SetDirty(_dirty_flags_handle, DirtyFlagsManager::DIRTY_ORIENTATION, true);
}

void BaseObject::LookAt(const XMFLOAT3& look_at_position)
{
	CollisionLibrary::LookAt(_matrix, look_at_position);

	// Dirty the orientation
	DirtyFlagsManager::SetDirty(_dirty_flags_handle, DirtyFlagsManager::DIRTY_ORIENTATION, true);
}

void BaseObject::LookAt2D(const XMFLOAT3& look_at_position)
{
	CollisionLibrary::LookAt2D(_matrix, look_at_position);

	// Dirty the orientation
	DirtyFlagsManager::SetDirty(_dirty_flags_handle, DirtyFlagsManager::DIRTY_ORIENTATION, true);
}

void BaseObject::LookInDirection(const XMFLOAT3& direction_to_look)
{
	CollisionLibrary::LookInDirection(_matrix, direction_to_look);

	// Dirty the orientation
	DirtyFlagsManager::SetDirty(_dirty_flags_handle, DirtyFlagsManager::DIRTY_ORIENTATION, true);
}

void BaseObject::ClearStatesToIdle()
{
	// pop all but the first state
	while (_states.size() > 1)
	{
		_states.pop_back();
	}
}

void BaseObject::AddState(StateInterface* state)
{
	_states.push_back(std::unique_ptr<StateInterface>(state));
}

void BaseObject::AddComponent(std::unique_ptr<IComponent> component)
{
	if (component)
	{
		// If a component of this type already exists, overwrite it
		auto type = component->GetComponentType();

		_components[type].swap(component);
	}
}

std::unique_ptr<IComponent> BaseObject::RemoveComponent(int component_type)
{
	std::unique_ptr<IComponent> removed_component = nullptr;

	auto iter = _components.find(IComponent::COMPONENT_TYPE(component_type));

	if (iter != _components.end())
	{
		iter->second.swap(removed_component);

		_components.erase(iter);
	}

	// return the removed component incase the user wishes to save it for later
	return removed_component;
}

void BaseObject::AddComponentFeature(std::unique_ptr<IFeature> feature)
{
	auto component_type = feature->GetComponentType();

	auto iter = _components.find(IComponent::COMPONENT_TYPE(component_type));

	if (iter != _components.end())
	{
		auto& component = iter->second;

		component->AddFeature(std::move(feature));
	}
	else
	{
		Log::Warning("BaseObject::AddComponentFeature - Cannot add feature. Component does not exist\n");
	}
}

void BaseObject::RemoveComponentFeature(int component_type, int feature_type)
{
	auto iter = _components.find(IComponent::COMPONENT_TYPE(component_type));

	if (iter != _components.end())
	{
		auto& component = iter->second;

		component->RemoveFeature(feature_type);
	}
	else
	{
		Log::Warning("BaseObject::RemoveComponentFeature - Could not find feature\n");
	}
}

bool BaseObject::OnClicked(std::shared_ptr<BaseObject> clicker)
{
	Log::Info("%d clicked %d\n", clicker, this);

	if (this->_object_name == OBJECT_NAME::NAME_EQUIPMENT)
	{
		clicker->AttachObject(
			shared_from_this(),
			CHILD_TYPE::EQUIPMENT,
			ATTACHMENT_TYPE::ATTACH,
			XMFLOAT3(0.7f, 1.0f, 1.0f),
			XMFLOAT3(0.0f, 0.0f, 0.0f));

		this->AddComponentFeature(FeatureFactory::CreateFeature(IFeature::FEATURE_RENDER_ON_TOP));

		this->RemoveComponent(IComponent::COMPONENT_GROUND_CLAMP);
		this->RemoveComponent(IComponent::COMPONENT_COLLISION);

		this->RemoveComponentFeature(IComponent::COMPONENT_RENDER, IFeature::FEATURE_RENDER_OUTLINE_WHEN_NEAR_OBJECT);
	}

	return true;
}
 
const char* BaseObject::GetInstanceData() const
{
	// static so it still persists after pointer is returned
	static C_BUFFER_INSTANCE_MESH_OBJECT instance_data;

	GetWorldMatrix(instance_data.world_matrix);

	return (const char*)&instance_data;
}

void BaseObject::AttachObject(std::shared_ptr<BaseObject> child, CHILD_TYPE child_type, ATTACHMENT_TYPE attachment_type, const XMFLOAT3& offset, const XMFLOAT3& rotation)
{
	// Create an offset matrix based off the position and rotation relative to the parent
	XMFLOAT4X4 offset_matrix;

	auto ratation_radians = XMFLOAT3(XMConvertToRadians(rotation.x), XMConvertToRadians(rotation.y), XMConvertToRadians(rotation.z));

	XMStoreFloat4x4(&offset_matrix, XMMatrixRotationRollPitchYawFromVector(XMLoadFloat3(&ratation_radians)));
	offset_matrix._41 = offset.x;
	offset_matrix._42 = offset.y;
	offset_matrix._43 = offset.z;

	// Set the child's local matrix to the offset matrix. This way, the child's matrix is relative to its parent.
	child->SetLocalMatrix(offset_matrix);

	child->SetParent(std::make_pair(shared_from_this(), attachment_type));

	_children[child_type].push_back(child.get());
}

void BaseObject::DetachObject(CHILD_TYPE child_type, std::shared_ptr<BaseObject> child)
{
	XMFLOAT4X4 childs_new_matrix;

	child->GetWorldMatrix(childs_new_matrix);

	child->SetLocalMatrix(childs_new_matrix);

	child->SetParent(std::make_pair(nullptr, ATTACHMENT_TYPE::ATTACH));

	auto iter = _children.find(child_type);

	if (iter != _children.end())
	{
		auto& children = iter->second;

		auto child_iter = std::find(children.begin(), children.end(), child.get());

		if (child_iter != children.end())
		{
			children.erase(child_iter);
		}
		else
		{
			Log::Warning("BaseObject::DetachObject - Child not found\n");
		}
	}
	else
	{
		Log::Warning("BaseObject::DetachObject - No children found of this type\n");
	}
}

void BaseObject::DetachObject(BaseObject* child)
{
	for (auto& child_list : _children)
	{
		auto iter = std::find(child_list.second.begin(), child_list.second.end(), child);

		if (iter != child_list.second.end())
		{
			child_list.second.erase(iter);

			return;
		}
	}

	Log::Warning("BaseObject::DetachObject - Child not found\n");
}

void BaseObject::GetWorldMatrix(XMFLOAT4X4& world_matrix) const
{
	if (_parent.first)
	{
		_parent.first->GetWorldMatrix(world_matrix);

		if (_parent.second == ATTACHMENT_TYPE::TETHER)
		{
			XMFLOAT4X4 out_matrix;
			memcpy_s(&out_matrix, sizeof(out_matrix), &_matrix, sizeof(_matrix));

			out_matrix._41 += world_matrix._41;
			out_matrix._42 += world_matrix._42;
			out_matrix._43 += world_matrix._43;

			memcpy_s(&world_matrix, sizeof(world_matrix), &out_matrix, sizeof(out_matrix));
		}
		else if (_parent.second == ATTACHMENT_TYPE::ATTACH)
		{
			XMStoreFloat4x4(&world_matrix, XMMatrixMultiply(XMLoadFloat4x4(&_matrix), XMLoadFloat4x4(&world_matrix)));
		}
		else if (_parent.second == ATTACHMENT_TYPE::ORBIT)
		{
			XMFLOAT4X4 out_matrix;
			memcpy_s(&out_matrix, sizeof(out_matrix), &_matrix, sizeof(_matrix));

			XMFLOAT3 delta_position;

			delta_position.x = (cos(_matrix._41) * _matrix._43);
			delta_position.z = (sin(_matrix._41) * _matrix._43);
			delta_position.y = _matrix._42;

			out_matrix._41 = delta_position.x + world_matrix._41;
			out_matrix._42 = delta_position.y + world_matrix._42;
			out_matrix._43 = delta_position.z + world_matrix._43;

			// Have the child look at the parent's position
			CollisionLibrary::LookAt(out_matrix, XMFLOAT3(world_matrix._41, world_matrix._42, world_matrix._43));

			memcpy_s(&world_matrix, sizeof(world_matrix), &out_matrix, sizeof(out_matrix));
		}
		else
		{
			Log::Error("BaseObject::GetWorldMatrix - Unknown attachment type\n");
		}
	}
	else
	{
		memcpy_s(&world_matrix, sizeof(world_matrix), &_matrix, sizeof(_matrix));
	}
}

void BaseObject::GetLocalMatrix(XMFLOAT4X4& local_matrix) const
{
	memcpy_s(&local_matrix, sizeof(local_matrix), &_matrix, sizeof(_matrix));
}

void BaseObject::GetWorldPosition(XMFLOAT3& world_position) const
{
	// This if check isn't really needed, we could just do the first case. I'm only checking parent to avoid doing the call to GetWorldMatrix.
	if (_parent.first)
	{
		XMFLOAT4X4 world_matrix;

		this->GetWorldMatrix(world_matrix);

		memcpy_s(&world_position, sizeof(world_position), world_matrix.m[3], sizeof(world_position));
	}
	else
	{
		memcpy_s(&world_position, sizeof(world_position), _matrix.m[3], sizeof(world_position));
	}
}

void BaseObject::GetLocalPosition(XMFLOAT3& local_position) const
{
	memcpy_s(&local_position, sizeof(local_position), _matrix.m[3], sizeof(local_position));
}

XMFLOAT3 BaseObject::GetWorldPosition() const
{
	XMFLOAT3 position_vector;

	GetWorldPosition(position_vector);

	return position_vector;
}

const XMFLOAT3& BaseObject::GetWorldOldPosition() const
{
	return _old_position;
}

void BaseObject::GetWorldForward(XMFLOAT3& forward_vector) const
{
	// This if check isn't really needed, we could just do the first case. I'm only checking parent to avoid doing the call to GetWorldMatrix.
	if (_parent.first)
	{
		XMFLOAT4X4 world_matrix;

		this->GetWorldMatrix(world_matrix);

		memcpy_s(&forward_vector, sizeof(forward_vector), world_matrix.m[2], sizeof(forward_vector));
	}
	else
	{
		memcpy_s(&forward_vector, sizeof(forward_vector), _matrix.m[2], sizeof(forward_vector));
	}
}

void BaseObject::GetWorldUp(XMFLOAT3& up_vector) const
{
	// This if check isn't really needed, we could just do the first case. I'm only checking parent to avoid doing the call to GetWorldMatrix.
	if (_parent.first)
	{
		XMFLOAT4X4 world_matrix;

		this->GetWorldMatrix(world_matrix);

		memcpy_s(&up_vector, sizeof(up_vector), world_matrix.m[1], sizeof(up_vector));
	}
	else
	{
		memcpy_s(&up_vector, sizeof(up_vector), _matrix.m[1], sizeof(up_vector));
	}
}

void BaseObject::GetWorldSide(XMFLOAT3& side_vector) const
{
	// This if check isn't really needed, we could just do the first case. I'm only checking parent to avoid doing the call to GetWorldMatrix.
	if (_parent.first)
	{
		XMFLOAT4X4 world_matrix;

		this->GetWorldMatrix(world_matrix);

		memcpy_s(&side_vector, sizeof(side_vector), world_matrix.m[0], sizeof(side_vector));
	}
	else
	{
		memcpy_s(&side_vector, sizeof(side_vector), _matrix.m[0], sizeof(side_vector));
	}
}

void BaseObject::SetLocalMatrix(XMFLOAT4X4& local_matrix)
{
	memcpy_s(&_matrix, sizeof(_matrix), &local_matrix, sizeof(local_matrix));
}

void BaseObject::SetLocalPosition(XMFLOAT3& local_position)
{
	memcpy_s(&_matrix.m[3], sizeof(local_position), &local_position, sizeof(local_position));
}

bool BaseObject::GetDirty(DirtyFlagsManager::DIRTY_FLAGS dirty_flag) const
{
	if (_parent.first && _parent.first->GetDirty(dirty_flag))
	{
		return true;
	}

	return DirtyFlagsManager::IsDirty(_dirty_flags_handle, dirty_flag);
}

