#include "stdafx.h"
#include "FeatureFactory.h"
#include "FeatureRenderOnTop.h"
#include "FeatureRenderOutline.h"
#include "FeatureCollisionClickable.h"
#include "FeatureCollisionCollidable.h"
#include "FeatureCollisionTriggerable.h"
#include "FeatureCollisionTrigger.h"
#include "FeatureParams.h"
#include "Service.h"

using namespace std::placeholders;

std::vector<std::function<std::unique_ptr<IFeature>(const IFeatureParams&)>> FeatureFactory::_create_functions;
Service* FeatureFactory::_service = nullptr;

void FeatureFactory::Initialize(Service* service)
{
	_service = service;

	_create_functions.resize(IFeature::NUM_FEATURE_TYPES);

	_create_functions[IFeature::FEATURE_RENDER_ON_TOP] = std::bind(&FeatureFactory::CreateRenderOnTopFeature, _1);
	_create_functions[IFeature::FEATURE_RENDER_OUTLINE_WHEN_NEAR_OBJECT] = std::bind(&FeatureFactory::CreateRenderOutlineFeature, _1);
	_create_functions[IFeature::FEATURE_COLLISION_CLICKABLE] = std::bind(&FeatureFactory::CreateCollisionClickableFeature, _1);
	_create_functions[IFeature::FEATURE_COLLISION_COLLIDABLE] = std::bind(&FeatureFactory::CreateCollisionCollidableFeature, _1);
	_create_functions[IFeature::FEATURE_COLLISION_TRIGGERABLE] = std::bind(&FeatureFactory::CreateCollisionTriggerableFeature, _1);
	_create_functions[IFeature::FEATURE_COLLISION_TRIGGER] = std::bind(&FeatureFactory::CreateCollisionTriggerFeature, _1);
}

std::unique_ptr<IFeature> FeatureFactory::CreateFeature(IFeature::FEATURE_TYPE feature_type, const IFeatureParams& feature_params)
{
	if (feature_type >= 0 && feature_type < IFeature::NUM_FEATURE_TYPES)
	{
		return _create_functions[feature_type](feature_params);
	}

	return nullptr;
}

std::unique_ptr<IFeature> FeatureFactory::CreateRenderOnTopFeature(const IFeatureParams& feature_params)
{
	// unused
	(void)feature_params;

	return std::make_unique<FeatureRenderOnTop>(_service->GetCameraManager());
}

std::unique_ptr<IFeature> FeatureFactory::CreateRenderOutlineFeature(const IFeatureParams& feature_params)
{
	auto& render_outline_params = (const FeatureRenderOutlineParams&)feature_params;

	return std::make_unique<FeatureRenderOutline>(render_outline_params._base_object,
		render_outline_params._outline_range,
		render_outline_params._outline_color);
}

std::unique_ptr<IFeature> FeatureFactory::CreateCollisionClickableFeature(const IFeatureParams& feature_params)
{
	// unused
	(void)feature_params;

	return std::make_unique<FeatureCollisionClickable>();
}

std::unique_ptr<IFeature> FeatureFactory::CreateCollisionCollidableFeature(const IFeatureParams& feature_params)
{
	// unused
	(void)feature_params;

	return std::make_unique<FeatureCollisionCollidable>();
}

std::unique_ptr<IFeature> FeatureFactory::CreateCollisionTriggerableFeature(const IFeatureParams& feature_params)
{
	// unused
	(void)feature_params;

	return std::make_unique<FeatureCollisionTriggerable>();
}

std::unique_ptr<IFeature> FeatureFactory::CreateCollisionTriggerFeature(const IFeatureParams& feature_params)
{
	auto& collision_trigger_params = (FeatureCollisionTriggerParams&) feature_params;

	return std::make_unique<FeatureCollisionTrigger>(_service->GetInputManager(), collision_trigger_params._key);
}