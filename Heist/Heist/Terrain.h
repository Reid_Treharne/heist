#pragma once
#include "BaseObject.h"
#include "../CollisionManager/CollisionLibrary.h"
#include "../TileManager/TileManager.h"
#include "../Renderer/MeshObject.h"
#include <vector>

using namespace CollisionLibrary;

class CollisionManager;
class TileManager;
class Tile;

class Terrain : public BaseObject
{
public:
	Terrain(void);
	~Terrain(void);

	void Initialize(MeshObject* mesh_object, TileManager* tile_manager, CollisionManager* collision_manager, std::vector<TileMeshData>& vertices, std::vector<unsigned int>& indices);
	virtual void Update(float delta_time) override;

	// Overwriten in order to update tile manager's
	// position whenever the terrain's position is set
	virtual void SetPosition(const XMFLOAT3& position) override;

	bool CollidesWithLine(const XMFLOAT3& start_point, const XMFLOAT3& end_point, XMFLOAT3& collision_point) const;

	void RaiseVertex(unsigned int vertex_index, float raise_amount);
	const TileMeshData* GetVertex(unsigned int index) const;

	// Accessors
	// Trianlges are stored in a vector with index 0 and 1 being the botton left tile. index 1 and 2 are the
	// tile in the row just above it, etc...
	inline const Tile* GetClickedTile() const { return _clicked_tile; }
	inline const unsigned int GetClickedTriangleIndex() const { return _clicked_triangle_index; }

private:
	void InitializeTriangles();
	void UpdateTriangles(float delta_time);

	// The triangles that make up the terrain
	std::vector<Triangle> _triangles;

	std::vector<TileMeshData> _vertices;
	std::vector<unsigned int> _indices;

	// A pointer to the tile manager
	TileManager* _tile_manager;

	// A pointer to the collision manager
	CollisionManager* _collision_manager;

	mutable const Tile* _clicked_tile;
	mutable unsigned int _clicked_triangle_index;

	// These get applied to the terrain's vertices every update and are then cleared.
	// They are currently only modifiable via the RaiseVertex function.
	// It is a map of the vertex indices and their height offsets
	std::unordered_map<int, float> _vertex_offsets;

	MeshObject* _mesh_object;
};

