#pragma once
#include <memory>
#include <vector>

#include <DirectXMath.h>
using DirectX::XMFLOAT3;

class BaseObject;

class BaseObjectUtil
{
public:
	// Sorts the objects in the vector from their distance to the given position.
	static void SortByDistance(std::vector<std::shared_ptr<BaseObject>>& base_objects, const XMFLOAT3& posistion);

	// Sorts the objects in the vector from their specified point to the given position.
	static void SortByPoint(std::vector<std::pair<std::shared_ptr<BaseObject>, XMFLOAT3>>& base_objects, const XMFLOAT3& posistion);

private:
	BaseObjectUtil(const BaseObjectUtil&);
	BaseObjectUtil& operator=(const BaseObjectUtil&);
};

