#pragma once
#include "IComponent.h"

class InputManager;
class Camera;
class BaseObject;
class TileManager;
class Terrain;
class CollisionManager;
class Tile;

#include "../CollisionManager/ObjectTree.h"

class ComponentInteract : public IComponent
{
public:
	ComponentInteract(
		const InputManager* input_manager,
		std::shared_ptr<const Camera> camera,
		const ObjectTree<std::shared_ptr<BaseObject>>* object_tree,
		const TileManager* tile_manager,
		std::shared_ptr<Terrain> terrain);

	virtual ~ComponentInteract(void);

	virtual void Update(BaseObject* base_object, float delta_time) override;
	virtual IComponent::COMPONENT_TYPE GetComponentType() const override;

private:
	const InputManager* _input_manager;
	std::shared_ptr<const Camera> _camera;
	const ObjectTree<std::shared_ptr<BaseObject>>* _object_tree;
	const TileManager* _tile_manager;
	std::shared_ptr<Terrain> _terrain;
};

