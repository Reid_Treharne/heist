#pragma once
#include "BaseObject.h"
#include "../Renderer/Renderer.h"
#include "../CollisionManager/ObjectTree.h"
#include "ObjectNames.h"
#include "ObjectManagerStateBasic.h"
#include "ObjectManagerStateDebug.h"
#include <unordered_map>

class Tile;

class ObjectManager
{
public:
	ObjectManager(void);
	~ObjectManager(void);

	friend class ObjectManagerState;
	friend class ObjectManagerStateBasic;
	friend class ObjectManagerStateDebug;

	void Initialize(Renderer* renderer, ObjectFactory* object_factory, ObjectTree<std::shared_ptr<BaseObject>>* object_tree);
	void AddObject(std::shared_ptr<BaseObject> base_render_object, OBJECT_NAME object_name);
	void RemoveObject(std::shared_ptr<BaseObject> base_render_object, OBJECT_NAME object_name);

	void Update(float dtime);

	void SetAsDebug();
	void SetAsBasic();

private:

	void SetState(ObjectManagerState* state);

	std::map<OBJECT_NAME, std::vector<std::shared_ptr<BaseObject>>> _object_map;

	// Current state
	ObjectManagerState* _current_state;

	// States
	ObjectManagerStateBasic _state_basic;
	ObjectManagerStateDebug _state_debug;
};

