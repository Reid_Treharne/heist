#include "stdafx.h"
#include "CameraManager.h"
#include "../Logger/Log.h"
#include "../InputManager/InputManager.h"

CameraManager::CameraManager() :
	_camera_state(nullptr)
{
}


CameraManager::~CameraManager()
{
	if (_camera_state)
	{
		_camera_state->Exit();
	}
}

void CameraManager::Initialize(std::shared_ptr<Camera> camera, InputManager* input_manager)
{
	_camera = camera;
	_input_manager = input_manager;
	// Intialize the camera states
	_camera_state_free.Initialize(camera);
	_camera_state_third_person.Initialize(camera);
	_camera_state_first_person.Initialize(camera);
	_camera_state_top_down.Initialize(camera);

	// Set the camera state pointer
	SetCameraState(&_camera_state_free, nullptr);
}

void CameraManager::Update(float delta_time)
{
	if (_camera_state)
	{
		_camera_state->Input(_input_manager);

		_camera_state->Update(delta_time);
	}
}

void CameraManager::Detach()
{
	_input_manager->HideCursor(false);

	// Set the camera state to free
	SetCameraState(&_camera_state_free, nullptr);

	_camera_state_type = CAMERA_STATE_DETACHED;
}

void CameraManager::SetAsThirdPerson(std::shared_ptr<BaseObject> object_to_attach_to)
{
	_input_manager->HideCursor(false);

	// Set the camera state to third person
	SetCameraState(&_camera_state_third_person, object_to_attach_to);

	_camera_state_type = CAMERA_STATE_THIRD_PERSON;
}

void CameraManager::SetAsFirstPerson(std::shared_ptr<BaseObject> object_to_attach_to)
{
	_input_manager->HideCursor(true);

	// Set the camera state to first person
	SetCameraState(&_camera_state_first_person, object_to_attach_to);

	_camera_state_type = CAMERA_STATE_FIRST_PERSON;
}

void CameraManager::SetAsTopDown(std::shared_ptr<BaseObject> object_to_attach_to)
{
	_input_manager->HideCursor(false);

	// Set the camera state to top down
	SetCameraState(&_camera_state_top_down, object_to_attach_to);

	_camera_state_type = CAMERA_STATE_TOP_DOWN;
}

void CameraManager::SetCameraState(CameraState* camera_state, std::shared_ptr<BaseObject> object_attached_to)
{
	if (camera_state != _camera_state)
	{
		if (_camera_state != nullptr)
		{
			_camera_state->Exit();
		}
		if (camera_state != nullptr)
		{
			_camera_state = camera_state;

			_camera_state->Enter(object_attached_to);
		}
		else
		{
			Log::Warning("Camera::SetCameraState - Trying to set camera state to null\n");
		}
	}
}