#include "stdafx.h"
#include "ComponentMouseOver.h"
#include "BaseObject.h"
#include "BaseObjectUtil.h"
#include "ObjectFactory.h"
#include "ObjectManager.h"
#include "../InputManager/InputManager.h"
#include "Camera.h"
#include "../Logger/Log.h"

ComponentMouseOver::ComponentMouseOver(const InputManager* input_manager, const ObjectFactory* object_factory, ObjectManager* object_manager, std::shared_ptr<const Camera> camera, const ObjectTree<std::shared_ptr<BaseObject>>* object_tree) :
	_input_manager(input_manager),
	_object_factory(object_factory),
	_object_manager(object_manager),
	_camera(camera),
	_object_tree(object_tree)
{
	_collision_point_marker = _object_factory->CreateObject("Collision_Point_Marker", OBJECT_NAME::NAME_AESTHETIC, XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f));

	_object_manager->AddObject(_collision_point_marker, OBJECT_NAME::NAME_AESTHETIC);
}


ComponentMouseOver::~ComponentMouseOver(void)
{
	_object_manager->RemoveObject(_collision_point_marker, OBJECT_NAME::NAME_AESTHETIC);
}

void ComponentMouseOver::Update(BaseObject* base_object, float delta_time)
{
	int _mouse_x_client_position;
	int _mouse_y_client_position;

	_input_manager->GetMouseClientPosition(_mouse_x_client_position, _mouse_y_client_position);

	XMFLOAT3 world_space_start;
	XMFLOAT3 world_space_end;

	_camera->Unproject(_mouse_x_client_position, _mouse_y_client_position, world_space_start, world_space_end);

	// Check collision with objects
	auto selected_objects = _object_tree->CheckCollisionWithRay(world_space_start, world_space_end, COLLISION_FLAG::CLICKABLE);

	if (selected_objects.empty() == false)
	{
		// If the mouse is over an object, make sure the collision point marker is visible by having the render component
		if (_collision_point_marker_render_component)
		{
			_collision_point_marker->AddComponent(std::move(_collision_point_marker_render_component));
		}

		BaseObjectUtil::SortByPoint(selected_objects, world_space_start);

		const auto& nearest_object = selected_objects.front().first;
		const auto& nearest_collision_point = selected_objects.front().second;

		_collision_point_marker->SetPosition(nearest_collision_point);
	}
	else
	{
		// If the mouse is not currently over in objects, hide the collision point marker by removing the render component
		auto render_component = _collision_point_marker->RemoveComponent(IComponent::COMPONENT_RENDER);

		if (render_component)
		{
			_collision_point_marker_render_component = std::move(render_component);
		}
	}
}

IComponent::COMPONENT_TYPE ComponentMouseOver::GetComponentType() const
{
	return IComponent::COMPONENT_MOUSE_OVER;
}
