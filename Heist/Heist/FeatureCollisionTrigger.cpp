#include "stdafx.h"
#include "FeatureCollisionTrigger.h"
#include "ComponentCollision.h"
#include "StateInteract.h"
#include "BaseObject.h"
#include "BaseObjectUtil.h"
#include "../InputManager/InputManager.h"

using namespace std::placeholders;

FeatureCollisionTrigger::FeatureCollisionTrigger(const InputManager* input_manager, unsigned char key) :
	_input_manager(input_manager),
	_key(key)
{
}


FeatureCollisionTrigger::~FeatureCollisionTrigger()
{
}

IFeature::FEATURE_TYPE FeatureCollisionTrigger::GetType() const
{
	return IFeature::FEATURE_COLLISION_TRIGGER;
}

unsigned int FeatureCollisionTrigger::GetComponentType() const
{
	return IComponent::COMPONENT_COLLISION;
}

void FeatureCollisionTrigger::OnAdded(IComponent& component)
{
	if (component.GetComponentType() == GetComponentType())
	{
		auto& collision_component = (ComponentCollision&)component;

		auto& collider = collision_component._collider;

		collider.BindOnTriggerFunction(std::bind(&FeatureCollisionTrigger::OnTrigger, this, _1, _2));
	}
}

void FeatureCollisionTrigger::OnRemoved(IComponent& component)
{
	if (component.GetComponentType() == GetComponentType())
	{
		auto& collision_component = (ComponentCollision&)component;

		auto& collider = collision_component._collider;

		collider.BindOnTriggerFunction(nullptr);
	}
}

void FeatureCollisionTrigger::Update(BaseObject* base_object, IComponent& component)
{
	(void)component;

	if (_current_triggers.second.empty() == false)
	{
		if (base_object == _current_triggers.first.get())
		{
			if (_input_manager->GetKeyPressed(_key))
			{
				// interact with the object closest to the trigger
				BaseObjectUtil::SortByDistance(_current_triggers.second, base_object->GetWorldPosition());

				_current_triggers.first->AddState(new StateInteract(*_current_triggers.second.begin()));

				_current_triggers.second.erase(_current_triggers.second.begin());
			}
		}
		else
		{
			Log::Error("FeatureCollisionTrigger::Update - baseobject does not equal collider1\n");
		}

		// Clear out the current triggers every frame since they are re-added every frame to ensure accuracy
		_current_triggers.first = nullptr;
		_current_triggers.second.clear();
	}
}

// Called every frame that collider1 is colliding (triggering) collider2. For example if the player is standing on a weapon,
// This function will be called every frame until the player or weapon moves. collider1 being the player and collider2 being the weapon.
void FeatureCollisionTrigger::OnTrigger(std::shared_ptr<BaseObject> collider1, std::shared_ptr<BaseObject> collider2)
{
	if (_current_triggers.first == nullptr)
	{
		_current_triggers.first = collider1;
	}

	if (_current_triggers.first == collider1)
	{
		// Only add the trigger to the list if it doesn't already exist
		auto iter = std::find(_current_triggers.second.begin(), _current_triggers.second.end(), collider2);

		if (iter == _current_triggers.second.end())
		{
			_current_triggers.second.push_back(collider2);
		}
	}
	else
	{
		Log::Error("FeatureCollisionTrigger::OnTrigger - collider1 has changed\n");
	}
}
