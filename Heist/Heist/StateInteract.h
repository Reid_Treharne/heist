#pragma once
#include "StateInterface.h"

#include <memory>

class BaseObject;

class StateInteract : public StateInterface
{
public:
	StateInteract(std::shared_ptr<BaseObject> interact_object);
	virtual ~StateInteract(void);

	virtual void Enter() override;

	virtual bool Update(std::shared_ptr<BaseObject> base_object, float delta_time) override;
protected:
	std::shared_ptr<BaseObject> _interact_object;
};

