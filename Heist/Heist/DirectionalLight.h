#pragma once

#include "BaseObject.h"

class MeshObject;

class DirectionalLight : public BaseObject
{
public:
	DirectionalLight(void);
	~DirectionalLight(void);

	virtual void Update(float delta_time) override;
	virtual const char* GetInstanceData() const override;

private:
	XMFLOAT4 _ambient_color;
	XMFLOAT4 _diffuse_color;
};

