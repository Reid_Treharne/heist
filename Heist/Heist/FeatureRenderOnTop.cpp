#include "stdafx.h"
#include "FeatureRenderOnTop.h"
#include "IComponent.h"
#include "CameraManager.h"
#include "ComponentRender.h"

FeatureRenderOnTop::FeatureRenderOnTop(CameraManager* camera_manager) :
	_camera_manager(camera_manager)
{
}


FeatureRenderOnTop::~FeatureRenderOnTop()
{
}

IFeature::FEATURE_TYPE FeatureRenderOnTop::GetType() const
{
	return IFeature::FEATURE_RENDER_ON_TOP;
}

unsigned int FeatureRenderOnTop::GetComponentType() const
{
	return IComponent::COMPONENT_RENDER;
}

void FeatureRenderOnTop::Update(BaseObject* base_object, IComponent& component)
{
	if (component.GetComponentType() == IComponent::COMPONENT_RENDER)
	{
		auto& render_component = (ComponentRender&)component;

		if (_camera_manager->GetCameraState() == CameraManager::CAMERA_STATE_FIRST_PERSON)
		{
			render_component.EnableRenderFlag(RENDER_FLAG::DRAW_ON_TOP);
		}
		else
		{
			render_component.DisableRenderFlag(RENDER_FLAG::DRAW_ON_TOP);
		}
	}
}



