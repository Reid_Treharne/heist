#pragma once

class Renderer;
class ObjectManager;
class GameStateManager;
class Service;

class IGameState
{
public:
	IGameState(void);
	virtual ~IGameState(void) = 0;

	virtual void Enter() {};
	virtual void Exit() {};

	virtual void Input(GameStateManager& game_state_manager) {};
	virtual void Update(float delta_time) {};
};

