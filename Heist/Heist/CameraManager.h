#pragma once

#include "CameraStateFree.h"
#include "CameraStateThirdPerson.h"
#include "CameraStateFirstPerson.h"
#include "CameraStateTopDown.h"

class CameraState;
class InputManager;

class CameraManager
{
public:
	CameraManager();
	~CameraManager();

	enum CAMERA_STATE { CAMERA_STATE_DETACHED, CAMERA_STATE_THIRD_PERSON, CAMERA_STATE_FIRST_PERSON, CAMERA_STATE_TOP_DOWN };

	void Initialize(std::shared_ptr<Camera> camera, InputManager* input_manager);
	void Update(float delta_time);

	// Attaching/Detaching
	void Detach();
	void SetAsThirdPerson(std::shared_ptr<BaseObject> object_to_attach_to);
	void SetAsFirstPerson(std::shared_ptr<BaseObject> object_to_attach_to);
	void SetAsTopDown(std::shared_ptr<BaseObject> object_to_attach_to);

	inline CAMERA_STATE GetCameraState() const { return _camera_state_type;  }
private:
	void SetCameraState(CameraState* camera_state, std::shared_ptr<BaseObject> object_attached_to);

	// A pointer to the current state of the camera
	CameraState* _camera_state;
	CAMERA_STATE _camera_state_type;

	// The different camera states
	CameraStateFree _camera_state_free;
	CameraStateThirdPerson _camera_state_third_person;
	CameraStateFirstPerson _camera_state_first_person;
	CameraStateTopDown _camera_state_top_down;

	std::shared_ptr<Camera> _camera;

	// A pointer to the input manager
	InputManager* _input_manager;
};

