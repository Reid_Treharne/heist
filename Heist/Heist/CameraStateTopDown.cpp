#include "stdafx.h"
#include "CameraStateTopDown.h"
#include "Camera.h"
#include "Terrain.h"
#include "IComponent.h"
#include "ComponentFactory.h"
#include "ComponentGroundClamp.h"
#include "../InputManager/InputManager.h"
#include "../TileManager/Tile.h"

static const XMFLOAT3 kDefaulOrientation(80.0f, 0.0f, 0.0f);
static const float kCameraHeightAboveTerrain = 20.0f;

CameraStateTopDown::CameraStateTopDown(void) :
	_delta_translation(0.0f, 0.0f, 0.0f)
{
}


CameraStateTopDown::~CameraStateTopDown(void)
{
}

void CameraStateTopDown::Enter(std::shared_ptr<BaseObject> object_attached_to)
{
	// Set the camera's orientation to face down
	_camera->SetOrientation(kDefaulOrientation);

	// Ground clamp the camera to the terrain but at the specified height above the terrain
	GroundClampComponentParams ground_clamp_parameters(OBJECT_NAME::NAME_CAMERA, kCameraHeightAboveTerrain);

	_camera->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_GROUND_CLAMP, ground_clamp_parameters));

	CameraState::Enter(object_attached_to);
}

void CameraStateTopDown::Exit()
{
	_camera->RemoveComponent(IComponent::COMPONENT_GROUND_CLAMP);

	CameraState::Exit();
}

void CameraStateTopDown::Input(InputManager* input_manager)
{
	InputManager& input = *input_manager;

	Camera& camera = *_camera;

	// Check for camera movement
	if (input.GetKeyDown('A'))
	{
		_delta_translation = _delta_translation + XMFLOAT3(-1.0f, 0.0f, 0.0f);
	}
	else if (input.GetKeyDown('D'))
	{
		_delta_translation = _delta_translation + XMFLOAT3(1.0f, 0.0f, 0.0f);
	}
	if (input.GetKeyDown('W'))
	{
		_delta_translation = _delta_translation + XMFLOAT3(0.0f, 1.0f, 0.0f);
	}
	else if (input.GetKeyDown('S'))
	{
		_delta_translation = _delta_translation + XMFLOAT3(0.0f, -1.0f, 0.0f);
	}
	if (input.GetKeyDown('Q'))
	{
		_delta_translation = _delta_translation + XMFLOAT3(0.0f, -1.0f, 0.0f);
	}
	else if (input.GetKeyDown('E'))
	{
		_delta_translation = _delta_translation + XMFLOAT3(0.0f, 1.0f, 0.0f);
	}
}

void CameraStateTopDown::Update(float delta_time)
{
	if (_delta_translation != XMFLOAT3(0.0f, 0.0f, 0.0f))
	{
		_camera->Translate(_delta_translation * delta_time);

		_delta_translation = XMFLOAT3(0.0f, 0.0f, 0.0f);
	}
}

