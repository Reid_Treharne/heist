#include "stdafx.h"
#include "GameStateDebug.h"
#include "ObjectManager.h"
#include "HUDManager.h"
#include "GameStateManager.h"
#include "CameraManager.h"
#include "Service.h"
#include "ComponentFactory.h"
#include "../InputManager/InputManager.h"

GameStateDebug::GameStateDebug(void) :
	_service(nullptr)
{
}


GameStateDebug::~GameStateDebug(void)
{
	Exit();
}

void GameStateDebug::Initialize(Service* service)
{
	_service = service;

	_test_HUD_mesh_object.Initialize(_service->GetRenderer(), "../../Resources/Textures/DDSTextures/Success.dds", 0.25f, 0.35f);

	_test_HUD_object = std::make_shared<HUDObject>();

	_test_HUD_object->SetPosition(XMFLOAT2(0.1f, 0.5f));
	_test_HUD_object->SetColor(XMFLOAT4(1.0f, 1.0f, 0.0f, 0.2f));
	_test_HUD_object->Initalize(&_test_HUD_mesh_object);
}

void GameStateDebug::Enter()
{
	_service->GetObjectManager()->SetAsDebug();

	// Add the Debug HUD objects
	_service->GetHUDManager()->AddObject(_test_HUD_object);

	// Detach the camera from any object it may have
	// been attached to so it can be freely moved around.
	_service->GetCameraManager()->Detach();

	// Add the forge component
	_service->GetCamera()->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_FORGE));
}

void GameStateDebug::Exit()
{
	_service->GetObjectManager()->SetAsBasic();

	// Remove the Debug HUD objects
	_service->GetHUDManager()->RemoveObject(_test_HUD_object);

	// Remove the forge component
	_service->GetCamera()->RemoveComponent(IComponent::COMPONENT_FORGE);
}

void GameStateDebug::Input(GameStateManager& game_state_manager)
{
	InputManager& input_manager = *_service->GetInputManager();
	Camera& camera = *_service->GetCamera();

	// Check for the debug key (enter)
	if (input_manager.GetKeyDown(VK_RETURN))
	{
		if (input_manager.GetKeyPressed('D'))
		{
			// Return to previous state (game play)
			game_state_manager.PopState();

			return;
		}
	}

	// Check for speed change
	if (input_manager.GetKeyPressed(VK_OEM_PLUS))
	{
		camera.ChangeMoveSpeed(10);
	}
	else if (input_manager.GetKeyPressed(VK_OEM_MINUS))
	{
		camera.ChangeMoveSpeed(-10);
	}
}

void GameStateDebug::Update(float delta_time)
{
	_service->GetObjectManager()->Update(delta_time);

	_service->GetHUDManager()->Update(delta_time);
}
