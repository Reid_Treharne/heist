#pragma once

#include "IFeature.h"
#include "FeatureParams.h"
#include <vector>
#include <functional>

class Service;

class FeatureFactory
{
public:
	static void Initialize(Service* service);
	static std::unique_ptr<IFeature> CreateFeature(IFeature::FEATURE_TYPE feature_type, const IFeatureParams& feature_params = IFeatureParams());
private:
	FeatureFactory() = delete;
	~FeatureFactory() = delete;

	static std::unique_ptr<IFeature> CreateRenderOnTopFeature(const IFeatureParams& feature_params);
	static std::unique_ptr<IFeature> CreateRenderOutlineFeature(const IFeatureParams& feature_params);
	static std::unique_ptr<IFeature> CreateCollisionClickableFeature(const IFeatureParams& feature_params);
	static std::unique_ptr<IFeature> CreateCollisionCollidableFeature(const IFeatureParams& feature_params);
	static std::unique_ptr<IFeature> CreateCollisionTriggerableFeature(const IFeatureParams& feature_params);
	static std::unique_ptr<IFeature> CreateCollisionTriggerFeature(const IFeatureParams& feature_params);

	static std::vector<std::function<std::unique_ptr<IFeature>(const IFeatureParams&)>> _create_functions;

	static Service* _service;
};

