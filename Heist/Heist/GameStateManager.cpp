#include "stdafx.h"
#include "GameStateManager.h"
#include "../Logger/Log.h"
#include <cassert>

GameStateManager::GameStateManager(void)
{
}


GameStateManager::~GameStateManager(void)
{
}

void GameStateManager::PushState(IGameState* game_state)
{
	if (game_state != nullptr)
	{
		game_state->Enter();
		_game_states.push(game_state);
	}
	else
	{
		Log::Warning("GameStateManager::PushState - Trying to push NULL state");
	}
}

void GameStateManager::PopState()
{
	if (_game_states.empty() == false)
	{
		_game_states.top()->Exit();
		_game_states.pop();

		if (_game_states.empty() == false)
		{
			_game_states.top()->Enter();
		}
		else
		{
			Log::Warning("GameStateManager::PopState - Popped last state\n");
		}
	}
	else
	{
		Log::Warning("GameState::PopState - Trying to pop from empty stack");
	}
}

void GameStateManager::ChangeState(IGameState* game_state)
{
	PopState();

	PushState(game_state);
}

void GameStateManager::Input()
{
	if (_game_states.empty() == false)
	{
		_game_states.top()->Input(*this);
	}
}

void GameStateManager::Update(float delta_time)
{
	if (_game_states.empty() == false)
	{
		_game_states.top()->Update(delta_time);
	}
}
