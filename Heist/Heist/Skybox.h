#pragma once

#include "BaseObject.h"

class MeshObject;
class Camera;

class Skybox : public BaseObject
{
public:
	Skybox();
	~Skybox();

	void Initialize(/*MeshObject* mesh_object,*/ const std::shared_ptr<Camera> camera);
	virtual void Update(float delta_time) override;

private:
	//MeshObject* _mesh_object;
	
	std::shared_ptr<Camera> _camera;
};

