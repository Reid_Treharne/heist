#include "stdafx.h"
#include "HUDManager.h"
#include "HUDObject.h"
#include "../Renderer/Renderer.h"

HUDManager::HUDManager(void)
{
}


HUDManager::~HUDManager(void)
{
}

void HUDManager::Initialize(Renderer* renderer)
{
}

void HUDManager::AddObject(std::shared_ptr<HUDObject> hud_object)
{
	if (hud_object != nullptr)
	{
		_hud_objects.push_back(hud_object);

		hud_object = nullptr;
	}
	else
	{
		assert(false && "ObjectManager::AddObject - Trying to add null object");
	}
}

void HUDManager::RemoveObject(std::shared_ptr<HUDObject> hud_object)
{
	if (hud_object != nullptr)
	{
		for (unsigned int i = 0; i < _hud_objects.size(); ++i)
		{
			if (_hud_objects[i] == hud_object)
			{
				_hud_objects.erase(_hud_objects.begin() + i);
			}
		}
	}
	else
	{
		assert(false && "ObjectManager::RemoveObject - Trying to remove null object");
	}
}

void HUDManager::Update(float delta_time)
{
	std::vector<std::shared_ptr<HUDObject>>::iterator object_iter = _hud_objects.begin();

	for (; object_iter != _hud_objects.end(); ++object_iter)
	{
		(*object_iter)->Update(delta_time);
	}
}

void HUDManager::Render()
{
	if (_hud_objects.empty() == false)
	{
		// since hud objects are instanced, we only need to render the first one
		_hud_objects[0]->Render();
	}
}