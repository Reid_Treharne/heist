#pragma once

#include "IFeature.h"
#include <memory>
#include <DirectXMath.h>

using namespace DirectX;

class BaseObject;

class FeatureRenderOutline : public IFeature
{
public:
	FeatureRenderOutline(std::shared_ptr<BaseObject> outline_distance_base_object, float outline_range, const XMFLOAT3& outline_color);

	virtual ~FeatureRenderOutline();

	virtual FEATURE_TYPE GetType() const override;
	virtual unsigned int GetComponentType() const override;

	virtual void OnAdded(IComponent& component);
	virtual void OnRemoved(IComponent& component);

	virtual void Update(BaseObject* base_object, IComponent& component) override;

private:
	std::shared_ptr<BaseObject> _outline_distance_base_object;

	float _outline_range;

	XMFLOAT3 _outline_color;
};

