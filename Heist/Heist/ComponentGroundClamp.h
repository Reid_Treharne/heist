#pragma once
#include "IComponent.h"
#include "ObjectNames.h"
#include "../EventSystem/IListener.h"
#include <functional>
#include <unordered_set>
#include "DirectXMath.h"

using namespace DirectX;

class Terrain;
class TileManager;
class GroundClutter;
class CollisionManager;

class ComponentGroundClamp : public IComponent, public IListener
{
public:
	ComponentGroundClamp(OBJECT_NAME object_name, float offset_y, std::shared_ptr<const Terrain> terrain, const CollisionManager* collision_manager, const TileManager* tile_manager);
	virtual ~ComponentGroundClamp(void);

	// IComponent Interface
	virtual void Update(BaseObject* base_object, float delta_time) override;
	virtual IComponent::COMPONENT_TYPE GetComponentType() const override;

	// IListener Interface
	virtual void OnEvent(const IEvent* the_event) override;

private:
	void GroundClamp(BaseObject* base_object);
	void GroundClampGroundClutter(BaseObject* base_object);

	bool ClampPoint(XMFLOAT3& point);

	std::function<void(BaseObject*)> _ground_clamp_fptr;

	OBJECT_NAME _object_name;

	std::shared_ptr<const Terrain> _terrain;

	const CollisionManager* _collision_manager;

	const TileManager* _tile_manager;

	std::unordered_set<int> _moved_tile_indices;

	float _offset_y;
	bool _first_update;
};

