#pragma once
#include "ObjectManagerState.h"
#include "../Renderer/MeshObject.h"
#include "../CollisionManager/CollisionLibrary.h"
#include "../CollisionManager/ObjectTree.h"
#include <unordered_map>
#include <memory>

class BaseObject;
class ObjectFactory;

class ObjectManagerStateDebug : public ObjectManagerState
{
public:
	ObjectManagerStateDebug(void);
	~ObjectManagerStateDebug(void);

	virtual void Initialize(ObjectManager* object_manager, Renderer* renderer, ObjectFactory* object_factory, ObjectTree<std::shared_ptr<BaseObject>>* object_tree);

	virtual void Enter();
	virtual void Exit();

private:
	
	std::vector<std::shared_ptr<BaseObject>> _aabb_objects;
};

