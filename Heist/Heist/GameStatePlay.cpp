#include "stdafx.h"
#include "GameStateManager.h"
#include "GameStatePlay.h"
#include "GameStateDebug.h"
#include "HUDManager.h"
#include "ObjectManager.h"
#include "CameraManager.h"
#include "ObjectFactory.h"
#include "Terrain.h"
#include "Service.h"
#include "ComponentFactory.h"
#include "../InputManager/InputManager.h"
#include "../TileManager/Tile.h"
#include "../TileManager/TileManager.h"
#include "../TileManager/PathFinder.h"
#include "../Logger/Log.h"
#include "../Renderer/ShapeFactory.h"

GameStatePlay::GameStatePlay(void) : 
	_game_state_debug(nullptr),
	_service(nullptr)
{

}

GameStatePlay::~GameStatePlay(void)
{
	Exit();
}

void GameStatePlay::Initialize(Service* service, GameStateDebug* game_state_debug)
{
	_service = service;

	// Create a directional light
	auto directional_light = _service->GetObjectFactory()->CreateObject("Directional_Light", OBJECT_NAME::NAME_DIRECTIONAL_LIGHT, XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(45.0f, 40.0f, 0.0f));

	_directional_light = std::dynamic_pointer_cast<DirectionalLight>(directional_light);

	// Create the initial player
	auto start_x = _service->GetTileManager()->GetTile(1, 1)->GetWorldX();
	auto start_z = _service->GetTileManager()->GetTile(1, 1)->GetWorldZ();

	auto start_position = XMFLOAT3(start_x, 0.0f, start_z);
	_player = _service->GetObjectFactory()->CreateObject("Player", OBJECT_NAME::NAME_INTERACTABLE, start_position, XMFLOAT3(0.0f, 0.0f, 0.0f));

	start_position.z += 10;
	auto m4a1 = _service->GetObjectFactory()->CreateObject("M4A1", OBJECT_NAME::NAME_EQUIPMENT, start_position, XMFLOAT3(0.0f, 0.0f, 90.0f));

	// Set the object manager and add the objects to the object manager
	service->GetObjectManager()->AddObject(_directional_light, OBJECT_NAME::NAME_DIRECTIONAL_LIGHT);
	service->GetObjectManager()->AddObject(_player, OBJECT_NAME::NAME_INTERACTABLE);
	service->GetObjectManager()->AddObject(m4a1, OBJECT_NAME::NAME_INTERACTABLE);

	// Set the possible gamestate changes
	_game_state_debug = game_state_debug;

	// Initialize the path finder
	PathFinder::GetInstance().Initialize(_service->GetTileManager());
}

void GameStatePlay::Enter()
{
	// Attach the camera to the player
	_service->GetCameraManager()->SetAsThirdPerson(_player);
}

void GameStatePlay::Exit()
{
}

void GameStatePlay::Input(GameStateManager& game_state_manager)
{
	InputManager& input_manager = *_service->GetInputManager();
	Camera& camera = *_service->GetCamera();

	// Check for the debug key (enter)
	if (input_manager.GetKeyDown(VK_RETURN))
	{
		if (input_manager.GetKeyPressed('D'))
		{
			// Push the debug state onto the stack
			game_state_manager.PushState(_game_state_debug);
		}
	}

	if (input_manager.GetKeyPressed(0x31))
	{
		_service->GetCameraManager()->SetAsFirstPerson(_player);
	}
	else if (input_manager.GetKeyPressed(0x32))
	{
		_service->GetCameraManager()->SetAsThirdPerson(_player);
	}
	else if (input_manager.GetKeyPressed(0x33))
	{
		_service->GetCameraManager()->SetAsTopDown(_service->GetTerrain());
	}
}

void GameStatePlay::Update(float delta_time)
{
	_service->GetObjectManager()->Update(delta_time);

	_service->GetHUDManager()->Update(delta_time);
}
