#include "stdafx.h"
#include "ObjectFactory.h"
#include "ObjectNames.h"
#include "BaseObject.h"
#include "DirectionalLight.h"
#include "Terrain.h"
#include "ComponentFactory.h"
#include "FeatureFactory.h"
#include "Skybox.h"
#include "Service.h"
#include "../Renderer/ModelManager.h"
#include "../Renderer/LightManager.h"
#include "../Renderer/MeshObject.h"
#include "../Logger/Log.h"
#include <sstream>

using namespace std::placeholders;

ObjectFactory::ObjectFactory(void) :
	_object_manager(nullptr),
	_model_manager(nullptr),
	_light_manager(nullptr),
	_service(nullptr)
{
}


ObjectFactory::~ObjectFactory(void)
{
}

void ObjectFactory::Initialize(ObjectManager* object_manager, ModelManager* model_manager, LightManager* light_manager, const Service* service)
{
	_object_manager = object_manager;
	_model_manager = model_manager;
	_light_manager = light_manager;
	_service = service;

	_creator[OBJECT_NAME::NAME_CAMERA] = std::bind(&ObjectFactory::CreateNull, this, _1);
	_creator[OBJECT_NAME::NAME_INTERACTABLE] = std::bind(&ObjectFactory::CreateInteractable, this, _1);
	_creator[OBJECT_NAME::NAME_EQUIPMENT] = std::bind(&ObjectFactory::CreateEquipment, this, _1);
	_creator[OBJECT_NAME::NAME_AESTHETIC] = std::bind(&ObjectFactory::CreateAesthetic, this, _1);
	_creator[OBJECT_NAME::NAME_GROUND_CLUTTER] = std::bind(&ObjectFactory::CreateNull, this, _1);
	_creator[OBJECT_NAME::NAME_SKYBOX] = std::bind(&ObjectFactory::CreateSkybox, this, _1);
	_creator[OBJECT_NAME::NAME_TERRAIN] = std::bind(&ObjectFactory::CreateNull,this,  _1);
	_creator[OBJECT_NAME::NAME_DIRECTIONAL_LIGHT] = std::bind(&ObjectFactory::CreateDirectionalLight, this, _1);
}

std::shared_ptr<BaseObject> ObjectFactory::CreateObject(const std::string& mesh_name, OBJECT_NAME object_name, const XMFLOAT3& position, const XMFLOAT3& rotation) const
{
	std::shared_ptr<BaseObject> created_object = nullptr;

	created_object = _creator[object_name](mesh_name);

	if (created_object)
	{
		created_object->SetName(mesh_name);
		created_object->SetObjectName(object_name);
		created_object->SetPosition(position);
		created_object->SetOrientation(rotation);
	}

	return created_object;
}

std::shared_ptr<BaseObject> ObjectFactory::CreateAABB(const XMFLOAT3& min, const XMFLOAT3& max, const XMFLOAT4& color) const
{
	auto name = GenerateAABBMeshObject(min, max, color);

	auto created_object = _creator[OBJECT_NAME::NAME_AESTHETIC](name);

	return created_object;
}

// TODO :: Have this take in vertex structure so it can be used w\ normals for example
std::shared_ptr<BaseObject> ObjectFactory::CreateAABB(const XMFLOAT3& max, const XMFLOAT4& color) const
{
	auto name = GenerateAABBMeshObject(max, color);

	auto created_object = _creator[OBJECT_NAME::NAME_AESTHETIC](name);

	return created_object;
}

std::shared_ptr<BaseObject> ObjectFactory::CreateNull(const std::string& mesh_name)
{
	// This function should not be called on purpose and is used to bind to
	// when no creation is supported for an object
	Log::Warning("ObjecetFactory::CreateNull - No create function bound for this type\n");

	return nullptr;
}

std::shared_ptr<BaseObject> ObjectFactory::CreateInteractable(const std::string& mesh_name)
{
	// Initialize the object using the mesh object
	auto interactable = std::make_shared<BaseObject>();

	// Get the mesh objects associated with this name
	auto mesh_objects = _model_manager->GetModel(mesh_name);

	if (mesh_objects)
	{
		// This needs to be set here so it's available when creating the collision component
		interactable->SetName(mesh_name);

		// Add the collision component
		CollisionComponentParams collision_params(interactable);
		interactable->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_COLLISION, collision_params));
		interactable->AddComponentFeature(FeatureFactory::CreateFeature(IFeature::FEATURE_COLLISION_CLICKABLE));
		interactable->AddComponentFeature(FeatureFactory::CreateFeature(IFeature::FEATURE_COLLISION_COLLIDABLE));

		// Add the ground clamp component
		GroundClampComponentParams ground_clamp_params(OBJECT_NAME::NAME_INTERACTABLE);
		interactable->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_GROUND_CLAMP, ground_clamp_params));

		// Add the render component
		RenderComponentParams render_params(*mesh_objects);
		interactable->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_RENDER, render_params));
	}

	// Return the object incase the user needs it
	return interactable;
}

std::shared_ptr<BaseObject> ObjectFactory::CreateEquipment(const std::string& mesh_name)
{
	// Initialize the object using the mesh object
	auto equipment = std::make_shared<BaseObject>();

	// Get the mesh objects associated with this name
	auto mesh_objects = _model_manager->GetModel(mesh_name);

	if (mesh_objects)
	{
		// This needs to be set here so it's available when creating the collision component
		equipment->SetName(mesh_name);

		// Add the collision component
		CollisionComponentParams collision_params(equipment);
		equipment->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_COLLISION, collision_params));
		equipment->AddComponentFeature(FeatureFactory::CreateFeature(IFeature::FEATURE_COLLISION_TRIGGERABLE));
		equipment->AddComponentFeature(FeatureFactory::CreateFeature(IFeature::FEATURE_COLLISION_CLICKABLE));

		// Add the ground clamp component
		GroundClampComponentParams ground_clamp_params(OBJECT_NAME::NAME_INTERACTABLE, 0.2f);
		equipment->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_GROUND_CLAMP, ground_clamp_params));

		// Add the render component
		RenderComponentParams render_params(*mesh_objects);
		equipment->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_RENDER, render_params));
		FeatureRenderOutlineParams outline_params(_service->GetCamera(), 10.0f, XMFLOAT3(1.0f, 1.0f, 1.0f));
		equipment->AddComponentFeature(FeatureFactory::CreateFeature(IFeature::FEATURE_RENDER_OUTLINE_WHEN_NEAR_OBJECT, outline_params));
	}

	// Return the object incase the user needs it
	return equipment;
}


std::shared_ptr<BaseObject> ObjectFactory::CreateAesthetic(const std::string& mesh_name)
{
	// Initialize the object using the mesh object
	auto base_object = std::make_shared<BaseObject>();

	// Get the mesh objects associated with this name
	auto mesh_objects = _model_manager->GetModel(mesh_name);

	if (mesh_objects)
	{
		// Add the render component
		RenderComponentParams parameters(*mesh_objects);
		base_object->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_RENDER, parameters));
	}

	// Return the object incase the user needs it
	return base_object;
}

std::shared_ptr<BaseObject> ObjectFactory::CreateSkybox(const std::string& mesh_name)
{
	// Initialize the object using the mesh object
	auto skybox = std::make_shared<Skybox>();

	// Attach the skybox to the camera
	_service->GetCamera()->AttachObject(skybox, BaseObject::NORMAL, BaseObject::TETHER, XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f));

	// Get the mesh objects associated with this name
	auto mesh_objects = _model_manager->GetModel(mesh_name);

	if (mesh_objects)
	{
		// Add the render component
		RenderComponentParams parameters(*mesh_objects);
		skybox->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_RENDER, parameters));
	}

	// Return the object incase the user needs it
	return skybox;
}

std::shared_ptr<BaseObject> ObjectFactory::CreateDirectionalLight(const std::string& mesh_name)
{
	auto base_object = std::make_shared<DirectionalLight>();

	// Get the light associated with this name
	auto mesh_object = _light_manager->GetModel(mesh_name);

	if (mesh_object)
	{
		// Add the render component
		RenderComponentParams parameters(mesh_object);
		base_object->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_RENDER, parameters));
	}

	return base_object;
}

std::string ObjectFactory::GenerateAABBMeshObject(const XMFLOAT3& max, const XMFLOAT4 color) const
{
	auto min = XMFLOAT3(0.0f, 0.0f, 0.0f);

	return GenerateAABBMeshObject(min, max, color);
}

std::string ObjectFactory::GenerateAABBMeshObject(const XMFLOAT3& min, const XMFLOAT3& max, const XMFLOAT4 color) const
{
	std::stringstream model_name;

	// TODO :: Add color to instance buffer so a new instance doesnt need created just because
	// the aabb is a different color
	model_name << "AABB_" << max.x << "_" << max.y << "_" << max.z << "_" << color.x << "_" << color.y << "_" << color.z;

	auto mesh_objects = _model_manager->GetModel(model_name.str());

	if (mesh_objects == nullptr)
	{
		std::vector<VERTEX_POS_COL> vertices;
		std::vector<unsigned int> indices;

		vertices.clear();
		vertices.reserve(8);

		indices.clear();
		indices.reserve(36);

		VERTEX_POS_COL current_vertex;

		current_vertex.color = color;

		// left top front
		current_vertex.position = XMFLOAT3(min.x, max.y, min.z);
		vertices.push_back(current_vertex);

		// right top front  
		current_vertex.position = XMFLOAT3(max.x, max.y, min.z);
		vertices.push_back(current_vertex);

		// left bottom front
		current_vertex.position = min;
		vertices.push_back(current_vertex);

		// right bottom front
		current_vertex.position = XMFLOAT3(max.x, min.y, min.z);
		vertices.push_back(current_vertex);

		// left top back
		current_vertex.position = XMFLOAT3(min.x, max.y, max.z);
		vertices.push_back(current_vertex);

		// right top back  
		current_vertex.position = max;
		vertices.push_back(current_vertex);

		// left bottom back
		current_vertex.position = XMFLOAT3(min.x, min.y, max.z);
		vertices.push_back(current_vertex);

		// right bottom back
		current_vertex.position = XMFLOAT3(max.x, min.y, max.z);
		vertices.push_back(current_vertex);

		// front
		indices.push_back(0);
		indices.push_back(1);
		indices.push_back(2);

		indices.push_back(2);
		indices.push_back(1);
		indices.push_back(3);

		// back
		indices.push_back(5);
		indices.push_back(4);
		indices.push_back(7);

		indices.push_back(7);
		indices.push_back(4);
		indices.push_back(6);

		// right
		indices.push_back(5);
		indices.push_back(7);
		indices.push_back(1);

		indices.push_back(1);
		indices.push_back(7);
		indices.push_back(3);

		// left
		indices.push_back(4);
		indices.push_back(0);
		indices.push_back(6);

		indices.push_back(6);
		indices.push_back(0);
		indices.push_back(2);

		// top
		indices.push_back(5);
		indices.push_back(1);
		indices.push_back(4);

		indices.push_back(4);
		indices.push_back(1);
		indices.push_back(0);

		// bottom
		indices.push_back(2);
		indices.push_back(3);
		indices.push_back(6);

		indices.push_back(6);
		indices.push_back(3);
		indices.push_back(7);

		_model_manager->AddIndexedInstanced<VERTEX_POS_COL, MeshObject>(
			model_name.str(),
			RENDER_TYPE::TYPE_WIRE_FRAME,
			vertices,
			indices);
	}

	return model_name.str();
}

