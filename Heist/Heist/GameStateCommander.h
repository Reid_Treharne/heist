#pragma once
#include "IGameState.h"

class Service;
class GameStateDebug;

class GameStateCommander : public IGameState
{
public:
	GameStateCommander(void);
	virtual ~GameStateCommander(void) = 0;

	void Initialize(Service* service, GameStateDebug* game_state_debug);

	virtual void Enter() override;

	virtual void Input(GameStateManager& game_state_manager) {};
	virtual void Update(float delta_time) {};

private:
	Service* _service;
};

