#pragma once
#include "StateInterface.h"
#include <memory>
#include <vector>

class Tile;

class StateWalking : public StateInterface
{
public:
	StateWalking(BaseObject* base_object, const Tile* destination_tile);
	~StateWalking(void);

	virtual void Enter() override;

	virtual bool Update(std::shared_ptr<BaseObject> base_object, float delta_time) override;

private:
	std::vector<const Tile*> _destination_path;

	float _move_speed;
};

