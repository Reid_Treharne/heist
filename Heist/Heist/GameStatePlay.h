#pragma once
#include "IGameState.h"
#include "DirectionalLight.h"
#include "../Renderer/MeshObject.h"
#include "../Renderer/VertexDeclarations.h"

class GameStateDebug;
class Service;

class GameStatePlay : public IGameState
{
public:
	GameStatePlay(void);
	~GameStatePlay(void);

	void Initialize(Service* service, GameStateDebug* game_state_debug);

	void Enter() override;
	void Exit() override;

	void Input(GameStateManager& game_state_manager) override;
	void Update(float delta_time) override;

private:
	// Service
	Service* _service;

	// Pointers to game states accessible from this state
	GameStateDebug* _game_state_debug;

	// The player
	std::shared_ptr<BaseObject> _player;

	// Directional Light
	std::shared_ptr<DirectionalLight> _directional_light;
};

