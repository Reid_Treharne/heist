#pragma once

#include <DirectXMath.h>
using namespace DirectX;

#include <map>
#include <memory>
#include <vector>
#include <unordered_map>
#include "ObjectNames.h"
#include "DirtyFlagsManager.h"

class IComponent;
class IFeature;
class StateInterface;

class BaseObject : public std::enable_shared_from_this<BaseObject>
{
public:
	BaseObject(void);
	virtual ~BaseObject(void);

	enum ATTACHMENT_TYPE { ATTACH, TETHER, ORBIT, NUM_ATTACHMENT_TYPES };
	enum CHILD_TYPE { NORMAL, EQUIPMENT };

	virtual void Update(float delta_time);

	virtual void SetPosition(const XMFLOAT3& position);
	virtual void SetOrientation(const XMFLOAT3& rotation);
	virtual void Translate(const XMFLOAT3& translation);
	virtual void TranslateWorld(const XMFLOAT3& translation);
	// Rotates the object. NOTE: This function automatically factors in delta time.
	virtual void Rotate(const XMFLOAT3& rotation);
	virtual bool OnClicked(std::shared_ptr<BaseObject> clicker);
	virtual	const char* GetInstanceData() const;

	// Attaching/Detaching objects
	void AttachObject(std::shared_ptr<BaseObject> child, CHILD_TYPE child_type, ATTACHMENT_TYPE attachment_type, const XMFLOAT3& offset, const XMFLOAT3& rotation);
	void DetachObject(CHILD_TYPE child_type, std::shared_ptr<BaseObject> child);
	void DetachObject(BaseObject* child);

	void LookAt(const XMFLOAT3& look_at_position);
	void LookInDirection(const XMFLOAT3& direction_to_look);
	void LookAt2D(const XMFLOAT3& look_at_position);

	void ClearStatesToIdle();
	void AddState(StateInterface* state);

	void BaseObject::AddComponent(std::unique_ptr<IComponent> component);
	std::unique_ptr<IComponent> RemoveComponent(int component_type);

	void AddComponentFeature(std::unique_ptr<IFeature> feature);
	void RemoveComponentFeature(int component_type, int feature_type);

	// Accessors
	void GetWorldMatrix(XMFLOAT4X4& world_matrix) const;
	void GetLocalMatrix(XMFLOAT4X4& local_matrix) const;
	void GetWorldPosition(XMFLOAT3& world_position) const;
	void GetLocalPosition(XMFLOAT3& local_position) const;
	XMFLOAT3 GetWorldPosition() const;
	const XMFLOAT3& GetWorldOldPosition() const;
	void GetWorldForward(XMFLOAT3& forward_vector) const;
	void GetWorldUp(XMFLOAT3& up_vector) const;
	void GetWorldSide(XMFLOAT3& side_vector) const;
	inline const std::string& GetName() const { return _name; }

	// Mutators
	void SetLocalMatrix(XMFLOAT4X4& local_matrix);
	void SetLocalPosition(XMFLOAT3& local_position);
	inline void SetName(const std::string& name) { _name = name; }
	inline void SetObjectName(OBJECT_NAME object_name) { _object_name = object_name; }
	inline void SetParent(const std::pair<std::shared_ptr<BaseObject>, ATTACHMENT_TYPE>& parent) { _parent = parent; }

	bool GetDirty(DirtyFlagsManager::DIRTY_FLAGS dirty_flag) const;

protected:
	XMFLOAT4X4 _matrix;

	unsigned int _dirty_flags_handle;

	std::vector<std::unique_ptr<StateInterface>> _states;

	std::string _name;

	OBJECT_NAME _object_name;
private:
	XMFLOAT3 _old_position;

	std::map<int, std::unique_ptr<IComponent>> _components;
	std::pair<std::shared_ptr<BaseObject>, ATTACHMENT_TYPE> _parent;

	// Storing the children as shared pointers causes a gpu memory leak due to cyclic dependencies
	// http://boost.2283326.n4.nabble.com/memory-leak-when-using-shared-from-this-td3214313.html
	std::unordered_map<CHILD_TYPE, std::vector<BaseObject*>> _children;
};

