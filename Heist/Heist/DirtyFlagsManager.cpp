#include "stdafx.h"
#include "DirtyFlagsManager.h"
#include "../Logger/Log.h"

std::vector<DirtyFlagsManager::Flags> DirtyFlagsManager::_dirty_flags;
std::unordered_set<unsigned int> DirtyFlagsManager::_free_indices;

int DirtyFlagsManager::Subscribe()
{
	auto handle = 0;
	
	if (_free_indices.empty())
	{
		_dirty_flags.emplace_back();
		handle = _dirty_flags.size() - 1;
	}
	else
	{
		auto iter = _free_indices.begin();
		handle = *iter;
		_free_indices.erase(iter);
	}
	
	return handle;
}

void DirtyFlagsManager::Unsubscribe(unsigned int handle)
{
	if (handle < _dirty_flags.size())
	{
		ZeroMemory(&_dirty_flags[handle], sizeof(_dirty_flags[handle]));
		_free_indices.insert(handle);
	}
}

bool DirtyFlagsManager::IsDirty(unsigned int handle, DIRTY_FLAGS flag)
{
	auto dirty = false;
	
	if (handle < _dirty_flags.size() && flag >= 0 && flag < NUM_DIRTY_FLAGS)
	{
		dirty = _dirty_flags[handle].flags[flag];
	}
	else
	{
		Log::Warning("DirtyFlagsManager::IsDirty - Invalid handle or flag\n");
	}
	
	return dirty;
}

void DirtyFlagsManager::SetDirty(unsigned int handle, DIRTY_FLAGS flag, bool dirty)
{
	if (handle < _dirty_flags.size() && flag >= 0 && flag < NUM_DIRTY_FLAGS)
	{
		_dirty_flags[handle].flags[flag] = dirty;
	}
	else
	{
		Log::Warning("DirtyFlagsManager::SetDirty - Invalid handle or flag\n");
	}
}

void DirtyFlagsManager::ResetAll()
{
	if (_dirty_flags.empty() == false)
	{
		ZeroMemory(&_dirty_flags[0], sizeof(_dirty_flags[0]) * _dirty_flags.size());
	}
}

DirtyFlagsManager::DirtyFlagsManager()
{
}


DirtyFlagsManager::~DirtyFlagsManager()
{
}
