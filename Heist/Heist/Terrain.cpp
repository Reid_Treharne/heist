#include "stdafx.h"
#include "Terrain.h"
#include "ComponentFactory.h"
#include "DirtyFlagsManager.h"
#include "../TileManager/TileManager.h"
#include "../Logger/Log.h"
#include "../Renderer/MeshObject.h"
#include "../Renderer/VertexDeclarations.h"
#include "../EventSystem/EventSystem.h"
#include "../CollisionManager/CollisionManager.h"
#include "EventTerrainHeightChanged.h"
#include <unordered_set>

Terrain::Terrain(void) :
	_tile_manager(nullptr),
	_mesh_object(nullptr)
{
}


Terrain::~Terrain(void)
{
}

void Terrain::Initialize(MeshObject* mesh_object, TileManager* tile_manager, CollisionManager* collision_manager, std::vector<TileMeshData>& vertices, std::vector<unsigned int>& indices)
{
	_name = "Terrain";

	_tile_manager = tile_manager;
	_collision_manager = collision_manager;

	_vertices = std::move(vertices);
	_indices = std::move(indices);

	_mesh_object = mesh_object;

	InitializeTriangles();

	// Add the render component
	RenderComponentParams parameters(mesh_object);
	AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_RENDER, parameters));
}

void Terrain::Update(float delta_time)
{
	if (_vertex_offsets.empty() == false)
	{
		UpdateTriangles(delta_time);

		_vertex_offsets.clear();
	}

	BaseObject::Update(delta_time);
}

void Terrain::SetPosition(const XMFLOAT3& position)
{
	// Update the tile manager's position with the new terrain position
	_tile_manager->SetTerrainPosition(position.x, position.y, position.z);

	BaseObject::SetPosition(position);
}

bool Terrain::CollidesWithLine(const XMFLOAT3& start_point, const XMFLOAT3& end_point, XMFLOAT3& collision_point) const
{
	XMFLOAT3 terrain_space_start, terrain_space_end;

	XMFLOAT4X4 world_matrix;

	GetWorldMatrix(world_matrix);

	// Convert the start and end points from world space to terrain space
	XMStoreFloat3(&terrain_space_start, XMVector3Transform(XMLoadFloat3(&start_point), XMMatrixInverse(nullptr, XMLoadFloat4x4(&world_matrix))));
	XMStoreFloat3(&terrain_space_end, XMVector3Transform(XMLoadFloat3(&end_point), XMMatrixInverse(nullptr, XMLoadFloat4x4(&world_matrix))));

	// Check collision with the ray to the terrain's triangles.
	unsigned int triangle_index;

	auto collided = CollisionLibrary::LineSegmentToTriangle(collision_point,
		triangle_index,
		&_triangles[0],
		_triangles.size(),
		terrain_space_start,
		terrain_space_end);

	if (collided == true)
	{
		_clicked_tile = _tile_manager->GetTile(triangle_index / 2);
		_clicked_triangle_index = triangle_index;
	}

	return collided;
}

void Terrain::RaiseVertex(unsigned int vertex_index, float raise_amount)
{
	if (vertex_index < _vertices.size())
	{
		_vertex_offsets[vertex_index] += raise_amount;
	}
}

const TileMeshData* Terrain::GetVertex(unsigned int index) const
{
	const TileMeshData* vertex = nullptr;

	if (index < _vertices.size())
	{
		vertex = &_vertices[index];
	}

	return vertex;
}

void Terrain::InitializeTriangles()
{
	_triangles.clear();

	// Calculate the triangles of the terrain
	Triangle current_triangle;

	for (unsigned int i = 0; i < _indices.size(); i += 3)
	{
		current_triangle.vertices[0].x = _vertices[i].position_x;
		current_triangle.vertices[0].y = _vertices[i].position_y;
		current_triangle.vertices[0].z = _vertices[i].position_z;

		current_triangle.vertices[1].x = _vertices[i+1].position_x;
		current_triangle.vertices[1].y = _vertices[i+1].position_y;
		current_triangle.vertices[1].z = _vertices[i+1].position_z;

		current_triangle.vertices[2].x = _vertices[i+2].position_x;
		current_triangle.vertices[2].y = _vertices[i+2].position_y;
		current_triangle.vertices[2].z = _vertices[i+2].position_z;

		// Calculate the normals for the triangle based on its vertices
		NormalizeTriangle(current_triangle);

		_vertices[i].normal_x = current_triangle.normal.x;
		_vertices[i].normal_y = current_triangle.normal.y;
		_vertices[i].normal_z = current_triangle.normal.z;

		_vertices[i+1].normal_x = current_triangle.normal.x;
		_vertices[i+1].normal_y = current_triangle.normal.y;
		_vertices[i+1].normal_z = current_triangle.normal.z;

		_vertices[i+2].normal_x = current_triangle.normal.x;
		_vertices[i+2].normal_y = current_triangle.normal.y;
		_vertices[i+2].normal_z = current_triangle.normal.z;

		_triangles.push_back(current_triangle);
		_collision_manager->AddTriangle(_name, current_triangle.vertices);
	}

	D3D11_BUFFER_DESC desc;
	memset(&desc, 0, sizeof(desc));

	// Create vertex buffer
	desc.ByteWidth = sizeof(VERTEX_POS_NORM_UV) * _vertices.size();
	desc.CPUAccessFlags = NULL;
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.Usage = D3D11_USAGE_IMMUTABLE;
	desc.MiscFlags = NULL;
	desc.StructureByteStride = sizeof(VERTEX_POS_NORM_UV);

	_mesh_object->SetVertexBuffer(desc, &_vertices[0]);

	// Set index buffer
	desc.ByteWidth = sizeof(unsigned int) * _indices.size();
	desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	desc.StructureByteStride = sizeof(unsigned int);

	_mesh_object->SetIndexBuffer(desc, &_indices[0]);

	// Set the instance buffer
	desc.ByteWidth = sizeof(C_BUFFER_INSTANCE_MESH_OBJECT) * MAX_INSTANCES;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.StructureByteStride = sizeof(C_BUFFER_INSTANCE_MESH_OBJECT);

	_mesh_object->AddInstanceBuffer(desc, 1, -1, -1, MeshObject::INSTANCE_BUFFER_MAIN);
}

void Terrain::UpdateTriangles(float delta_time)
{
	// A unique list of all the triangles that are being raised or lowered
	std::unordered_set<int> moved_triangle_indices;

	// Loop through all the offsets
	for (const auto& vertex_offset : _vertex_offsets)
	{
		// grab the index of the vertex to raise or lower and the offset amount
		auto index = vertex_offset.first;
		auto y_offset = vertex_offset.second;

		// scale the offset amount by delta time
		y_offset *= delta_time;

		_vertices[index].position_y += y_offset;

		// get the triangle that contains this vertex
		auto& triangle = _triangles[(index / 3)];

		// add the triangle to the moved triangles list
		moved_triangle_indices.insert(index / 3);

		// update the vertex of the triangle
		triangle.vertices[index % 3].y += y_offset;
	}

	// Now that all of the vertices are finished being raised or lowered, recalculate the normals
	// for all of the triangles that have been moved.
	for (auto index : moved_triangle_indices)
	{
		auto& triangle = _triangles[index];

		_collision_manager->EditTriangle(_name, triangle.vertices, index);

		// Recalculate the normals for the triangle based on its new vertices
		NormalizeTriangle(triangle);

		// Update the normals for the 3 vertices assoicated with this triangle in the mesh object
		auto vert_index = index * 3;

		for (int i = 0; i < 3; ++i)
		{
			_vertices[vert_index + i].normal_x = triangle.normal.x;
			_vertices[vert_index + i].normal_y = triangle.normal.y;
			_vertices[vert_index + i].normal_z = triangle.normal.z;
		}
	}

	D3D11_BUFFER_DESC desc;
	memset(&desc, 0, sizeof(desc));

	// Create vertex buffer
	desc.ByteWidth = sizeof(VERTEX_POS_NORM_UV) * _vertices.size();
	desc.CPUAccessFlags = NULL;
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.Usage = D3D11_USAGE_IMMUTABLE;
	desc.MiscFlags = NULL;
	desc.StructureByteStride = sizeof(VERTEX_POS_NORM_UV);

	_mesh_object->SetVertexBuffer(desc, &_vertices[0]);

	// Dirty the geometry
	DirtyFlagsManager::SetDirty(_dirty_flags_handle, DirtyFlagsManager::DIRTY_GEOMETRY, true);

	// Send the terrain height changed event
	EventTerrainHeightChanged the_event(std::move(moved_triangle_indices));

	EventSystem::Get().SendEvent(&the_event);
}
