#pragma once
#include "IFeature.h"
class FeatureCollisionCollidable : public IFeature
{
public:
	FeatureCollisionCollidable();
	~FeatureCollisionCollidable();

	virtual FEATURE_TYPE GetType() const override;
	virtual unsigned int GetComponentType() const override;

	virtual void OnAdded(IComponent& component) override;
	virtual void OnRemoved(IComponent& component) override;
};

