#include "stdafx.h"
#include "GameStateCommander.h"
#include "Service.h"
#include "CameraManager.h"
#include "Terrain.h"

GameStateCommander::GameStateCommander(void)
{
}


GameStateCommander::~GameStateCommander(void)
{
}

void GameStateCommander::Initialize(Service* service, GameStateDebug* game_state_debug)
{
	_service = service;
}

void GameStateCommander::Enter()
{
	// Set the camera as top down
	_service->GetCameraManager()->SetAsTopDown(_service->GetTerrain());
}