#pragma once

#include "GameStatePlay.h"
#include "GameStateDebug.h"
#include "GameStateManager.h"
#include "HUDManager.h"
#include "HUDObject.h"
#include "ObjectManager.h"
#include "ObjectFactory.h"
#include "Service.h"
#include "CameraManager.h"
#include "Skybox.h"
#include "../CollisionManager/CollisionManager.h"
#include "../Renderer/HUDMeshObject.h"
#include "../Renderer/Renderer.h"
#include "../Renderer/VertexDeclarations.h"
#include "../Renderer/ModelManager.h"
#include "../Renderer/LightManager.h"
#include "../Renderer/PostProcessManager.h"
#include "../InputManager/InputManager.h"
#include "../TileManager/TileManager.h"
#include "../Renderer/MeshObjectTexturedSlope.h"
#include "../Renderer/MeshObjectTextured.h"

class DirectionalLight;
class Terrain;
class Camera;
class GroundClutter;

class HeistMain
{
public:
	HeistMain();
	~HeistMain(void);

	void Initialize(unsigned width, unsigned height, InputManager* input_manager, HWND h_wnd, bool windowed);
	void Input();
	void Update(float delta_time);
	void Render();
private:

	Renderer _renderer;

	unsigned int _window_width;
	unsigned int _window_height;

	// the service
	Service _service;

	// the camera
	std::shared_ptr<Camera> _camera;

	// the camera manager
	CameraManager _camera_manager;

	// the object tree (for collision)
	ObjectTree<std::shared_ptr<BaseObject>> _object_tree;

	// The object and HUD manager
	ObjectManager _object_manager;
	HUDManager _HUD_manager;

	// Game State Manager
	GameStateManager _game_state_manager;

	// Game States
	GameStatePlay _game_state_play;
	GameStateDebug _game_state_debug;

	// Model Manager
	ModelManager _model_manager;

	// Light Manager
	LightManager _light_manager;

	// Post Process Manager
	PostProcessManager _post_process_manager;

	// Object Factor
	ObjectFactory _object_factory;

	// The input manager
	InputManager* _input_manager;

	// The tile manager
	TileManager _tile_manager;

	// Collision Manager
	CollisionManager _collision_manager;

	// Terrain
	MeshObjectTexturedSlope _terrain_mesh;

	std::shared_ptr<Terrain> _terrain;

	// Ground clutter (grass, weeds, etc...)
	MeshObjectTextured _ground_clutter_mesh;

	std::shared_ptr<GroundClutter> _ground_clutter;

	std::shared_ptr<Skybox> _skybox;
};

