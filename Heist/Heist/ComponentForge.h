#pragma once
#include "IComponent.h"

class InputManager;
class Camera;
class Terrain;
class TileManager;

class ComponentForge : public IComponent
{
public:
	ComponentForge(const InputManager* input_manager,
		std::shared_ptr<const Camera> camera,
		std::shared_ptr<Terrain> terrain,
		const TileManager* tile_manager);

	virtual ~ComponentForge(void);

	virtual void Update(BaseObject* base_object, float delta_time) override;
	virtual IComponent::COMPONENT_TYPE GetComponentType() const override;

private:

	void RaiseTile(unsigned int triangle_index, float raise_amount);

	const InputManager* _input_manager;
	std::shared_ptr<const Camera> _camera;
	std::shared_ptr<Terrain> _terrain;
	const TileManager* _tile_manager;
};

