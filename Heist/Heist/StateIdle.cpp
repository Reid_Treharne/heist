#include "stdafx.h"
#include "StateIdle.h"


StateIdle::StateIdle(void)
{
}


StateIdle::~StateIdle(void)
{
}

void StateIdle::Enter()
{
}

bool StateIdle::Update(std::shared_ptr<BaseObject> base_object, float delta_time)
{
	// return false so this state is never popped
	return false;
}
