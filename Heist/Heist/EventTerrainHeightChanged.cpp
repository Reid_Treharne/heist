#include "stdafx.h"
#include "EventTerrainHeightChanged.h"


EventTerrainHeightChanged::EventTerrainHeightChanged(std::unordered_set<int>&& moved_triangle_indices) :
	_moved_triangle_indices(std::move(moved_triangle_indices))
{
}


EventTerrainHeightChanged::~EventTerrainHeightChanged(void)
{
}
