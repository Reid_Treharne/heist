#pragma once

#include <vector>
#include <unordered_set>

class DirtyFlagsManager
{
public:
	enum DIRTY_FLAGS { DIRTY_POSITION, DIRTY_ORIENTATION, DIRTY_GEOMETRY, NUM_DIRTY_FLAGS };

	struct Flags
	{
		Flags()
		{
			ZeroMemory(flags, sizeof(flags));
		}

		bool flags[3];
	};

	static int Subscribe();
	static void Unsubscribe(unsigned int handle);

	static bool IsDirty(unsigned int handle, DIRTY_FLAGS flag);
	static void SetDirty(unsigned int handle, DIRTY_FLAGS flag, bool dirty);
	static void ResetAll();

private:
	DirtyFlagsManager();
	~DirtyFlagsManager();

	static std::vector<Flags> _dirty_flags;
	static std::unordered_set<unsigned int> _free_indices;
};

