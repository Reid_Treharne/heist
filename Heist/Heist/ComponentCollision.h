#pragma once
#include "IComponent.h"
#include "../CollisionManager/ObjectTree.h"
#include "../CollisionManager/CollisionFlags.h"
#include "FeatureCollisionTrigger.h"
#include "FeatureCollisionCollidable.h"
#include "FeatureCollisionClickable.h"
#include "FeatureCollisionTriggerable.h"

class CollisionManager;

class ComponentCollision : public IComponent
{
	friend FeatureCollisionCollidable;
	friend FeatureCollisionClickable;
	friend FeatureCollisionTriggerable;
	friend FeatureCollisionTrigger;
public:
	ComponentCollision(ObjectTree<std::shared_ptr<BaseObject>>* object_tree, CollisionManager* collision_manager, std::shared_ptr<BaseObject> base_object);
	~ComponentCollision(void);

	virtual void Update(BaseObject* base_object, float delta_time) override;
	virtual IComponent::COMPONENT_TYPE GetComponentType() const override;

	void OnCollide(std::shared_ptr<BaseObject> collider1, std::shared_ptr<BaseObject> collider2, bool collided_xyz[3]);

private:
	void UpdateCollider();

	bool _object_added_to_tree;
	unsigned int _current_attempts_to_add_to_tree;
	unsigned int _max_attempts_to_add_to_tree;

	ObjectTree<std::shared_ptr<BaseObject>>* _object_tree;

	CollisionManager* _collision_manager;

	Collider<std::shared_ptr<BaseObject>> _collider;
};

