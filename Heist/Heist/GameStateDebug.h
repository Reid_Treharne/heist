#pragma once
#include "IGameState.h"
#include "HUDObject.h"
#include "../Renderer/HUDMeshObject.h"

class Service;

class GameStateDebug : public IGameState
{
public:
	GameStateDebug(void);
	~GameStateDebug(void);

	void Initialize(Service* service);

	void Enter() override;
	void Exit() override;

	void Input(GameStateManager& game_state_manager) override;
	void Update(float delta_time) override;

private:
	Service* _service;

	// TEMP
	std::shared_ptr<HUDObject> _test_HUD_object;
	HUDMeshObject _test_HUD_mesh_object;
	// TEMP
};

