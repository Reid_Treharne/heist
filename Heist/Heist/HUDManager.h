#pragma once

#include <vector>
#include <functional>
#include <memory>

class HUDObject;
class Renderer;

class HUDManager
{
public:
	HUDManager(void);
	~HUDManager(void);

	void Initialize(Renderer* renderer);
	void AddObject(std::shared_ptr<HUDObject> hud_object);
	void RemoveObject(std::shared_ptr<HUDObject> hud_object);

	void Update(float delta_time);
	void Render();

private:
	std::vector<std::shared_ptr<HUDObject>> _hud_objects;
};

