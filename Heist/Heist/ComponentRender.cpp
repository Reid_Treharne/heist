#include "stdafx.h"
#include "ComponentRender.h"
#include "BaseObject.h"
#include "../Renderer/Renderer.h"
#include "../Renderer/MeshObject.h"

ComponentRender::ComponentRender(const std::vector<MeshObject*>& mesh_objects) :
	_mesh_objects(mesh_objects)
{
	ZeroMemory(&_render_flags, sizeof(_render_flags));
}

ComponentRender::~ComponentRender(void)
{
}

void ComponentRender::Update(BaseObject* base_object, float delta_time)
{
	IComponent::Update(base_object, delta_time);

	for (auto& mesh_object : _mesh_objects)
	{
		mesh_object->AddInstance(std::bind(&BaseObject::GetInstanceData, base_object), MeshObject::INSTANCE_BUFFER_MAIN);

		if (mesh_object->HasInstanceBuffer(MeshObject::INSTANCE_BUFFER_RENDER_FLAGS))
		{
			mesh_object->AddInstance((const char*)&_render_flags, MeshObject::INSTANCE_BUFFER_RENDER_FLAGS);
		}
	}
}

IComponent::COMPONENT_TYPE ComponentRender::GetComponentType() const
{
	return IComponent::COMPONENT_RENDER;
}

void ComponentRender::EnableRenderFlag(RENDER_FLAG render_flag)
{
	_render_flags.render_flags |= render_flag;
}

void ComponentRender::DisableRenderFlag(RENDER_FLAG render_flag)
{
	_render_flags.render_flags &= ~render_flag;
}
