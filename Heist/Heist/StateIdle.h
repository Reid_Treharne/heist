#pragma once
#include "StateInterface.h"

class StateIdle : public StateInterface
{
public:
	StateIdle(void);
	~StateIdle(void);

	virtual void Enter() override;

	virtual bool Update(std::shared_ptr<BaseObject> base_object, float delta_time) override;
};

