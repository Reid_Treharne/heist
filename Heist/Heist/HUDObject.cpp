#include "stdafx.h"
#include "HUDObject.h"

#include "../Renderer/HUDMeshObject.h"

#include <cassert>

HUDObject::HUDObject(void) :
	_num_references(0),
	_position(0.0f, 0.0f),
	_UV_offset(0.0f, 0.0f),
	_color(1.0f, 1.0f, 1.0f, 1.0f),
	_hud_mesh_object(nullptr)
{
}


HUDObject::~HUDObject(void)
{
}

void HUDObject::Initalize(HUDMeshObject* hud_mesh_object)
{
	_hud_mesh_object = hud_mesh_object;
}

void HUDObject::Update(float delta_time)
{
	C_BUFFER_INSTANCE_HUD_OBJECT instance;

	instance.position = _position;
	instance.UV_offset = _UV_offset;
	instance.color = _color;

	_hud_mesh_object->AddInstance((const char*)&instance, MeshObject::INSTANCE_BUFFER_MAIN);
}

void HUDObject::Render()
{
	_hud_mesh_object->Render();
}

void HUDObject::SetPosition(const XMFLOAT2& left_top)
{
	// Check if the values are within the range of 0 to 1
	bool is_valid_position = (left_top.x >= 0.0f && left_top.x <= 1.0f) &&
		(left_top.y >= 0.0f && left_top.y <= 1.0f);

	if (is_valid_position)
	{
		_position = left_top;
	}
	else
	{
		assert (false && "HUDObject::SetPosition - Invalid position");
	}
}

void HUDObject::SetColor(const XMFLOAT4& color)
{
	_color = color;
}
