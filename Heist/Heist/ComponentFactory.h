#pragma once

#include "IComponent.h"
#include "ComponentParams.h"
#include <vector>
#include <functional>
#include <memory>
#include <DirectXMath.h>
using namespace DirectX;

class Service;

class ComponentFactory
{
public:
	static void Initialize(Service* service);

	static std::unique_ptr<IComponent> CreateComponent(IComponent::COMPONENT_TYPE component_type, const IComponentParams& parameters = IComponentParams());

private:
	ComponentFactory(void) = delete;
	~ComponentFactory(void) = delete;

	static std::unique_ptr<IComponent> CreateInteractComponent(const IComponentParams& parameters);
	static std::unique_ptr<IComponent> CreateMouseOverComponent(const IComponentParams& parameters);
	static std::unique_ptr<IComponent> CreateWalkComponent(const IComponentParams& parameters);
	static std::unique_ptr<IComponent> CreateCollisionComponent(const IComponentParams& parameters);
	static std::unique_ptr<IComponent> CreateGroundClampComponent(const IComponentParams& parameters);
	static std::unique_ptr<IComponent> CreateForgeComponent(const IComponentParams& parameters);
	static std::unique_ptr<IComponent> CreateRenderComponent(const IComponentParams& parameters);

	static Service* _service;

	static std::vector<std::function<std::unique_ptr<IComponent>(const IComponentParams& parameters)>> _create_functions;
};

