#include "stdafx.h"
#include "DirectionalLight.h"

#include "../Renderer/MeshObject.h"

DirectionalLight::DirectionalLight(void) :
	_ambient_color(XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f)),
	_diffuse_color(XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f))
{
}

DirectionalLight::~DirectionalLight(void)
{
}

void DirectionalLight::Update(float delta_time)
{
	BaseObject::Update(delta_time);
}

const char* DirectionalLight::GetInstanceData() const
{
	static C_BUFFER_DIRECTIONAL_LIGHT directional_light_buffer;
	GetWorldMatrix(directional_light_buffer.world_matrix);
	directional_light_buffer.ambient_color = _ambient_color;
	directional_light_buffer.diffuse_color = _diffuse_color;

	return (const char*)&directional_light_buffer;
}


