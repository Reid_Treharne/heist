#pragma once
#include "IComponent.h"
#include <vector>
#include "../Renderer/ConstantBufferDeclarations.h"

class MeshObject;

class ComponentRender : public IComponent
{
	friend class FeatureRenderOutline;
	friend class FeatureRenderOnTop;
public:
	ComponentRender(const std::vector<MeshObject*>& mesh_objects);
	virtual ~ComponentRender(void);

	virtual void Update(BaseObject* base_object, float delta_time) override;
	virtual IComponent::COMPONENT_TYPE GetComponentType() const override;

private:
	void EnableRenderFlag(RENDER_FLAG render_flag);
	void DisableRenderFlag(RENDER_FLAG render_flag);

	const std::vector<MeshObject*> _mesh_objects;

	C_BUFFER_INSTANCE_RENDER_FLAGS _render_flags;
};

