#include "stdafx.h"
#include "CameraStateThirdPerson.h"
#include "Camera.h"
#include "ComponentFactory.h"
#include "../CollisionManager/CollisionLibrary.h"
#include "../InputManager/InputManager.h"

using namespace CollisionLibrary;

CameraStateThirdPerson::CameraStateThirdPerson(void) :
	_dirty_camera_position(false),
	_old_attached_object_position(0.0f, 0.0f, 0.0f),
	_default_translation(0.0f, 0.0f, 0.0f),
	_rotation_speed(0.0f),
	_raise_lower_speed(0.0f),
	_max_height(0.0f),
	_min_height(0.0f)
{
}


CameraStateThirdPerson::~CameraStateThirdPerson(void)
{
}

void CameraStateThirdPerson::Initialize(std::shared_ptr<Camera> camera)
{
	CameraState::Initialize(camera);

	_default_translation = XMFLOAT3(3.0f * PI / 2.0f, 5.0f, 5.0f);
	_delta_translation = XMFLOAT3(0.0f, 0.0f, 0.0f);

	_rotation_speed = 0.25f;
	_raise_lower_speed = 1.0f;
	_max_height = 20.0f;
	_min_height = 1.5f;
}

void CameraStateThirdPerson::Enter(std::shared_ptr<BaseObject> object_attached_to)
{
	if (object_attached_to)
	{
		// Attach the camera to the object
		object_attached_to->AttachObject(_camera, BaseObject::NORMAL, BaseObject::ORBIT, _default_translation, XMFLOAT3(0.0f, 0.0f, 0.0f));

		object_attached_to->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_INTERACT));
		object_attached_to->RemoveComponent(IComponent::COMPONENT_COLLISION);

		object_attached_to->AddComponent(ComponentFactory::CreateComponent(IComponent::COMPONENT_MOUSE_OVER));
	}

	_dirty_camera_position = true;

	CameraState::Enter(object_attached_to);
}

void CameraStateThirdPerson::Exit()
{
	if (_object_attached_to)
	{
		_object_attached_to->DetachObject(BaseObject::NORMAL, _camera);

		_object_attached_to->RemoveComponent(IComponent::COMPONENT_INTERACT);
		_object_attached_to->RemoveComponent(IComponent::COMPONENT_MOUSE_OVER);
	}

	CameraState::Exit();
}

void CameraStateThirdPerson::Input(InputManager* input_manager)
{
	InputManager& input = *input_manager;

	// Check for camera movement
	if (input.GetKeyDown('A'))
	{
		Translate(XMFLOAT3(-1.0f, 0.0f, 0.0f));
	}
	else if (input.GetKeyDown('D'))
	{
		Translate(XMFLOAT3(1.0f, 0.0f, 0.0f));
	}
	if (input.GetKeyDown('W'))
	{
		Translate(XMFLOAT3(0.0f, 1.0f, 0.0f));
	}
	else if (input.GetKeyDown('S'))
	{
		Translate(XMFLOAT3(0.0f, -1.0f, 0.0f));
	}
}

void CameraStateThirdPerson::Update(float delta_time)
{
	if (_dirty_camera_position)
	{
		_dirty_camera_position = false;

		_camera->Translate(_delta_translation * delta_time);

		// Make sure the height of the camera is not above or below the limits
		XMFLOAT3 camera_local_position;

		_camera->GetLocalPosition(camera_local_position);

		auto& camera_height = camera_local_position.y;

		if (camera_height > _max_height)
		{
			camera_height = _max_height;

			_camera->SetLocalPosition(camera_local_position);
		}
		else if (camera_height < _min_height)
		{
			camera_height = _min_height;

			_camera->SetLocalPosition(camera_local_position);
		}
	}
}

void CameraStateThirdPerson::Translate(const XMFLOAT3& translation)
{
	_delta_translation.x = translation.x * _rotation_speed;
	_delta_translation.y = translation.y * _raise_lower_speed;
	_delta_translation.z = translation.z * _raise_lower_speed;

	_dirty_camera_position = true;
}