#pragma once
#include "CameraState.h"

class CameraStateFree : public CameraState
{
public:
	CameraStateFree(void);
	~CameraStateFree(void);

	virtual void Initialize(std::shared_ptr<Camera> camera) override;

	virtual void Enter(std::shared_ptr<BaseObject> object_attached_to) override;

	virtual void Input(InputManager* input_manager) override;
	virtual void Update(float delta_time) override;

private:
	XMFLOAT3 _delta_translation;
	XMFLOAT3 _delta_rotation;
};

