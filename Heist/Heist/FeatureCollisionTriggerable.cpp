#include "stdafx.h"
#include "FeatureCollisionTriggerable.h"
#include "ComponentCollision.h"

FeatureCollisionTriggerable::FeatureCollisionTriggerable()
{
}


FeatureCollisionTriggerable::~FeatureCollisionTriggerable()
{
}

IFeature::FEATURE_TYPE FeatureCollisionTriggerable::GetType() const
{
	return IFeature::FEATURE_COLLISION_TRIGGERABLE;
}

unsigned int FeatureCollisionTriggerable::GetComponentType() const
{
	return IComponent::COMPONENT_COLLISION;
}

void FeatureCollisionTriggerable::OnAdded(IComponent& component)
{
	if (component.GetComponentType() == GetComponentType())
	{
		auto& collision_component = (ComponentCollision&)component;

		auto& collider = collision_component._collider;

		collider.EnableCollisionFlag(COLLISION_FLAG::TRIGGERABLE);
	}
}

void FeatureCollisionTriggerable::OnRemoved(IComponent& component)
{
	if (component.GetComponentType() == GetComponentType())
	{
		auto& collision_component = (ComponentCollision&)component;

		auto& collider = collision_component._collider;

		collider.DisableCollisionFlag(COLLISION_FLAG::TRIGGERABLE);
	}
}
