#include "stdafx.h"
#include "ComponentGroundClamp.h"
#include "Terrain.h"
#include "GroundClutter.h"
#include "EventIDs.h"
#include "EventTerrainHeightChanged.h"
#include "DirtyFlagsManager.h"
#include "../CollisionManager/CollisionManager.h"
#include "../Logger/Log.h"
#include "../TileManager/TileManager.h"
#include "../TileManager/Tile.h"
#include "DirectXMath.h"
#include "../EventSystem/EventSystem.h"

using namespace DirectX;
using namespace std::placeholders;

// The distance to offset the collision line above and below the object being ground clamped.
// If the object is not within this distance from the terrain, it will not be ground clamped.
static const auto kGroundClampLineOffset = 1000.0f;

ComponentGroundClamp::ComponentGroundClamp(OBJECT_NAME object_name, float offset_y, std::shared_ptr<const Terrain> terrain, const CollisionManager* collision_manager, const TileManager* tile_manager) :
	_object_name(object_name),
	_offset_y(offset_y),
	_terrain(terrain),
	_collision_manager(collision_manager),
	_tile_manager(tile_manager),
	_first_update(true)
{
	// Objects are ground clamped differently depending on what they are. For example, ground clutter
	// needs each individual piece of clutter clamped, but a tree only needs its origin clamped.
	if (object_name >= 0 && object_name < OBJECT_NAME::NUM_NAMES)
	{
		// Assign the ground clamp function pointer
		switch (object_name)
		{
		case OBJECT_NAME::NAME_GROUND_CLUTTER:
			{
				_ground_clamp_fptr = std::bind(&ComponentGroundClamp::GroundClampGroundClutter, this, _1);

				break;
			}
		default:
			_ground_clamp_fptr = std::bind(&ComponentGroundClamp::GroundClamp, this, _1);
		}
	}
	else
	{
		_ground_clamp_fptr = nullptr;

		Log::Error("ComponentGroundClamp::ComponentGroundClamp - Invalid object name: %i\n", object_name);
	}

	// Add as a listener to the event terrain height changed event if it's grass
	EventSystem::Get().AddListener(this, EVENT_ID::EVENT_TERRAIN_HEIGHT_CHANGED);
}


ComponentGroundClamp::~ComponentGroundClamp(void)
{
	// Remove as a listener from all events
	EventSystem::Get().RemoveListener(this, EVENT_ID::EVENT_TERRAIN_HEIGHT_CHANGED);
}

void ComponentGroundClamp::Update(BaseObject* base_object, float delta_time)
{
	if (_ground_clamp_fptr)
	{
		_ground_clamp_fptr(base_object);

		_first_update = false;
	}
}

void ComponentGroundClamp::GroundClamp(BaseObject* base_object)
{
	if (base_object->GetDirty(DirtyFlagsManager::DIRTY_FLAGS::DIRTY_POSITION) ||
		base_object->GetDirty(DirtyFlagsManager::DIRTY_FLAGS::DIRTY_GEOMETRY) ||
		_first_update)
	{
		XMFLOAT3 object_world_position;
		base_object->GetWorldPosition(object_world_position);

		auto clamped = ClampPoint(object_world_position);

		if (clamped)
		{
			base_object->SetPosition(object_world_position);
		}
	}
	else if (_moved_tile_indices.empty() == false)
	{
		XMFLOAT3 object_world_position;
		base_object->GetWorldPosition(object_world_position);

		auto tile = _tile_manager->GetTile(object_world_position.x, object_world_position.z);

		if (tile)
		{
			// If the tile that this object is on has moved, re-groundclamp it
			if (_moved_tile_indices.find(tile->GetIndex()) != _moved_tile_indices.end())
			{
				auto clamped = ClampPoint(object_world_position);

				if (clamped)
				{
					base_object->SetPosition(object_world_position);
				}
			}
		}

		_moved_tile_indices.clear();

	}
}

void ComponentGroundClamp::GroundClampGroundClutter(BaseObject* base_object)
{
	auto& ground_clutter = (GroundClutter&)*base_object;

	auto& points = ground_clutter.GetPoints();

	// If this is the first update or the entire ground clutter object has moved (hopefully that doesn't happen), ground clamp all the points
	if (base_object->GetDirty(DirtyFlagsManager::DIRTY_FLAGS::DIRTY_POSITION) ||
		base_object->GetDirty(DirtyFlagsManager::DIRTY_FLAGS::DIRTY_GEOMETRY) ||
		_first_update)
	{
		for (auto& point : points)
		{
			auto new_point = point.second;

			auto clamped = ClampPoint(new_point);

			if (clamped)
			{
				ground_clutter.AddOrUpdatePoint(point.first, new_point);
			}
		}
	}
	else if (_moved_tile_indices.empty() == false)
	{
		// Loop through the list of moved tile indices and re-groundclamp the points on those tiles
		for (auto& moved_tile_index : _moved_tile_indices)
		{
			auto iter = points.find(moved_tile_index);

			// If no point exists on this tile, continue to the next tile
			if (iter != points.end())
			{
				auto point = points.at(moved_tile_index);

				auto clamped = ClampPoint(point);

				if (clamped)
				{
					ground_clutter.AddOrUpdatePoint(moved_tile_index, point);
				}
			}
		}

		_moved_tile_indices.clear();
	}
}

bool ComponentGroundClamp::ClampPoint(XMFLOAT3& point)
{
	// Get the tile at the object's world position
	auto tile = _tile_manager->GetTile(point.x, point.z);

	bool collided = false;

	if (tile)
	{
		auto col = tile->GetColumn();
		auto row = tile->GetRow();

		static const auto kTrianglesPerTile = 2;

		// Get the triangles that make up the tile
		auto triangle_index = (col * _tile_manager->GetNumRows() * kTrianglesPerTile) + (row * kTrianglesPerTile);

		Triangle tile_triangles[kTrianglesPerTile];

		auto success1 = _collision_manager->GetModelTriangle(_terrain->GetName(), tile_triangles[0], triangle_index);
		auto success2 = _collision_manager->GetModelTriangle(_terrain->GetName(), tile_triangles[1], triangle_index + 1);

		if (success1 && success2)
		{
			XMFLOAT4X4 terrain_world_matrix;
			_terrain->GetWorldMatrix(terrain_world_matrix);

			// Put the object into terrain space
			XMFLOAT3 terrain_space;

			XMStoreFloat3(&terrain_space, XMVector3Transform(XMLoadFloat3(&point), XMMatrixInverse(nullptr, XMLoadFloat4x4(&terrain_world_matrix))));

			// Create a vertical line at the object's position (in terrain space)
			XMFLOAT3 object_position;
			memcpy_s(&object_position, sizeof(object_position), &terrain_space, sizeof(object_position));

			XMFLOAT3 start = object_position;
			XMFLOAT3 end = object_position;

			start.y += kGroundClampLineOffset;
			end.y -= kGroundClampLineOffset;

			// Calculate the collision point
			unsigned int collided_triangle_index;

			collided = CollisionLibrary::LineSegmentToTriangle(point, collided_triangle_index, tile_triangles, kTrianglesPerTile, start, end);

			// Check if the vertical line collided with any of the triangles
			if (collided == true)
			{
				// Transform the collision point back into world space
				XMStoreFloat3(&point, XMVector3Transform(XMLoadFloat3(&point), XMLoadFloat4x4(&terrain_world_matrix)));

				// Set the out value to the height of the collision point plus the y offset
				point.y += _offset_y;
			}
			else
			{
				Log::Warning("ObjectManager::GroundClampGroundClutter - Can't find terrain\n");
			}
		}
	}

	return collided;
}

IComponent::COMPONENT_TYPE ComponentGroundClamp::GetComponentType() const
{
	return IComponent::COMPONENT_GROUND_CLAMP;
}

void ComponentGroundClamp::OnEvent(const IEvent* the_event)
{
	auto event_id = the_event->GetID();

	switch (event_id)
	{
	case EVENT_ID::EVENT_TERRAIN_HEIGHT_CHANGED:
		{
			auto& terrain_height_changed_event = (EventTerrainHeightChanged&)*the_event;

			const auto& moved_triangle_indices = terrain_height_changed_event.GetMovedTriangleIndices();

			// convert the tringle indices to tile indices
			for (auto& triangle_index : moved_triangle_indices)
			{
				auto tile_index = triangle_index / 2;

				_moved_tile_indices.insert(tile_index);
			}

			break;
		}
	default:
		{
			Log::Error("ComponentGroundClamp::OnEvent - Event ID %u not registered for this listener\n", event_id);
		}
	}
}

