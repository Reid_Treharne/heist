#pragma once

#include <memory>
#include <vector>
#include <algorithm>
#include "IFeature.h"

class BaseObject;

class IComponent
{
public:
	IComponent(void) {};
	virtual ~IComponent(void) = 0 {};

	// The order of the components in this enum are important as it is the order the will get executed in
	enum COMPONENT_TYPE {
		COMPONENT_MOUSE_OVER,
		COMPONENT_INTERACT,
		COMPONENT_WALK,
		COMPONENT_FORGE,
		COMPONENT_GROUND_CLAMP,
		COMPONENT_COLLISION,
		COMPONENT_RENDER, // Render component needs to happen last
		COMPONENT_COUNT };

	virtual IComponent::COMPONENT_TYPE GetComponentType() const = 0;

	virtual void Update(BaseObject* base_object, float delta_time);

	virtual void AddFeature(std::unique_ptr<IFeature> feature) final;
	virtual void RemoveFeature(unsigned int feature_type) final;

	template<typename Derived, typename Base, typename Del>
	static std::unique_ptr<Derived, Del>
		static_unique_ptr_cast(std::unique_ptr<Base, Del>&& p)
	{
		auto d = static_cast<Derived *>(p.release());
		return std::unique_ptr<Derived, Del>(d, std::move(p.get_deleter()));
	}

protected:
	std::vector<std::unique_ptr<IFeature>>::iterator FindFeature(unsigned int feature_type);

	std::vector<std::unique_ptr<IFeature>> _features;
};

