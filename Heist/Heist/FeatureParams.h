#pragma once
#include <memory>
#include <DirectXMath.h>

using namespace DirectX;

class BaseObject;
class CameraManager;

struct IFeatureParams
{
	IFeatureParams() = default;
	IFeatureParams(const IFeatureParams& copy) = delete;
	IFeatureParams operator=(const IFeatureParams& copy) = delete;

	virtual ~IFeatureParams()
	{
	}
};

struct FeatureRenderOutlineParams : public IFeatureParams
{
	FeatureRenderOutlineParams(std::shared_ptr<BaseObject> base_object, float outline_range, const XMFLOAT3& outline_color) :
		_base_object(base_object),
		_outline_color(outline_color),
		_outline_range(outline_range)
	{

	}

	std::shared_ptr<BaseObject> _base_object;
	float _outline_range;
	XMFLOAT3 _outline_color;
};

struct FeatureCollisionTriggerParams : public IFeatureParams
{
	FeatureCollisionTriggerParams(unsigned char key) :
		_key(key)
	{

	}

	// The key that needs to be pressed to trigger the object
	unsigned char _key;
};
