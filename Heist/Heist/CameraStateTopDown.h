#pragma once
#include "CameraState.h"

class Terrain;

class CameraStateTopDown : public CameraState
{
public:
	CameraStateTopDown(void);
	~CameraStateTopDown(void);

	virtual void Enter(std::shared_ptr<BaseObject> object_attached_to) override;
	virtual void Exit() override;

	virtual void Input(InputManager* input_manager) override;
	virtual void Update(float delta_time) override;

private:
	const Terrain* _terrain;

	XMFLOAT3 _delta_translation;
};

