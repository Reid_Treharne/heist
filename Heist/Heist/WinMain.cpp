// Heist.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "WinMain.h"
#include "HeistMain.h"
#include "../InputManager/InputManager.h"
#include "../Logger/Log.h"
#include <Windowsx.h>
#include <ctime>

#define MAX_LOADSTRING 100
#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 768
#define WINDOW_X 1920 - WINDOW_WIDTH - 100
#define WINDOW_Y 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
static HeistMain g_heist_man;					// The game
static HWND g_hWnd;								// handle to the window
static InputManager g_input_manager;			// The input manager

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
					   _In_opt_ HINSTANCE hPrevInstance,
					   _In_ LPTSTR    lpCmdLine,
					   _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

#ifdef DEBUG
	// Initialize the console window and the logger
	Log::Initialize();
#endif // DEBUG

	MSG msg;
	memset(&msg, 0, sizeof(msg));
	HACCEL hAccelTable;

	// Initialize global strings.
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_HEIST, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	g_input_manager.Initialize(g_hWnd);

	g_heist_man.Initialize(WINDOW_WIDTH, WINDOW_HEIGHT, &g_input_manager, g_hWnd, true);


	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_HEIST));

	// Seed random
	srand(unsigned int(time(nullptr)));

	// Set up timer
	LARGE_INTEGER starting_time, ending_time, elsapsed_microseconds, frequency;

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&starting_time);

	while (msg.message != WM_QUIT)
	{
		// Main message loop:
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		// Update timer
		QueryPerformanceCounter(&ending_time);
		elsapsed_microseconds.QuadPart = ending_time.QuadPart - starting_time.QuadPart;
		elsapsed_microseconds.QuadPart *= 1;
		float delta_time = (float)elsapsed_microseconds.QuadPart / (float)frequency.QuadPart;

		if (delta_time > 0.125f)
		{
			delta_time = 0.125f;
		}

		starting_time = ending_time;

		if (g_input_manager.GetKeyReleased(VK_ESCAPE))
		{
			break;
		}

		// Do game logic
		g_heist_man.Input();
		g_heist_man.Update(delta_time);
		g_heist_man.Render();

	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_HEIST));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_HEIST);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	// Calculates the required size of the window rectangle, based on the desired client-rectangle size
	RECT client_rect = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
	AdjustWindowRect(&client_rect, WS_OVERLAPPEDWINDOW, TRUE);

	g_hWnd = CreateWindow(szWindowClass,
		szTitle,
		WS_OVERLAPPEDWINDOW,
		WINDOW_X,
		WINDOW_Y,
		client_rect.right - client_rect.left,
		client_rect.bottom - client_rect.top,
		NULL,
		NULL,
		hInstance,
		NULL);


	if (!g_hWnd)
	{
		return FALSE;
	}

	ShowWindow(g_hWnd, nCmdShow);
	UpdateWindow(g_hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_KEYDOWN:
		g_input_manager.OnKeyDown(wParam);
		break;
	case WM_KEYUP:
		g_input_manager.OnKeyUp(wParam);
		break;
	case WM_LBUTTONDOWN:
		g_input_manager.OnLeftMouseButtonDown();
		break;
	case WM_LBUTTONUP:
		g_input_manager.OnLeftMouseButtonUp();
		break;
	case WM_RBUTTONDOWN:
		g_input_manager.OnRightMouseButtonDown();
		break;
	case WM_RBUTTONUP:
		g_input_manager.OnRightMouseButtonUp();
		break;
	case WM_MOUSEMOVE:
		{
			int x_position = GET_X_LPARAM(lParam);
			int y_position = GET_Y_LPARAM(lParam);
			g_input_manager.OnMouseMove(x_position, y_position);
			break;
		}
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}