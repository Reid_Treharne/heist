#include "stdafx.h"
#include "StateInteract.h"
#include "BaseObject.h"

StateInteract::StateInteract(std::shared_ptr<BaseObject> interact_object) :
	_interact_object(interact_object)
{
}


StateInteract::~StateInteract(void)
{
}

void StateInteract::Enter()
{
}

bool StateInteract::Update(std::shared_ptr<BaseObject> base_object, float delta_time)
{
	bool state_finished = true;

	if (_interact_object)
	{
		state_finished = _interact_object->OnClicked(base_object);
	}

	return state_finished;
}