#include "TileManager.h"
#include "Tile.h"
#include "../TinyXML/tinyxml.h"
#include "../Logger/Log.h"
#include <cassert>
#include <fstream>

// The number of tiles one full sample of the applied terrain texture will make up before repeating.
static const int kTilesPerTexture = 10;

TileManager::TileManager(void) :
	_num_columns(0),
	_num_rows(0),
	_terrain_position_x(0.0f),
	_terrain_position_y(0.0f),
	_terrain_position_z(0.0f)
{
}


TileManager::~TileManager(void)
{
}

void TileManager::LoadMap(const std::string& file_name,
						  std::vector<TileMeshData>& vertices,
						  std::vector<unsigned int>& indices,
						  MapData& map_data)
{
#if 0
	std::fstream fin(file_name, std::ios::in | std::ios::binary);

	if (fin.is_open())
	{
		int num_vertices = 0;
		int num_indices = 0;

		fin.read((char*)&num_vertices, sizeof(num_vertices));
		fin.read((char*)&num_indices, sizeof(num_indices));

		vertices.resize(num_vertices);
		indices.resize(num_indices);
		fin.read((char*)&vertices[0], sizeof(vertices[0]) * vertices.size());
		fin.read((char*)&indices[0], sizeof(indices[0]) * indices.size());
		fin.close();
	}
#else
	TiXmlDocument doc(file_name.c_str());

	if (doc.LoadFile() == true)
	{
		vertices.clear();
		indices.clear();
		map_data._objects.clear();
		map_data._grass_points.clear();

		TiXmlElement* xml_root = doc.RootElement();

		// Get the number of rows and columns
		int num_columns = 0;
		int num_rows = 0;

		xml_root->Attribute("Cols", &num_columns);
		xml_root->Attribute("Rows", &num_rows);

		_num_columns = num_columns;
		_num_rows = num_rows;

		// Allocate the map with default tiles
		_map.resize(num_columns);

		for (unsigned int i = 0; i < _map.size(); ++i)
		{
			_map[i].resize(num_rows);
		}

		// The UV scale will scale the UV coordinates on the terrain. The higher the UV scale, the higher
		// number of times the applied textures will be wrapped resulting in a higher texture detail
		XMFLOAT2 uv_scale;
		uv_scale.x = (float)_num_columns / max(kTilesPerTexture, 1);
		uv_scale.y = (float)_num_rows / max(kTilesPerTexture, 1);

		// Loop through all the tiles and store their data
		TiXmlElement* xml_tile = xml_root->FirstChildElement("Tile");

		int row = 0;
		int column = 0;
		int walkable = 0; // 0 == false, 1 == true
		int has_grass = 0;

		TileMeshData tile_mesh_data;

		int index_count = 0;

		while (xml_tile != nullptr)
		{
			// Get the column and row of the tile
			xml_tile->Attribute("Col", &column);
			xml_tile->Attribute("Row", &row);
			xml_tile->Attribute("Walkable", &walkable);
			xml_tile->Attribute("HasGrass", &has_grass);

			// Set the column and row  of the tile
			Tile& tile = _map[column][row];
			tile.Initialize(column, row, walkable == 1 ? true : false, this);

			// If this tile has grass, add it to the vector of grass points
			if (has_grass == 1)
			{
				map_data._grass_points[tile.GetIndex()] = XMFLOAT2((float)column, (float)row);
			}

			// Loop through the two triangles that make up the tile
			TiXmlElement* xml_triangle = xml_tile->FirstChildElement("Triangle");

			float average_height = 0.0f;
			int vertex_count = 0;

			while (xml_triangle != nullptr)
			{
				// Store the normal of the current triangle
				double normal_x, normal_y, normal_z;
				xml_triangle->Attribute("NormalX", &normal_x);
				xml_triangle->Attribute("NormalY", &normal_y);
				xml_triangle->Attribute("NormalZ", &normal_z);

				// Loop through the vertices of the triangle
				TiXmlElement* xml_vertex = xml_triangle->FirstChildElement("Vertex");

				while (xml_vertex != nullptr)
				{
					double position_x, position_y, position_z;

					// Get the x, y, and z position of the current vertex
					xml_vertex->Attribute("X", &position_x);
					xml_vertex->Attribute("Y", &position_y);
					xml_vertex->Attribute("Z", &position_z);

					tile_mesh_data.position_x = (float)position_x;
					tile_mesh_data.position_y = (float)position_y;
					tile_mesh_data.position_z = (float)position_z;

					tile_mesh_data.normal_x = (float)normal_x;
					tile_mesh_data.normal_y = (float)normal_y;
					tile_mesh_data.normal_z = (float)normal_z;

					tile_mesh_data.u = ((float)position_x / num_columns) * uv_scale.x;
					tile_mesh_data.v = (1.0f - (float)position_z / num_rows) * uv_scale.y;

					average_height += tile_mesh_data.position_y;
					vertex_count++;

					vertices.push_back(tile_mesh_data);
					indices.push_back(index_count++);

					xml_vertex = xml_vertex->NextSiblingElement("Vertex");
				}
				xml_triangle = xml_triangle->NextSiblingElement("Triangle");
			}

			// calculate the average height of the verts in order to determine where to place the objects
			average_height /= vertex_count;

			// Loop through the objects on this tile
			TiXmlElement* xml_object = xml_tile->FirstChildElement("Object");

			while (xml_object != nullptr)
			{
				ObjectData current_object_data;

				double position_x;
				double position_z;

				xml_object->Attribute("ObjectName", &current_object_data._object_name);
				xml_object->Attribute("PositionX", &position_x);
				xml_object->Attribute("PositionZ", &position_z);

				current_object_data._position = XMFLOAT3((float)position_x, average_height, (float)position_z);

				// Get all the mesh names associated with this object
				TiXmlElement* xml_mesh_name = xml_object->FirstChildElement("MeshName");

				if (xml_mesh_name != nullptr)
				{
					current_object_data._mesh_names = xml_mesh_name->GetText();
				}

				// Add the object to the list of terrain objects
				map_data._objects.push_back(current_object_data);

				xml_object = xml_object->NextSiblingElement("Object");
			}

			xml_tile = xml_tile->NextSiblingElement("Tile");
		}
	}
	else
	{
		assert(false && "MeshObject::LoadMap - failed to load map");
	}
#endif
}

void TileManager::ConnectNeighboringTiles()
{
	for (unsigned int col = 0; col < _map.size(); ++col)
	{
		for (unsigned int row = 0; row < _map[col].size(); ++row)
		{
			_map[col][row].FindNeighbors();
		}
	}
}

const Tile* TileManager::GetTile(int column, int row) const
{
	const Tile* tile = nullptr;

	if (IsValidTile(column, row))
	{
		tile = &_map[column][row];
	}

	return tile;
}

const Tile* TileManager::GetTile(int index) const
{
	const Tile* tile = nullptr;

	int column = index / _num_rows;
	int row = index % _num_columns;

	return GetTile(column, row);
}

const Tile* TileManager::GetTile(float position_x, float position_z) const
{
	int column = static_cast<int>(position_x - _terrain_position_x);
	int row = static_cast<int>(position_z - _terrain_position_z);

	return GetTile(column, row);
}

void TileManager::SetTerrainPosition(float position_x, float position_y, float position_z)
{
	_terrain_position_x = position_x;
	_terrain_position_y = position_y;
	_terrain_position_z = position_z;
}

bool TileManager::IsValidTile(int column, int row) const
{
	bool invalid = (column >= _num_columns || row  >= _num_rows || column < 0 || row < 0);

	if (invalid == true)
	{
		Log::Error("TileManager::IsValidTile - Invalid tile (%d, %d)\n", column, row);
	}

	return !invalid;
}
