#include "PlannerNode.h"
#include "Tile.h"
#include "TileManager.h"

PlannerNode::PlannerNode() :
	_tile(nullptr),
	_parent(nullptr),
	_g(INT_MAX),
	_f(INT_MAX),
	_in_open_set(false),
	_in_closed_set(false)
{
}

PlannerNode::~PlannerNode(void)
{
}

bool PlannerNode::operator==(const PlannerNode* other) const
{
	return this->_tile == other->_tile;
}

int PlannerNode::GetDistance(const PlannerNode* node) const
{
	// This function might need to be modified to get a more accurate distance, but
	// this cheap calculation seems to work for now
	auto a = _tile->GetColumn() - node->_tile->GetColumn();
	auto b = _tile->GetRow() - node->_tile->GetRow();

	return (abs(a) + abs((b)));
}

