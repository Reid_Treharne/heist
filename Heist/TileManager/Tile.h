#pragma once

#include <bitset>
#include <vector>

class TileManager;

class Tile
{
public:
	Tile();
	Tile(int column, int row);
	~Tile(void);

	void Initialize(int column, int row, bool is_walkable, TileManager* tile_manager);
	void FindNeighbors();

	void SetColumn(int column);
	void SetRow(int row);

	int GetColumn() const;
	int GetRow() const;
	int GetIndex() const;
	bool GetIsWalkable() const { return _is_walkable; }
	float GetWorldX() const;
	float GetWorldY() const;
	float GetWorldZ() const;
	const std::vector<const Tile*>& GetNeighbors() const { return _neighbors; }
private:

	void AddNeighborIfValid(const Tile* neighbor);
	int _column;
	int _row;
	int _index;
	bool _is_walkable;
	std::vector<const Tile*> _neighbors;

	TileManager* _tile_manager;
};

