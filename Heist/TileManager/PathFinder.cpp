#include "PathFinder.h"
#include "PlannerNode.h"
#include "TileManager.h"
#include "Tile.h"
#include "../Logger/Log.h"
#include <set>
#include <map>
#include <memory>

PathFinder::PathFinder(void) :
	_tile_manager(nullptr),
	_planner_node_pool_index(0)
{
}

PathFinder::PathFinder(const PathFinder&)
{
}

PathFinder::~PathFinder(void)
{
}

PathFinder& PathFinder::GetInstance()
{
	static PathFinder instance;

	return instance;
}

void PathFinder::Initialize(TileManager* tile_manager)
{
	_tile_manager = tile_manager;

	_planner_node_pool.resize(tile_manager->GetNumColumns() * tile_manager->GetNumRows());

	_planner_node_pool_index = 0;
}

bool PathFinder::FindPath(float start_position_x,
						  float start_position_z,
						  float end_position_x,
						  float end_position_z,
						  std::vector<const Tile*>& out_path)
{
	bool found_path = false;

	// Make sure the path finder has been initialized
	if (_tile_manager != nullptr)
	{
		// Clear the tile path
		out_path.clear();

		// store the start and end tiles
		const Tile* start_tile = _tile_manager->GetTile(start_position_x, start_position_z);
		const Tile* end_tile = _tile_manager->GetTile(end_position_x, end_position_z);

		// reset the planner node pool index to 0
		_planner_node_pool_index = 0;

		// store the start and end nodes
		PlannerNode* start_node = new PlannerNode();
		start_node->_tile = start_tile;

		PlannerNode* end_node = new PlannerNode();
		end_node->_tile = end_tile;

		start_node->_f = start_node->GetDistance(end_node);
		start_node->_g = 0;

		std::multiset<PlannerNode*, NodeCompare> openset;
		std::unordered_map<const Tile*, PlannerNode*> visited;

		openset.insert(start_node);
		start_node->_in_open_set = true;

		visited[start_tile] = start_node;
		visited[end_tile] = end_node;

		PlannerNode* current_node = nullptr;

		while (openset.empty() == false)
		{
			current_node = *openset.begin();

			if (current_node->_tile == end_node->_tile)
			{
				// check if the end tile is walkable
				if (end_tile->GetIsWalkable() == true)
				{
					// reached goal tile
					found_path = true;
				}
				// if not, set the end tile its parent
				else
				{
					current_node = current_node->_parent;

					if (current_node != nullptr)
					{
						// reached tile right before goal tile
						found_path = true;
					}
				}

				break;
			}

			openset.erase(openset.begin());
			current_node->_in_open_set = false;
			current_node->_in_closed_set = true;

			const std::vector<const Tile*>& neighbors = current_node->_tile->GetNeighbors();

			for (unsigned int i = 0; i < neighbors.size(); ++i)
			{
				const Tile* neighbor = neighbors[i];

				if (neighbor->GetIsWalkable() == false && neighbor != end_tile)
				{
					continue;
				}

				PlannerNode *nn = visited[neighbor];

				if (nn == nullptr)
				{
					nn = &_planner_node_pool[_planner_node_pool_index++];
					nn->_f = INT_MAX;
					nn->_g = INT_MAX;
					nn->_in_closed_set = false;
					nn->_in_open_set = false;
					nn->_parent = nullptr;
					nn->_tile = neighbor;
					visited[neighbor] = nn;
				}
				else if (nn->_in_closed_set)
				{
					continue;
				}


				auto tentative_gScore = current_node->_g + current_node->GetDistance(nn);

				// if not in open set
				if (nn->_in_open_set == false)
				{
					// only set the parent if it's not the start node as we don't want the start
					// node to be part of the final path returned from this functions. If it is,
					// the entity following the path my do an abrupt 180 for a few frames to try
					// to get to the center of the tile it's already on.
					nn->_parent = current_node == start_node ? nullptr : current_node;
					nn->_g = tentative_gScore;
					nn->_f = tentative_gScore + nn->GetDistance(end_node);
					nn->_in_open_set = true;
					openset.insert(nn);
				}
				// This is not a better path.
				else if (tentative_gScore >= nn->_g)
				{
					continue;
				}
				// This is the best path yet. We need to reinsert the node into the openset with it's new f-value.
				else
				{
					// Since the open set if sorted by f-value, get all the nodes with nn's f-value
					auto iter_range = openset.equal_range(nn);

					// Find which node is actually nn
					auto iter = std::find(iter_range.first, iter_range.second, nn);

					// Erase it, update it's f and g value, and reinsert it.
					openset.erase(iter);

					nn->_g = tentative_gScore;
					nn->_f = tentative_gScore + nn->GetDistance(end_node);

					openset.insert(nn);
				}

			}
		}

		// Generate the path
		if (found_path)
		{
			out_path.push_back(current_node->_tile);

			PlannerNode* parent = current_node->_parent;

			while (parent != nullptr)
			{
				out_path.push_back(parent->_tile);
				parent = parent->_parent;
			}
		}

		// delete allocated memory
		delete start_node;
		delete end_node;
	}
	else
	{
		Log::Error("PathFinder::FindPath - Path finder not yet initialized\n");
	}

	return found_path;
}
