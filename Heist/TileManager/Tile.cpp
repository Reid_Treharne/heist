#include "Tile.h"
#include "TileManager.h"

Tile::Tile() :
	_column(0),
	_row(0),
	_tile_manager(nullptr),
	_is_walkable(false)
{
}

Tile::Tile(int column, int row) :
	_column(column),
	_row(row)
{
}

Tile::~Tile(void)
{
}

void Tile::Initialize(int column, int row, bool is_walkable, TileManager* tile_manager)
{
	// Set the column and row
	_column = column;
	_row = row;

	// Set if the tile is walkable
	_is_walkable = is_walkable;

	// Store the tile manager
	_tile_manager = tile_manager;

	// Calculate and store this triangle's index for easy access
	_index = (column * _tile_manager->GetNumRows()) + row;
}

void Tile::FindNeighbors()
{
	const TileManager& tile_manager = *_tile_manager;

	// bools for if this tile has a neighbor to the...
	bool up = false;
	bool down = false;
	bool right = false;
	bool left = false;

	left = _column > 0;
	right = _column < tile_manager.GetNumColumns() - 1;
	down = _row > 0;
	up = _row < tile_manager.GetNumRows() - 1;

	if (left)
	{
		AddNeighborIfValid(tile_manager.GetTile(_column - 1, _row));
	}
	if (right)
	{
		AddNeighborIfValid(tile_manager.GetTile(_column + 1, _row));
	}
	if (down)
	{
		AddNeighborIfValid(tile_manager.GetTile(_column, _row - 1));
	}
	if (up)
	{
		AddNeighborIfValid(tile_manager.GetTile(_column, _row + 1));
	}
	if (left && up)
	{
		AddNeighborIfValid(tile_manager.GetTile(_column - 1, _row + 1));
	}
	if (right && up)
	{
		AddNeighborIfValid(tile_manager.GetTile(_column + 1, _row + 1));
	}
	if (right && down)
	{
		AddNeighborIfValid(tile_manager.GetTile(_column + 1, _row - 1));
	}
	if (left && down)
	{
		AddNeighborIfValid(tile_manager.GetTile(_column - 1, _row - 1));
	}
}

void Tile::SetColumn(int column)
{
	_column = column;
}
void Tile::SetRow(int row)
{
	_row = row;
}

int Tile::GetColumn() const
{
	return _column;
}

int Tile::GetRow() const
{
	return _row;
}

int Tile::GetIndex() const
{
	return _index;
}

float Tile::GetWorldX() const
{
	return _column + (1.0f * 0.5f) + _tile_manager->GetTerrainPositionX();
}

float Tile::GetWorldY() const
{
	return _tile_manager->GetTerrainPositionY();
}

float Tile::GetWorldZ() const
{
	return _row + (1.0f * 0.5f) + _tile_manager->GetTerrainPositionZ();
}

void Tile::AddNeighborIfValid(const Tile* neighbor)
{
	_neighbors.push_back(neighbor);
}
