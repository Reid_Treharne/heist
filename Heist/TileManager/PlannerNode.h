#pragma once

class Tile;
class TileManager;

class PlannerNode
{
public:
	PlannerNode(void);
	~PlannerNode(void);

	bool operator==(const PlannerNode* other) const;

	int GetDistance(const PlannerNode* end_node) const;

	const Tile* _tile;

	PlannerNode* _parent;

	int _g; // g of parent plus movement cost
	int _f; // g + h

	bool _in_open_set;
	bool _in_closed_set;
};

struct NodeCompare
{
	bool operator() (const PlannerNode* lhs, const PlannerNode* rhs) const
	{
		return (lhs->_f < rhs->_f);
	}
};

