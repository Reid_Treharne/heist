#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <DirectXMath.h>

using namespace DirectX;

class Tile;

struct TileMeshData
{
	float position_x;
	float position_y;
	float position_z;

	float normal_x;
	float normal_y;
	float normal_z;

	float u;
	float v;
};

struct ObjectData
{
	std::string _mesh_names;
	int _object_name;
	XMFLOAT3 _position;
};

struct MapData
{
	std::vector<ObjectData> _objects;
	// Maps tile index number to position of the piece of ground clutter.
	// Note: The position is in terrain space, not local to the tile.
	std::unordered_map<int, XMFLOAT2> _grass_points;
};

class TileManager
{
public:
	TileManager(void);
	~TileManager(void);

	void LoadMap(const std::string& file_name,
		std::vector<TileMeshData>& vertices,
		std::vector<unsigned int>& indices,
		MapData& map_data);

	void ConnectNeighboringTiles();

	// Accessors
	inline int GetNumColumns() const { return _num_columns; }
	inline int GetNumRows() const { return _num_rows; }
	float GetTerrainPositionX() const { return _terrain_position_x; }
	float GetTerrainPositionY() const { return _terrain_position_y; }
	float GetTerrainPositionZ() const { return _terrain_position_z; }
	inline const std::vector<std::vector<Tile>>& GetTileMapByReference() const { return _map; }

	const Tile* GetTile(int column, int row) const;
	const Tile* GetTile(int index) const;
	const Tile* GetTile(float position_x, float position_z) const;

	// Mutators
	void SetTerrainPosition(float position_x, float position_y, float position_z);

private:
	bool IsValidTile(int column, int row) const;

	// 2D array of tiles as col by row
	std::vector<std::vector<Tile>> _map;

	// the world position of the origin (bottom left) of the terrain
	float _terrain_position_x;
	float _terrain_position_y;
	float _terrain_position_z;

	int _num_columns;
	int _num_rows;
};

