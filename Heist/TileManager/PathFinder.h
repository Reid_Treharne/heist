#pragma once

#include <vector>

#include "PlannerNode.h"

class TileManager;
class Tile;

class PathFinder
{
public:

	static PathFinder& GetInstance();

	void Initialize(TileManager* tile_manager);

	bool FindPath(float start_position_x,
		float start_position_z,
		float end_position_x,
		float end_position_z,
		std::vector<const Tile*>& out_path);

private:
	PathFinder(void);
	PathFinder(const PathFinder&);
	~PathFinder(void);

	// A pointer to the tile manager
	TileManager* _tile_manager;

	std::vector<PlannerNode> _planner_node_pool;

	unsigned int _planner_node_pool_index;
};

