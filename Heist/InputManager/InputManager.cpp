#include "InputManager.h"
#include <assert.h>
#include <memory.h>

InputManager::InputManager(void)
{
}


InputManager::~InputManager(void)
{
}

void InputManager::Initialize(HWND hwnd)
{
	_hwnd = hwnd;

	_cursor_hidden = false;

	memset(&_new_keys, 0, sizeof(_new_keys));
	memset(&_old_keys, 0, sizeof(_old_keys));

	_mouse_x_position = 0;
	_mouse_y_position = 0;

	_delta_mouse_x_position = 0;
	_delta_mouse_y_position = 0;
}

void InputManager::Update()
{
	// copy the new keys into the old keys
	memcpy_s(&_old_keys[0], sizeof(_old_keys), &_new_keys[0], sizeof(_new_keys));

	// copy the new mouse buttons into the old mouse buttons
	memcpy_s(&_old_mouse_buttons[0], sizeof(_old_mouse_buttons), &_new_mouse_buttons[0], sizeof(_new_mouse_buttons));

	// If the cursor is hidden, lock it to the center of the screen
	if (_cursor_hidden && GetFocus() == _hwnd)
	{
		// NOTE: All calculations below done in screen space
		POINT center, mouse;
		RECT window_rect;

		GetCursorPos(&mouse);

		GetWindowRect(_hwnd, &window_rect);
		ClipCursor(&window_rect);

		auto center_x = (window_rect.right - window_rect.left) / 2.0f;
		auto center_y = (window_rect.bottom - window_rect.top) / 2.0f; 

		center.x = (LONG)center_x;
		center.y = (LONG)center_y;

		// Convert the center point to screen coordinates
		ClientToScreen(_hwnd, &center);

		// Reset cursor to the center of the window
		SetCursorPos(center.x, center.y);

		// Calculate the delta mouse position
		_delta_mouse_x_position = center.x - mouse.x;
		_delta_mouse_y_position = center.y - mouse.y;
	}
}

void InputManager::HideCursor(bool hide)
{
	if (_cursor_hidden != hide)
	{
		_cursor_hidden = hide;

		if (hide)
		{
			ShowCursor(false);
		}
		else
		{
			ShowCursor(true);
		}
	}
}

void InputManager::OnKeyDown(unsigned char key)
{
	if (ValidKey(key))
	{
		_new_keys[key] = 1;
	}
}

void InputManager::OnKeyUp(unsigned char key)
{
	if (ValidKey(key))
	{
		_new_keys[key] = 0;
	}
}

void InputManager::OnLeftMouseButtonDown()
{
	_new_mouse_buttons[MOUSE_BUTTON_LEFT] = 1;
}

void InputManager::OnLeftMouseButtonUp()
{
	_new_mouse_buttons[MOUSE_BUTTON_LEFT] = 0;
}

void InputManager::OnRightMouseButtonDown()
{
	_new_mouse_buttons[MOUSE_BUTTON_RIGHT] = 1;
}

void InputManager::OnRightMouseButtonUp()
{
	_new_mouse_buttons[MOUSE_BUTTON_RIGHT] = 0;
}

void InputManager::OnMouseMove(int x_position, int y_position)
{
	_mouse_x_position = x_position;
	_mouse_y_position = y_position;
}

bool InputManager::GetKeyDown(unsigned char key) const
{
	bool result = false;

	if (ValidKey(key))
	{
		result = _new_keys[key] == 1;
	}

	return result;
}

bool InputManager::GetKeyReleased(unsigned char key) const
{
	bool result = false;

	if (ValidKey(key))
	{
		result = _new_keys[key] == 0 && _old_keys[key] == 1;
	}

	return result;
}

bool InputManager::GetKeyPressed(unsigned char key) const
{
	bool result = false;

	if (ValidKey(key))
	{
		result = _old_keys[key] == 0 && _new_keys[key] == 1;
	}

	return result;
}

bool InputManager::GetLeftMouseDown() const
{
	return _new_mouse_buttons[MOUSE_BUTTON_LEFT] == 1;
}

bool InputManager::GetLeftMouseReleased() const
{
	return _new_mouse_buttons[MOUSE_BUTTON_LEFT] == 0 && _old_mouse_buttons[MOUSE_BUTTON_LEFT] == 1;
}

bool InputManager::GetLeftMousePressed() const
{
	return _old_mouse_buttons[MOUSE_BUTTON_LEFT] == 0 && _new_mouse_buttons[MOUSE_BUTTON_LEFT] == 1;
}

bool InputManager::GetRightMouseDown() const
{
	return _new_mouse_buttons[MOUSE_BUTTON_RIGHT] == 1;
}

bool InputManager::GetRightMouseReleased() const
{
	return _new_mouse_buttons[MOUSE_BUTTON_RIGHT] == 0 && _old_mouse_buttons[MOUSE_BUTTON_RIGHT] == 1;
}

bool InputManager::GetRightMousePressed() const
{
	return _old_mouse_buttons[MOUSE_BUTTON_RIGHT] == 0 && _new_mouse_buttons[MOUSE_BUTTON_RIGHT] == 1;
}

void InputManager::GetMouseClientPosition(int& x_position, int& y_position) const
{
	x_position = _mouse_x_position;
	y_position = _mouse_y_position;
}

void InputManager::GetMouseDeltaPosition(int& x_delta, int& y_delta) const
{
	x_delta = _delta_mouse_x_position;
	y_delta = _delta_mouse_y_position;
}

bool InputManager::ValidKey(unsigned char key) const
{
	if (key < KEY_COUNT)
	{
		return true;
	}
	else
	{
		assert(false && "InputManager::ValidKey - Key not supporrted");
		return false;
	}
}
