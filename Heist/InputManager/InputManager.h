#pragma once

#include <Windows.h>

// Must be multiple of 8
#define KEY_COUNT 256

class InputManager
{
public:
	InputManager(void);
	~InputManager(void);

	void Initialize(HWND hwnd);
	void Update();
	void HideCursor(bool hide);

	// Mutators
	void OnKeyDown(unsigned char key);
	void OnKeyUp(unsigned char key);
	void OnLeftMouseButtonDown();
	void OnLeftMouseButtonUp();
	void OnRightMouseButtonDown();
	void OnRightMouseButtonUp();
	void OnMouseMove(int x_position, int y_position);

	// Accessors
	bool GetKeyDown(unsigned char key) const;
	bool GetKeyReleased(unsigned char key) const;
	bool GetKeyPressed(unsigned char key) const;
	bool GetLeftMouseDown() const;
	bool GetLeftMouseReleased() const;
	bool GetLeftMousePressed() const;
	bool GetRightMouseDown() const;
	bool GetRightMouseReleased() const;
	bool GetRightMousePressed() const;
	void GetMouseClientPosition(int& x_position, int& y_position) const;
	void GetMouseDeltaPosition(int& x_delta, int& y_delta) const;

private:
	bool ValidKey(unsigned char key) const;

	enum MouseButtons {MOUSE_BUTTON_LEFT, MOUSE_BUTTON_MIDDLE, MOUSE_BUTTON_RIGHT, NUM_MOUSE_BUTTONS};

	bool _cursor_hidden;

	unsigned char _old_keys[KEY_COUNT];
	unsigned char _new_keys[KEY_COUNT];
	unsigned char _old_mouse_buttons[MouseButtons::NUM_MOUSE_BUTTONS];
	unsigned char _new_mouse_buttons[MouseButtons::NUM_MOUSE_BUTTONS];

	int _mouse_x_position;
	int _mouse_y_position;

	mutable int _delta_mouse_x_position;
	mutable int _delta_mouse_y_position;

	// A handle to the window
	HWND _hwnd;
};

