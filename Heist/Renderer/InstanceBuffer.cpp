#include "InstanceBuffer.h"
#include "MeshObject.h"
#include "Renderer.h"
#include "../Logger/Log.h"

InstanceBuffer::InstanceBuffer() :
	_instances(nullptr),
	_number_of_instances(0),
	_instance_structure_byte_stride(0)
{
}


InstanceBuffer::~InstanceBuffer()
{
	delete [] _instances;
}

void InstanceBuffer::Initialize(Renderer* renderer, const D3D11_BUFFER_DESC& desc, int vs_slot, int gs_slot, int ps_slot)
{
	ConstantBuffer::Initialize(renderer, desc, vs_slot, gs_slot, ps_slot);

	_instance_structure_byte_stride = desc.StructureByteStride;

	_instances = new char[_instance_structure_byte_stride * MAX_INSTANCES];
}

void InstanceBuffer::AddInstance(const char* instance) const
{
	if (_number_of_instances < MAX_INSTANCES)
	{
		memcpy_s(&_instances[_number_of_instances * _instance_structure_byte_stride],
			_instance_structure_byte_stride,
			instance,
			_instance_structure_byte_stride);

		++_number_of_instances;
	}
	else
	{
		Log::Warning("InstanceBuffer::AddInstance - Instance buffer full\n");
	}
}

void InstanceBuffer::AddInstance(std::function<const char*()> get_instance_data_function) const
{
	_get_instance_data_funtions.push_back(get_instance_data_function);
}

void InstanceBuffer::RemoveAllInstance() const
{
	_number_of_instances = 0;

	_get_instance_data_funtions.clear();
}

void InstanceBuffer::Map() const
{
	// Go through all the deffered mesh object instances and get their instance data
	for (auto& get_instance_data_function : _get_instance_data_funtions)
	{
		AddInstance(get_instance_data_function());
	}

	_get_instance_data_funtions.clear();


	// Map the instance buffer
	if (_constant_buffer)
	{
		ConstantBuffer::Map(&_instances[0], _instance_structure_byte_stride * _number_of_instances);
	}
}

//void InstanceBuffer::UnMap() const
//{
//	// Clear the instance buffer
//	if (_constant_buffer)
//	{
//		memset(_instances, 0, _instance_structure_byte_stride * MAX_INSTANCES);
//
//		ConstantBuffer::Map(&_instances[0], _instance_structure_byte_stride * _number_of_instances);
//	}
//}
