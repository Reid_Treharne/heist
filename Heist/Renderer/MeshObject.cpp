#include "MeshObject.h"
#include "Renderer.h"
#include "VertexDeclarations.h"
#include "../Logger/Log.h"

MeshObject::MeshObject(void) :
	_vertex_buffer(nullptr),
	_index_buffer(nullptr),
	_vertex_structure_byte_stride(0),
	_index_structure_byte_stride(0),
	_vs_instance_buffer_slot(-1),
	_ps_instance_buffer_slot(-1)
{
}

MeshObject::~MeshObject(void)
{
}

void MeshObject::Initialize(Renderer* renderer, RENDER_TYPE render_type)
{
	_renderer = renderer;
	_render_type = render_type;
}

void MeshObject::SetVertexBuffer(const D3D11_BUFFER_DESC& desc, const void* vertex_data)
{
	_vertex_buffer = nullptr;

	D3D11_SUBRESOURCE_DATA subresource_data;
	memset(&subresource_data, 0, sizeof(subresource_data));

	subresource_data.pSysMem = vertex_data;

	_renderer->CreateBuffer(desc, &subresource_data, _vertex_buffer);

	_vertex_structure_byte_stride = desc.StructureByteStride;
}

void MeshObject::SetIndexBuffer(const D3D11_BUFFER_DESC& desc, const void* index_data)
{
	_index_buffer = nullptr;

	D3D11_SUBRESOURCE_DATA subresource_data;
	memset(&subresource_data, 0, sizeof(subresource_data));

	subresource_data.pSysMem = index_data;

	_renderer->CreateBuffer(desc, &subresource_data, _index_buffer);

	_index_structure_byte_stride = desc.StructureByteStride;
}

void MeshObject::AddInstanceBuffer(const D3D11_BUFFER_DESC& desc, int vs_slot, int gs_slot, int ps_slot, MeshObject::InstanceBufferID id)
{
	auto& iter = _instance_buffers.find(id);

	if (iter == _instance_buffers.end())
	{
		auto& instance_buffer = _instance_buffers[id];

		instance_buffer.Initialize(_renderer, desc, vs_slot, gs_slot, ps_slot);
	}
	else
	{
		Log::Error("MeshObject::AddInstanceBuffer - instance buffer alread exists for ID: %u\n", id);
	}
}

void MeshObject::AddConstantBuffer(const D3D11_BUFFER_DESC& desc, int vs_slot, int gs_slot, int ps_slot, MeshObject::ConstantBufferID id)
{
	auto& iter = _constant_buffers.find(id);

	if (iter == _constant_buffers.end())
	{
		auto& constant_buffer = _constant_buffers[id];

		constant_buffer.Initialize(_renderer, desc, vs_slot, gs_slot, ps_slot);
	}
	else
	{
		Log::Error("MeshObject::AddConstantBuffer - constant buffer alread exists for ID: %u\n", id);
	}
}

void MeshObject::MapConstantBuffer(const void* source, unsigned int source_size, MeshObject::ConstantBufferID id) const
{
	auto iter = _constant_buffers.find(id);

	if (iter != _constant_buffers.end())
	{
		auto& constant_buffer = iter->second;

		constant_buffer.Map(source, source_size);
	}
	else
	{
		Log::Warning("MeshObject::MapConstantBuffer - Constant buffer not found\n");
	}
}

void MeshObject::AddInstance(const char* instance, MeshObject::InstanceBufferID id) const
{
	auto iter = _instance_buffers.find(id);

	if (iter != _instance_buffers.end())
	{
		auto& instance_buffer = iter->second;

		instance_buffer.AddInstance(instance);
	}
	else
	{
		Log::Warning("MeshObject::AddInstance - Failed to find instance buffer with ID: %u\n", id);
	}
}

void MeshObject::AddInstance(std::function<const char*()> get_instance_data_function, MeshObject::InstanceBufferID id) const
{
	auto iter = _instance_buffers.find(id);

	if (iter != _instance_buffers.end())
	{
		auto& instance_buffer = iter->second;

		instance_buffer.AddInstance(get_instance_data_function);
	}
	else
	{
		Log::Warning("MeshObject::AddInstance - Failed to find instance buffer with ID: %u\n", id);
	}
}

void MeshObject::RemoveAllInstances() const
{
	for (auto& instance_buffer : _instance_buffers)
	{
		instance_buffer.second.RemoveAllInstance();
	}
}

bool MeshObject::HasInstanceBuffer(MeshObject::InstanceBufferID id) const
{
	return _instance_buffers.find(id) != _instance_buffers.end();
}

void MeshObject::Render() const
{
	if (HasStuffToRender())
	{
		for (auto& instance_buffer : _instance_buffers)
		{
			instance_buffer.second.Map();
		}

		_renderer->SetFormat(_render_type);

		// Get the index and vertex beffer descriptions
		D3D11_BUFFER_DESC vertex_buffer_desc;
		_vertex_buffer->GetDesc(&vertex_buffer_desc);

		unsigned int offset = 0;

		// Set the vertex buffer
		_renderer->_device_context->IASetVertexBuffers(0, 1, &_vertex_buffer.p, &_vertex_structure_byte_stride, &offset);

		if (_index_buffer)
		{
			D3D11_BUFFER_DESC index_buffer_desc;
			_index_buffer->GetDesc(&index_buffer_desc);

			// Set the index buffer
			_renderer->_device_context->IASetIndexBuffer(_index_buffer, DXGI_FORMAT_R32_UINT, 0);

			// Get the number of indices
			unsigned int num_indices = index_buffer_desc.ByteWidth / _index_structure_byte_stride;

			if (_instance_buffers.empty() == false)
			{
				// Draw this meshobject and all its instances.
				_renderer->_device_context->DrawIndexedInstanced(
					num_indices,
					_instance_buffers.at(MeshObject::INSTANCE_BUFFER_MAIN).GetNumberOfInstance(),
					0,
					0,
					0);
			}
			else
			{
				_renderer->_device_context->DrawIndexed(num_indices, 0, 0);
			}
		}
		else
		{
			_renderer->_device_context->Draw(vertex_buffer_desc.ByteWidth / _vertex_structure_byte_stride, 0);
		}

		//for (auto& instance_buffer : _instance_buffers)
		//{
		//	instance_buffer.second.UnMap();
		//}

		RemoveAllInstances();
	}
}

bool MeshObject::HasStuffToRender() const
{
  return (_instance_buffers.empty() == false && _instance_buffers.at(MeshObject::INSTANCE_BUFFER_MAIN).GetNumberOfInstance() > 0) || _instance_buffers.empty();
}