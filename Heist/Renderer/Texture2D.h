#pragma once

#include <atlbase.h> // for CComPtr
#include <d3d11.h>
#include <string>

class Texture2D
{
public:
	Texture2D(void);
	~Texture2D(void);

	unsigned int GetWidth() const;
	unsigned int GetHeight() const;

	CComPtr<ID3D11ShaderResourceView> GetShaderResourceView() const;

	void LoadTexture(CComPtr<ID3D11Device> device, const std::string& file_path);

private:
	void QueryDimensions() const;

	// the shader resource view
	CComPtr<ID3D11ShaderResourceView> _shader_resource_view;

	// whether or not the width and height have been set
	mutable bool _queried_deminsions;

	// the width and height of the texture.
	// Note: changing these will not change the actual width and height of the texture
	mutable unsigned int _width;
	mutable unsigned int _height;
};

