#pragma once

#include <DirectXMath.h>
using namespace DirectX;

struct VERTEX_POS
{
	XMFLOAT3 position;
};

struct VERTEX_POS_COL
{
	XMFLOAT3 position;
	XMFLOAT4 color;
};

struct VERTEX_POS_NORM_COL
{
	XMFLOAT3 position;
	XMFLOAT3 normal;
	XMFLOAT4 color;
};

struct VERTEX_POS_NORM_UV
{
	XMFLOAT3 position;
	XMFLOAT3 normal;
	XMFLOAT2 UV;
};

struct VERTEX_ORTHO_POS
{
	XMFLOAT2 position;
};

struct VERTEX_ORTHO_POS_UV
{
	XMFLOAT2 position;
	XMFLOAT2 UV;
};