#pragma once
#include "ConstantBuffer.h"
#include <functional>
#include <vector>

class InstanceBuffer : public ConstantBuffer
{
public:
	InstanceBuffer();
	~InstanceBuffer();

	void Initialize(Renderer* renderer, const D3D11_BUFFER_DESC& desc, int vs_slot, int gs_slot, int ps_slot);

	void AddInstance(const char* instance) const;
	void AddInstance(std::function<const char*()> get_instance_data_function) const;

	inline unsigned int GetNumberOfInstance() const { return _number_of_instances + _get_instance_data_funtions.size(); }

	void RemoveAllInstance() const;

	void Map() const;
	//void UnMap() const;
private:
	mutable char* _instances;
	mutable unsigned int _number_of_instances;
	unsigned int _instance_structure_byte_stride; // size of instance

	mutable std::vector<std::function<const char*()>> _get_instance_data_funtions;
};

