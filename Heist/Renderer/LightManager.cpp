#include "LightManager.h"
#include "VertexDeclarations.h"
#include "ShapeFactory.h"
#include "../Logger/Log.h"
#include "Renderer.h"

LightManager::LightManager(void) :
	_buffer(Buffer::DEFAULT)
{
}


LightManager::~LightManager(void)
{
}

void LightManager::Initialize(Renderer* renderer)
{
	_renderer = renderer;

	// Create the directional light mesh object along with the special buffer types
	std::string mesh_object_name = "Directional_Light";

	if (_lights.find(mesh_object_name) == _lights.end())
	{
		MeshObject& mesh_object = _lights[mesh_object_name];

		mesh_object.Initialize(_renderer, RENDER_TYPE::TYPE_DIRECTIONAL_LIGHT);

		// Initialize the normal, depth, and color buffer mesh objects
		_buffers[Buffer::NORMAL].Initialize(_renderer, RENDER_TYPE::TYPE_BUFFER_NORMAL);
		_buffers[Buffer::DEPTH].Initialize(_renderer, RENDER_TYPE::TYPE_BUFFER_DEPTH);
		_buffers[Buffer::COLOR].Initialize(_renderer, RENDER_TYPE::TYPE_BUFFER_COLOR);

		std::vector<VERTEX_ORTHO_POS> vertices;
		std::vector<unsigned int> indices;

		ShapeFactory::CreateQuad(vertices, indices, XMFLOAT2(-1.0f, 1.0f), XMFLOAT2(1.0f, -1.0f));

		D3D11_BUFFER_DESC desc;

		// Create vertex buffer
		desc.ByteWidth = sizeof(VERTEX_ORTHO_POS) * vertices.size();
		desc.CPUAccessFlags = NULL;
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		desc.Usage = D3D11_USAGE_IMMUTABLE;
		desc.MiscFlags = NULL;
		desc.StructureByteStride = sizeof(VERTEX_ORTHO_POS);

		mesh_object.SetVertexBuffer(desc, &vertices[0]);

		_buffers[Buffer::NORMAL].SetVertexBuffer(desc, &vertices[0]);
		_buffers[Buffer::DEPTH].SetVertexBuffer(desc, &vertices[0]);
		_buffers[Buffer::COLOR].SetVertexBuffer(desc, &vertices[0]);

		// Create index Buffer
		desc.ByteWidth = sizeof(unsigned int) * indices.size();
		desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		desc.StructureByteStride = sizeof(unsigned int);
		mesh_object.SetIndexBuffer(desc, &indices[0]);

		_buffers[Buffer::NORMAL].SetIndexBuffer(desc, &indices[0]);
		_buffers[Buffer::DEPTH].SetIndexBuffer(desc, &indices[0]);
		_buffers[Buffer::COLOR].SetIndexBuffer(desc, &indices[0]);

		// Create instance buffer
		desc.ByteWidth = sizeof(C_BUFFER_DIRECTIONAL_LIGHT) * MAX_INSTANCES;
		desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		desc.Usage = D3D11_USAGE_DYNAMIC;
		desc.StructureByteStride = sizeof(C_BUFFER_DIRECTIONAL_LIGHT);

		mesh_object.AddInstanceBuffer(desc, -1, -1, 0, MeshObject::INSTANCE_BUFFER_MAIN);
	}
	else
	{
		Log::Warning("LightManager::Initialize - Light already generated.\n");
	}
}

void LightManager::Render() const
{
	// If the buffer is set to something other than NONE, we dont want to render any of the lights so that
	// the data in the buffer can be rendered at be the correct color.
	if (_buffer == Buffer::DEFAULT)
	{
		for (auto& light : _lights)
		{
			light.second.Render();
		}
	}
	else
	{
		_buffers[_buffer].Render();

		// Remove all instances of lights created this frame so that the instance buffer doesn't overflow
		for (auto& light : _lights)
		{
			light.second.RemoveAllInstances();
		}
	}
}

MeshObject* LightManager::GetModel(const std::string& mesh_name)
{
	MeshObject* light = nullptr;

	auto iter = _lights.find(mesh_name);

	if (iter != _lights.end())
	{
		light = &iter->second;
	}

	return light;
}
