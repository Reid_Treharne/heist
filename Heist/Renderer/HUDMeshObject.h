#pragma once

#include <d3d11.h>
#include <atlbase.h> // for CComPtr
#include <string>

#include "Texture2D.h"
#include "MeshObject.h"

class Renderer;

class HUDMeshObject : public MeshObject
{
public:
	HUDMeshObject(void);
	~HUDMeshObject(void);
	
	// renderer					a pointer to the renderer
	// color_texture_filepath	the file path to the color texture
	// width					the width of the image from 0 to 1
	// height					the height of the image from 0 to 1
  void Initialize(Renderer* renderer, const std::string& color_texture_filepath, float width, float height);
	
  void Render();
private:

	// the color texture
	Texture2D _texture2D_color;
};

