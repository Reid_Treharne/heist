#pragma once

#include "VertexDeclarations.h"
#include "MeshObject.h"

#include <string>
#include <vector>
#include <unordered_map>
#include <array>
#include <functional>

struct aiMesh;
struct aiMaterial;

class MeshObjectTextured;

class ModelManager
{
public:
	ModelManager(void);
	~ModelManager(void);

	void Initialize(Renderer* renderer, std::function<void(const std::string&, XMFLOAT3[3])> for_each_triangle);

	void PreLoad(const std::string& directory);

	void Render() const;

	const std::vector<MeshObject*>* GetModel(const std::string& mesh_name);

	template <typename VERTEX_STRUCT>
	void SetIndices(
		const std::string& model_name,
		const aiMesh& mesh,
		const std::vector<VERTEX_STRUCT>& vertices,
		std::vector<unsigned int>& indices);

	// This function will add a new MeshObject to the specified model
	template<typename VERTEX_STRUCT, typename MESH_OBJECT_CLASS>
	MESH_OBJECT_CLASS* AddIndexedInstanced(
		const std::string& model_name,
		RENDER_TYPE render_type,
		const std::vector<VERTEX_STRUCT>& vertices,
		const std::vector<unsigned int>& indices);

private:
	void LoadModel(const std::string& file_path, const std::string& sub_directory);
	void LoadModelPosNormCol(const std::string& model_name, const aiMesh& mesh);
	MeshObjectTextured* LoadModelPosNormUV(const std::string& model_name, const aiMesh& mesh, bool render_on_top);
	MeshObjectTextured* LoadModelSkybox(const std::string& model_name, const aiMesh& mesh);


	// A pointer to the renderer
	Renderer* _renderer;

	std::unordered_map<std::string, std::vector<MeshObject*>> _models;

	std::function<void(const std::string&, XMFLOAT3[3])> _for_each_triangle;
};

template <typename VERTEX_STRUCT>
void ModelManager::SetIndices(
	const std::string& model_name,
	const aiMesh& mesh,
	const std::vector<VERTEX_STRUCT>& vertices,
	std::vector<unsigned int>& indices)
{
	for (unsigned int face = 0; face < mesh.mNumFaces; face++)
	{
		const aiFace& current_face = mesh.mFaces[face];

		XMFLOAT3 triangle[3];

		for (unsigned int index = 0; index < current_face.mNumIndices; index++)
		{
			auto i = current_face.mIndices[index];

			indices.push_back(i);

			triangle[index] = vertices[i].position;
		}

		_for_each_triangle(model_name, triangle);
	}
}

template <typename VERTEX_STRUCT, typename MESH_OBJECT_CLASS>
MESH_OBJECT_CLASS* ModelManager::AddIndexedInstanced(
	const std::string& model_name,
	RENDER_TYPE render_type,
	const std::vector<VERTEX_STRUCT>& vertices,
	const std::vector<unsigned int>& indices)
{
	D3D11_BUFFER_DESC vertex_buffer_desc;
	D3D11_BUFFER_DESC index_buffer_desc;
	D3D11_BUFFER_DESC instance_buffer_desc;
	D3D11_BUFFER_DESC render_flags_desc;

	memset(&vertex_buffer_desc, 0, sizeof(vertex_buffer_desc));
	memset(&index_buffer_desc, 0, sizeof(index_buffer_desc));
	memset(&instance_buffer_desc, 0, sizeof(instance_buffer_desc));
	memset(&render_flags_desc, 0, sizeof(render_flags_desc));

	// Create vertex buffer
	vertex_buffer_desc.ByteWidth = sizeof(VERTEX_STRUCT) * vertices.size();
	vertex_buffer_desc.CPUAccessFlags = NULL;
	vertex_buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertex_buffer_desc.Usage = D3D11_USAGE_IMMUTABLE;
	vertex_buffer_desc.MiscFlags = NULL;
	vertex_buffer_desc.StructureByteStride = sizeof(VERTEX_STRUCT);

	// Set index Buffer
	index_buffer_desc.ByteWidth = sizeof(unsigned int) * indices.size();
	index_buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	index_buffer_desc.StructureByteStride = sizeof(unsigned int);

	// Set the instance buffer
	instance_buffer_desc.ByteWidth = sizeof(C_BUFFER_INSTANCE_MESH_OBJECT) * MAX_INSTANCES;
	instance_buffer_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	instance_buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	instance_buffer_desc.Usage = D3D11_USAGE_DYNAMIC;
	instance_buffer_desc.StructureByteStride = sizeof(C_BUFFER_INSTANCE_MESH_OBJECT);

	// Set the instance buffer
	render_flags_desc.ByteWidth = sizeof(C_BUFFER_INSTANCE_RENDER_FLAGS) * MAX_INSTANCES;
	render_flags_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	render_flags_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	render_flags_desc.Usage = D3D11_USAGE_DYNAMIC;
	render_flags_desc.StructureByteStride = sizeof(C_BUFFER_INSTANCE_RENDER_FLAGS);

	auto mesh_object = new MESH_OBJECT_CLASS();

	auto& mesh_objects = _models[model_name];

	mesh_objects.push_back(mesh_object);

	mesh_object->Initialize(_renderer, render_type);

	mesh_object->SetVertexBuffer(vertex_buffer_desc, &vertices[0]);
	mesh_object->SetIndexBuffer(index_buffer_desc, &indices[0]);
	mesh_object->AddInstanceBuffer(instance_buffer_desc, 1, -1, 1, MeshObject::INSTANCE_BUFFER_MAIN);
	mesh_object->AddInstanceBuffer(render_flags_desc, 2, -1, 2, MeshObject::INSTANCE_BUFFER_RENDER_FLAGS);

	return mesh_object;
}
