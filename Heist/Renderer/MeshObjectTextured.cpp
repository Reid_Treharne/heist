#include "MeshObjectTextured.h"
#include "Renderer.h"

MeshObjectTextured::MeshObjectTextured(void)
{
}


MeshObjectTextured::~MeshObjectTextured(void)
{
}

void MeshObjectTextured::SetTexture(const std::string& file_name)
{
	_texture.LoadTexture(_renderer->_device, file_name);
}

void MeshObjectTextured::Render() const
{
	if (HasStuffToRender())
	{
		// set the shader resource views
		const unsigned int kNumShaderResourceViews = 1;

		if (kNumShaderResourceViews > 0)
		{
			ID3D11ShaderResourceView* const shader_resource_views[kNumShaderResourceViews] = {
				_texture.GetShaderResourceView()};

			_renderer->_device_context->PSSetShaderResources(0, kNumShaderResourceViews, &shader_resource_views[0]);
		}

		// Call parent's render
		MeshObject::Render();
	}
}
