#pragma once
#include "MeshObject.h"
#include "Texture2D.h"
#include "ConstantBuffer.h"
#include <string>
#include <vector>

class MeshObjectTexturedSlope : public MeshObject
{
public:
	MeshObjectTexturedSlope(void);
	virtual ~MeshObjectTexturedSlope(void) override;

	void Initialize(Renderer* renderer);

	virtual void Render() const override;

	void SetPrimaryTexture(const std::string& file_name);
	void SetSecondaryTexture(const std::string& file_name, float strength);

private:
	// the primary texture for the terrain
	Texture2D _primary_texture;
	Texture2D _secondary_texture;

	// 0 = not visible, 
	float _secondary_texture_strength;
};

