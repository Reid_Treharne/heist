#pragma once

#include <d3d11.h>
#pragma comment(lib, "d3d11.lib")

#include <atlbase.h> // for CComPtr

#include "RenderTypes.h"
#include "MeshObject.h"
#include "ConstantBuffer.h"
#include <functional>

class Renderer
{
	friend class MeshObject;
	friend class MeshObjectTextured;
	friend class MeshObjectTexturedSlope;
	friend class HUDMeshObject;
	friend class ConstantBuffer;
public:
	Renderer(void);
	~Renderer(void);

	void Initialize(unsigned width, unsigned height, HWND h_wnd, bool windowed);

	void ClearBackBuffer(const XMFLOAT4& clear_color);

	void Present();

	void CreateBuffer(const D3D11_BUFFER_DESC& desc,
		const D3D11_SUBRESOURCE_DATA* subresource_data,
		CComPtr<ID3D11Buffer> &);

	void MapBuffer(const CComPtr<ID3D11Buffer>& buffer, const void* source, unsigned int source_size) const;

	void SetFormat(RENDER_TYPE render_type);

	// Accessors
	unsigned int GetWindowHeight() const { return _window_height; }
	unsigned int GetWindowWidth() const { return _window_width; }
	void GetViewport(D3D11_VIEWPORT& viewport) const;

private:
	// This function should do nothing. It is called for objects that are not actually
	// rendered but whose render functions must be called to set data (like the camera).
	void SetFormatAsVoid();
	void SetFormatAsWireFrame();
	void SetFormatAsGroundClutter();
	void SetFormatAsPosCol();
	void SetFormatAsPosNormCol();
	void SetFormatAsPosNormUV();
	void SetFormatAsPosNormUVOnTop();
	void SetFormatAsPosNormUV_Slope();
	void SetFormatAsSkybox();
	void SetFormatAsDirectionlLight();
	void SetFormatBackBufferToPostProcess();
	void SetFormatPostProcessToBackBuffer();
	void SetFormatAsNormalBuffer();
	void SetFormatAsDepthBuffer();
	void SetFormatAsColorBuffer();
	void SetFormatAsHUD();

	void CreateVertexShaders();
	void CreatePixelShaders();
	void CreateGeometryShaders();
	void CreateInputLayouts();
	void CreateTextures();
	void CreateDepthStencilViews();
	void CreateRenderTargetViews();
	void CreateShaderResourceViews();
	void CreateDepthStencilStates();
	void CreateRasterizerStates();
	void CreateBlendStates();
	void CreateSamplersStates();

	unsigned int _window_width;
	unsigned int _window_height;

	CComPtr<ID3D11Device> _device;
	CComPtr<ID3D11DeviceContext> _device_context;
	CComPtr<IDXGISwapChain> _swap_chain;

	D3D11_VIEWPORT _viewport;

	// The input layouts
	CComPtr<ID3D11InputLayout> _input_layout_pos;
	CComPtr<ID3D11InputLayout> _input_layout_pos_col;
	CComPtr<ID3D11InputLayout> _input_layout_pos_norm_col;
	CComPtr<ID3D11InputLayout> _input_layout_pos_norm_uv;
	CComPtr<ID3D11InputLayout> _input_layout_ortho_pos;
	CComPtr<ID3D11InputLayout> _input_layout_ortho_pos_UV;

	// Backbuffer
	CComPtr<ID3D11Resource> _back_buffer;

	// The geometry buffers
	CComPtr<ID3D11Texture2D> _post_process_buffer;
	CComPtr<ID3D11Texture2D> _normal_buffer;
	CComPtr<ID3D11Texture2D> _color_buffer;
	CComPtr<ID3D11Texture2D> _depth_buffer;
	CComPtr<ID3D11Texture2D> _outline_buffer;

	// Render target views
	CComPtr<ID3D11RenderTargetView> _rtv_back_buffer;
	CComPtr<ID3D11RenderTargetView> _rtv_post_process;
	CComPtr<ID3D11RenderTargetView> _rtv_normal;
	CComPtr<ID3D11RenderTargetView> _rtv_color;
	CComPtr<ID3D11RenderTargetView> _rtv_outline;

	// Shader resource views
	CComPtr<ID3D11ShaderResourceView> _srv_back_buffer;
	CComPtr<ID3D11ShaderResourceView> _srv_post_process;
	CComPtr<ID3D11ShaderResourceView> _srv_normal;
	CComPtr<ID3D11ShaderResourceView> _srv_color;
	CComPtr<ID3D11ShaderResourceView> _srv_depth;
	CComPtr<ID3D11ShaderResourceView> _srv_outline;

	// Depth stencil views
	CComPtr<ID3D11DepthStencilView> _dsv_depth_buffer;

	// Depth stencil states
	CComPtr<ID3D11DepthStencilState> _dss_depth_test_disabled;
	CComPtr<ID3D11DepthStencilState> _dss_depth_test_enabled;

	// Rasterizer states
	CComPtr<ID3D11RasterizerState> _rs_cull_back;
	CComPtr<ID3D11RasterizerState> _rs_cull_front;
	CComPtr<ID3D11RasterizerState> _rs_cull_none;
	CComPtr<ID3D11RasterizerState> _rs_wire_frame;

	// Blend states
	CComPtr<ID3D11BlendState> _bs_source;
	CComPtr<ID3D11BlendState> _bs_alpha;
	CComPtr<ID3D11BlendState> _bs_additive;

	// Samplers state
	CComPtr<ID3D11SamplerState> _ss_wrap;

	// Vertex shaders
	CComPtr<ID3D11VertexShader> _vs_pos_col;
	CComPtr<ID3D11VertexShader> _vs_pos_norm_col;
	CComPtr<ID3D11VertexShader> _vs_pos_norm_uv;
	CComPtr<ID3D11VertexShader> _vs_pos_norm_uv_on_top;
	CComPtr<ID3D11VertexShader> _vs_ortho_pos;
	CComPtr<ID3D11VertexShader> _vs_HUD;
	CComPtr<ID3D11VertexShader> _vs_ground_clutter;
	CComPtr<ID3D11VertexShader> _vs_skybox;

	// Pixel shaders
	CComPtr<ID3D11PixelShader> _ps_pos_col;
	CComPtr<ID3D11PixelShader> _ps_pos_norm_col;
	CComPtr<ID3D11PixelShader> _ps_pos_norm_uv;
	CComPtr<ID3D11PixelShader> _ps_pos_norm_uv_outline;
	CComPtr<ID3D11PixelShader> _ps_pos_norm_uv_slope;
	CComPtr<ID3D11PixelShader> _ps_directional_light;
	CComPtr<ID3D11PixelShader> _ps_back_buffer_to_post_process;
	CComPtr<ID3D11PixelShader> _ps_post_process_to_back_buffer;
	CComPtr<ID3D11PixelShader> _ps_buffer_normal;
	CComPtr<ID3D11PixelShader> _ps_buffer_depth;
	CComPtr<ID3D11PixelShader> _ps_buffer_color;
	CComPtr<ID3D11PixelShader> _ps_HUD;
	CComPtr<ID3D11PixelShader> _ps_skybox;

	// Geometry shaders
	CComPtr<ID3D11GeometryShader> _gs_ground_clutter;

	std::function<void(void)> _render_settings[NUM_RENDER_TYPES];
};

