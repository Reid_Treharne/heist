#pragma once

#include "MeshObject.h"

class Renderer;

class PostProcessManager
{
public:
	PostProcessManager();
	~PostProcessManager();

	void Initialize(Renderer* renderer);
	void Render();

private:
	Renderer* _renderer;

	MeshObject _mesh_object_back_buffer_to_post_process; // the meshobject that renders to the post process buffer
	MeshObject _mesh_object_post_process_to_back_buffer; // the meshobject that renders back to the back buffer
};

