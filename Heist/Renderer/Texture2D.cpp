#include "Texture2D.h"
#include "../Logger/Log.h"

#pragma warning(push)
#pragma warning(disable:4005)
#include "DDSTextureLoader.h"
#pragma warning(pop)

#include <cassert>

Texture2D::Texture2D(void) :
	_shader_resource_view(nullptr),
	_queried_deminsions(false),
	_width(0),
	_height(0)
{
}


Texture2D::~Texture2D(void)
{
}

unsigned int Texture2D::GetWidth() const
{
	if (_queried_deminsions == false)
	{
		QueryDimensions();
	}

	return _width;
}

unsigned int Texture2D::GetHeight() const
{
	if (_queried_deminsions == false)
	{
		QueryDimensions();
	}

	return _height;
}

CComPtr<ID3D11ShaderResourceView> Texture2D::GetShaderResourceView() const
{
	return _shader_resource_view;
}

void Texture2D::LoadTexture(CComPtr<ID3D11Device> device, const std::string& file_path)
{
	std::wstring w_filepath(file_path.begin(), file_path.end());

	HRESULT hr = CreateDDSTextureFromFile(device, w_filepath.c_str(), NULL, &_shader_resource_view);

	if (hr != S_OK)
	{
		Log::Warning("Texture2D::LoadTexture - Failed to load texture: %s\n", file_path.c_str());
	}
}
void Texture2D::QueryDimensions() const
{
	if (_shader_resource_view != nullptr)
	{
		// Query and store the texture's dimensions
		ID3D11Resource* resource_interface = nullptr;
		ID3D11Texture2D* texture_interface = nullptr;
		D3D11_TEXTURE2D_DESC texture_desc;

		_shader_resource_view->GetResource(&resource_interface);
		resource_interface->QueryInterface(&texture_interface);
		texture_interface->GetDesc(&texture_desc);
		_width = texture_desc.Width;
		_height = texture_desc.Height;

		// Release the resources
		resource_interface->Release();
		texture_interface->Release();

		// Set queried deminsions to true
		_queried_deminsions = true;
	}
	else
	{
		Log::Warning("Texture2D::QueryDimensions - Shader resource view not yet initialized\n");
	}
}