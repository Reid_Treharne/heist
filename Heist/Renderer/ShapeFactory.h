#pragma once

#include <vector>
#include "VertexDeclarations.h"

class ShapeFactory
{
public:
	static void CreateCube(std::vector<VERTEX_POS_NORM_COL>& vertices,
		std::vector<unsigned int>& indices,
		float width,
		const XMFLOAT4& color);

	static void CreateQuad(std::vector<VERTEX_ORTHO_POS>& vertices,
		std::vector<unsigned int>& indices,
		const XMFLOAT2& top_left,
		const XMFLOAT2& bottom_right);

private:
	ShapeFactory(void);
	~ShapeFactory(void);
};

