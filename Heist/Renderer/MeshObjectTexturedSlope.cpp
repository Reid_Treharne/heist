#include "MeshObjectTexturedSlope.h"
#include "Renderer.h"

MeshObjectTexturedSlope::MeshObjectTexturedSlope(void) :
	_secondary_texture_strength(0.0f)
{
}


MeshObjectTexturedSlope::~MeshObjectTexturedSlope(void)
{
}

void MeshObjectTexturedSlope::Initialize(Renderer* renderer)
{
	//_constant_buffer.Initialize(renderer);

	MeshObject::Initialize(renderer, RENDER_TYPE::TYPE_POS_NORM_UV_SLOPE);

	D3D11_BUFFER_DESC desc;
	memset(&desc, 0, sizeof(desc));
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.ByteWidth = sizeof(C_BUFFER_MESH_OBJECT_TEXTURED_SLOPE);
	desc.StructureByteStride = sizeof(C_BUFFER_MESH_OBJECT_TEXTURED_SLOPE);

	this->AddConstantBuffer(desc, -1, -1, 0, MeshObject::CONSTANT_BUFFER_0);
}

void MeshObjectTexturedSlope::SetPrimaryTexture(const std::string& file_name)
{
	_primary_texture.LoadTexture(_renderer->_device, file_name);
}

void MeshObjectTexturedSlope::SetSecondaryTexture(const std::string& file_name, float strength)
{
	_secondary_texture.LoadTexture(_renderer->_device, file_name);

	_secondary_texture_strength = strength;
}

void MeshObjectTexturedSlope::Render() const
{
	if (HasStuffToRender())
	{
		// set the shader resource views
		const unsigned int kNumShaderResourceViews = 2;

		if (kNumShaderResourceViews > 0)
		{
			ID3D11ShaderResourceView* const shader_resource_views[kNumShaderResourceViews] = {
				_primary_texture.GetShaderResourceView(),
				_secondary_texture.GetShaderResourceView()};

			_renderer->_device_context->PSSetShaderResources(0, kNumShaderResourceViews, &shader_resource_views[0]);
		}

		// set the constant buffer
		C_BUFFER_MESH_OBJECT_TEXTURED_SLOPE constant_buffer;
		constant_buffer.secondary_texture_strength = _secondary_texture_strength;

		MapConstantBuffer(&constant_buffer, sizeof(constant_buffer), MeshObject::CONSTANT_BUFFER_0);

		// Call parent's render
		MeshObject::Render();
	}
}
