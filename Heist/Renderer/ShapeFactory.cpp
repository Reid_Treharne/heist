#include "ShapeFactory.h"


ShapeFactory::ShapeFactory(void)
{
}


ShapeFactory::~ShapeFactory(void)
{
}

void ShapeFactory::CreateCube(std::vector<VERTEX_POS_NORM_COL>&  vertices,
							  std::vector<unsigned int>& indices,
							  float width,
							  const XMFLOAT4& color)
{
	vertices.clear();
	vertices.reserve(24); // 6 sides * 4 vertices = 24

	indices.clear();
	indices.reserve(36); // 12 triangles * 3 vertices = 36

	float half_width = width * 0.5f;

	VERTEX_POS_NORM_COL current_vertex;

	current_vertex.color = color;

	// Front
	current_vertex.normal = XMFLOAT3(0.0f, 0.0f, -1.0f);

	current_vertex.position = XMFLOAT3(-half_width, 0.0f, -half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(-half_width, width, -half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(half_width, width, -half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(half_width, 0, -half_width);
	vertices.push_back(current_vertex);

	// Back
	current_vertex.color = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);

	current_vertex.normal = XMFLOAT3(0.0f, 0.0f, 1.0f);

	current_vertex.position = XMFLOAT3(half_width, 0, half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(half_width, width, half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(-half_width, width, half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(-half_width, 0, half_width);
	vertices.push_back(current_vertex);

	// Left
	current_vertex.color = color;

	current_vertex.normal = XMFLOAT3(-1.0f, 0.0f, 0.0f);

	current_vertex.position = XMFLOAT3(-half_width, 0, half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(-half_width, width, half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(-half_width, width, -half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(-half_width, 0, -half_width);
	vertices.push_back(current_vertex);

	// Right
	current_vertex.normal = XMFLOAT3(1.0f, 0.0f, 0.0f);

	current_vertex.position = XMFLOAT3(half_width, 0, -half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(half_width, width, -half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(half_width, width, half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(half_width, 0, half_width);
	vertices.push_back(current_vertex);

	// Top
	current_vertex.normal = XMFLOAT3(0.0f, 1.0f, 0.0f);

	current_vertex.position = XMFLOAT3(-half_width, width, -half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(-half_width, width, half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(half_width, width, half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(half_width, width, -half_width);
	vertices.push_back(current_vertex);

	// Bottom
	current_vertex.normal = XMFLOAT3(0.0f, -1.0f, 0.0f);

	current_vertex.position = XMFLOAT3(-half_width, 0, half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(-half_width, 0, -half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(half_width, 0, -half_width);
	vertices.push_back(current_vertex);

	current_vertex.position = XMFLOAT3(half_width, 0, half_width);
	vertices.push_back(current_vertex);

	// front
	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(2);

	indices.push_back(2);
	indices.push_back(3);
	indices.push_back(0);

	// back
	indices.push_back(7);
	indices.push_back(5);
	indices.push_back(6);

	indices.push_back(7);
	indices.push_back(4);
	indices.push_back(5);

	// left
	indices.push_back(8);
	indices.push_back(9);
	indices.push_back(10);

	indices.push_back(8);
	indices.push_back(10);
	indices.push_back(11);

	// right
	indices.push_back(12);
	indices.push_back(13);
	indices.push_back(14);

	indices.push_back(12);
	indices.push_back(14);
	indices.push_back(15);


	// top
	indices.push_back(16);
	indices.push_back(17);
	indices.push_back(18);

	indices.push_back(16);
	indices.push_back(18);
	indices.push_back(19);

	// bottom
	indices.push_back(20);
	indices.push_back(21);
	indices.push_back(22);

	indices.push_back(22);
	indices.push_back(23);
	indices.push_back(20);
}


void ShapeFactory::CreateQuad(std::vector<VERTEX_ORTHO_POS>& vertices,
							  std::vector<unsigned int>& indices,
							  const XMFLOAT2& top_left,
							  const XMFLOAT2& bottom_right)
{
	vertices.clear();
	vertices.reserve(4);

	indices.clear();
	indices.reserve(6);

	VERTEX_ORTHO_POS current_vertex;

	// top left
	current_vertex.position = top_left;
	vertices.push_back(current_vertex);

	// top right
	current_vertex.position = XMFLOAT2(bottom_right.x, top_left.y);
	vertices.push_back(current_vertex);

	// bottom left
	current_vertex.position = XMFLOAT2(top_left.x, bottom_right.y);
	vertices.push_back(current_vertex);

	// bottom right
	current_vertex.position = bottom_right;
	vertices.push_back(current_vertex);

	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(2);

	indices.push_back(1);
	indices.push_back(3);
	indices.push_back(2);
}