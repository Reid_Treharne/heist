#pragma once

#include <DirectXMath.h>
using namespace DirectX;

struct C_BUFFER_CAMERA
{
	XMFLOAT4X4 view_matrix;
	XMFLOAT4X4 projection_matrix;
};

struct C_BUFFER_INSTANCE_MESH_OBJECT
{
	XMFLOAT4X4 world_matrix;
};

enum RENDER_FLAG
{
	OUTLINE = 1 << 0,
	DRAW_ON_TOP = 1 << 1
};

struct C_BUFFER_INSTANCE_RENDER_FLAGS
{
	UINT32 render_flags;
	XMFLOAT3 outline_color;
};

struct C_BUFFER_DIRECTIONAL_LIGHT
{
	XMFLOAT4X4 world_matrix;
	XMFLOAT4 ambient_color;
	XMFLOAT4 diffuse_color;
};

struct C_BUFFER_MESH_OBJECT_TEXTURED_SLOPE
{
	FLOAT secondary_texture_strength;
	FLOAT padding1;
	FLOAT padding2;
	FLOAT padding3;
};

/*__declspec( align( 16 ) )*/struct C_BUFFER_INSTANCE_HUD_OBJECT
{
	XMFLOAT2 position;
	XMFLOAT2 UV_offset;
	XMFLOAT4 color;
};

struct C_BUFFER_SCREEN_RESOLUTION
{
	unsigned int screen_width;
	unsigned int screen_height;
	unsigned int padding1;
	unsigned int padding2;
};