#include "ConstantBuffer.h"
#include "Renderer.h"
#include "../Logger/Log.h"

ConstantBuffer::ConstantBuffer(void) :
	_renderer(nullptr),
	_constant_buffer(nullptr),
	_vs_slot(-1),
	_gs_slot(-1),
	_ps_slot(-1)
{
}

ConstantBuffer::~ConstantBuffer(void)
{
}

void ConstantBuffer::Initialize(Renderer* renderer, const D3D11_BUFFER_DESC& desc, int vs_slot, int gs_slot, int ps_slot)
{
	_renderer = renderer;

	_renderer->CreateBuffer(desc, nullptr, _constant_buffer);

	_vs_slot = vs_slot;
	_gs_slot = gs_slot;
	_ps_slot = ps_slot;
}

void ConstantBuffer::Map(const void* source, unsigned int source_size) const
{
	// Map the buffer
	_renderer->MapBuffer(_constant_buffer, source, source_size);

	if (_vs_slot > -1)
	{
		_renderer->_device_context->VSSetConstantBuffers(_vs_slot, 1, &_constant_buffer.p);
	}
	if (_gs_slot > -1)
	{
		_renderer->_device_context->GSSetConstantBuffers(_gs_slot, 1, &_constant_buffer.p);
	}
	if (_ps_slot > -1)
	{
		_renderer->_device_context->PSSetConstantBuffers(_ps_slot, 1, &_constant_buffer.p);
	}
}

void ConstantBuffer::BindBufferToVertexShader(unsigned int slot) const
{
	// bind the camera buffer to the vertex shader
	_renderer->_device_context->VSSetConstantBuffers(slot, 1, &_constant_buffer.p);
}

void ConstantBuffer::BindBufferToGeometryShader(unsigned int slot) const
{
	// bind the camera buffer to the geometry shader
	_renderer->_device_context->GSSetConstantBuffers(slot, 1, &_constant_buffer.p);
}

void ConstantBuffer::BindBufferToPixelShader(unsigned int slot) const
{
	// bind the camera buffer to the pixel shader
	_renderer->_device_context->PSSetConstantBuffers(slot, 1, &_constant_buffer.p);
}