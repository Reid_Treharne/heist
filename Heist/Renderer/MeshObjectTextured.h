#pragma once
#include "MeshObject.h"
#include "Texture2D.h"

class MeshObjectTextured : public MeshObject
{
public:
	MeshObjectTextured(void);
	virtual ~MeshObjectTextured(void) override;

	virtual void Render() const override;
	void SetTexture(const std::string& file_name);

private:
	Texture2D _texture;
};

