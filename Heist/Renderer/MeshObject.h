#pragma once

#include <d3d11.h>
#include <atlbase.h> // for CComPtr

#include <DirectXMath.h>
using namespace DirectX;

#include "ConstantBufferDeclarations.h"
#include "RenderTypes.h"
#include "InstanceBuffer.h"

#include <vector>
#include <functional>
#include <unordered_map>

class Renderer;

#define MAX_INSTANCES 10000

class MeshObject
{
public:
	MeshObject(void);
	virtual ~MeshObject(void);

	enum InstanceBufferID
	{
		INSTANCE_BUFFER_MAIN,
		INSTANCE_BUFFER_RENDER_FLAGS
	};

	enum ConstantBufferID
	{
		CONSTANT_BUFFER_0,
		CONSTANT_BUFFER_1,
	};

	void Initialize(Renderer* renderer, RENDER_TYPE render_type);

	void SetVertexBuffer(const D3D11_BUFFER_DESC& desc, const void* vertex_data);
	void SetIndexBuffer(const D3D11_BUFFER_DESC& desc, const void* index_data);
	//void SetConstantBuffer(const D3D11_BUFFER_DESC& desc, unsigned int unique_id);

	// desc		the buffer description.
	// vs_slot	the constant buffer slot in the vertex shader to assign the instance buffer to.
	//			pass -1 to bind to no slot.
	// gs_slot	the constant buffer slot in the geometry shader to assign the instance buffer to.
	//			pass -1 to bind to no slot.
	// vs_slot	the constant buffer slot in the pixel shader to assign the instance buffer to.
	//			pass -1 to bind to no slot.
	void AddInstanceBuffer(const D3D11_BUFFER_DESC& desc, int vs_slot, int gs_slot, int ps_slot, MeshObject::InstanceBufferID id);
	void AddConstantBuffer(const D3D11_BUFFER_DESC& desc, int vs_slot, int gs_slot, int ps_slot, MeshObject::ConstantBufferID id);

	void MapConstantBuffer(const void* source, unsigned int source_size, MeshObject::ConstantBufferID id) const;

	void AddInstance(const char* instance, MeshObject::InstanceBufferID id) const;
	void AddInstance(std::function<const char*()> get_instance_data_function, MeshObject::InstanceBufferID id) const;
	
	void RemoveAllInstances() const;

	bool HasInstanceBuffer(MeshObject::InstanceBufferID id) const;

	virtual void Render() const;

protected:
	bool HasStuffToRender() const;

	Renderer* _renderer;

	RENDER_TYPE _render_type;

	// vertex and index buffer
	CComPtr<ID3D11Buffer> _vertex_buffer;
	CComPtr<ID3D11Buffer> _index_buffer;
	
	// instance buffers
	std::unordered_map<MeshObject::InstanceBufferID, InstanceBuffer> _instance_buffers;

	// constant buffers
	std::unordered_map<MeshObject::ConstantBufferID, ConstantBuffer> _constant_buffers;

	unsigned int _vertex_structure_byte_stride;
	unsigned int _index_structure_byte_stride;

	// -1 to not bind
	int _vs_instance_buffer_slot;
	int _ps_instance_buffer_slot;
};

