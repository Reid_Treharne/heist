#pragma once

#include <unordered_map>
#include <string>
#include "MeshObject.h"

class Renderer;

class LightManager
{
public:
	LightManager(void);
	~LightManager(void);

	enum Buffer
	{
		NORMAL,
		DEPTH,
		COLOR,
		COUNT,

		DEFAULT,
	};

	void Initialize(Renderer* renderer);

	void Render() const;

	MeshObject* GetModel(const std::string& mesh_name);

	inline void SetBuffer(Buffer buffer) { _buffer = buffer; }

private:
	// A pointer to the renderer
	Renderer* _renderer;

	std::unordered_map<std::string, MeshObject> _lights;

	// Meshobjects for rendering the normal, depth, and color buffers
	MeshObject _buffers[Buffer::COUNT];

	// The current buffer to render
	Buffer _buffer;
};

