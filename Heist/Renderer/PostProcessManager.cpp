
#include "PostProcessManager.h"
#include "Renderer.h"
#include "VertexDeclarations.h"
#include "ShapeFactory.h"

PostProcessManager::PostProcessManager()
{
}


PostProcessManager::~PostProcessManager()
{
}

void PostProcessManager::Initialize(Renderer* renderer)
{
	_renderer = renderer;

	// Create the post process mesh
	_mesh_object_back_buffer_to_post_process.Initialize(_renderer, RENDER_TYPE::TYPE_BACK_BUFFER_TO_POST_PROCESS);
	_mesh_object_post_process_to_back_buffer.Initialize(_renderer, RENDER_TYPE::TYPE_POST_PROCESS_TO_BACK_BUFFER);

	std::vector<VERTEX_ORTHO_POS> vertices;
	std::vector<unsigned int> indices;

	ShapeFactory::CreateQuad(vertices, indices, XMFLOAT2(-1.0f, 1.0f), XMFLOAT2(1.0f, -1.0f));

	D3D11_BUFFER_DESC desc;

	// Create vertex buffer
	desc.ByteWidth = sizeof(VERTEX_ORTHO_POS) * vertices.size();
	desc.CPUAccessFlags = NULL;
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.Usage = D3D11_USAGE_IMMUTABLE;
	desc.MiscFlags = NULL;
	desc.StructureByteStride = sizeof(VERTEX_ORTHO_POS);

	_mesh_object_back_buffer_to_post_process.SetVertexBuffer(desc, &vertices[0]);
	_mesh_object_post_process_to_back_buffer.SetVertexBuffer(desc, &vertices[0]);

	// Create index Buffer
	desc.ByteWidth = sizeof(unsigned int) * indices.size();
	desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	desc.StructureByteStride = sizeof(unsigned int);

	_mesh_object_back_buffer_to_post_process.SetIndexBuffer(desc, &indices[0]);
	_mesh_object_post_process_to_back_buffer.SetIndexBuffer(desc, &indices[0]);

	// Create a constant buffer for the screen resolution
	memset(&desc, 0, sizeof(desc));
	desc.ByteWidth = sizeof(C_BUFFER_SCREEN_RESOLUTION);
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.StructureByteStride = sizeof(C_BUFFER_SCREEN_RESOLUTION);

	_mesh_object_back_buffer_to_post_process.AddConstantBuffer(desc, 0, -1, 0, MeshObject::CONSTANT_BUFFER_0);
}

void PostProcessManager::Render()
{
	C_BUFFER_SCREEN_RESOLUTION screen_resolution;
	
	screen_resolution.screen_width = _renderer->GetWindowWidth();
	screen_resolution.screen_height = _renderer->GetWindowHeight();

	_mesh_object_back_buffer_to_post_process.MapConstantBuffer(&screen_resolution, sizeof(screen_resolution), MeshObject::CONSTANT_BUFFER_0);

	// Render to post process buffer
	_mesh_object_back_buffer_to_post_process.Render();

	// Render back to back buffer
	_mesh_object_post_process_to_back_buffer.Render();
}

