#include "Renderer.h"
#include "HUDMeshObject.h"
#include "VertexDeclarations.h"

static const int kNumberVertices = 4;
static const int kNumberIndices = 6;

HUDMeshObject::HUDMeshObject(void)
{
}

HUDMeshObject::~HUDMeshObject(void)
{
}

void HUDMeshObject::Initialize(Renderer* renderer, const std::string& color_texture_filepath, float width, float height)
{
	MeshObject::Initialize(renderer, RENDER_TYPE::TYPE_HUD);

	// Create the vertex buffer
	D3D11_BUFFER_DESC desc;
	memset(&desc, 0, sizeof(desc));

	desc.ByteWidth = sizeof(VERTEX_ORTHO_POS_UV) * kNumberVertices;
	desc.CPUAccessFlags = NULL;
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.Usage = D3D11_USAGE_IMMUTABLE;
	desc.MiscFlags = NULL;
	desc.StructureByteStride = sizeof(VERTEX_ORTHO_POS_UV);

	// convert the width and height from (0, 1) to (-1, 1)
	float new_width = (width * 2.0f) - 1.0f;
	float new_height = (height * 2.0f) - 1.0f;

	// Create the vertices in clip space
	VERTEX_ORTHO_POS_UV vertices[4] = {
		// Vertex								// UV
		{XMFLOAT2(-1.0f, -new_height),			XMFLOAT2(0.0f, 1.0f)},
		{XMFLOAT2(-1.0f, 1.0f),					XMFLOAT2(0.0f, 0.0f)},
		{XMFLOAT2(new_width, -new_height),		XMFLOAT2(1.0f, 1.0f)},
		{XMFLOAT2(new_width, 1.0f),				XMFLOAT2(1.0f, 0.0f)}
	};

	SetVertexBuffer(desc, &vertices[0]);

	// Set index buffer
	desc.ByteWidth = sizeof(UINT) * kNumberIndices;
	desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	desc.StructureByteStride = sizeof(UINT);

	// Create the indices to match the vertices
	//		1--------3
	//		|		 |
	//		0--------2
	UINT indices[6] = {0, 1, 2,
		1, 3, 2};

	SetIndexBuffer(desc, &indices[0]);

	// Set the instance buffer
	memset(&desc, 0, sizeof(desc));
	desc.ByteWidth = sizeof(C_BUFFER_INSTANCE_HUD_OBJECT) * MAX_INSTANCES;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.StructureByteStride = sizeof(C_BUFFER_INSTANCE_HUD_OBJECT);

	AddInstanceBuffer(desc, 0, -1, -1, MeshObject::INSTANCE_BUFFER_MAIN);

	// Load the color texture
	_texture2D_color.LoadTexture(_renderer->_device, color_texture_filepath.c_str());
}

void HUDMeshObject::Render()
{
	if (HasStuffToRender())
	{
		// set the shader resource views
		const unsigned int kNumShaderResourceViews = 1;

		ID3D11ShaderResourceView* const shader_resource_views[kNumShaderResourceViews] = {
			_texture2D_color.GetShaderResourceView()};

		_renderer->_device_context->PSSetShaderResources(0, kNumShaderResourceViews, shader_resource_views);

		MeshObject::Render();
	}
}