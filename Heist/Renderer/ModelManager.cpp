#include "ModelManager.h"
#include "RenderTypes.h"
#include "MeshObjectTextured.h"
#include "../Logger/Log.h"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <stdio.h>
#include <dirent.h>

#pragma comment(lib, "assimp.lib")

static const auto kObjectsFolder = "Objects/";
static const auto kSkyboxFolder = "Skybox/";

ModelManager::ModelManager(void) :
	_renderer(nullptr),
	_for_each_triangle(nullptr)
{
}


ModelManager::~ModelManager(void)
{
	for (auto& model : _models)
	{
		for (auto& mesh_object : model.second)
		{
			delete mesh_object;
		}
	}

	_models.clear();
}

void ModelManager::Initialize(Renderer* renderer, std::function<void(const std::string&, XMFLOAT3[3])> for_each_triangle)
{
	_renderer = renderer;
	_for_each_triangle = for_each_triangle;
}

void ModelManager::PreLoad(const std::string& directory)
{
	auto preload = [this, &directory](const std::string& sub_directory)
	{
		std::string new_directory = directory + sub_directory;

		dirent *entry = nullptr;
		DIR *dir = nullptr;

		dir = opendir(new_directory.c_str());
		if (dir != nullptr)
		{
			while (entry = readdir(dir))
			{
				if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
				{
					continue;
				}
				std::string file_path = new_directory + entry->d_name;

				LoadModel(file_path, sub_directory);
			}

			closedir(dir);
		}
		else
		{
			Log::Warning("ModelManager::PreLoad - Invalid Directory: %s\n", new_directory.c_str());
		}
	};

	preload(kObjectsFolder);
	preload(kSkyboxFolder);
}

void ModelManager::Render() const
{
	for (auto& model : _models)
	{
		for(auto& mesh_object : model.second)
		{
			mesh_object->Render();
		}
	}
}

const std::vector<MeshObject*>* ModelManager::GetModel(const std::string& mesh_name)
{
	std::vector<MeshObject*>* mesh_objects = nullptr;

	auto mesh_object_iter = _models.find(mesh_name);

	if (mesh_object_iter != _models.end())
	{
		mesh_objects = &mesh_object_iter->second;
	}

	return mesh_objects;
}

void ModelManager::LoadModel(const std::string& file_path, const std::string& sub_directory)
{
	std::string file = PathFindFileNameA(file_path.c_str());
	std::string model_name = file.substr(0, file.find_first_of("."));

	Assimp::Importer _importer;

	const aiScene *scene = _importer.ReadFile(file_path, aiProcessPreset_TargetRealtime_Quality);

	if (scene != nullptr)
	{
		for (unsigned int i = 0; i < scene->mNumMeshes; i++)
		{
			const aiMesh& mesh = *scene->mMeshes[i];

			if (mesh.HasPositions())
			{
				auto& material = *scene->mMaterials[mesh.mMaterialIndex];

				auto num_diffuse_textures = material.GetTextureCount(aiTextureType_DIFFUSE);

				if (num_diffuse_textures > 0)
				{
					MeshObjectTextured* mesh_object_textured = nullptr;

					if (sub_directory == kSkyboxFolder)
					{
						mesh_object_textured = LoadModelSkybox(model_name, mesh);
					}
					else if (sub_directory == kObjectsFolder)
					{
						mesh_object_textured = LoadModelPosNormUV(model_name, mesh, false);
					}

					aiString texture_file_name;

					// TODO :: Handle multiple textures
					for (unsigned int texture_index = 0; texture_index < num_diffuse_textures; ++texture_index)
					{
						material.GetTexture(aiTextureType_DIFFUSE, texture_index, &texture_file_name);
					}

					mesh_object_textured->SetTexture("../../Resources/Models/" + std::string(texture_file_name.C_Str()));
				}
				else
				{
					LoadModelPosNormCol(model_name, mesh);
				}
			}
		}
	}
	else
	{
		Log::Warning("ModelManager::LoadModel - Invalid Model: %s\n", file_path);
	}
}

void ModelManager::LoadModelPosNormCol(const std::string& model_name, const aiMesh& mesh)
{
	std::vector<VERTEX_POS_NORM_COL> vertices;
	std::vector<unsigned int> indices;

	auto& texture_cords = mesh.mTextureCoords[0];

	VERTEX_POS_NORM_COL current_vertex;

	for(unsigned int vertex = 0; vertex < mesh.mNumVertices; vertex++)
	{
		// Note :: Ignoring color
		const aiVector3D& position = mesh.mVertices[vertex];
		const aiVector3D& normal = mesh.mNormals[vertex];

		current_vertex.position = XMFLOAT3(position.x, position.y, position.z);
		current_vertex.normal = XMFLOAT3(normal.x, normal.y, normal.z);
		current_vertex.color = XMFLOAT4(1.0f, 0.5f, 0.2f, 1.0f);
		vertices.push_back(current_vertex);
	}

	SetIndices<VERTEX_POS_NORM_COL>(model_name, mesh, vertices, indices);

	this->AddIndexedInstanced<VERTEX_POS_NORM_COL, MeshObject>(model_name,
		RENDER_TYPE::TYPE_POS_NORM_COL,
		vertices,
		indices);
}

MeshObjectTextured* ModelManager::LoadModelPosNormUV(const std::string& model_name, const aiMesh& mesh, bool render_on_top)
{
	std::vector<VERTEX_POS_NORM_UV> vertices;
	std::vector<unsigned int> indices;

	auto& texture_cords = mesh.mTextureCoords[0];

	VERTEX_POS_NORM_UV current_vertex;

	for(unsigned int vertex = 0; vertex < mesh.mNumVertices; vertex++)
	{
		// Note :: Ignoring color
		const aiVector3D& position = mesh.mVertices[vertex];
		const aiVector3D& normal = mesh.mNormals[vertex];
		const aiVector3D& uvs = texture_cords[vertex];

		current_vertex.position = XMFLOAT3(position.x, position.y, position.z);
		current_vertex.normal = XMFLOAT3(normal.x, normal.y, normal.z);
		current_vertex.UV = XMFLOAT2(uvs.x, -uvs.y);

		vertices.push_back(current_vertex);
	}

	SetIndices<VERTEX_POS_NORM_UV>(model_name, mesh, vertices, indices);

	auto mesh_object = this->AddIndexedInstanced<VERTEX_POS_NORM_UV, MeshObjectTextured>(
		model_name,
		render_on_top ? RENDER_TYPE::TYPE_POS_NORM_UV_ON_TOP : RENDER_TYPE::TYPE_POS_NORM_UV,
		vertices,
		indices);

	return mesh_object;
}

MeshObjectTextured* ModelManager::LoadModelSkybox(const std::string& model_name, const aiMesh& mesh)
{
	std::vector<VERTEX_POS> vertices;
	std::vector<unsigned int> indices;

	VERTEX_POS current_vertex;

	for (unsigned int vertex = 0; vertex < mesh.mNumVertices; vertex++)
	{
		const aiVector3D& position = mesh.mVertices[vertex];

		current_vertex.position = XMFLOAT3(position.x, position.y, position.z);

		vertices.push_back(current_vertex);
	}

	SetIndices<VERTEX_POS>(model_name, mesh, vertices, indices);

	auto mesh_object = this->AddIndexedInstanced<VERTEX_POS, MeshObjectTextured>(
		model_name,
		RENDER_TYPE::TYPE_SKYBOX,
		vertices,
		indices);

	return mesh_object;
}
