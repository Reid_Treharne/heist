#pragma once

#include <d3d11.h>
#include <atlbase.h> // for CComPtr

class Renderer;

class ConstantBuffer
{
public:
	ConstantBuffer();
	virtual ~ConstantBuffer(void);

	void Initialize(Renderer* renderer, const D3D11_BUFFER_DESC& desc, int vs_slot, int gs_slot, int ps_slot);

	void Map(const void* source, unsigned int source_size) const;

	void BindBufferToVertexShader(unsigned int slot) const;
	void BindBufferToGeometryShader(unsigned int slot) const;
	void BindBufferToPixelShader(unsigned int slot) const;

protected:
	Renderer* _renderer;
	// constant buffer
	CComPtr<ID3D11Buffer> _constant_buffer;

	int _vs_slot;
	int _gs_slot;
	int _ps_slot;
};

