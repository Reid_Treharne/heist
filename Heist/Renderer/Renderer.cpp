#include "Renderer.h"
#include <cassert>

// vertex shaders
#include "../Renderer/CompiledShaderHeaders/VS_PosCol.csh"
#include "../Renderer/CompiledShaderHeaders/VS_PosNormCol.csh"
#include "../Renderer/CompiledShaderHeaders/VS_PosNormUV.csh"
#include "../Renderer/CompiledShaderHeaders/VS_PosNormUVOnTop.csh"
#include "../Renderer/CompiledShaderHeaders/VS_OrthoPos.csh"
#include "../Renderer/CompiledShaderHeaders/VS_HUD.csh"
#include "../Renderer/CompiledShaderHeaders/VS_GroundClutter.csh"
#include "../Renderer/CompiledShaderHeaders/VS_Skybox.csh"

// pixel shaders
#include "../Renderer/CompiledShaderHeaders/PS_PosCol.csh"
#include "../Renderer/CompiledShaderHeaders/PS_PosNormCol.csh"
#include "../Renderer/CompiledShaderHeaders/PS_PosNormUV.csh"
#include "../Renderer/CompiledShaderHeaders/PS_PosNormUVOutline.csh"
#include "../Renderer/CompiledShaderHeaders/PS_PosNormUVSlope.csh"
#include "../Renderer/CompiledShaderHeaders/PS_DirectionalLight.csh"
#include "../Renderer/CompiledShaderHeaders/PS_BackBufferToPostProcess.csh"
#include "../Renderer/CompiledShaderHeaders/PS_PostProcessToBackBuffer.csh"
#include "../Renderer/CompiledShaderHeaders/PS_HUD.csh"
#include "../Renderer/CompiledShaderHeaders/PS_BufferNormal.csh"
#include "../Renderer/CompiledShaderHeaders/PS_BufferDepth.csh"
#include "../Renderer/CompiledShaderHeaders/PS_BufferColor.csh"
#include "../Renderer/CompiledShaderHeaders/PS_Skybox.csh"

// geometry shaders
#include "../Renderer/CompiledShaderHeaders/GS_GroundClutter.csh"

#include <DXGIDebug.h>

// for D3D_SET_OBJECT_NAME_A macro
#pragma comment(lib, "dxguid.lib")
//#pragma comment(lib, "dxgi.lib")
//#pragma comment(lib, "DXGIDebug.lib")

Renderer::Renderer(void) :
	_window_width(0),
	_window_height(0),
	_device(nullptr),
	_device_context(nullptr),
	_swap_chain(nullptr),
	_input_layout_pos(nullptr),
	_input_layout_pos_col(nullptr),
	_input_layout_pos_norm_col(nullptr),
	_input_layout_pos_norm_uv(nullptr),
	_input_layout_ortho_pos(nullptr),
	_input_layout_ortho_pos_UV(nullptr),
	_back_buffer(nullptr),
	_normal_buffer(nullptr),
	_post_process_buffer(nullptr),
	_color_buffer(nullptr),
	_depth_buffer(nullptr),
	_outline_buffer(nullptr),
	_rtv_back_buffer(nullptr),
	_rtv_post_process(nullptr),
	_rtv_color(nullptr),
	_rtv_outline(nullptr),
	_rtv_normal(nullptr),
	_srv_back_buffer(nullptr),
	_srv_post_process(nullptr),
	_srv_normal(nullptr),
	_srv_color(nullptr),
	_srv_depth(nullptr),
	_srv_outline(nullptr),
	_dsv_depth_buffer(nullptr),
	_dss_depth_test_disabled(nullptr),
	_dss_depth_test_enabled(nullptr),
	_rs_cull_back(nullptr),
	_rs_cull_front(nullptr),
	_rs_cull_none(nullptr),
	_rs_wire_frame(nullptr),
	_bs_source(nullptr),
	_bs_alpha(nullptr),
	_bs_additive(nullptr),
	_ss_wrap(nullptr),
	_vs_pos_col(nullptr),
	_vs_pos_norm_col(nullptr),
	_vs_pos_norm_uv(nullptr),
	_vs_pos_norm_uv_on_top(nullptr),
	_vs_ortho_pos(nullptr),
	_vs_HUD(nullptr),
	_vs_ground_clutter(nullptr),
	_vs_skybox(nullptr),
	_ps_pos_norm_col(nullptr),
	_ps_pos_norm_uv(nullptr),
	_ps_pos_norm_uv_outline(nullptr),
	_ps_pos_norm_uv_slope(nullptr),
	_ps_directional_light(nullptr),
	_ps_back_buffer_to_post_process(nullptr),
	_ps_post_process_to_back_buffer(nullptr),
	_ps_buffer_normal(nullptr),
	_ps_buffer_depth(nullptr),
	_ps_buffer_color(nullptr),
	_ps_HUD(nullptr),
	_ps_skybox(nullptr),
	_gs_ground_clutter(nullptr)
{
	memset(&_viewport, 0, sizeof(_viewport));

	_render_settings[RENDER_TYPE::TYPE_NOT_RENDERED] = nullptr;
	_render_settings[RENDER_TYPE::TYPE_VOID] = std::bind(&Renderer::SetFormatAsVoid, this);
	_render_settings[RENDER_TYPE::TYPE_WIRE_FRAME] = std::bind(&Renderer::SetFormatAsWireFrame, this);
	_render_settings[RENDER_TYPE::TYPE_GROUND_CLUTTER] = std::bind(&Renderer::SetFormatAsGroundClutter, this);
	_render_settings[RENDER_TYPE::TYPE_POS_COL] = std::bind(&Renderer::SetFormatAsPosCol, this);
	_render_settings[RENDER_TYPE::TYPE_POS_NORM_COL] = std::bind(&Renderer::SetFormatAsPosNormCol, this);
	_render_settings[RENDER_TYPE::TYPE_POS_NORM_UV] = std::bind(&Renderer::SetFormatAsPosNormUV, this);
	_render_settings[RENDER_TYPE::TYPE_POS_NORM_UV_ON_TOP] = std::bind(&Renderer::SetFormatAsPosNormUVOnTop, this);
	_render_settings[RENDER_TYPE::TYPE_POS_NORM_UV_SLOPE] = std::bind(&Renderer::SetFormatAsPosNormUV_Slope, this);
	_render_settings[RENDER_TYPE::TYPE_SKYBOX] = std::bind(&Renderer::SetFormatAsSkybox, this);
	_render_settings[RENDER_TYPE::TYPE_DIRECTIONAL_LIGHT] = std::bind(&Renderer::SetFormatAsDirectionlLight, this);
	_render_settings[RENDER_TYPE::TYPE_BACK_BUFFER_TO_POST_PROCESS] = std::bind(&Renderer::SetFormatBackBufferToPostProcess, this);
	_render_settings[RENDER_TYPE::TYPE_POST_PROCESS_TO_BACK_BUFFER] = std::bind(&Renderer::SetFormatPostProcessToBackBuffer, this);
	_render_settings[RENDER_TYPE::TYPE_BUFFER_NORMAL] = std::bind(&Renderer::SetFormatAsNormalBuffer, this);
	_render_settings[RENDER_TYPE::TYPE_BUFFER_DEPTH] = std::bind(&Renderer::SetFormatAsDepthBuffer, this);
	_render_settings[RENDER_TYPE::TYPE_BUFFER_COLOR] = std::bind(&Renderer::SetFormatAsColorBuffer, this);
	_render_settings[RENDER_TYPE::TYPE_HUD] = std::bind(&Renderer::SetFormatAsHUD, this);
}


Renderer::~Renderer(void)
{
}

void Renderer::Initialize(unsigned width, unsigned height, HWND h_wnd, bool windowed)
{
	_window_width = width;
	_window_height = height;

	DXGI_SWAP_CHAIN_DESC swap_chain_desc;
	memset(&swap_chain_desc, 0, sizeof(swap_chain_desc));

	swap_chain_desc.BufferCount = 1;
	swap_chain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swap_chain_desc.BufferDesc.Width = width;
	swap_chain_desc.BufferDesc.Height = height;
	swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_SHADER_INPUT;
	swap_chain_desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	swap_chain_desc.OutputWindow = h_wnd;
	swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swap_chain_desc.Windowed = windowed;
	swap_chain_desc.SampleDesc.Count = 1;
	swap_chain_desc.SampleDesc.Quality = 0;

	unsigned num_levels = 7;

	D3D_FEATURE_LEVEL* feature_levels = new D3D_FEATURE_LEVEL[num_levels];

	feature_levels[0] = D3D_FEATURE_LEVEL_11_1;
	feature_levels[1] = D3D_FEATURE_LEVEL_11_0;
	feature_levels[2] = D3D_FEATURE_LEVEL_10_1;
	feature_levels[3] = D3D_FEATURE_LEVEL_10_0;
	feature_levels[4] = D3D_FEATURE_LEVEL_9_3;
	feature_levels[5] = D3D_FEATURE_LEVEL_9_2;
	feature_levels[6] = D3D_FEATURE_LEVEL_9_1;

	D3D_FEATURE_LEVEL out_feature_level;

	HRESULT hr = D3D11CreateDeviceAndSwapChain(
		NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
#ifdef _DEBUG
		D3D11_CREATE_DEVICE_DEBUG,
#else
		NULL,
#endif
		feature_levels,
		num_levels,
		D3D11_SDK_VERSION,
		&swap_chain_desc,
		&_swap_chain,
		&_device,
		&out_feature_level,
		&_device_context);

	delete [] feature_levels;

	assert(hr == S_OK && "Renderer::Initialize - D3D11CreateDeviceAndSwapChain failed");

	// Set the view port
	_viewport.MinDepth = 0.0f;
	_viewport.MaxDepth = 1.0f;
	_viewport.TopLeftX = 0.0f;
	_viewport.TopLeftY = 0.0f;
	_viewport.Width = (float)width;
	_viewport.Height = (float)height;

	_device_context->RSSetViewports(1, &_viewport);
	// Create all the vertex and pixel shaders
	CreateVertexShaders();
	CreateGeometryShaders();
	CreatePixelShaders();
	// Create all the input layouts
	CreateInputLayouts();
	// Create all the textures (buffers)
	CreateTextures();
	// Create all depth stencil views
	CreateDepthStencilViews();

	// Get the back buffer from the swap chain
	hr = _swap_chain->GetBuffer(0, __uuidof(_back_buffer.p), reinterpret_cast<void**>(&_back_buffer.p));
	assert(hr == S_OK && "Renderer::Initialize - GetBuffer(back_buffer) failed");

	// Create all the render target views
	CreateRenderTargetViews();
	// Create all the shader resource views
	CreateShaderResourceViews();
	// Create al the depth stencil states
	CreateDepthStencilStates();
	// Create all the rasterizer states
	CreateRasterizerStates();
	// Create all the blend states
	CreateBlendStates();
	// Create all the sampler states
	CreateSamplersStates();
}

void Renderer::ClearBackBuffer(const XMFLOAT4& clear_color)
{
	const float xm_clear_color[4] = {clear_color.x, clear_color.y, clear_color.z, clear_color.w};

	// Clear the back buffer
	_device_context->ClearRenderTargetView(_rtv_back_buffer, xm_clear_color);

	// Cleat the geometry buffers
	_device_context->ClearRenderTargetView(_rtv_post_process, xm_clear_color);
	_device_context->ClearRenderTargetView(_rtv_normal, xm_clear_color);
	_device_context->ClearRenderTargetView(_rtv_color, xm_clear_color);
	_device_context->ClearRenderTargetView(_rtv_outline, xm_clear_color);

	// Cleat the depth stencil view
	_device_context->ClearDepthStencilView(_dsv_depth_buffer, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1, 1);

	// Sicne this is a deferred renderer, the shader resources for depths, normals, and colors are
	// first written to and then read from. Before we begin are next render pass we need to make
	// sure they are all set back to null so they can be written to again.
	const unsigned int kNumShaderResourceViews = 4;

	ID3D11ShaderResourceView* const shader_resource_views[kNumShaderResourceViews] = {
		nullptr,
		nullptr,
		nullptr,
		nullptr};

	_device_context->PSSetShaderResources(0, kNumShaderResourceViews, shader_resource_views);
}

void Renderer::Present()
{
	// Present the scene to the back buffer
	_swap_chain->Present(0, 0);
}

void Renderer::CreateBuffer(const D3D11_BUFFER_DESC& desc,
							const D3D11_SUBRESOURCE_DATA* subresource_data,
							CComPtr<ID3D11Buffer> &ptr)
{
	HRESULT hr = _device->CreateBuffer(&desc, subresource_data, &ptr);

	assert(hr == S_OK && "Renderer::CreateBuffer - CreateBuffer failed");
}

void Renderer::MapBuffer(const CComPtr<ID3D11Buffer>& buffer, const void* source, unsigned int source_size) const
{
	HRESULT hr;

	D3D11_MAPPED_SUBRESOURCE subresource;
	memset(&subresource, 0, sizeof(subresource));

	// map the buffer
	hr = _device_context->Map(buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &subresource);
	assert(hr == S_OK && "Renderer::MapBuffer - Map failed");

	// set the subresouce data
	memcpy_s(subresource.pData, source_size, source, source_size);

	// unmap the buffer
	_device_context->Unmap(buffer, 0);
}

void Renderer::SetFormat(RENDER_TYPE render_type)
{
	if (render_type >= 0 && render_type < RENDER_TYPE::NUM_RENDER_TYPES)
	{
		_render_settings[render_type]();
	}
}

void Renderer::GetViewport(D3D11_VIEWPORT& viewport) const
{
	viewport = _viewport;
}

void Renderer::SetFormatAsVoid()
{
	// *** Do nothing ***
	// See .h for explanation
}

void Renderer::SetFormatAsWireFrame()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_pos_col, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_pos_col, nullptr, 0);

	// set rasterizer state to back face culling
	_device_context->RSSetState(_rs_wire_frame);

	// set default blend state
	_device_context->OMSetBlendState(nullptr, nullptr, 0xFFFFFFFF);

	// set render target views
	const unsigned int kNumRenderTargets = 2;
	ID3D11RenderTargetView* const render_target_views[kNumRenderTargets] = {
		_rtv_normal,
		_rtv_color};

	_device_context->OMSetRenderTargets(kNumRenderTargets, &render_target_views[0], _dsv_depth_buffer);

	// set the input layout
	_device_context->IASetInputLayout(_input_layout_pos_col);

	// enable depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_enabled, 0);

	// set primitive topology
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void Renderer::SetFormatAsGroundClutter()
{
	// Set the appropriate shaders
	_device_context->VSSetShader(_vs_ground_clutter, nullptr, 0);
	_device_context->GSSetShader(_gs_ground_clutter, nullptr, 0);
	_device_context->PSSetShader(_ps_pos_norm_uv, nullptr, 0);

	// set rasterizer state to back face culling
	_device_context->RSSetState(_rs_cull_none);

	// set default blend state
	_device_context->OMSetBlendState(nullptr, nullptr, 0xFFFFFFFF);

	// set render target views
	const unsigned int kNumRenderTargets = 3;
	ID3D11RenderTargetView* const render_target_views[kNumRenderTargets] = {
		_rtv_normal,
		_rtv_color,
		_rtv_outline };

	_device_context->OMSetRenderTargets(kNumRenderTargets, &render_target_views[0], _dsv_depth_buffer);

	// set the input layout
	_device_context->IASetInputLayout(_input_layout_pos);

	// enable depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_enabled, 0);

	// set primitive topology
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
}

void Renderer::SetFormatAsPosCol()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_pos_col, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_pos_col, nullptr, 0);

	// set rasterizer state to back face culling
	_device_context->RSSetState(_rs_cull_back);

	// set default blend state
	_device_context->OMSetBlendState(nullptr, nullptr, 0xFFFFFFFF);

	// set render target views
	const unsigned int kNumRenderTargets = 3;
	ID3D11RenderTargetView* const render_target_views[kNumRenderTargets] = {
		_rtv_normal,
		_rtv_color,
		_rtv_outline };

	_device_context->OMSetRenderTargets(kNumRenderTargets, &render_target_views[0], _dsv_depth_buffer);

	// set the input layout
	_device_context->IASetInputLayout(_input_layout_pos_col);

	// enable depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_enabled, 0);

	// set primitive topology
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void Renderer::SetFormatAsPosNormCol()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_pos_norm_col, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_pos_norm_col, nullptr, 0);

	// set rasterizer state to back face culling
	_device_context->RSSetState(_rs_cull_back);

	// set default blend state
	_device_context->OMSetBlendState(nullptr, nullptr, 0xFFFFFFFF);

	// set render target views
	const unsigned int kNumRenderTargets = 3;
	ID3D11RenderTargetView* const render_target_views[kNumRenderTargets] = {
		_rtv_normal,
		_rtv_color,
		_rtv_outline };

	_device_context->OMSetRenderTargets(kNumRenderTargets, &render_target_views[0], _dsv_depth_buffer);

	// set the input layout
	_device_context->IASetInputLayout(_input_layout_pos_norm_col);

	// enable depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_enabled, 0);

	// set primitive topology
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void Renderer::SetFormatAsPosNormUV()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_pos_norm_uv, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_pos_norm_uv_outline, nullptr, 0);

	// set rasterizer state to back face culling
	_device_context->RSSetState(_rs_cull_back);

	// set default blend state
	_device_context->OMSetBlendState(nullptr, nullptr, 0xFFFFFFFF);

	// set render target views
	const unsigned int kNumRenderTargets = 3;
	ID3D11RenderTargetView* const render_target_views[kNumRenderTargets] = {
		_rtv_normal,
		_rtv_color,
		_rtv_outline};

	_device_context->OMSetRenderTargets(kNumRenderTargets, &render_target_views[0], _dsv_depth_buffer);

	// set the input layout
	_device_context->IASetInputLayout(_input_layout_pos_norm_uv);

	// enable depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_enabled, 0);

	// set primitive topology
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// set the sampler states to wrap for the primary and secondary textures
	_device_context->PSSetSamplers(0, 1, &_ss_wrap.p);
}

void Renderer::SetFormatAsPosNormUVOnTop()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_pos_norm_uv_on_top, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_pos_norm_uv, nullptr, 0);

	// set rasterizer state to back face culling
	_device_context->RSSetState(_rs_cull_back);

	// set default blend state
	_device_context->OMSetBlendState(nullptr, nullptr, 0xFFFFFFFF);

	// set render target views
	const unsigned int kNumRenderTargets = 3;
	ID3D11RenderTargetView* const render_target_views[kNumRenderTargets] = {
		_rtv_normal,
		_rtv_color,
		_rtv_outline };

	_device_context->OMSetRenderTargets(kNumRenderTargets, &render_target_views[0], _dsv_depth_buffer);

	// set the input layout
	_device_context->IASetInputLayout(_input_layout_pos_norm_uv);

	// enable depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_enabled, 0);

	// set primitive topology
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// set the sampler states to wrap for the primary and secondary textures
	_device_context->PSSetSamplers(0, 1, &_ss_wrap.p);
}

void Renderer::SetFormatAsPosNormUV_Slope()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_pos_norm_uv, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_pos_norm_uv_slope, nullptr, 0);

	// set rasterizer state to back face culling
	_device_context->RSSetState(_rs_cull_back);

	// set default blend state
	_device_context->OMSetBlendState(nullptr, nullptr, 0xFFFFFFFF);

	// set render target views
	const unsigned int kNumRenderTargets = 3;
	ID3D11RenderTargetView* const render_target_views[kNumRenderTargets] = {
		_rtv_normal,
		_rtv_color,
		_rtv_outline };

	_device_context->OMSetRenderTargets(kNumRenderTargets, &render_target_views[0], _dsv_depth_buffer);

	// set the input layout
	_device_context->IASetInputLayout(_input_layout_pos_norm_uv);

	// enable depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_enabled, 0);

	// set primitive topology
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// set the sampler states to wrap for the primary and secondary textures
	_device_context->PSSetSamplers(0, 1, &_ss_wrap.p);
}

void Renderer::SetFormatAsSkybox()
{
	// Set the appropriate shaders
	_device_context->VSSetShader(_vs_skybox, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_skybox, nullptr, 0);

	// set rasterizer state to front facing to only render the inside of the skybox
	_device_context->RSSetState(_rs_cull_front);

	// set default blend state
	_device_context->OMSetBlendState(nullptr, nullptr, 0xFFFFFFFF);

	// set render target views
	const unsigned int kNumRenderTargets = 1;
	ID3D11RenderTargetView* const render_target_views[kNumRenderTargets] = {
		_rtv_color
	};

	_device_context->OMSetRenderTargets(kNumRenderTargets, &render_target_views[0], _dsv_depth_buffer);

	// set the input layout
	_device_context->IASetInputLayout(_input_layout_pos);

	// enable depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_enabled, 0);

	// set primitive topology
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void Renderer::SetFormatAsDirectionlLight()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_ortho_pos, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_directional_light, nullptr, 0);

	// Set rasterizer state to normal
	_device_context->RSSetState(_rs_cull_back);

	// set blend state to additive
	_device_context->OMSetBlendState(_bs_source, nullptr, 0xFFFFFFFF);

	// set render target to back buffer and disable writes to depth buffer
	_device_context->OMSetRenderTargets(1, &_rtv_back_buffer.p, nullptr);

	// set the shader resource views
	const unsigned int kNumShaderResourceViews = 4;

	ID3D11ShaderResourceView* const shader_resource_views[kNumShaderResourceViews] = {
		_srv_depth,
		_srv_normal,
		_srv_color,
		_srv_outline };

	_device_context->PSSetShaderResources(0, kNumShaderResourceViews, shader_resource_views);

	// set the input layout to orthogonal position
	_device_context->IASetInputLayout(_input_layout_ortho_pos);

	// disbale depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_disabled, 0);

	// set primitive topology to triangle list
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// set the sampler state to wrap
	_device_context->PSSetSamplers(0, 1, &_ss_wrap.p);
}

void Renderer::SetFormatBackBufferToPostProcess()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_ortho_pos, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_back_buffer_to_post_process, nullptr, 0);

	// Set rasterizer state to normal
	_device_context->RSSetState(_rs_cull_back);

	// set blend state to additive
	_device_context->OMSetBlendState(_bs_source, nullptr, 0xFFFFFFFF);

	// set render target to the post process buffer
	_device_context->OMSetRenderTargets(1, &_rtv_post_process.p, nullptr);

	// set the shader resource views
	const unsigned int kNumShaderResourceViews = 3;

	ID3D11ShaderResourceView* const shader_resource_views[kNumShaderResourceViews] = {
		_srv_back_buffer,
		_srv_depth,
		_srv_outline};

	_device_context->PSSetShaderResources(0, kNumShaderResourceViews, shader_resource_views);

	// set the input layout to orthogonal position
	_device_context->IASetInputLayout(_input_layout_ortho_pos);

	// disbale depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_disabled, 0);

	// set primitive topology to triangle list
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// set the sampler state to wrap
	_device_context->PSSetSamplers(0, 1, &_ss_wrap.p);
}

void Renderer::SetFormatPostProcessToBackBuffer()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_ortho_pos, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_post_process_to_back_buffer, nullptr, 0);

	// Set rasterizer state to normal
	_device_context->RSSetState(_rs_cull_back);

	// set blend state to additive
	_device_context->OMSetBlendState(_bs_source, nullptr, 0xFFFFFFFF);

	// set the shader resource views
	const unsigned int kNumShaderResourceViews = 1;

	ID3D11ShaderResourceView* shader_resource_views[kNumShaderResourceViews] = {
		nullptr };

	_device_context->PSSetShaderResources(0, kNumShaderResourceViews, shader_resource_views);

	// set render target to the post process buffer
	_device_context->OMSetRenderTargets(1, &_rtv_back_buffer.p, nullptr);

	shader_resource_views[0] = _srv_post_process;

	_device_context->PSSetShaderResources(0, kNumShaderResourceViews, shader_resource_views);

	// set the input layout to orthogonal position
	_device_context->IASetInputLayout(_input_layout_ortho_pos);

	// disbale depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_disabled, 0);

	// set primitive topology to triangle list
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// set the sampler state to wrap
	_device_context->PSSetSamplers(0, 1, &_ss_wrap.p);
}

void Renderer::SetFormatAsNormalBuffer()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_ortho_pos, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_buffer_normal, nullptr, 0);

	// Set rasterizer state to normal
	_device_context->RSSetState(_rs_cull_back);

	// set blend state to additive
	_device_context->OMSetBlendState(_bs_additive, nullptr, 0xFFFFFFFF);

	// set render target to back buffer and disable writes to depth buffer
	_device_context->OMSetRenderTargets(1, &_rtv_back_buffer.p, nullptr);

	// the only shader resource we need is the normal buffer
	_device_context->PSSetShaderResources(0, 1, &_srv_normal.p);

	// set the input layout to orthogonal position
	_device_context->IASetInputLayout(_input_layout_ortho_pos);

	// disbale depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_disabled, 0);

	// set primitive topology to triangle list
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// set the sampler state to wrap
	_device_context->PSSetSamplers(0, 1, &_ss_wrap.p);
}

void Renderer::SetFormatAsDepthBuffer()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_ortho_pos, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_buffer_depth, nullptr, 0);

	// Set rasterizer state to normal
	_device_context->RSSetState(_rs_cull_back);

	// set blend state to additive
	_device_context->OMSetBlendState(_bs_additive, nullptr, 0xFFFFFFFF);

	// set render target to back buffer and disable writes to depth buffer
	_device_context->OMSetRenderTargets(1, &_rtv_back_buffer.p, nullptr);

	// the only shader resource we need is the depth buffer
	_device_context->PSSetShaderResources(0, 1, &_srv_depth.p);

	// set the input layout to orthogonal position
	_device_context->IASetInputLayout(_input_layout_ortho_pos);

	// disbale depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_disabled, 0);

	// set primitive topology to triangle list
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// set the sampler state to wrap
	_device_context->PSSetSamplers(0, 1, &_ss_wrap.p);
}

void Renderer::SetFormatAsColorBuffer()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_ortho_pos, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_buffer_color, nullptr, 0);

	// Set rasterizer state to normal
	_device_context->RSSetState(_rs_cull_back);

	// set blend state to additive
	_device_context->OMSetBlendState(_bs_additive, nullptr, 0xFFFFFFFF);

	// set render target to back buffer and disable writes to depth buffer
	_device_context->OMSetRenderTargets(1, &_rtv_back_buffer.p, nullptr);

	// the only shader resource we need is the color buffer
	_device_context->PSSetShaderResources(0, 1, &_srv_color.p);

	// set the input layout to orthogonal position
	_device_context->IASetInputLayout(_input_layout_ortho_pos);

	// disbale depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_disabled, 0);

	// set primitive topology to triangle list
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// set the sampler state to wrap
	_device_context->PSSetSamplers(0, 1, &_ss_wrap.p);
}

void Renderer::SetFormatAsHUD()
{
	// Set the appropriate vertex and pixel shaders
	_device_context->VSSetShader(_vs_HUD, nullptr, 0);
	_device_context->GSSetShader(nullptr, nullptr, 0);
	_device_context->PSSetShader(_ps_HUD, nullptr, 0);

	// Set rasterizer state to normal
	_device_context->RSSetState(_rs_cull_back);

	// set blend state to alpha so hud objects can be transparent
	_device_context->OMSetBlendState(_bs_alpha, nullptr, 0xFFFFFFFF);

	// set render target to back buffer and disable writes to depth buffer
	_device_context->OMSetRenderTargets(1, &_rtv_back_buffer.p, nullptr);

	// set the input layout to orthogonal position and UV
	_device_context->IASetInputLayout(_input_layout_ortho_pos_UV);

	// disbale depth testing
	_device_context->OMSetDepthStencilState(_dss_depth_test_disabled, 0);

	// set primitive topology to triangle list
	_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// set the sampler state to wrap
	_device_context->PSSetSamplers(0, 1, &_ss_wrap.p);
}

void Renderer::CreateVertexShaders()
{
	HRESULT hr;

	// Create vertex shader for position, color
	hr = _device->CreateVertexShader(VS_PosCol, sizeof(VS_PosCol), nullptr, &_vs_pos_col);
	assert(hr == S_OK && "Renderer::CreateVertexShader - CreateVertexShader(_vs_pos_col) failed");
	D3D_SET_OBJECT_NAME_A(_vs_pos_col, "Pos Col");

	// Create vertex shader for position, normal, color
	hr = _device->CreateVertexShader(VS_PosNormCol, sizeof(VS_PosNormCol), nullptr, &_vs_pos_norm_col);
	assert(hr == S_OK && "Renderer::CreateVertexShader - CreateVertexShader(_vs_pos_norm_col) failed");
	D3D_SET_OBJECT_NAME_A(_vs_pos_norm_col, "Pos Norm Col");

	// Create vertex shader for position, normal, uv
	hr = _device->CreateVertexShader(VS_PosNormUV, sizeof(VS_PosNormUV), nullptr, &_vs_pos_norm_uv);
	assert(hr == S_OK && "Renderer::CreateVertexShader - CreateVertexShader(_vs_pos_norm_uv) failed");
	D3D_SET_OBJECT_NAME_A(_vs_pos_norm_uv, "Pos Norm UV");

	// Create vertex shader for position, normal, uv where the object is always in the front of the scene
	hr = _device->CreateVertexShader(VS_PosNormUVOnTop, sizeof(VS_PosNormUVOnTop), nullptr, &_vs_pos_norm_uv_on_top);
	assert(hr == S_OK && "Renderer::CreateVertexShader - CreateVertexShader(_vs_pos_norm_uv_on_top) failed");
	D3D_SET_OBJECT_NAME_A(_vs_pos_norm_uv_on_top, "Pos Norm UV On Top");

	// Create vertex shader for orthogonal position
	hr = _device->CreateVertexShader(VS_OrthoPos, sizeof(VS_OrthoPos), nullptr, &_vs_ortho_pos);
	assert(hr == S_OK && "Renderer::CreateVertexShader - CreateVertexShader(_vs_ortho_pos) failed");
	D3D_SET_OBJECT_NAME_A(_vs_ortho_pos, "Ortho Pos");

	// Create vertex shader for HUD objects
	hr = _device->CreateVertexShader(VS_HUD, sizeof(VS_HUD), nullptr, &_vs_HUD);
	assert(hr == S_OK && "Renderer::CreateVertexShader - CreateVertexShader(_vs_HUD) failed");
	D3D_SET_OBJECT_NAME_A(_vs_HUD, "HUD");

	// Create dummy vertex shader for ground clutter
	hr = _device->CreateVertexShader(VS_GroundClutter, sizeof(VS_GroundClutter), nullptr, &_vs_ground_clutter);
	assert(hr == S_OK && "Renderer::CreateVertexShader - CreateVertexShader(_vs_ground_clutter) failed");
	D3D_SET_OBJECT_NAME_A(_vs_ground_clutter, "Ground Clutter");

	// Create vertex shader for skybox
	hr = _device->CreateVertexShader(VS_Skybox, sizeof(VS_Skybox), nullptr, &_vs_skybox);
	assert(hr == S_OK && "Renderer::CreateVertexShader - CreateVertexShader(_vs_skybox) failed");
	D3D_SET_OBJECT_NAME_A(_vs_skybox, "Skybox");
}

void Renderer::CreatePixelShaders()
{
	HRESULT hr;

	// Create pixel shader for position, normal, color
	hr = _device->CreatePixelShader(PS_PosCol, sizeof(PS_PosCol), nullptr, &_ps_pos_col);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_pos_col) failed");
	D3D_SET_OBJECT_NAME_A(_ps_pos_col, "Pos Col");

	// Create pixel shader for position, normal, color
	hr = _device->CreatePixelShader(PS_PosNormCol, sizeof(PS_PosNormCol), nullptr, &_ps_pos_norm_col);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_pos_norm_col) failed");
	D3D_SET_OBJECT_NAME_A(_ps_pos_norm_col, "Pos Norm Col");

	// Create pixel shader for position, normal, uv
	hr = _device->CreatePixelShader(PS_PosNormUV, sizeof(PS_PosNormUV), nullptr, &_ps_pos_norm_uv);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_pos_norm_uv) failed");
	D3D_SET_OBJECT_NAME_A(_ps_pos_norm_uv, "Pos Norm UV");

	// Create pixel shader for position, normal, uv and drawing outline around objects
	hr = _device->CreatePixelShader(PS_PosNormUVOutline, sizeof(PS_PosNormUVOutline), nullptr, &_ps_pos_norm_uv_outline);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_pos_norm_uv_outline) failed");
	D3D_SET_OBJECT_NAME_A(_ps_pos_norm_uv_outline, "Pos Norm UV Outline");

	// Create pixel shader for position, normal, uv, slope
	hr = _device->CreatePixelShader(PS_PosNormUVSlope, sizeof(PS_PosNormUVSlope), nullptr, &_ps_pos_norm_uv_slope);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_pos_norm_uv_slope) failed");
	D3D_SET_OBJECT_NAME_A(_ps_pos_norm_uv_slope, "Pos Norm UV Slope");

	// Create pixel shader for directional lights
	hr = _device->CreatePixelShader(PS_DirectionalLight, sizeof(PS_DirectionalLight), nullptr, &_ps_directional_light);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_directional_light) failed");
	D3D_SET_OBJECT_NAME_A(_ps_directional_light, "Directional Light");

	// Create pixel shader for back buffer to post process rendering
	hr = _device->CreatePixelShader(PS_BackBufferToPostProcess, sizeof(PS_BackBufferToPostProcess), nullptr, &_ps_back_buffer_to_post_process);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_back_buffer_to_post_process) failed");
	D3D_SET_OBJECT_NAME_A(_ps_back_buffer_to_post_process, "Back Buffer To Post Process");

	// Create pixel shader for post process to back buffer rendering
	hr = _device->CreatePixelShader(PS_PostProcessToBackBuffer, sizeof(PS_PostProcessToBackBuffer), nullptr, &_ps_post_process_to_back_buffer);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_post_process_to_back_buffer) failed");
	D3D_SET_OBJECT_NAME_A(_ps_post_process_to_back_buffer, "Post Process To Back Buffer");

	// Create pixel shader for normal buffer
	hr = _device->CreatePixelShader(PS_BufferNormal, sizeof(PS_BufferNormal), nullptr, &_ps_buffer_normal);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_normal_buffer) failed");
	D3D_SET_OBJECT_NAME_A(_ps_buffer_normal, "Normal Buffer");

	// Create pixel shader for depth buffer
	hr = _device->CreatePixelShader(PS_BufferDepth, sizeof(PS_BufferDepth), nullptr, &_ps_buffer_depth);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_buffer_depth) failed");
	D3D_SET_OBJECT_NAME_A(_ps_buffer_depth, "Depth Buffer");

	// Create pixel shader for color buffer
	hr = _device->CreatePixelShader(PS_BufferColor, sizeof(PS_BufferColor), nullptr, &_ps_buffer_color);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_buffer_color) failed");
	D3D_SET_OBJECT_NAME_A(_ps_buffer_color, "Color Buffer");

	// Create pixel shader for HUD objects
	hr = _device->CreatePixelShader(PS_HUD, sizeof(PS_HUD), nullptr, &_ps_HUD);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_HUD) failed");
	D3D_SET_OBJECT_NAME_A(_ps_HUD, "HUD");

	// Create pixel shader for skybox
	hr = _device->CreatePixelShader(PS_Skybox, sizeof(PS_Skybox), nullptr, &_ps_skybox);
	assert(hr == S_OK && "Renderer::CreatePixelShader - CreatePixelShader(_ps_skybox) failed");
	D3D_SET_OBJECT_NAME_A(_ps_skybox, "Skybox");
}

void Renderer::CreateGeometryShaders()
{
	HRESULT hr;

	// Create the geometry shader for ground clutter
	hr = _device->CreateGeometryShader(GS_GroundClutter, sizeof(GS_GroundClutter), nullptr, &_gs_ground_clutter);
	assert(hr == S_OK && "Renderer::CreateGeometryShaders - CreateGeometryShader(_gs_ground_clutter) failed");
	D3D_SET_OBJECT_NAME_A(_gs_ground_clutter, "Ground Clutter");
}

void Renderer::CreateInputLayouts()
{
	HRESULT hr;

	// Create inputlayout for the position
	D3D11_INPUT_ELEMENT_DESC desc_pos[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	hr = _device->CreateInputLayout(desc_pos,
		sizeof(desc_pos) / sizeof(desc_pos[0]),
		VS_GroundClutter,
		sizeof(VS_GroundClutter),
		&_input_layout_pos);

	assert(hr == S_OK && "Renderer::CreateInputLayouts() - CreateInputLayout(desc_pos) failed");
	D3D_SET_OBJECT_NAME_A(_input_layout_pos, "Pos");

	// Create inputlayout for the position, color
	D3D11_INPUT_ELEMENT_DESC desc_pos_col[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	hr = _device->CreateInputLayout(desc_pos_col,
		sizeof(desc_pos_col) / sizeof(desc_pos_col[0]),
		VS_PosCol,
		sizeof(VS_PosCol),
		&_input_layout_pos_col);

	assert(hr == S_OK && "Renderer::CreateInputLayouts() - CreateInputLayout(desc_pos_col) failed");
	D3D_SET_OBJECT_NAME_A(_input_layout_pos_col, "Pos Col");

	// Create inputlayout for the position, normal, color
	D3D11_INPUT_ELEMENT_DESC desc_pos_norm_col[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	hr = _device->CreateInputLayout(desc_pos_norm_col,
		sizeof(desc_pos_norm_col) / sizeof(desc_pos_norm_col[0]),
		VS_PosNormCol,
		sizeof(VS_PosNormCol),
		&_input_layout_pos_norm_col);

	assert(hr == S_OK && "Renderer::CreateInputLayouts() - CreateInputLayout(desc_pos_norm_col) failed");
	D3D_SET_OBJECT_NAME_A(_input_layout_pos_norm_col, "Pos Norm Col");

	// Create inputlayout for the position, normal, uv
	D3D11_INPUT_ELEMENT_DESC desc_pos_norm_uv[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXTCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	hr = _device->CreateInputLayout(desc_pos_norm_uv,
		sizeof(desc_pos_norm_uv) / sizeof(desc_pos_norm_uv[0]),
		VS_PosNormUV,
		sizeof(VS_PosNormUV),
		&_input_layout_pos_norm_uv);

	assert(hr == S_OK && "Renderer::CreateInputLayouts() - CreateInputLayout(desc_pos_norm_uv) failed");
	D3D_SET_OBJECT_NAME_A(_input_layout_pos_norm_uv, "Pos Norm UV");

	// Create inputlayout for orthogonal position
	D3D11_INPUT_ELEMENT_DESC desc_ortho_pos[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	hr = _device->CreateInputLayout(desc_ortho_pos,
		sizeof(desc_ortho_pos) / sizeof(desc_ortho_pos[0]),
		VS_OrthoPos,
		sizeof(VS_OrthoPos),
		&_input_layout_ortho_pos);

	assert(hr == S_OK && "Renderer::CreateInputLayouts() - CreateInputLayout(_input_layout_ortho_pos) failed");
	D3D_SET_OBJECT_NAME_A(_input_layout_ortho_pos, "Ortho Pos");

	// Create inputlayout for orthogonal position and UV
	D3D11_INPUT_ELEMENT_DESC desc_ortho_pos_UV[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXTCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	hr = _device->CreateInputLayout(desc_ortho_pos_UV,
		sizeof(desc_ortho_pos_UV) / sizeof(desc_ortho_pos_UV[0]),
		VS_HUD,
		sizeof(VS_HUD),
		&_input_layout_ortho_pos_UV);

	assert(hr == S_OK && "Renderer::CreateInputLayouts() - CreateInputLayout(_input_layout_ortho_pos_UV) failed");
	D3D_SET_OBJECT_NAME_A(_input_layout_ortho_pos_UV, "HUD");
}

void Renderer::CreateTextures()
{
	HRESULT hr;

	D3D11_TEXTURE2D_DESC texture_desc;
	ZeroMemory(&texture_desc, sizeof(texture_desc));

	texture_desc.Width = _window_width;
	texture_desc.Height = _window_height;
	texture_desc.MipLevels = 1;
	texture_desc.ArraySize = 1;
	texture_desc.Format = DXGI_FORMAT_R24G8_TYPELESS;
	texture_desc.SampleDesc.Count = 1;
	texture_desc.SampleDesc.Quality = 0;
	texture_desc.Usage = D3D11_USAGE_DEFAULT;
	texture_desc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	texture_desc.MiscFlags = NULL;
	texture_desc.CPUAccessFlags = NULL;

	// Create depth buffer
	hr = _device->CreateTexture2D(&texture_desc, NULL,  &_depth_buffer);
	assert(hr == S_OK && "Renderer::CreateTextures - CreateTexture2D(_depth_buffer) failed");
	D3D_SET_OBJECT_NAME_A(_depth_buffer, "Depth Buffer");

	texture_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texture_desc.Usage = D3D11_USAGE_DEFAULT;
	texture_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;

	// Create post process buffer
	hr = _device->CreateTexture2D(&texture_desc, NULL, &_post_process_buffer);
	assert(hr == S_OK && "Renderer::CreateTextures - CreateTexture2D(_post_process_buffer) failed");
	D3D_SET_OBJECT_NAME_A(_post_process_buffer, "Post Process Buffer");

	// Create normal buffer
	hr = _device->CreateTexture2D(&texture_desc, NULL, &_normal_buffer);
	assert(hr == S_OK && "Renderer::CreateTextures - CreateTexture2D(_normal_buffer) failed");
	D3D_SET_OBJECT_NAME_A(_normal_buffer, "Normal Buffer");

	// Create color buffer
	hr = _device->CreateTexture2D(&texture_desc, NULL, &_color_buffer);
	assert(hr == S_OK && "Renderer::CreateTextures - CreateTexture2D(_color_buffer) failed");
	D3D_SET_OBJECT_NAME_A(_color_buffer, "Color Buffer");

	// Create outline buffer
	hr = _device->CreateTexture2D(&texture_desc, NULL, &_outline_buffer);
	assert(hr == S_OK && "Renderer::CreateTextures - CreateTexture2D(_outline_buffer) failed");
	D3D_SET_OBJECT_NAME_A(_outline_buffer, "Outline Buffer");
}

void Renderer::CreateDepthStencilViews()
{
	HRESULT hr;

	D3D11_DEPTH_STENCIL_VIEW_DESC depth_stecil_view_desc;
	memset(&depth_stecil_view_desc, 0, sizeof(depth_stecil_view_desc));
	depth_stecil_view_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depth_stecil_view_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;

	// Create depth stencil view for the depth buffer
	hr = _device->CreateDepthStencilView(_depth_buffer, &depth_stecil_view_desc, &_dsv_depth_buffer);
	assert(hr == S_OK && "Renderer::CreateTextures - CreateDepthStencilView(_depth_buffer) failed");
	D3D_SET_OBJECT_NAME_A(_depth_buffer, "Depth Buffer");
}

void Renderer::CreateShaderResourceViews()
{
	HRESULT hr;

	D3D11_SHADER_RESOURCE_VIEW_DESC shader_resource_view_desc;
	memset(&shader_resource_view_desc, 0, sizeof(shader_resource_view_desc));
	shader_resource_view_desc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	shader_resource_view_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shader_resource_view_desc.Texture2D.MipLevels = 1;
	shader_resource_view_desc.Texture2D.MostDetailedMip = 0;

	// Create the shader resource view for the depth buffer
	hr = _device->CreateShaderResourceView(_depth_buffer, &shader_resource_view_desc, &_srv_depth);
	assert(hr == S_OK && "Renderer::CreateShaderResourceView - CreateShaderResourceView(_srv_depth) failed");
	D3D_SET_OBJECT_NAME_A(_srv_depth, "SRV Depth Buffer");

	// Create the shader resource view for the back buffer
	hr = _device->CreateShaderResourceView(_back_buffer, nullptr, &_srv_back_buffer);
	assert(hr == S_OK && "Renderer::CreateShaderResourceView - CreateShaderResourceView(_srv_back_buffer) failed");
	D3D_SET_OBJECT_NAME_A(_srv_back_buffer, "SRV Back Buffer");

	// Create the shader resource view for the post process buffer
	hr = _device->CreateShaderResourceView(_post_process_buffer, nullptr, &_srv_post_process);
	assert(hr == S_OK && "Renderer::CreateShaderResourceView - CreateShaderResourceView(_srv_post_process) failed");
	D3D_SET_OBJECT_NAME_A(_srv_post_process, "SRV Post Process");

	// Create the shader resource view for the normal buffer
	hr = _device->CreateShaderResourceView(_normal_buffer, nullptr, &_srv_normal);
	assert(hr == S_OK && "Renderer::CreateShaderResourceView - CreateShaderResourceView(_srv_normal) failed");
	D3D_SET_OBJECT_NAME_A(_srv_normal, "SRV Normal Buffer");

	// Create the shader resource view for the color buffer
	hr = _device->CreateShaderResourceView(_color_buffer, nullptr, &_srv_color);
	assert(hr == S_OK && "Renderer::CreateShaderResourceView - CreateShaderResourceView(_srv_color) failed");
	D3D_SET_OBJECT_NAME_A(_srv_color, "SRV Color Buffer");

	// Create the shader resource view for the outline buffer
	hr = _device->CreateShaderResourceView(_outline_buffer, nullptr, &_srv_outline);
	assert(hr == S_OK && "Renderer::CreateShaderResourceView - CreateShaderResourceView(_srv_outline) failed");
	D3D_SET_OBJECT_NAME_A(_srv_outline, "SRV Outline Buffer");
}

void Renderer::CreateRenderTargetViews()
{
	HRESULT hr;

	// Create render target view for the back buffer
	hr = _device->CreateRenderTargetView(_back_buffer, nullptr, &_rtv_back_buffer);
	assert(hr == S_OK && "Renderer::CreateRenderTargetViews - CreateRenderTargetView(_rtv_back_buffer) failed");
	D3D_SET_OBJECT_NAME_A(_rtv_back_buffer, "Back Buffer");

	// Create render target view for the post process buffer
	hr = _device->CreateRenderTargetView(_post_process_buffer, nullptr, &_rtv_post_process);
	assert(hr == S_OK && "Renderer::CreateRenderTargetViews - CreateRenderTargetView(_rtv_post_process) failed");
	D3D_SET_OBJECT_NAME_A(_rtv_post_process, "Post Process Buffer");

	// Create render target view for the normal buffer
	hr = _device->CreateRenderTargetView(_normal_buffer, nullptr, &_rtv_normal);
	assert(hr == S_OK && "Renderer::CreateRenderTargetViews - CreateRenderTargetView(_rtv_normal) failed");
	D3D_SET_OBJECT_NAME_A(_rtv_normal, "Normal Buffer");

	// Create render target view for the color buffer
	hr = _device->CreateRenderTargetView(_color_buffer, nullptr, &_rtv_color);
	assert(hr == S_OK && "Renderer::CreateRenderTargetViews - CreateRenderTargetView(_rtv_color) failed");
	D3D_SET_OBJECT_NAME_A(_rtv_color, "Color Buffer");

	// Create render target view for the outline buffer
	hr = _device->CreateRenderTargetView(_outline_buffer, nullptr, &_rtv_outline);
	assert(hr == S_OK && "Renderer::CreateRenderTargetViews - CreateRenderTargetView(_rtv_outline) failed");
	D3D_SET_OBJECT_NAME_A(_rtv_outline, "Outline Buffer");
}

void Renderer::CreateDepthStencilStates()
{
	HRESULT hr;

	// set depth depth stencil state values
	D3D11_DEPTH_STENCIL_DESC desc = CD3D11_DEPTH_STENCIL_DESC(CD3D11_DEFAULT());

	// create depth stencil state with depth testing disbaled
	desc.DepthEnable = false;
	hr = _device->CreateDepthStencilState(&desc, &_dss_depth_test_disabled);
	assert(hr == S_OK && "Renderer::CreateDepthStencilStates - CreateDepthStencilState(_dss_depth_test_disabled) failed");
	D3D_SET_OBJECT_NAME_A(_dss_depth_test_disabled, "Depth Test Disabled");

	// create depth stencil state with depth testing enabled
	desc.DepthEnable = true;
	hr = _device->CreateDepthStencilState(&desc, &_dss_depth_test_enabled);
	assert(hr == S_OK && "Renderer::CreateDepthStencilStates - CreateDepthStencilState(_dss_depth_test_enabled) failed");
	D3D_SET_OBJECT_NAME_A(_dss_depth_test_enabled, "Depth Test Enabled");
}

void Renderer::CreateRasterizerStates()
{
	HRESULT hr;

	// Create reasterizer state description
	D3D11_RASTERIZER_DESC rasterizer_desc;
	rasterizer_desc.AntialiasedLineEnable = false;
	rasterizer_desc.CullMode = D3D11_CULL_BACK;
	rasterizer_desc.DepthBias = 0;
	rasterizer_desc.DepthBiasClamp = 0.0f;
	rasterizer_desc.DepthClipEnable = true;
	rasterizer_desc.FillMode = D3D11_FILL_SOLID;
	rasterizer_desc.FrontCounterClockwise = false;
	rasterizer_desc.MultisampleEnable = false;
	rasterizer_desc.ScissorEnable = false;
	rasterizer_desc.SlopeScaledDepthBias = 0.0f;

	// Create normal back face culling rasterizer state
	hr = _device->CreateRasterizerState(&rasterizer_desc, &_rs_cull_back);
	assert(hr == S_OK && "Renderer::CreateRasterizerStates - CreateRasterizerState(_rs_cull_back) failed");
	D3D_SET_OBJECT_NAME_A(_rs_cull_back, "Cull Back");

	// Create front face culling rasterizer state
	rasterizer_desc.CullMode = D3D11_CULL_FRONT;
	hr = _device->CreateRasterizerState(&rasterizer_desc, &_rs_cull_front);
	assert(hr == S_OK && "Renderer::CreateRasterizerStates - CreateRasterizerState(_rs_cull_front) failed");
	D3D_SET_OBJECT_NAME_A(_rs_cull_front, "Cull Front");

	// Create no culling rasterizer state
	rasterizer_desc.CullMode = D3D11_CULL_NONE;
	hr = _device->CreateRasterizerState(&rasterizer_desc, &_rs_cull_none);
	assert(hr == S_OK && "Renderer::CreateRasterizerStates - CreateRasterizerState(_rs_cull_none) failed");
	D3D_SET_OBJECT_NAME_A(_rs_cull_none, "Cull None");

	// Create wire frame rasterizer state
	rasterizer_desc.FillMode = D3D11_FILL_WIREFRAME;
	rasterizer_desc.AntialiasedLineEnable = true;
	hr = _device->CreateRasterizerState(&rasterizer_desc, &_rs_wire_frame);
	assert(hr == S_OK && "Renderer::CreateRasterizerStates - CreateRasterizerState(_rs_wire_frame) failed");
	D3D_SET_OBJECT_NAME_A(_rs_wire_frame, "Wire Frame");

}

void Renderer::CreateBlendStates()
{
	HRESULT hr;

	D3D11_BLEND_DESC desc;
	memset(&desc, 0, sizeof(desc));

	desc.AlphaToCoverageEnable = FALSE;
	desc.IndependentBlendEnable = FALSE;
	desc.RenderTarget[0].BlendEnable = TRUE;
	desc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	desc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
	desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	// create the blend state for source values (copies 100% color from source, 0% from destination)
	hr = _device->CreateBlendState(&desc, &_bs_source);
	assert(hr == S_OK && "Renderer::CreateBlendStates - CreateBlendState(_bs_source) failed");
	D3D_SET_OBJECT_NAME_A(_bs_source, "Blend State Source");

	desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	desc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;

	// create the blend state alpha values
	hr = _device->CreateBlendState(&desc, &_bs_alpha);
	assert(hr == S_OK && "Renderer::CreateBlendStates - CreateBlendState(_bs_alpha) failed");
	D3D_SET_OBJECT_NAME_A(_bs_alpha, "Blend State Alpha");

	desc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	desc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;

	// create the blend state for adding values
	hr = _device->CreateBlendState(&desc, &_bs_additive);
	assert(hr == S_OK && "Renderer::CreateBlendStates - CreateBlendState(_bs_additive) failed");
	D3D_SET_OBJECT_NAME_A(_bs_additive, "Blend State Additive");
}

void Renderer::CreateSamplersStates()
{
	HRESULT hr;

	D3D11_SAMPLER_DESC sampler_state;
	memset(&sampler_state, 0, sizeof(sampler_state));

	sampler_state.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampler_state.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampler_state.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	hr = _device->CreateSamplerState(&sampler_state, &_ss_wrap);

	assert(hr == S_OK && "Renderer::CreateSamplersStates - CreateSamplersState failed");
	D3D_SET_OBJECT_NAME_A(_ss_wrap, "Wrap");
}
