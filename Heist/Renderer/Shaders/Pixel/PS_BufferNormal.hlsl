#pragma pack_matrix(row_major) // dont put semicolon here

texture2D Normal : register(t0);

SamplerState filter : register (s0);

#include "../Headers/CB_Deferred.hlsli"

struct P_IN
{
	float4 pos : SV_POSITION;
	float4 posClip : POSITION;
};

float4 main(P_IN input) : SV_TARGET
{
	// All we want to do here is return the color from the normal buffer
	float4 normal;
	float2 textCord;
	
	input.posClip.xy /= input.posClip.w;
	textCord = float2(input.posClip.x, -input.posClip.y);
	textCord = (textCord + 1) / 2.0f;
	
	normal = Normal.Sample(filter, textCord);

	return normal;
}