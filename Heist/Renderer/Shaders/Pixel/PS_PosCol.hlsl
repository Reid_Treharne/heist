#pragma pack_matrix(row_major) // dont put semicolon here

struct PS_OUT
{
	float4 gbuffer_normal : SV_TARGET0;
	float4 gbuffer_color : SV_TARGET1;
	float4 gbuffer_outline : SV_TARGET2;
};

struct PS_IN
{
	float4 screen_position : SV_POSITION;
	float4 color : COLOR;
};

PS_OUT main(PS_IN input)
{
	PS_OUT output;

	// write diffuse color out to color buffer
	output.gbuffer_color = input.color;

	// setting normal to up so most light hits it
	output.gbuffer_normal = float4(0.0f, 1.0f, 0.0f, 1.0f);

	output.gbuffer_outline = float4(0.0f, 0.0f, 0.0f, 1.0f);

	return output;
}