#pragma pack_matrix(row_major) // dont put semicolon here

texture2D Depth : register(t0);

SamplerState filter : register (s0);

#include "../Headers/CB_Deferred.hlsli"

struct P_IN
{
	float4 pos : SV_POSITION;
	float4 posClip : POSITION;
};

float4 main(P_IN input) : SV_TARGET
{
	// All we want to do here is return the depth from the depth buffer as a color
	float depth;
	float2 textCord;
	
	input.posClip.xy /= input.posClip.w;
	textCord = float2(input.posClip.x, -input.posClip.y);
	textCord = (textCord + 1) / 2.0f;
	
	// Raise the depth to the power of 100 so it's easier to see.
	// To see the exact value in the depth buffer, change 100 to 1.
	depth = pow(Depth.Sample(filter, textCord).r, 100);

	return float4(depth, depth, depth, 1.0f);
}