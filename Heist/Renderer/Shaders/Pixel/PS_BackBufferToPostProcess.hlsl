#pragma pack_matrix(row_major) // dont put semicolon here

texture2D BackBuffer : register(t0);
texture2D Depth : register(t1);
texture2D Outline : register(t2);

SamplerState filter : register (s0);

#include "../Headers/CB_PostProcess.hlsli"

struct P_IN
{
	float4 pos : SV_POSITION;
	float4 posClip : POSITION;
	unsigned int whoAmI : TEXTCOORD;
};

float4 main(P_IN input) : SV_TARGET
{	
	input.posClip.xy /= input.posClip.w;
	float2 textCord = float2(input.posClip.x, -input.posClip.y);
	textCord = (textCord + 1) / 2.0f; // 0 to 1
	
	float4 outline_color = Outline.Sample(filter, textCord);

	// If color is anything other than black
	if (outline_color.r > 0.0f || outline_color.g > 0.0f || outline_color.b > 0.0f)
	{
		float pixel_width = 1.0f / screen_width;
		float pixel_height = 1.0f / screen_height;

		float depth = Depth.Sample(filter, textCord).r;

		if (depth < Depth.Sample(filter, float2(textCord.x - pixel_width, textCord.y)).r)
		{
			if (dot(float3(1.0f, 1.0f, 1.0f), Outline.Sample(filter, float2(textCord.x - pixel_width, textCord.y)).xyz) == 0.0f)
			{
				return outline_color;
			}
		}
		if (depth < Depth.Sample(filter, float2(textCord.x + pixel_width, textCord.y)).r)
		{
			if (dot(float3(1.0f, 1.0f, 1.0f), Outline.Sample(filter, float2(textCord.x + pixel_width, textCord.y)).xyz) == 0.0f)
			{
				return outline_color;
			}
		}
		if (depth < Depth.Sample(filter, float2(textCord.x, textCord.y + pixel_height)).r)
		{
			if (dot(float3(1.0f, 1.0f, 1.0f), Outline.Sample(filter, float2(textCord.x, textCord.y + pixel_height)).xyz) == 0.0f)
			{
				return outline_color;
			}
		}
		if (depth < Depth.Sample(filter, float2(textCord.x, textCord.y - pixel_height)).r)
		{
			if (dot(float3(1.0f, 1.0f, 1.0f), Outline.Sample(filter, float2(textCord.x, textCord.y - pixel_height)).xyz) == 0.0f)
			{
				return outline_color;
			}
		}
	}

	// Investigate Load
	return BackBuffer.Sample(filter, textCord); //BackBuffer.Load(float3(input.pos.xy, 0));
}