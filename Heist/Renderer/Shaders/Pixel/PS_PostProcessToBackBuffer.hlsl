#pragma pack_matrix(row_major) // dont put semicolon here

texture2D PostProcess : register(t0);

SamplerState filter : register (s0);

#include "../Headers/CB_DirectionalLight.hlsli"

struct P_IN
{
	float4 pos : SV_POSITION;
	float4 posClip : POSITION;
	unsigned int whoAmI : TEXTCOORD;
};

float4 main(P_IN input) : SV_TARGET
{
	input.posClip.xy /= input.posClip.w;
	float2 textCord = float2(input.posClip.x, -input.posClip.y);
	textCord = (textCord + 1) / 2.0f; // 0 to 1

	// Return the color of the post process buffer
	return PostProcess.Sample(filter, textCord);
}