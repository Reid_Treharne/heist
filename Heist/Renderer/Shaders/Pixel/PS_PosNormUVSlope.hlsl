#pragma pack_matrix(row_major) // dont put semicolon here

texture2D Texture0 : register( t0 );
texture2D Texture1 : register( t1 );

SamplerState filter : register ( s0 );

#include "../Headers/CB_Deferred.hlsli"

struct PS_OUT
{
	float4 gbuffer_normal : SV_TARGET0;
	float4 gbuffer_color : SV_TARGET1;
	float4 gbuffer_outline : SV_TARGET2;
};

struct PS_IN
{
	float4 screen_position : SV_POSITION;
	float3 world_normal : NORMAL;
	float2 uv : TEXTCOORD;
};

PS_OUT main(PS_IN input)
{
	PS_OUT output;

	// convert the normal to a color value (-1, 1) to (0, 1)
	float3 normal_color = (input.world_normal + 1.0f) * 0.5f;

	// write normal color out to normal buffer
	output.gbuffer_normal = float4(normal_color, 1.0f);

	float texture1_percent = dot(output.gbuffer_normal.xyz, float3(0.0f, 1.0f, 0.0f));
	//texture1_percent = texture1_percent * texture1_percent * texture1_percent;

	texture1_percent = pow(abs(texture1_percent), secondary_texture_slope);

	// sample the color and write it out to the color buffer
	float4 color = Texture0.Sample(filter, input.uv) * texture1_percent;
	color += Texture1.Sample(filter, input.uv) * (1.0f - texture1_percent);

	output.gbuffer_color = color;

	output.gbuffer_outline = float4(0.0f, 0.0f, 0.0f, 1.0f);

	return output;
}