#pragma pack_matrix(row_major) // dont put semicolon here

texture2D Depth : register( t0 );
texture2D Normal : register( t1 );
texture2D Color : register( t2 );
texture2D Outline : register( t3 );

SamplerState filter : register ( s0 );

#include "../Headers/CB_DirectionalLight.hlsli"

struct P_IN
{
	float4 pos : SV_POSITION;
	float4 posClip : POSITION;
	unsigned int instance : TEXTCOORD;
};

float4 main(P_IN input) : SV_TARGET
{
	// before devide is clip/proj after is ndc
	float4 finalAmbient, finalDiffuse;
	float4 diffuse, normal;
	float2 textCord;
	float nDotL;

	input.posClip.xy /= input.posClip.w;
	textCord = float2(input.posClip.x, -input.posClip.y);
	textCord = (textCord + 1) / 2.0f; // 0 to 1

	diffuse = Color.Sample(filter, textCord);
	normal = Normal.Sample(filter, textCord);
	normal = normal * 2.0f - 1.0f;

	// Ambient
	finalAmbient = instance_directional_light[input.instance].ambient_color * diffuse * diffuse.w;

	// Diffuse
	nDotL = saturate(dot(normal.xyz, -instance_directional_light[input.instance].world_matrix[2].xyz));
	finalDiffuse = nDotL * instance_directional_light[input.instance].diffuse_color * diffuse;

	return finalAmbient + finalDiffuse;
}