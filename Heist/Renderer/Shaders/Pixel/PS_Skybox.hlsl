#pragma pack_matrix(row_major) // dont put a semiclon here

TextureCube Texture : register(t0); // first texture

SamplerState filter : register (s0);

struct PS_IN
{
	float3 dir : TEXTCOORD0;
};

struct PS_OUT
{
	float4 gbuffer_color : SV_TARGET0;
};

PS_OUT main(PS_IN input)
{
	PS_OUT output;

	// Write the color the skybox to the color buffer.
	output.gbuffer_color = Texture.Sample(filter, input.dir);

	return output;
}