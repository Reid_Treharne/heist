#pragma pack_matrix(row_major) // dont put semicolon here

texture2D Color : register( t0 );

SamplerState filter : register ( s0 );

#include "../Headers/CB_Deferred.hlsli"

struct P_IN
{
	float4 pos : SV_POSITION;
	float2 UV : TEXTCOORD;
	float4 color: COLOR;
	unsigned int whoAmI : TEXTCOORD2;
};

float4 main(P_IN input) : SV_TARGET
{
	float4 final_color;

	final_color = Color.Sample(filter, input.UV);

	final_color *= input.color;

	return final_color;
	//return saturate(HUD_Color * Texture.Sample(filter, input.UVs));

	//return float4(0.0f, 0.0f, 1.0f, 1.0f);
}