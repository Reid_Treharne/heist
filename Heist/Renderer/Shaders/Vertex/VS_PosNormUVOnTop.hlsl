#pragma pack_matrix(row_major) // dont put semicolon here

#include "../Headers/CB_Deferred.hlsli"

struct VS_IN
{
	float3 local_position : POSITION;
	float3 local_normal : NORMAL;
	float2 uv : TEXTCOORD;
};

struct VS_OUT
{
	float4 screen_position : SV_POSITION;
	float3 world_normal : NORMAL;
	float2 uv : TEXTCOORD;
};

VS_OUT main(VS_IN input, uint instance : SV_InstanceID)
{
	VS_OUT output = (VS_OUT)0;

	// transfer the vertex position from local to world to screen
	float4 position = float4(input.local_position, 1.0f);
	position = mul(position, instance_mesh_object[instance].world_matrix); // to world space
	position = mul(position, view_matrix); // to camera space
	position = mul(position, projection_matrix); // to screen space

	output.screen_position = position;
	output.screen_position.z = output.screen_position.z * 0.0001f;

	// tranfer the normal into world position
	output.world_normal = mul(float4(input.local_normal, 0.0f), instance_mesh_object[instance].world_matrix).xyz;

	// pass the uvs through to the pixel shader
	output.uv = input.uv;

	return output;
}