#pragma pack_matrix(row_major) // dont put semicolon here

#include "../Headers/CB_Deferred.hlsli"

struct VS_IN
{
	float2 pos : POSITION;
};

struct VS_OUT
{
	float4 pos : SV_POSITION;
	float4 posClip : POSITION;
	unsigned int whoAmI : TEXTCOORD;
};

VS_OUT main( VS_IN input, uint instance : SV_InstanceID )
{
	VS_OUT output = (VS_OUT)0;

	output.pos = float4(input.pos, 0.0f, 1.0f);
	output.posClip = output.pos;
	output.whoAmI = instance;

	return output;
}