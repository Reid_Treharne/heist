#pragma pack_matrix(row_major) // dont put a semiclon here

#include "../Headers/CB_Deferred.hlsli"

struct V_IN
{
	float3 local_position : POSITION;
};

struct V_OUT
{
	float3 dir : TEXTCOORD0;
	float4 posH : SV_POSITION;
};

V_OUT main(V_IN input, uint instance : SV_InstanceID)
{
	V_OUT output = (V_OUT)0;

	// ensures translation is preserved during matrix multiply  
	float4 localH = float4(input.local_position, 1);

	output.dir = localH.xyz;
	localH = mul(localH, instance_mesh_object[instance].world_matrix);
	localH = mul(localH, view_matrix);
	localH = mul(localH, projection_matrix);

	// Set the depth z value to w - a small amount so that when
	// x/w, y/w, z/w happens, z will be around 0.99999, forcing it
	// to the very back of the depth buffer and rendering behind everything else
	output.posH = localH.xyww;

	output.posH.z -= 0.000001;

	return output; // send projected vertex to the rasterizer stage
}
