#pragma pack_matrix(row_major) // dont put semicolon here

struct VS_IN
{
	float3 world_position : POSITION;
};

struct VS_OUT
{
	float3 world_position : POSITION;
};

VS_OUT main( VS_IN input )// : SV_POSITION
{
	VS_OUT output = (VS_OUT)0;

	// simply pass the input position through
	output.world_position = input.world_position;

	return output;
}