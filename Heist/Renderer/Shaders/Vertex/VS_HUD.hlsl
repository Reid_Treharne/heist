#pragma pack_matrix(row_major) // dont put semicolon here

#include "../Headers/CB_HUD.hlsli"

struct VS_IN
{
	float2 pos : POSITION;
	float2 UV : TEXTCOORD;
};

struct VS_OUT
{
	float4 pos : SV_POSITION;
	float2 UV : TEXTCOORD;
	float4 color: COLOR;
	unsigned int whoAmI : TEXTCOORD2;
};

VS_OUT main( VS_IN input, uint instance : SV_InstanceID )
{
	VS_OUT output = (VS_OUT)0;

	// Convert this instance's position to an offset (0 to 1) -> (-1, 1)
	float2 offset = float2(instance_hud_object[instance].position.x * 2.0f, instance_hud_object[instance].position.y * -2.0f);
	// Offset the vertex
	output.pos = float4(input.pos.xy + offset, 0.0f, 1.0f);

	// Pass the UVs
	output.UV = input.UV;

	// Pass the color
	output.color = instance_hud_object[instance].color;

	// Pass the instance
	output.whoAmI = instance;

	return output;
}