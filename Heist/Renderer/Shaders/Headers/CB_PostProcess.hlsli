#ifndef _CB_DIRECTIONAL_LIGHT_HLSLI_
#define _CB_DIRECTIONAL_LIGHT_HLSLI_

#pragma pack_matrix(row_major) // dont put semicolon here

/*******************INSTANCES START**********************/


/********************INSTANCES END***********************/

/***************CONSTANT BUFFERS START*******************/

cbuffer C_BUFFER_SCREEN_RESOLUTION : register (b0)
{
	unsigned int screen_width;
	unsigned int screen_height;
	unsigned int c_buffer_screen_resolution_padding1;
	unsigned int c_buffer_screen_resolution_padding2;
};

/****************CONSTANT BUFFERS END********************/

#endif