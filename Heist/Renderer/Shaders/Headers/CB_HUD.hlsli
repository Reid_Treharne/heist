#ifndef _CB_HUD_HLSLI_
#define _CB_HUD_HLSLI_

#pragma pack_matrix(row_major) // dont put semicolon here

#define HUD_OBJECT_INSTANCES 10

/*******************INSTANCES START**********************/

struct INSTANCE_HUD_OBJECT
{
	float2 position;
	float2 UV_offset;
	float4 color;
};

/********************INSTANCES END***********************/

/***************CONSTANT BUFFERS START*******************/

// VERTEX SHADER
cbuffer C_BUFFER_INSTANCE_HUD : register(b0)
{
	INSTANCE_HUD_OBJECT instance_hud_object[HUD_OBJECT_INSTANCES];
};

/****************CONSTANT BUFFERS END********************/

#endif