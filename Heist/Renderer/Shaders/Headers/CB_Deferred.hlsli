#ifndef _CB_DEFERRED_HLSLI_
#define _CB_DEFERRED_HLSLI_

#pragma pack_matrix(row_major) // dont put semicolon here

#define MESH_OBJECT_INSTANCES 1000
#define RENDER_FLAG_INSTANCES MESH_OBJECT_INSTANCES

/*******************INSTANCES START**********************/
struct INSTANCE_MESH_OBJECT
{
	float4x4 world_matrix;
};

// RENDER FLAGS
static const int OUTLINE = 1 << 0;
static const int DRAW_ON_TOP = 1 << 1;

struct INSTANCE_RENDER_FLAGS
{
	unsigned int render_flags;
	float3 outline_color;
};

/********************INSTANCES END***********************/

/***************CONSTANT BUFFERS START*******************/

// PIXEL SHADER
cbuffer C_BUFFER_MESH_OBJECT_TEXTURED_SLOPE : register( b0 )
{
	float secondary_texture_slope;
	float c_buffer_mesh_object_textured_sloped_padding1;
	float c_buffer_mesh_object_textured_sloped_padding2;
	float c_buffer_mesh_object_textured_sloped_padding3;
};

// VERTEX SHADER AND PIXEL SHADER
cbuffer C_BUFFER_INSTANCE_MESH_OBJECT : register(b1)
{
	INSTANCE_MESH_OBJECT instance_mesh_object[MESH_OBJECT_INSTANCES];
};

cbuffer C_BUFFER_INSTANCE_MESH_OBJECT : register(b2)
{
	INSTANCE_RENDER_FLAGS instance_render_flags[RENDER_FLAG_INSTANCES];
};

// VERTEX SHADER & GEOMETRY SHADER
cbuffer C_BUFFER_CAMERA : register(b0)
{
	float4x4 view_matrix;
	float4x4 projection_matrix;
};
/****************CONSTANT BUFFERS END********************/

#endif