#ifndef _CB_DIRECTIONAL_LIGHT_HLSLI_
#define _CB_DIRECTIONAL_LIGHT_HLSLI_

#pragma pack_matrix(row_major) // dont put semicolon here

#define DIRECTIONAL_LIGHT_INSTANCES 1

/*******************INSTANCES START**********************/

struct INSTANCE_DIRECTIONAL_LIGHT
{
	float4x4 world_matrix;
	float4 ambient_color;
	float4 diffuse_color;
};

/********************INSTANCES END***********************/

/***************CONSTANT BUFFERS START*******************/

// PIXEL SHADER
cbuffer C_BUFFER_INSTANCE_DIRECTIONAL_LIGHT : register (b0)
{
	INSTANCE_DIRECTIONAL_LIGHT instance_directional_light[DIRECTIONAL_LIGHT_INSTANCES];
};

/****************CONSTANT BUFFERS END********************/

#endif