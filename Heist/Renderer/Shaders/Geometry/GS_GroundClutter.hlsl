#pragma pack_matrix(row_major) // dont put semicolon here

#include "../Headers/CB_Deferred.hlsli"

struct GS_IN
{
	float3 world_position : POSITION;
};

struct GS_OUT
{
	float4 screen_position : SV_POSITION;
	float3 world_normal : NORMAL;
	float2 uv : TEXTCOORD;
};

[maxvertexcount(12)]
void main(point GS_IN input[1], inout TriangleStream< GS_OUT > output)
{
	GS_OUT element;

	float width = 2.0f;
	float height = 2.0f;
	float half_width = width * 0.5f;

	element.world_normal = float3(0.0f, 1.0f, 0.0f);

	{
		// bottom left vert
		float4 bottom_left = float4(input[0].world_position, 1.0f);
			bottom_left.x -= half_width;
		bottom_left = mul(bottom_left, view_matrix); // to camera space
		bottom_left = mul(bottom_left, projection_matrix); // to screen space

		// top left vert
		float4 top_left = float4(input[0].world_position, 1.0f);
			top_left.x -= half_width;
		top_left.y += height;
		top_left = mul(top_left, view_matrix); // to camera space
		top_left = mul(top_left, projection_matrix); // to screen space

		// bottom right vert
		float4 bottom_right = float4(input[0].world_position, 1.0f);
			bottom_right.x += half_width;
		bottom_right = mul(bottom_right, view_matrix); // to camera space
		bottom_right = mul(bottom_right, projection_matrix); // to screen space

		// bottom right vert
		float4 top_right = float4(input[0].world_position, 1.0f);
			top_right.x += half_width;
		top_right.y += height;
		top_right = mul(top_right, view_matrix); // to camera space
		top_right = mul(top_right, projection_matrix); // to screen space

		// Triangle 1
		element.screen_position = bottom_left;
		element.uv = float2(0.0f, 1.0f);
		output.Append(element);
		element.screen_position = top_left;
		element.uv = float2(0.0f, 0.0f);
		output.Append(element);
		element.screen_position = bottom_right;
		element.uv = float2(1.0f, 1.0f);
		output.Append(element);

		// Triangle 2
		element.screen_position = bottom_right;
		element.uv = float2(1.0f, 1.0f);
		output.Append(element);
		element.screen_position = top_left;
		element.uv = float2(0.0f, 0.0f);
		output.Append(element);
		element.screen_position = top_right;
		element.uv = float2(1.0f, 0.0f);
		output.Append(element);
	}

	output.RestartStrip();

	{
		// bottom left vert
		float4 bottom_left = float4(input[0].world_position, 1.0f);
			bottom_left.z -= half_width;
		bottom_left = mul(bottom_left, view_matrix); // to camera space
		bottom_left = mul(bottom_left, projection_matrix); // to screen space

		// top left vert
		float4 top_left = float4(input[0].world_position, 1.0f);
			top_left.z -= half_width;
		top_left.y += height;
		top_left = mul(top_left, view_matrix); // to camera space
		top_left = mul(top_left, projection_matrix); // to screen space

		// bottom right vert
		float4 bottom_right = float4(input[0].world_position, 1.0f);
			bottom_right.z += half_width;
		bottom_right = mul(bottom_right, view_matrix); // to camera space
		bottom_right = mul(bottom_right, projection_matrix); // to screen space

		// bottom right vert
		float4 top_right = float4(input[0].world_position, 1.0f);
			top_right.z += half_width;
		top_right.y += height;
		top_right = mul(top_right, view_matrix); // to camera space
		top_right = mul(top_right, projection_matrix); // to screen space

		// Triangle 1
		element.screen_position = bottom_left;
		element.uv = float2(0.0f, 1.0f);
		output.Append(element);
		element.screen_position = top_left;
		element.uv = float2(0.0f, 0.0f);
		output.Append(element);
		element.screen_position = bottom_right;
		element.uv = float2(1.0f, 1.0f);
		output.Append(element);

		// Triangle 2
		element.screen_position = bottom_right;
		element.uv = float2(1.0f, 1.0f);
		output.Append(element);
		element.screen_position = top_left;
		element.uv = float2(0.0f, 0.0f);
		output.Append(element);
		element.screen_position = top_right;
		element.uv = float2(1.0f, 0.0f);
		output.Append(element);
	}

	output.RestartStrip();
}