#pragma once

#include "CollisionLibrary.h"
#include "CollisionFlags.h"
#include <vector>
#include <functional>
#include <unordered_map>

using namespace CollisionLibrary;

template<class Type>
class Collider
{
	template<class Type>
	friend class ObjectTree;
public:
	Collider(void) = default;
	~Collider(void) = default;

	void BindOnCollideFunction(std::function<void(Type collider1, Type collider2, bool collided_xyz[3])> on_collide_function);
	void BindOnTriggerFunction(std::function<void(Type collider1, Type collider2)> on_trigger_function);

	void EnableCollisionFlag(unsigned int collision_flag);
	void DisableCollisionFlag(unsigned int collision_flag);
	bool IsCollisionFlagEnabled(unsigned int collision_flag) const;

	inline bool CollisionFlagsChanged() const { return _collision_flags_dirty; }

	Type _object;
	AABB _aabb;
	AABB _previous_aabb;

	XMFLOAT4X4 _world_matrix;

	const std::vector<Triangle>* _triangles = nullptr;

private:
	unsigned int _collision_flags = 0;

	bool _collision_flags_dirty = false;

	unsigned int _unique_id = 0;

	std::function<void(Type collider1, Type collider2, bool collided_xyz[3])> _on_collide_function = nullptr;
	std::function<void(Type collider1, Type collider2)> _on_trigger_function = nullptr;
};

template<class Type>
void Collider<Type>::BindOnCollideFunction(std::function<void(Type collider1, Type collider2, bool collided_xyz[3])> on_collide_function)
{
	if (on_collide_function == nullptr)
	{
		if (IsCollisionFlagEnabled(COLLISION_FLAG::COLLIDABLE))
		{
			DisableCollisionFlag(COLLISION_FLAG::COLLIDABLE);

			_on_collide_function = nullptr;
		}
		else
		{
			Log::Warning("Collider::BindOnCollideFunction - OnCollide function is set to null\n");
		}
	}
	else
	{
		if (IsCollisionFlagEnabled(COLLISION_FLAG::COLLIDABLE) == false)
		{
			EnableCollisionFlag(COLLISION_FLAG::COLLIDABLE);

			_on_collide_function = on_collide_function;
		}
		else
		{
			Log::Warning("Collider::BindOnCollideFunction - OnCollide function is already bound\n");
		}
	}
}

template<class Type>
void Collider<Type>::BindOnTriggerFunction(std::function<void(Type collider1, Type collider2)> on_trigger_function)
{
	if (on_trigger_function == nullptr)
	{
		if (IsCollisionFlagEnabled(COLLISION_FLAG::TRIGGER))
		{
			DisableCollisionFlag(COLLISION_FLAG::TRIGGER);

			_on_trigger_function = nullptr;
		}
		else
		{
			Log::Warning("Collider::BindOnTriggerFunction - OnTrigger function is already set to null\n");
		}
	}
	else
	{
		if (IsCollisionFlagEnabled(COLLISION_FLAG::TRIGGER) == false)
		{
			EnableCollisionFlag(COLLISION_FLAG::TRIGGER);

			_on_trigger_function = on_trigger_function;
		}
		else
		{
			Log::Warning("Collider::BindOnTriggerFunction - OnTrigger function is already bound\n");
		}
	}
}

template<class Type>
void Collider<Type>::EnableCollisionFlag(unsigned int collision_flag)
{
	if (IsCollisionFlagEnabled(collision_flag) == false)
	{
		_collision_flags |= collision_flag;

		_collision_flags_dirty = true;
	}
}

template<class Type>
void Collider<Type>::DisableCollisionFlag(unsigned int collision_flag)
{
	if (IsCollisionFlagEnabled(collision_flag))
	{
		_collision_flags &= ~collision_flag;

		_collision_flags_dirty = true;
	}
}

template<class Type>
bool Collider<Type>::IsCollisionFlagEnabled(unsigned int collision_flag) const
{
	return (_collision_flags & collision_flag) == collision_flag;
}