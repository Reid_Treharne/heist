#include "Advanced Collisions.h"

// studentvfiler\ed3\labs\sphereToTriangle.zip
bool IntersectRayTriangle( const vec3f &vert0, const vec3f &vert1, const vec3f &vert2, const vec3f &norm, const vec3f &start, const vec3f &d, float &t )
{
	// TODO: Read the header file comments for this function!

	// TODO: Complete this function
	// Tip: Use the SameSign() macro

	//		If the ray starts behind the triangle or the dot product of the ray normal and tri normal is greater than ED_EPSILON, return false.
	//		Implement the algorithm presented in "Intersecting Line to Triangle 2.ppt"
	//		Assume that the vertices are already sorted properly.

	// *Skip testing against backfacing triangles*
	//	If the ray starts behind the triangle plane OR the angle between ray direction and tri normal is greater than 90 degrees
	//		Stop testing
	if (dot_product(start, norm) - dot_product(vert0, norm) < 0)
		return false;
	if (dot_product(d, norm) > ED_EPSILON)
		return false;

	vec3f edge0 = vert0 - start;
	vec3f edge1 = vert1 - start;
	vec3f edge2 = vert2 - start;

	vec3f norm0, norm1, norm2;
	cross_product(norm0, edge2, edge1);
	cross_product(norm1, edge0, edge2);
	cross_product(norm2, edge1, edge0);

	float d0, d1, d2;
	d0 = dot_product(norm0, d);
	d1 = dot_product(norm1, d);
	d2 = dot_product(norm2, d);

	if (d0 == d1 == d2 == 0.0f)
		t = 0.0f;

	if (!SameSign(d0, d1))
		return false;
	if (!SameSign(d0, d2))
		return false;

	float offset = dot_product(vert0, norm);
	t = (offset - dot_product(start, norm)) / dot_product(norm, d);

	return true;
}

bool IntersectRaySphere(const vec3f &p, const vec3f &d, const vec3f &center, float radius, float &t, vec3f &q)
{
	// TODO: Read the header file comments for this function!
	//		p - start of the ray
	//		d - direction of the ray (normalized)
	//		center - center point of the sphere
	//		radius - radius of the sphere
	//
	// Out:
	//		t - Time of intersection, if any
	//		q - point of intersection, if any
	// TODO: Complete this function
	//		 BE SURE TO MODIFY THE ALGORITHM AS SPECIFIED IN THE HEADER FILE COMMENTS
	vec3f m = p - center;
	float b = dot_product( m, d );
	float c = dot_product( m, m ) - radius* radius; // Squared distance from start of ray to sphere surface
	if( /*c > 0.0f*/ b > 0.0f )
		return false; // Ray points away
	float discr = b*b - c;
	if( discr < 0.0f )
		return false; // Negative discriminant means ray missed
	t = -b - sqrtf( discr );
	if( t < 0.0f )
		t = 0.0f; // Ray starts inside sphere, clamp to 0
	q = p + d * t;

	return true;

}

bool IntersectRayCylinder(const vec3f &sa, const vec3f &n, const vec3f &p, const vec3f &q, float r, float &t)
{	
	// TODO: Read the header file comments for this function!

	vec3f d = q - p; // vector from first point on cylinder segment to the end point on cylinder segment
	vec3f m = sa - p; // vector from first point on cylinder segment to start point of ray

	// Values used to calculate coefficients of quadratic formula.
	// You do not necessarily have to use any of these directly for the rest of the algorithm.
	float dd = dot_product( d, d ); // dot product of d with d (squared magnitude of d)
	float nd = dot_product( n, d ); // dot product of ray normal (n) with d
	float mn = dot_product( m, n ); 
	float md = dot_product( m, d ); 
	float mm = dot_product( m, m ); 


	// TODO: Optimization by early out
	//		 If the ray starts outside the top or bottom planes and points away, there can be no intersection.
	float length = d.magnitude();
	vec3f normal = d / length;
	vec3f toRay = p - sa;
	vec3f toTop = q - sa;
	float RayStart = dot_product(toRay, normal);
	if (RayStart < 0 || RayStart > length)
	{
		if (dot_product(toRay, n) < 0 && dot_product(toRay, n) < 0)
			return false;
	}
	//float b0 = dot_product( n, d );
	//float dot = dot_product(d, toStart);
	//if (dot < 0 || dot > length)
	//	if (b0 > 0)
	//		return false;


	// Coefficients for the quadratic formula
	float a = dd - nd * nd;
	float b = dd*mn - nd*md;
	float c = dd*(mm - r*r) - md*md;

	// If a is approximately 0.0 then the ray is parallel to the cylinder and can't intersect
	if( abs(a) < FLT_EPSILON )
		return false;

	// TODO: Find time of intersection, if any
	//		 Use the quadratic formula to solve for t. Reference "Advanced Ray to Sphere.ppt" for an example.
	//		 As with "Advanced Ray to Sphere", the 2s and 4 in the formula ( x = (-b - sqrt(b*b - 4ac)) / 2a )
	//		 are cancelled out, resulting in a simplified form.
	t = 0;
	if ( a > -0.01f && a < 0.01f)
		return false;

	float disc = b*b-a*c;
	if (disc < 0.0f)
		return false;

	t = (-b - sqrtf(disc))/a;
	if (t < 0.0f)
		return false;
	vec3f cp = sa + n * t;

	vec3f ToCP = cp - p;
	d.normalize();
	float z = dot_product(d, ToCP);
	if (z < 0)
		return false;
	if (z > length)
		return false;
	return true;
}

bool IntersectRayCapsule(const vec3f &sa, const vec3f &n, const vec3f &p, const vec3f &q, float r, float &t)
{

	if (IntersectRayCylinder(sa, n, p, q, r, t))
		return true;

	vec3f idc;
	bool collided0 = false, collided1 = false;
	float bestT;
	if (IntersectRaySphere(sa, n, p, r, t, idc))
	{
		collided0 = true;
		bestT = t;
	}

	if (IntersectRaySphere(sa, n, q, r, t, idc))
	{
		collided1 = true;
		if (collided0)
		{
			if (t < bestT)
				bestT = t;
		}
		else
			bestT = t;
	}

	if (collided0 || collided1)
	{
		t = bestT;
		return true;
	}

	return false;
}

bool IntersectMovingSphereTriangle( const vec3f &vert0, const vec3f &vert1, const vec3f &vert2, const vec3f &norm, const vec3f &start, const vec3f &d, float r, float &t, vec3f &outNormal )
{
	// TODO: Read the header file comments for this function!

	bool bReturn = false;
	float fTime = FLT_MAX;
	t = FLT_MAX;

	vec3f verts[3] = { vert0, vert1, vert2 };

	// TODO: Complete this function	
	vec3f offset = norm * r;
	for (vec3f& v : verts)
		v += offset;

	outNormal = norm;
	if (IntersectRayTriangle(vert0, vert1, vert2, norm, start, d, t) == false)
	{
		//		sa - start of the ray
		//		n - direction of the ray (normalized)
		//		p - First point on the capsule segment
		//		q - Second point on the capsule segment
		//		r - radius of the capsule
		bool collision = false;
		float fTime = UINT_MAX;
		vec3f C;
		if (IntersectRayCapsule(start, d, vert0, vert1, r, t))
		{
			fTime = t;
			C = vert1 - vert0;
			collision = true;
		}
		if (IntersectRayCapsule(start, d, vert1, vert2, r, t))
		{
			if (t < fTime)
				fTime = t;
			C = vert2 - vert1;
			collision = true;
		}
		if (IntersectRayCapsule(start, d, vert2, vert0, r, t))
		{
			if (t < fTime)
				fTime = t;
			C = vert0 - vert2;
			collision = true;
		}
		if (collision)
		{
			vec3f poi; // point of intersection
			poi = start + d*t;
			vec3f V = poi - start;
			float dot = dot_product(V, C) / dot_product(C, C);
			if (dot < 0)
				dot = 0;
			else if (dot > 1)
				dot = 1;
			vec3f cp = start + C * dot;
			outNormal = (poi - cp).normalize();
			t = fTime;
			return true;
		}
		else
			return false;

	}

	return true;
}

bool IntersectMovingSphereMesh( const vec3f &start, const vec3f &d, float r, const ED2Mesh* mesh, float &t, vec3f &outNormal )
{
	// TODO: Read the header file comments for this function!

	bool bCollision = false;
	t = FLT_MAX;
	float fTime = FLT_MAX;

	// TODO: Complete this function
	//		For each triangle...
	//			Sort the indices from lowest to highest as described in "Intersecting Line to Triangle 2.ppt".
	//				This sorting can be done in a handful of "if" statements. You are sorting a very small set of values, keep it simple!
	//			Perform IntersectMovingSphereTriangle with each triangle, finding the earliest time of intersection.
	//			Return the surface normal at the point of earliest intersection as outNormal.
	float eariest = 1000000;
	for (const ED2Triangle& tri : mesh->m_Triangles)
	{
		unsigned s, m, l;
		s = tri.indices[0];
		if (tri.indices[1] < s)
			s = tri.indices[1];
		if (tri.indices[2] < s)
			s = tri.indices[2];

		l = tri.indices[0];
		if (tri.indices[1] > l)
			l = tri.indices[1];
		if (tri.indices[2] > l)
			l = tri.indices[2];

		if (s != tri.indices[0] && l != tri.indices[0])
			m = tri.indices[0];
		else if (l != tri.indices[1] && l != tri.indices[1])
			m = tri.indices[1];
		else
			m = tri.indices[2];


		vec3f vert0 = mesh->m_Vertices[s].pos;
		vec3f vert1 = mesh->m_Vertices[m].pos;
		vec3f vert2 = mesh->m_Vertices[l].pos;
		vec3f norm = mesh->m_Vertices[l].norm;

		vec3f outNorm;

		if (IntersectMovingSphereTriangle(vert0, vert1, vert2, norm, start, d, r, t, outNormal))
		{
			bCollision = true;
			if (t < eariest)
			{
				eariest = t;
				outNorm = outNormal;
			}
		}

		if (bCollision)
			t = eariest;

	}

	return bCollision;
}



