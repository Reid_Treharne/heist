#pragma once

#include "CollisionLibrary.h"
#include "CollisionFlags.h"
#include "Collider.h"
#include <vector>
#include <array>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include "../Logger/Log.h"

template<class Type>
class ObjectTree
{
	struct Node
	{
		Node()
		{
			memset(_nodes, 0, sizeof(_nodes));
		}

		Node* _nodes[8];

		AABB _aabb;

		// Maps collision flags to list of colliders 
		std::unordered_map<unsigned int, std::vector<Collider<Type>>> _objects;
	};

public:
	ObjectTree(void);
	ObjectTree(const AABB& aabb);
	~ObjectTree(void);

	bool AddObject(Collider<Type>& collider);
	void RemoveObject(Collider<Type>& collider);
	void UpdateObject(Collider<Type>& collider);
	std::vector<std::pair<Type, XMFLOAT3>> CheckCollisionWithRay(const XMFLOAT3& start_point, const XMFLOAT3& end_point, COLLISION_FLAG collision_flags) const;
	void GetAABBs(std::vector<AABB>& aabbs) const;
	void Resize(const AABB& aabb);
	void Clear();

private:
	Node* _head;

	float _min_size;

	bool AddObject(Node* node, Collider<Type>& object);
	void CheckCollision(Collider<Type>& collider);
	void CheckCollisionWithRay(Node* node, const XMFLOAT3& start_point, const XMFLOAT3& end_point, COLLISION_FLAG collision_flags, std::vector<std::pair<Type, XMFLOAT3>>& collided_objects) const;
	void GetAABBs(Node* node, std::vector<AABB>& aabbs) const;
	void Clear(Node* node);
	void SplitNode(Node* node);

	std::unordered_map<unsigned int, std::vector<Node*>> _keys;
};

template <class Type>
ObjectTree<Type>::ObjectTree(void) :
	_head(nullptr)
{
}

template <class Type>
ObjectTree<Type>::ObjectTree(const AABB& aabb) :
	_head(nullptr)
{
	Resize(aabb);
}

template <class Type>
ObjectTree<Type>::~ObjectTree(void)
{
	Clear();
}

template <class Type>
void ObjectTree<Type>::Resize(const AABB& aabb)
{
	Clear();

	auto min_x = aabb.max.x - aabb.min.x;
	auto min_y = aabb.max.y - aabb.min.y;
	auto min_z = aabb.max.z - aabb.min.z;

	auto minimum = min(min_z, min(min_x, min_y));

	_min_size = minimum / 5.0f;

	_head = new Node();
	_head->_aabb = aabb;

	SplitNode(_head);
}

template <class Type>
bool ObjectTree<Type>::AddObject(Collider<Type>& collider)
{
	static unsigned int unique_id = 1;

	collider._unique_id = unique_id;

	auto object_added = AddObject(_head, collider);

	if (object_added)
	{
		unique_id++;
	}

	return object_added;
}

template <class Type>
void ObjectTree<Type>::RemoveObject(Collider<Type>& collider)
{
	auto keys_iter = _keys.find(collider._unique_id);

	if (keys_iter != _keys.end())
	{
		auto found_object = false;

		auto& nodes = keys_iter->second;

		for (auto& node : nodes)
		{
			auto object_found_in_node = false;

			for (auto collidable_objects_iter = node->_objects.begin(); collidable_objects_iter != node->_objects.end();)
			{
				auto& colliders = collidable_objects_iter->second;

				auto iter = colliders.begin();

				for (; iter != colliders.end(); ++iter)
				{
					if (iter->_object == collider._object)
					{
						break;
					}
				}
				if (iter != colliders.end())
				{
					found_object = true;

					object_found_in_node = true;

					colliders.erase(iter);

					// If there are no more colliders in this set of collidable objects,
					// Remove this set of collidable objects from the objects map
					if (colliders.empty())
					{
						collidable_objects_iter = node->_objects.erase(collidable_objects_iter);
					}
					else
					{
						++collidable_objects_iter;
					}
				}
				else
				{
					++collidable_objects_iter;
				}
			}

			if (object_found_in_node == false)
			{
				// If this gets hit then nodes being added to the _keys
				// map do not all contain the object associated with their key
				Log::Warning("ObjectTree::RemoveObject - Could not find object in node\n");
			}
		}

		if (found_object == false)
		{
			Log::Warning("ObjectTree::RemoveObject - Could not find object in tree\n");
		}

		// clear the vector of nodes that contains this object
		_keys.erase(keys_iter);
	}
	else
	{
		Log::Warning("ObjectTree:RemoveObject - Could not find unique ID\n");
	}
}

template <class Type>
void ObjectTree<Type>::UpdateObject(Collider<Type>& collider)
{
	collider._collision_flags_dirty = false;

	auto keys_iter = _keys.find(collider._unique_id);

	if (keys_iter != _keys.end())
	{
		RemoveObject(collider);

		AddObject(_head, collider);

		if (collider.IsCollisionFlagEnabled(COLLISION_FLAG::COLLIDABLE) ||
			collider.IsCollisionFlagEnabled(COLLISION_FLAG::TRIGGER) ||
			collider.IsCollisionFlagEnabled(COLLISION_FLAG::TRIGGERABLE))
		{
			CheckCollision(collider);
		}
	}
}

template <class Type>
std::vector<std::pair<Type, XMFLOAT3>> ObjectTree<Type>::CheckCollisionWithRay(const XMFLOAT3& start_point, const XMFLOAT3& end_point, COLLISION_FLAG collision_flags) const
{
	// This function will return a vector of all the objects that the line collides with along with the collision point of each object. Note that they are not sorted.
	std::vector<std::pair<Type, XMFLOAT3>> collided_objects;

	CheckCollisionWithRay(_head, start_point, end_point, collision_flags, collided_objects);

	return collided_objects;
}

template <class Type>
void ObjectTree<Type>::CheckCollisionWithRay(
	Node* node,
	const XMFLOAT3& start_point,
	const XMFLOAT3& end_point,
	COLLISION_FLAG collision_flags,
	std::vector<std::pair<Type, XMFLOAT3>>& collided_objects) const
{
	auto has_children = node->_nodes[0] != nullptr;

	// If this isn't a child node, keep going...
	if (has_children)
	{
		if (CollisionLibrary::LineSegmentToAABB(start_point, end_point, node->_aabb))
		{
			for (auto n : node->_nodes)
			{
				CheckCollisionWithRay(n, start_point, end_point, collision_flags, collided_objects);
			}
		}
	}
	else
	{
		// If the line segment intersects with the AABB of the object tree then begin checking collision for each object in it.
		if (CollisionLibrary::LineSegmentToAABB(start_point, end_point, node->_aabb))
		{
			for (auto& collidable_objects : node->_objects)
			{
				auto& current_collision_flags = collidable_objects.first;

				// Check if the collision flags that we care about are set for this set of collidable objects 
				if ((current_collision_flags & collision_flags) == collision_flags)
				{
					auto& colliders = collidable_objects.second;

					for (const auto& collider : colliders)
					{
						auto find = [&collider](const std::pair<Type, XMFLOAT3>& object)
						{
							return collider._object == object.first;
						};

						// Since the same object can be stored in multiple nodes, make sure this object hasn't already been added
						// to the list of collided objects before doing expensive collision detection
						if (std::find_if(collided_objects.begin(), collided_objects.end(), find) == collided_objects.end())
						{
							if (CollisionLibrary::LineSegmentToAABB(start_point, end_point, collider._aabb))
							{
								XMFLOAT3 out;
								unsigned int collided_triangle_index;

								auto start_point_object_space = start_point;
								auto end_point_object_space = end_point;

								// Put the start and end point into object space
								XMStoreFloat3(&start_point_object_space, XMVector3Transform(XMLoadFloat3(&start_point), XMMatrixInverse(nullptr, XMLoadFloat4x4(&collider._world_matrix))));
								XMStoreFloat3(&end_point_object_space, XMVector3Transform(XMLoadFloat3(&end_point), XMMatrixInverse(nullptr, XMLoadFloat4x4(&collider._world_matrix))));

								auto collided = CollisionLibrary::LineSegmentToTriangle(out, collided_triangle_index, &(*collider._triangles)[0], collider._triangles->size(), start_point_object_space, end_point_object_space);

								if (collided)
								{
									// Move the collision point from object space world space
									XMStoreFloat3(&out, XMVector3Transform(XMLoadFloat3(&out), XMLoadFloat4x4(&collider._world_matrix)));

									collided_objects.emplace_back(collider._object, out);
								}
							}
						}
					}
				}
			}
		}
	}
}

template <class Type>
void ObjectTree<Type>::GetAABBs(std::vector<AABB>& aabbs) const
{
	GetAABBs(_head, aabbs);
}

template <class Type>
void ObjectTree<Type>::Clear()
{
	Clear(_head);
}

template <class Type>
bool ObjectTree<Type>::AddObject(Node* node, Collider<Type>& collider)
{
	auto object_added = false;

	if (node)
	{
		auto has_children = node->_nodes[0] != nullptr;

		if (has_children)
		{
			for (int i = 0; i < 8; i++)
			{
				auto& child = node->_nodes[i];

				if (CollisionLibrary::CheckCollision(collider._aabb, child->_aabb))
				{
					object_added = AddObject(child, collider);
				}

			}
		}
		else
		{
			if (CollisionLibrary::CheckCollision(collider._aabb, node->_aabb))
			{
				object_added = true;

				node->_objects[collider._collision_flags].push_back(collider);

				_keys[collider._unique_id].push_back(node);
			}
		}
	}

	return object_added;
}

template <class Type>
std::vector<unsigned int> GetCollisionFunctions(unsigned int collision_flags_1, unsigned int collision_flags_2)
{
	std::vector<unsigned int> collision_functions;

	// Collidable
	if ((collision_flags_1 & COLLISION_FLAG::COLLIDABLE) == COLLISION_FLAG::COLLIDABLE)
	{
		if ((collision_flags_2 & COLLISION_FLAG::COLLIDABLE) == COLLISION_FLAG::COLLIDABLE)
		{
			collision_functions.push_back(COLLISION_FLAG::COLLIDABLE);
		}
	}
	// Trigger
	if ((collision_flags_1 & COLLISION_FLAG::TRIGGER) == COLLISION_FLAG::TRIGGER)
	{
		if ((collision_flags_2 & COLLISION_FLAG::TRIGGERABLE) == COLLISION_FLAG::TRIGGERABLE)
		{
			collision_functions.push_back(COLLISION_FLAG::TRIGGER);
		}
	}
	// Triggerable
	if ((collision_flags_1 & COLLISION_FLAG::TRIGGERABLE) == COLLISION_FLAG::TRIGGERABLE)
	{
		if ((collision_flags_2 & COLLISION_FLAG::TRIGGER) == COLLISION_FLAG::TRIGGER)
		{
			collision_functions.push_back(COLLISION_FLAG::TRIGGERABLE);
		}
	}

	return collision_functions;
}

template <class Type>
void ObjectTree<Type>::CheckCollision(Collider<Type>& collider)
{
	auto& nodes = _keys[collider._unique_id];

	for (auto node : nodes)
	{
		for (auto& collidable_objects : node->_objects)
		{
			auto collision_functions = GetCollisionFunctions<Type>(collider._collision_flags, collidable_objects.first);

			// Check if the collision flag for COLLIDABLE is enabled for this set of collidable objects
			if (collision_functions.empty() == false)
			{
				auto& colliders = collidable_objects.second;

				for (auto& c : colliders)
				{
					if (c._object == collider._object)
					{
						continue;
					}

					auto collides = CollisionLibrary::CheckCollision(collider._aabb, c._aabb, true);

					if (collides)
					{
						for (auto collision_function : collision_functions)
						{
							if (collision_function == COLLISION_FLAG::COLLIDABLE)
							{
								// If we we've collided with an object, check collision on each axis individually.
								// This information is used to resolve the collision.
								auto aabb_x = collider._previous_aabb;
								auto aabb_y = collider._previous_aabb;
								auto aabb_z = collider._previous_aabb;

								aabb_x.min.x = collider._aabb.min.x; aabb_x.max.x = collider._aabb.max.x;
								aabb_y.min.y = collider._aabb.min.y; aabb_y.max.y = collider._aabb.max.y;
								aabb_z.min.z = collider._aabb.min.z; aabb_z.max.z = collider._aabb.max.z;

								bool collided_xyz[3] = { false, false, false };

								collided_xyz[0] = CollisionLibrary::CheckCollision(aabb_x, c._aabb, true);
								collided_xyz[1] = CollisionLibrary::CheckCollision(aabb_y, c._aabb, true);
								collided_xyz[2] = CollisionLibrary::CheckCollision(aabb_z, c._aabb, true);

								collider._on_collide_function(collider._object, c._object, collided_xyz);
							}
							else if (collision_function == COLLISION_FLAG::TRIGGER)
							{
								collider._on_trigger_function(collider._object, c._object);
							}
							else if (collision_function == COLLISION_FLAG::TRIGGERABLE)
							{
								c._on_trigger_function(c._object, collider._object);
							}
						}
					}
				}
			}
		}
	}
}

template <class Type>
void ObjectTree<Type>::GetAABBs(Node* node, std::vector<AABB>& aabbs) const
{
	if (node)
	{
		auto& n = *node;

		if (n._nodes[0])
		{
			for (int i = 0; i < 8; i++)
			{
				GetAABBs(n._nodes[i], aabbs);
			}
		}
		else
		{
			aabbs.push_back(n._aabb);
		}
	}
}

template <class Type>
void ObjectTree<Type>::Clear(Node* node)
{
	if (node)
	{
		for (int i = 0; i < 8; i++)
		{
			Clear(node->_nodes[i]);
		}
		delete node;
	}
}

template <class Type>
void ObjectTree<Type>::SplitNode(Node* node)
{
	const auto& parent_aabb = node->_aabb;

	AABB aabbs[8];

	const auto& min = parent_aabb.min;
	const auto& max = parent_aabb.max;
	const auto half_x = min.x + ((max.x - min.x) / 2.0f);
	const auto half_y = min.y + ((max.y - min.y) / 2.0f);
	const auto half_z = min.z + ((max.z - min.z) / 2.0f);

	/*
	Back		Front
	0 - 1		2 - 3
	|	|		|	| 
	4 - 5		6 - 7
	*/

	aabbs[0].min = XMFLOAT3(min.x, half_y, half_z);
	aabbs[0].max = XMFLOAT3(half_x, max.y, max.z);

	aabbs[1].min = XMFLOAT3(half_x, half_y, half_z);
	aabbs[1].max = max;

	aabbs[2].min = XMFLOAT3(min.x, half_y, min.z);
	aabbs[2].max = XMFLOAT3(half_x, max.y, half_z);

	aabbs[3].min = XMFLOAT3(half_x, half_y, min.z);
	aabbs[3].max = XMFLOAT3(max.x, max.y, half_z);

	aabbs[4].min = XMFLOAT3(min.x, min.y, half_z);
	aabbs[4].max = XMFLOAT3(half_x, half_y, max.z);

	aabbs[5].min = XMFLOAT3(half_x, min.y, half_z);
	aabbs[5].max = XMFLOAT3(max.x, half_y, max.z);

	aabbs[6].min = min;
	aabbs[6].max = XMFLOAT3(half_x, half_y, half_z);

	aabbs[7].min = XMFLOAT3(half_x, min.y, min.z);
	aabbs[7].max = XMFLOAT3(max.x, half_y, half_z);

	if (aabbs[0].max.x - aabbs[0].min.x >= _min_size)
	{
		for (int i = 0; i < 8; i++)
		{
			node->_nodes[i] = new Node();
			node->_nodes[i]->_aabb = aabbs[i];

			SplitNode(node->_nodes[i]);
		}
	}
}

