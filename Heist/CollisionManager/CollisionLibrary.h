#pragma once

#include <complex>
#include <DirectXMath.h>

#define PI 3.1415f
#define FL_EPSILON 0.00001f

using namespace DirectX;

namespace CollisionLibrary
{
	struct Plane
	{
		XMFLOAT3 normal;
		float offset;
	};

	struct AABB
	{
		XMFLOAT3 min;
		XMFLOAT3 max;

		AABB()
		{
			min = (XMFLOAT3(0.0f, 0.0f, 0.0f));
			max = (XMFLOAT3(0.0f, 0.0f, 0.0f));
		}

		// Note that this checks the dimensions of the AABB for equality, not the position
		bool operator==(const AABB& right) const
		{
			return (std::abs((max.x - min.x) - (right.max.x - right.min.x)) < FL_EPSILON) &&
				(std::abs((max.y - min.y) - (right.max.y - right.min.y)) < FL_EPSILON) &&
				(std::abs((max.z - min.z) - (right.max.z - right.min.z)) < FL_EPSILON);
		}
	};

	struct Frustum
	{
		Plane planes[6];
		XMFLOAT3 corners[8];
	};

	struct Segment
	{
		XMFLOAT3 m_Start;
		XMFLOAT3 m_End;
	};

	struct Sphere
	{
		XMFLOAT3 m_Center;
		float m_Radius;
	};

	struct Capsule
	{
		Segment m_Segment;
		float m_Radius;
	};

	struct Triangle
	{
		XMFLOAT3 vertices[3];
		XMFLOAT3 normal;
	};

	enum FrustumCorners { FTL = 0, FBL, FBR, FTR, NTL, NTR, NBR, NBL };
	enum FrustumPlanes { NEAR_PLANE = 0, FAR_PLANE, LEFT_PLANE, RIGHT_PLANE, TOP_PLANE, BOTTOM_PLANE };

	bool operator==(const XMFLOAT3& lhs, const XMFLOAT3& rhs);

	bool operator!=(const XMFLOAT3& lhs, const XMFLOAT3& rhs);

	XMFLOAT3 operator-(const XMFLOAT3& lhs, const XMFLOAT3& rhs);

	XMFLOAT3 operator+(const XMFLOAT3& lhs, const XMFLOAT3& rhs);

	XMFLOAT3 operator-(const XMFLOAT3& lhs, float value);

	XMFLOAT3 operator+(const XMFLOAT3& lhs, float value);

	XMFLOAT3 operator*(const XMFLOAT3& float3, float scaler);

	XMFLOAT3 Axis(const XMFLOAT4X4& matrix, unsigned int axis);

	void CrossProduct(const XMFLOAT3& lhs, const XMFLOAT3& rhs, XMFLOAT3& out);

	void Normalize(const XMFLOAT3& in, XMFLOAT3& out);

	void NormalizeTriangle(Triangle& in_out);

	float DotProduct(const XMFLOAT3& lhs, const XMFLOAT3& rhs);

	float Magnitude(const XMFLOAT3& float3);

	float MagnitudeSquared(const XMFLOAT3& float3);

	XMFLOAT3 Absolute(const XMFLOAT3& float3);

	bool AABBContains(const AABB& outer, const AABB& inner);

	void LookAt(XMFLOAT4X4& matrix, const XMFLOAT3& look_at_position);
	void LookAt2D(XMFLOAT4X4& matrix, const XMFLOAT3& look_at_position);
	void LookInDirection(XMFLOAT4X4& matrix, const XMFLOAT3& direction);

	void ComputePlane(Plane &plane, const XMFLOAT3& pointA, const XMFLOAT3& pointB, const XMFLOAT3 &pointC);
	void BuildFrustum(Frustum& frustum, float fov, float nearDist, float farDist, float ratio, const XMFLOAT4X4& camXform);

	int ClassifyPointToPlane(const Plane& plane, const XMFLOAT3& point);
	int ClassifySphereToPlane(const Plane& plane, const Sphere& sphere);
	int ClassifyAabbToPlane(const Plane& plane, const AABB& aabb);
	int ClassifyCapsuleToPlane(const Plane& plane, const Capsule& capsule);

	bool CheckCollision(const Frustum& frustum, const Sphere& sphere, bool resolve = false);
	bool CheckCollision(const Frustum& frustum, const AABB& aabb, bool resolve = false);
	bool CheckCollision(const Frustum& frustum, const Capsule& capsule, bool resolve = false);
	bool CheckCollision(const AABB& lhs, const AABB& rhs, bool resolve = false);
	bool CheckCollision(const Sphere& lhs, const Sphere& rhs, bool resolve = false);
	bool CheckCollision(const Sphere& sphere, const AABB& aabb, bool resolve = false);
	bool CheckCollision(const Sphere& sphere, const Capsule& capsule, bool resolve = false);

	bool LineSegmentToAABB(const XMFLOAT3& start, const XMFLOAT3& end, const AABB& aabb);
	bool LineSegmentToTriangle(XMFLOAT3 &vOut, unsigned int &uiTriIndex, const Triangle *pTris, unsigned int uiTriCount, const XMFLOAT3 &vStart, const XMFLOAT3 &vEnd);
}