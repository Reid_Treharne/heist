#pragma once
#include "CollisionLibrary.h"

#include <unordered_map>

using namespace CollisionLibrary;

class CollisionManager
{
public:
	void AddTriangle(const std::string& model_name, XMFLOAT3 triangle[3]);
	void EditTriangle(const std::string& model_name, XMFLOAT3 triangle[3], unsigned int index);
	bool GetModelTriangles(const std::string& model_name, const std::vector<Triangle>*& triangles) const;
	bool GetModelTriangle(const std::string& model_name, Triangle& triangle, unsigned int index) const;
	bool GetModelBounds(const std::string& model_name, AABB& bounds) const;
private:
	bool CreateTriangle(const std::string& model_name, XMFLOAT3 triangle[3], Triangle& out_triangle);

	// The triangles that make up each model
	std::unordered_map<std::string, std::vector<Triangle>> _triangles;

	// The bounding volumes represented as AABBs for each model
	std::unordered_map<std::string, AABB> _model_bounds;
};