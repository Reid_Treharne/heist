#include "CollisionManager.h"
#include "../Logger/Log.h"

void CollisionManager::AddTriangle(const std::string& model_name, XMFLOAT3 triangle[3])
{
	Triangle new_triangle;

	auto traingle_valid = CreateTriangle(model_name, triangle, new_triangle);

	if (traingle_valid)
	{
		auto& triangles = _triangles[model_name];

		triangles.push_back(std::move(new_triangle));
	}
}

void CollisionManager::EditTriangle(const std::string& model_name, XMFLOAT3 triangle[3], unsigned int index)
{
	// Get the triangles assoicated with this model
	auto _triangle_iter = _triangles.find(model_name);
	auto _model_bounds_iter = _model_bounds.find(model_name);

	if (_triangle_iter != _triangles.end() && _model_bounds_iter != _model_bounds.end())
	{
		Triangle new_triangle;

		auto traingle_valid = CreateTriangle(model_name, triangle, new_triangle);

		_triangle_iter->second[index] = std::move(new_triangle);
	}
	else
	{
		Log::Error("CollisionManager::EditTriangle - Model name \"%s\" not found");
	}
}

bool CollisionManager::GetModelTriangles(const std::string& mesh_name, const std::vector<Triangle>*& triangles) const
{
	auto found = false;

	auto iter = _triangles.find(mesh_name);

	found = iter != _triangles.end();

	if (found)
	{
		triangles = &iter->second;
	}
	else
	{
		Log::Error("CollisionManager::GetModelTriangles - Model name \"%s\" not found");
	}

	return found;
}

bool CollisionManager::GetModelTriangle(const std::string& model_name, Triangle& triangle, unsigned int index) const
{
	auto found = false;

	auto iter = _triangles.find(model_name);

	found = iter != _triangles.end();

	if (found)
	{
		if (index < iter->second.size())
		{
			triangle = iter->second[index];
		}
		else
		{
			Log::Error("CollisionManager::GetModelTriangle - Invalid index");
		}
	}
	else
	{
		Log::Error("CollisionManager::GetModelTriangle - Model name \"%s\" not found");
	}

	return found;
}

bool CollisionManager::GetModelBounds(const std::string& model_name, AABB& bounds) const
{
	auto found = false;

	auto iter = _model_bounds.find(model_name);

	found = iter != _model_bounds.end();

	if (found)
	{
		bounds = iter->second;
	}

	return found;
}

bool CollisionManager::CreateTriangle(const std::string& model_name, XMFLOAT3 triangle[3], Triangle& out_triangle)
{
	// Get the AABB assoicated with this model
	auto& bounds = _model_bounds[model_name];

	// Check if the new triangle is still within the bounds of its AABB. If not, update its AABB.
	for (int i = 0; i < 3; i++)
	{
		out_triangle.vertices[i] = triangle[i];

		if (triangle[i].x < bounds.min.x) bounds.min.x = triangle[i].x;
		else if (triangle[i].x > bounds.max.x) bounds.max.x = triangle[i].x;
		if (triangle[i].y < bounds.min.y) bounds.min.y = triangle[i].y;
		else if (triangle[i].y > bounds.max.y) bounds.max.y = triangle[i].y;
		if (triangle[i].z < bounds.min.z) bounds.min.z = triangle[i].z;
		else if (triangle[i].z > bounds.max.z) bounds.max.z = triangle[i].z;
	}

	// Before returning the triangle, we need to calculate the triangle's normal vector. We could use the one from the fbx file
	// but if the model has smoothed edges, we want to calculate our own normal that match the actual geometry so collision
	// detection functions will work properly.
	auto left = triangle[1] - triangle[0];
	auto right = triangle[2] - triangle[1];

	CrossProduct(left, right, out_triangle.normal);

	Normalize(out_triangle.normal, out_triangle.normal);

	if (Magnitude(out_triangle.normal) < 1.0f - 0.0001f)
	{
		Log::Error("CollisionManager::CreateTriangle - Invalid Triangle\n");

		return false;
	}

	return true;
}
