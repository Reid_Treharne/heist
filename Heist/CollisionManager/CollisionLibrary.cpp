#include "CollisionLibrary.h"

namespace CollisionLibrary
{
	bool float_equals(float lhs, float rhs, float epsilon = FLT_EPSILON)
	{
		return abs(lhs - rhs) < epsilon;
	}

	bool operator==(const XMFLOAT3& lhs, const XMFLOAT3& rhs)
	{
		return (float_equals(lhs.x, rhs.x) &&
			float_equals(lhs.y, rhs.y) &&
			float_equals(lhs.z, rhs.z));
	}

	bool operator!=(const XMFLOAT3& lhs, const XMFLOAT3& rhs)
	{
		return !(lhs == rhs);
	}

	XMFLOAT3 operator-(const XMFLOAT3& lhs, const XMFLOAT3& rhs)
	{
		return XMFLOAT3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
	}

	XMFLOAT3 operator+(const XMFLOAT3& lhs, const XMFLOAT3& rhs)
	{
		return XMFLOAT3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
	}

	XMFLOAT3 operator-(const XMFLOAT3& lhs, float value)
	{
		return XMFLOAT3(lhs.x - value, lhs.y - value, lhs.z - value);
	}

	XMFLOAT3 operator+(const XMFLOAT3& lhs, float value)
	{
		return XMFLOAT3(lhs.x + value, lhs.y + value, lhs.z + value);
	}

	XMFLOAT3 operator*(const XMFLOAT3& float3, float scaler)
	{
		return XMFLOAT3(float3.x * scaler, float3.y * scaler, float3.z * scaler);
	}

	XMFLOAT3 Axis(const XMFLOAT4X4& matrix, unsigned int axis)
	{
		return XMFLOAT3(matrix.m[axis]);
	}

	void CrossProduct(const XMFLOAT3& lhs, const XMFLOAT3& rhs, XMFLOAT3& out)
	{
		XMStoreFloat3(&out, XMVector3Cross(XMLoadFloat3(&lhs), XMLoadFloat3(&rhs)));
	}

	void Normalize(const XMFLOAT3& in, XMFLOAT3& out)
	{
		XMStoreFloat3(&out, XMVector3Normalize(XMLoadFloat3(&in)));
	}

	void NormalizeTriangle(Triangle& in_out)
	{
		CrossProduct(in_out.vertices[0] - in_out.vertices[1],
			in_out.vertices[1] - in_out.vertices[2],
			in_out.normal);

		Normalize(in_out.normal, in_out.normal);
	}

	float DotProduct(const XMFLOAT3& lhs, const XMFLOAT3& rhs)
	{
		return (lhs.x * rhs.x) + (lhs.y * rhs.y) + (lhs.z * rhs.z);
	}

	float Magnitude(const XMFLOAT3& float3)
	{
		return sqrtf(DotProduct(float3, float3));
	}

	float MagnitudeSquared(const XMFLOAT3& float3)
	{
		return DotProduct(float3, float3);
	}

	XMFLOAT3 Absolute(const XMFLOAT3& float3)
	{
		return XMFLOAT3(fabsf(float3.x), fabsf(float3.y), fabsf(float3.z));
	}


	bool AABBContains(const AABB& outer, const AABB& inner)
	{
		return (inner.max.x < outer.max.x &&
			inner.max.y < outer.max.y &&
			inner.max.z < outer.max.z);
	}

	void LookAt(XMFLOAT4X4& matrix, const XMFLOAT3& look_at_position)
	{
		// Get this position from the matrix
		auto matrix_position = XMFLOAT3(matrix._41, matrix._42, matrix._43);

		// Create a vector from the matrix position to the position we want to look at
		auto z_axis = look_at_position - matrix_position;

		Normalize(z_axis, z_axis);

		LookInDirection(matrix, z_axis);
	}

	void LookAt2D(XMFLOAT4X4& matrix, const XMFLOAT3& look_at_position)
	{
		// Get this position from the matrix
		auto matrix_position = XMFLOAT3(matrix._41, matrix._41, matrix._43);

		// Create a vector from the matrix position to the position we want to look at
		auto z_axis = look_at_position - matrix_position;

		// Set the y value to 0 so the object stays looking straight
		z_axis.y = 0.0f;

		Normalize(z_axis, z_axis);

		LookInDirection(matrix, z_axis);
	}

	void LookInDirection(XMFLOAT4X4& matrix, const XMFLOAT3& direction)
	{
		const XMFLOAT3& z_axis = direction;
		XMFLOAT3 y_axis;
		XMFLOAT3 x_axis;

		// Calculate the side vector
		CrossProduct(XMFLOAT3(0.0f, 1.0f, 0.0f), z_axis, x_axis);
		Normalize(x_axis, x_axis);

		// Calculate the up vector
		CrossProduct(z_axis, x_axis, y_axis);
		Normalize(y_axis, y_axis);

		// Copy the new vectors into this object's world matrix
		memcpy_s(&matrix.m[0], sizeof(x_axis), &x_axis, sizeof(x_axis));
		memcpy_s(&matrix.m[1], sizeof(y_axis), &y_axis, sizeof(y_axis));
		memcpy_s(&matrix.m[2], sizeof(z_axis), &z_axis, sizeof(z_axis));
	}

	// ComputePlane
	//
	// Calculate the plane normal and plane offset from the input points
	void ComputePlane(Plane &plane, const XMFLOAT3& pointA, const XMFLOAT3& pointB, const XMFLOAT3 &pointC)
	{
		const XMFLOAT3 a = pointB - pointA;
		const XMFLOAT3 b = pointC - pointB;

		CrossProduct(a, b, plane.normal);
		Normalize(plane.normal, plane.normal);

		plane.offset = DotProduct(pointA, plane.normal);
	}

	// BuildFrustum
	//
	// Calculates the corner points and planes of the frustum based upon input values.
	// Should call ComputePlane.
	void BuildFrustum(Frustum& frustum, float fov, float nearDist, float farDist, float ratio, const XMFLOAT4X4& camXform)
	{
		XMFLOAT3 nc = Axis(camXform, 3) - Axis(camXform, 2) * nearDist;
		XMFLOAT3 fc = Axis(camXform, 3) - Axis(camXform, 2) * farDist;

		float x = 2.0f * tan(fov / 2.0f);
		float HNear = x * nearDist;
		float HFar = x * farDist;
		float WNear = HNear * ratio;
		float WFar = HFar * ratio;


		frustum.corners[FTL] = fc + Axis(camXform, 1) * (HFar*0.5f) - Axis(camXform, 0) * (WFar*0.5f);
		frustum.corners[FTR] = fc + Axis(camXform, 1) * (HFar*0.5f) + Axis(camXform, 0) * (WFar*0.5f);
		frustum.corners[FBL] = fc - Axis(camXform, 1) * (HFar*0.5f) - Axis(camXform, 0) * (WFar*0.5f);
		frustum.corners[FBR] = fc - Axis(camXform, 1) * (HFar*0.5f) + Axis(camXform, 0) * (WFar*0.5f);

		frustum.corners[NTL] = nc + Axis(camXform, 1) * (HNear*0.5f) - Axis(camXform, 0) * (WNear*0.5f);
		frustum.corners[NTR] = nc + Axis(camXform, 1) * (HNear*0.5f) + Axis(camXform, 0) * (WNear*0.5f);
		frustum.corners[NBL] = nc - Axis(camXform, 1) * (HNear*0.5f) - Axis(camXform, 0) * (WNear*0.5f);
		frustum.corners[NBR] = nc - Axis(camXform, 1) * (HNear*0.5f) + Axis(camXform, 0) * (WNear*0.5f);

		// left
		ComputePlane(frustum.planes[LEFT_PLANE], frustum.corners[NBL], frustum.corners[FBL], frustum.corners[FTL]);
		// right
		ComputePlane(frustum.planes[RIGHT_PLANE], frustum.corners[NBR], frustum.corners[NTR], frustum.corners[FTR]);
		// bottom
		ComputePlane(frustum.planes[BOTTOM_PLANE], frustum.corners[NBL], frustum.corners[NBR], frustum.corners[FBR]);
		// top
		ComputePlane(frustum.planes[TOP_PLANE], frustum.corners[NTL], frustum.corners[FTL], frustum.corners[FTR]);
		// back
		ComputePlane(frustum.planes[FAR_PLANE], frustum.corners[FBL], frustum.corners[FBR], frustum.corners[FTR]);
		// front
		ComputePlane(frustum.planes[NEAR_PLANE], frustum.corners[NBL], frustum.corners[NTL], frustum.corners[NTR]);
	}

	// ClassifyPointToPlane
	//
	// Perform a half-space test. Returns 1 if the point is on or in front of the plane.
	// Returns 2 if the point is behind the plane.
	int ClassifyPointToPlane(const Plane& plane, const XMFLOAT3& point)
	{
		float d = DotProduct(plane.normal, point) - plane.offset;

		if (d >= 0.0f)
			return 1;

		return 2;
	}

	// ClassifySphereToPlane
	//
	// Perform a sphere-to-plane test. 
	// Returns 1 if the sphere is in front of the plane.
	// Returns 2 if the sphere is behind the plane.
	// Returns 3 if the sphere straddles the plane.
	int ClassifySphereToPlane(const Plane& plane, const Sphere& sphere)
	{
		float distFromPlane = DotProduct(sphere.m_Center, plane.normal) - plane.offset;

		if (distFromPlane > sphere.m_Radius)
			return 1;
		if (distFromPlane < -sphere.m_Radius)
			return 2;

		return 3;
	}

	// ClassifyAabbToPlane
	//
	// Performs a AABB-to-plane test.
	// Returns 1 if the aabb is in front of the plane.
	// Returns 2 if the aabb is behind the plane.
	// Returns 3 if the aabb straddles the plane.
	int ClassifyAabbToPlane(const Plane& plane, const AABB& aabb)
	{
		XMFLOAT3 center = (aabb.min + aabb.max) * 0.5f;
		XMFLOAT3 extends = aabb.max - center;

		float projRadius = extends.x * abs(plane.normal.x) +
			extends.y * abs(plane.normal.y) +
			extends.z * abs(plane.normal.z);

		float signedDist = DotProduct(center, plane.normal) - plane.offset;

		if (signedDist > projRadius)
			return 1;
		if (signedDist < -projRadius)
			return 2;

		return 3;
	}

	// ClassifyCapsuleToPlane
	//
	// Performs a Capsule-to-plane test.
	// Returns 1 if the aabb is in front of the plane.
	// Returns 2 if the aabb is behind the plane.
	// Returns 3 if the aabb straddles the plane.
	int ClassifyCapsuleToPlane(const Plane& plane, const Capsule& capsule)
	{
		Sphere s0, s1;

		s0.m_Center = capsule.m_Segment.m_Start;
		s1.m_Center = capsule.m_Segment.m_End;
		s0.m_Radius = s1.m_Radius = capsule.m_Radius;

		int return0 = ClassifySphereToPlane(plane, s0);
		int return1 = ClassifySphereToPlane(plane, s1);

		if (return0 == 1 && return1 == 1)
			return 1;
		if (return0 == 2 && return1 == 2)
			return 2;

		return 3;
	}

	// FrustumToSphere
	//
	// Perform a Sphere-to-Frustum check. Returns true if the sphere is inside. False if not.
	bool CheckCollision(const Frustum& frustum, const Sphere& sphere, bool resolve)
	{
		for (int i = 0; i < 6; i++)
		{
			if (ClassifySphereToPlane(frustum.planes[i], sphere) == 2) // outside
				return false;
		}

		return true;
	}

	// FrustumToAABB
	//
	// Perform a Aabb-to-Frustum check. Returns true if the aabb is inside. False if not.
	bool CheckCollision(const Frustum& frustum, const AABB& aabb, bool resolve)
	{
		for (int i = 0; i < 6; i++)
		{
			if (ClassifyAabbToPlane(frustum.planes[i], aabb) == 2) // outside
				return false;
		}

		return true;
	}

	// FrustumToCapsule
	//
	// Perform a Capsule-to-Frustum check. Returns true if the Capsule is inside. False if not.
	bool CheckCollision(const Frustum& frustum, const Capsule& capsule, bool resolve)
	{
		for (int i = 0; i < 6; i++)
		{
			if (ClassifyCapsuleToPlane(frustum.planes[i], capsule) == 2) // outside
				return false;
		}

		return true;
	}

	// AABBtoAABB
	//
	// Returns true if the AABBs collide. False if not.
	bool CheckCollision(const AABB& lhs, const AABB& rhs, bool resolve)
	{
		if (lhs.max.x < rhs.min.x || lhs.min.x > rhs.max.x) return false;
		if (lhs.max.y < rhs.min.y || lhs.min.y > rhs.max.y) return false;
		if (lhs.max.z < rhs.min.z || lhs.min.z > rhs.max.z) return false;

		return true;
	}

	// SphereToSphere
	//
	// Returns true if the Spheres collide. False if not.
	bool CheckCollision(const Sphere& lhs, const Sphere& rhs, bool resolve)
	{
		XMFLOAT3 toOther = rhs.m_Center - lhs.m_Center;

		float a = DotProduct(toOther, toOther);
		float b = (lhs.m_Radius + rhs.m_Radius);
		b *= b;

		return (a < b);
	}

	// SphereToAABB
	//
	// Returns true if the sphere collides with the AABB. False if not.
	bool CheckCollision(const Sphere& sphere, const AABB& aabb, bool resolve)
	{
		// The closest point on the AABB to the Sphere's center
		XMFLOAT3 point = sphere.m_Center;

		if (sphere.m_Center.x < aabb.min.x)
			point.x = aabb.min.x;
		else if (sphere.m_Center.x > aabb.max.x)
			point.x = aabb.max.x;

		if (sphere.m_Center.y < aabb.min.y)
			point.y = aabb.min.y;
		else if (sphere.m_Center.y > aabb.max.y)
			point.y = aabb.max.y;

		if (sphere.m_Center.z < aabb.min.z)
			point.z = aabb.min.z;
		else if (sphere.m_Center.z > aabb.max.z)
			point.z = aabb.max.z;

		XMFLOAT3 dist = point - sphere.m_Center;

		if (MagnitudeSquared(dist) < sphere.m_Radius * sphere.m_Radius)
			return true;

		return false;
	}

	// CapsuleToSphere
	//
	// Returns true if the capsule collides with the sphere. False if not.
	bool CheckCollision(const Sphere& sphere, const Capsule& capsule, bool resolve)
	{
		XMFLOAT3 C = capsule.m_Segment.m_End - capsule.m_Segment.m_Start;
		XMFLOAT3 V = sphere.m_Center - capsule.m_Segment.m_Start;

		float d = (DotProduct(V, C)) / (DotProduct(C, C));

		XMFLOAT3 cp;

		if (d < 0)
			cp = capsule.m_Segment.m_Start;
		else if (d > Magnitude(C))
			cp = capsule.m_Segment.m_End;
		else
			cp = capsule.m_Segment.m_Start + C * d;

		Sphere s;
		s.m_Center = cp;
		s.m_Radius = capsule.m_Radius;

		return CheckCollision(s, sphere);
	}

	bool LineSegmentToAABB(const XMFLOAT3& start, const XMFLOAT3& end, const AABB& aabb)
	{
		XMFLOAT3 d = (end - start) * 0.5f;
		XMFLOAT3 e = (aabb.max - aabb.min) * 0.5f;
		XMFLOAT3 c = start + d - (aabb.min + aabb.max) * 0.5f;
		XMFLOAT3 ad = Absolute(d); // Returns same vector with all components positive

		if (fabsf(c.x) > e.x + ad.x)
		{
			return false;
		}
		if (fabsf(c.y) > e.y + ad.y)
		{
			return false;
		}
		if (fabsf(c.z) > e.z + ad.z)
		{
			return false;
		}

		if (fabsf(d.y * c.z - d.z * c.y) > e.y * ad.z + e.z * ad.y + FLT_EPSILON)
		{
			return false;
		}
		if (fabsf(d.z * c.x - d.x * c.z) > e.z * ad.x + e.x * ad.z + FLT_EPSILON)
		{
			return false;
		}
		if (fabsf(d.x * c.y - d.y * c.x) > e.x * ad.y + e.y * ad.x + FLT_EPSILON)
		{
			return false;
		}

		return true;
	}

	// This function takes a line segment and a list of triangles to perform the line-to-triangle intersection algorithm with.
	// This implementation should find the intersecting triangle nearest to the start of the line.
	//
	// Return :
	//		bool - True if there was an intersection, false if not.
	//
	// In:
	//		EDTriangle *pTris - pointer to the list of triangles
	//		unsigned int uiTriCounnt - the number of triangles in the list
	//		const vec3f &vStart - the start of the line segment
	//		const vec3f &vEnd - the end of the line segment
	//
	// Out:
	//		vec3f &vOut - stores the collision point
	//		unsigned int &uiTriIndex - stores the index in the list of the intersected triangle
	bool LineSegmentToTriangle(XMFLOAT3 &vOut, unsigned int &uiTriIndex, const Triangle *pTris, unsigned int uiTriCount, const XMFLOAT3 &vStart, const XMFLOAT3 &vEnd)
	{
		/*Reminder: Find the NEAREST interesecting triangle*/
		// reset the line or end point to the new one found
		bool bCollided = false;
		XMFLOAT3 vNewEnd = vEnd;

		for (unsigned int i = 0; i < uiTriCount; i++)
		{
			XMFLOAT3 vNormal = pTris[i].normal;

			float fDS = DotProduct(vStart, vNormal);
			float fDE = DotProduct(vNewEnd, vNormal);
			float fDV = DotProduct(pTris[i].vertices[0], vNormal);

			// If the starting point is behind the triangle, continue
			if (fDS - fDV < 0)
			{
				continue;
			}
			// If the ending point is infront of the triangle, continue
			if (fDE - fDV > 0)
			{
				continue;
			}

			float fDist = fDS - fDV;

			XMFLOAT3 vLine = vNewEnd - vStart;

			float fDL = DotProduct(vNormal, vLine);
			float fRatio = -(fDist / fDL);

			XMFLOAT3 collisionPoint = vStart + vLine * fRatio;

			// triangle collision
			XMFLOAT3 vCurNormal;

			XMFLOAT3 vCurEdge = pTris[i].vertices[1] - pTris[i].vertices[0];
			CrossProduct(vCurEdge, vNormal, vCurNormal);
			if (DotProduct(vCurNormal, (collisionPoint - pTris[i].vertices[0])) > 0.001f)
				continue;

			vCurEdge = pTris[i].vertices[2] - pTris[i].vertices[1];
			CrossProduct(vCurEdge, vNormal, vCurNormal);
			if (DotProduct(vCurNormal, (collisionPoint - pTris[i].vertices[1])) > 0.001f)
				continue;

			vCurEdge = pTris[i].vertices[0] - pTris[i].vertices[2];
			CrossProduct(vCurEdge, vNormal, vCurNormal);
			if (DotProduct(vCurNormal, (collisionPoint - pTris[i].vertices[2])) > 0.001f)
				continue;

			bCollided = true;
			vNewEnd = collisionPoint;
			uiTriIndex = i;
		}

		vOut = vNewEnd;

		return bCollided;
	}
}