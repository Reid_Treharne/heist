#pragma once

namespace CollisionLibrary
{
	enum COLLISION_FLAG
	{
		CLICKABLE = 1 << 0,
		COLLIDABLE = 1 << 1,
		TRIGGERABLE = 1 << 2,
		TRIGGER = 1 << 3,
		NUM_OF_COLLISION_FLAGS = 4
	};
};