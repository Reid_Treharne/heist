#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include <Windows.h>

#define VS_2015_OR_NEWER (_MSC_VER >= 1900)

#if VS_2015_OR_NEWER
#define MAX_BYTES_TO_WRITE 256
#endif

class Log
{
public:
	static void Initialize();
	static void Info(const char* message, ...);
	static void Warning(const char* message, ...);
	static void Error(const char* message, ...);

private:
	Log(void) {}
	Log(const Log&) {}
	~Log(void) {}

	enum COLOR {
		BLACK,
		BLUE,
		GREEN,
		AQUA,
		RED,
		PURPLE,
		YELLOW,
		WHITE,
		GRAY,
		LIGHT_BLUE,
		LIGHT_GREEN,
		LIGHT_AQUA,
		LIGHT_RED,
		LIGHT_PURPLE,
		LIGHT_YELLOW,
		LIGHT_WHITE,

		FORGROUND_MASK = 0x0F,
		BACKGROUND_MASK = 0xF0
	};

	static void SetForeground(COLOR color);
	static void SetBackground(COLOR color);

	static bool _is_initialized;
	static HANDLE _handle;
	static WORD _current_color;

#if VS_2015_OR_NEWER
	static char message_buffer[MAX_BYTES_TO_WRITE];
#endif
};

