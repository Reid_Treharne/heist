// Windows Header Files:
#include "Log.h"
#include <stdarg.h>
#include <iostream>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>

bool Log::_is_initialized = false;
HANDLE Log::_handle = nullptr;
WORD Log::_current_color = 0;

#if VS_2015_OR_NEWER
char Log::message_buffer[MAX_BYTES_TO_WRITE];
#endif

void Log::Initialize()
{
	_is_initialized = true;

	AllocConsole();

	HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);

#if VS_2015_OR_NEWER
	memset(message_buffer, 0, MAX_BYTES_TO_WRITE);
#else
	int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
	FILE* hf_out = _fdopen(hCrt, "w");
	setvbuf(hf_out, NULL, _IONBF, 1);
	*stdout = *hf_out;

	HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
	hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
	FILE* hf_in = _fdopen(hCrt, "r");
	setvbuf(hf_in, NULL, _IONBF, 128);
	*stdin = *hf_in;
#endif
	_handle = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO info;

	GetConsoleScreenBufferInfo(_handle, &info);
	_current_color = info.wAttributes;
}

void Log::Info(const char* message, ...)
{
	if (_is_initialized == true)
	{
		SetBackground(BLACK);

		va_list argptr;
		va_start(argptr, message);
#if VS_2015_OR_NEWER
		vsprintf_s(message_buffer, MAX_BYTES_TO_WRITE, message, argptr);
		WriteFile(_handle, message_buffer, strnlen_s(message_buffer, MAX_BYTES_TO_WRITE), NULL, NULL);
#else
		vprintf(message, argptr);
#endif
		va_end(argptr);
	}
}

void Log::Warning(const char* message, ...)
{
	if (_is_initialized == true)
	{
		SetBackground(LIGHT_YELLOW);

		va_list argptr;
		va_start(argptr, message);
#if VS_2015_OR_NEWER
		vsprintf_s(message_buffer, MAX_BYTES_TO_WRITE, message, argptr);
		WriteFile(_handle, message_buffer, strnlen_s(message_buffer, MAX_BYTES_TO_WRITE), NULL, NULL);
#else
		vprintf(message, argptr);
#endif
		va_end(argptr);
	}
}

void Log::Error(const char* message, ...)
{
	if (_is_initialized == true)
	{
		SetBackground(LIGHT_RED);

		va_list argptr;
		va_start(argptr, message);
#if VS_2015_OR_NEWER
		vsprintf_s(message_buffer, MAX_BYTES_TO_WRITE, message, argptr);
		WriteFile(_handle, message_buffer, strnlen_s(message_buffer, MAX_BYTES_TO_WRITE), NULL, NULL);
#else
		vprintf(message, argptr);
#endif
		va_end(argptr);
	}
}

void Log::SetForeground(COLOR color)
{
	_current_color &= ~FORGROUND_MASK;
	_current_color |= color & FORGROUND_MASK;

	SetConsoleTextAttribute(_handle, _current_color);
}
void Log::SetBackground(COLOR color)
{
	_current_color &= ~BACKGROUND_MASK;
	_current_color |= (color << 4) & BACKGROUND_MASK;

	SetConsoleTextAttribute(_handle, _current_color);
}