#pragma once

#include <vector>

class IListener;

class IEvent
{
public:
	IEvent(void) {}
	virtual ~IEvent(void) {}

	virtual unsigned int GetID() const = 0;
};