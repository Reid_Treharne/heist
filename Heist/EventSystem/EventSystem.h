#pragma once

#include <unordered_map>

class IEvent;
class IListener;

class EventSystem
{
public:
	static EventSystem& Get();
	static void Delete();

	void AddListener(IListener* listener, unsigned int event_id);
	void RemoveListener(IListener* listener, unsigned int event_id);

	void SendEvent(const IEvent* the_event);
private:
	EventSystem(void) {}
	EventSystem(const EventSystem&) {}
	~EventSystem(void) {}

	static EventSystem* _event_system;

	std::unordered_map<unsigned int, std::vector<IListener*>> _events;
};

