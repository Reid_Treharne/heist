#pragma once

class IEvent;

class IListener
{
public:
	IListener(void) {}
	virtual ~IListener(void) {}

	virtual void OnEvent(const IEvent* the_event) = 0;
};