#include "EventSystem.h"
#include "IEvent.h"
#include "IListener.h"
#include "../Logger/Log.h"

EventSystem* EventSystem::_event_system = nullptr;

EventSystem& EventSystem::Get()
{
	if (_event_system == nullptr)
	{
		_event_system = new EventSystem();
	}

	return *_event_system;
}

void EventSystem::Delete()
{
	delete _event_system;

	_event_system = nullptr;
}

void EventSystem::AddListener(IListener* listener, unsigned int event_id)
{
	auto& listeners = _events[event_id];

	auto iter = std::find(listeners.begin(), listeners.end(), listener);

	if (iter == listeners.end())
	{
		listeners.push_back(listener);
	}
	else
	{
		Log::Error("EventSystem::AddListener - Listener already added\n");
	}
}

void EventSystem::RemoveListener(IListener* listener, unsigned int event_id)
{
	auto event_iter = _events.find(event_id);

	if (event_iter != _events.end())
	{
		auto& listeners = event_iter->second;

		auto iter = std::find(listeners.begin(), listeners.end(), listener);

		if (iter != listeners.end())
		{
			listeners.erase(iter);
		}
		else
		{
			Log::Warning("EventSystem::RemoveListener - Listener doesn't exist\n");
		}
	}
}

void EventSystem::SendEvent(const IEvent* the_event)
{
	auto event_id = the_event->GetID();

	auto iter = _events.find(event_id);

	if (iter != _events.end())
	{
		const auto& listeners = _events[event_id];

		for (auto& listener : listeners)
		{
			listener->OnEvent(the_event);
		}
	}
}

