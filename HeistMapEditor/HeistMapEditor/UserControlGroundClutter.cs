﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeistMapEditor
{
    public partial class UserControlGroundClutter : UserControlInterface
    {
        private Form1 _form1;

        public UserControlGroundClutter(Form1 form1)
        {
            InitializeComponent();

            _form1 = form1;
        }

        public override void OnClick(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
            {
                if (radioButtonRadius.Checked)
                {
                    bool add = e.Button == MouseButtons.Left;

                    _form1._graph.EditGroundClutter(new Point(e.X, e.Y), add, (int)numeric_up_down_radius.Value);
                }
            }
        }
    }
}
