﻿namespace HeistMapEditor
{
    partial class UserControlGroundClutter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.group_box_ground_clutter = new System.Windows.Forms.GroupBox();
            this.numeric_up_down_radius = new System.Windows.Forms.NumericUpDown();
            this.radioButtonRadius = new System.Windows.Forms.RadioButton();
            this.group_box_ground_clutter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_up_down_radius)).BeginInit();
            this.SuspendLayout();
            // 
            // group_box_ground_clutter
            // 
            this.group_box_ground_clutter.Controls.Add(this.numeric_up_down_radius);
            this.group_box_ground_clutter.Controls.Add(this.radioButtonRadius);
            this.group_box_ground_clutter.Location = new System.Drawing.Point(3, 3);
            this.group_box_ground_clutter.Name = "group_box_ground_clutter";
            this.group_box_ground_clutter.Size = new System.Drawing.Size(323, 144);
            this.group_box_ground_clutter.TabIndex = 0;
            this.group_box_ground_clutter.TabStop = false;
            this.group_box_ground_clutter.Text = "Ground Clutter";
            // 
            // numeric_up_down_radius
            // 
            this.numeric_up_down_radius.Location = new System.Drawing.Point(84, 23);
            this.numeric_up_down_radius.Name = "numeric_up_down_radius";
            this.numeric_up_down_radius.Size = new System.Drawing.Size(61, 22);
            this.numeric_up_down_radius.TabIndex = 19;
            // 
            // radioButtonRadius
            // 
            this.radioButtonRadius.AutoSize = true;
            this.radioButtonRadius.Checked = true;
            this.radioButtonRadius.Location = new System.Drawing.Point(5, 23);
            this.radioButtonRadius.Name = "radioButtonRadius";
            this.radioButtonRadius.Size = new System.Drawing.Size(73, 21);
            this.radioButtonRadius.TabIndex = 18;
            this.radioButtonRadius.TabStop = true;
            this.radioButtonRadius.Text = "Radius";
            this.radioButtonRadius.UseVisualStyleBackColor = true;
            // 
            // UserControlGroundClutter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.group_box_ground_clutter);
            this.Name = "UserControlGroundClutter";
            this.Size = new System.Drawing.Size(329, 150);
            this.group_box_ground_clutter.ResumeLayout(false);
            this.group_box_ground_clutter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_up_down_radius)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox group_box_ground_clutter;
        private System.Windows.Forms.NumericUpDown numeric_up_down_radius;
        private System.Windows.Forms.RadioButton radioButtonRadius;
    }
}
