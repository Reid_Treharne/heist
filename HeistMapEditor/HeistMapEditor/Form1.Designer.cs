﻿namespace HeistMapEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fILEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.combo_box_object_name = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.horizontal_scroll_bar = new System.Windows.Forms.HScrollBar();
            this.vertical_scroll_bar = new System.Windows.Forms.VScrollBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grid_canvas = new HeistMapEditor.Canvas();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fILEToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1104, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fILEToolStripMenuItem
            // 
            this.fILEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportAllToolStripMenuItem});
            this.fILEToolStripMenuItem.Name = "fILEToolStripMenuItem";
            this.fILEToolStripMenuItem.Size = new System.Drawing.Size(47, 24);
            this.fILEToolStripMenuItem.Text = "FILE";
            // 
            // exportAllToolStripMenuItem
            // 
            this.exportAllToolStripMenuItem.Name = "exportAllToolStripMenuItem";
            this.exportAllToolStripMenuItem.Size = new System.Drawing.Size(143, 24);
            this.exportAllToolStripMenuItem.Text = "Export All";
            this.exportAllToolStripMenuItem.Click += new System.EventHandler(this.exportAllToolStripMenuItem_Click);
            // 
            // combo_box_object_name
            // 
            this.combo_box_object_name.FormattingEnabled = true;
            this.combo_box_object_name.Items.AddRange(new object[] {
            "Camera",
            "Interactable",
            "Aesthetic",
            "Ground Clutter",
            "Terrain",
            "Directional_Light"});
            this.combo_box_object_name.Location = new System.Drawing.Point(6, 21);
            this.combo_box_object_name.Name = "combo_box_object_name";
            this.combo_box_object_name.Size = new System.Drawing.Size(318, 24);
            this.combo_box_object_name.TabIndex = 2;
            this.combo_box_object_name.SelectedIndexChanged += new System.EventHandler(this.combo_box_object_name_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.combo_box_object_name);
            this.groupBox2.Location = new System.Drawing.Point(12, 55);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(330, 54);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Object Name";
            // 
            // horizontal_scroll_bar
            // 
            this.horizontal_scroll_bar.Location = new System.Drawing.Point(348, 591);
            this.horizontal_scroll_bar.Name = "horizontal_scroll_bar";
            this.horizontal_scroll_bar.Size = new System.Drawing.Size(729, 21);
            this.horizontal_scroll_bar.TabIndex = 1;
            this.horizontal_scroll_bar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.horizontal_scroll_bar_Scroll);
            // 
            // vertical_scroll_bar
            // 
            this.vertical_scroll_bar.Location = new System.Drawing.Point(1074, 55);
            this.vertical_scroll_bar.Name = "vertical_scroll_bar";
            this.vertical_scroll_bar.Size = new System.Drawing.Size(21, 545);
            this.vertical_scroll_bar.TabIndex = 0;
            this.vertical_scroll_bar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vertical_scroll_bar_Scroll);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(13, 106);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(329, 298);
            this.panel1.TabIndex = 12;
            // 
            // grid_canvas
            // 
            this.grid_canvas.BackColor = System.Drawing.SystemColors.Window;
            this.grid_canvas.Location = new System.Drawing.Point(348, 55);
            this.grid_canvas.Name = "grid_canvas";
            this.grid_canvas.Size = new System.Drawing.Size(729, 545);
            this.grid_canvas.TabIndex = 4;
            this.grid_canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.grid_canvas_Paint);
            this.grid_canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.grid_canvas_MouseDown);
            this.grid_canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.grid_canvas_MouseMove);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 621);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.horizontal_scroll_bar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.vertical_scroll_bar);
            this.Controls.Add(this.grid_canvas);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fILEToolStripMenuItem;
        public System.Windows.Forms.ComboBox combo_box_object_name;
        public Canvas grid_canvas;
        private System.Windows.Forms.ToolStripMenuItem exportAllToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.VScrollBar vertical_scroll_bar;
        private System.Windows.Forms.HScrollBar horizontal_scroll_bar;
        private System.Windows.Forms.Panel panel1;
    }
}

