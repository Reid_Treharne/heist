﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeistMapEditor
{
    public partial class UserControlInteractable : UserControlInterface
    {
        private Form1 _form1;

        Dictionary<string, Interactable> _saved_types = new Dictionary<string, Interactable>();

        // TODO pass in ref to Form1
        public UserControlInteractable(Form1 form1)
        {
            InitializeComponent();

            _form1 = form1;
        }
        public override void OnClick(MouseEventArgs e)
        {
            int object_name = _form1.combo_box_object_name.SelectedIndex;

            // Check if we're adding (left) or removing (right) an object
            if (e.Button == MouseButtons.Left)
            {
                if (_saved_types.ContainsKey(combo_box_name.Text))
                {
                    Interactable object_to_create = _saved_types[combo_box_name.Text];

                    Point point = _form1._graph.MouseToColRow(new Point(e.X, e.Y));

                    BaseType new_base_type = new Interactable(
                        object_to_create._mesh_name,
                        object_to_create._secondary_mesh_name,
                        point,
                        object_to_create._icon_name,
                        object_to_create._interactable_name);

                    if (point.Y >= 0 && point.X < Graph.GRID_WIDTH)
                    {
                        _form1._graph.AddType(new_base_type);
                    }
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                _form1._graph.RemoveType(object_name, new Point(e.X, e.Y));
            }
        }
        public override void Draw(Graphics graphics)
        {
            // Do any drawing for this user control
        }

        private void button_browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.Filter = "Image Files (*.bmp, *.jpg, *.png)|*.bmp;*.jpg;*.png;";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Bitmap icon = new Bitmap(dialog.FileName);

                string file_name = Path.GetFileName(dialog.FileName);
                Icons._icons[file_name] = icon;

                text_box_icon.Text = file_name;
                picture_box_icon.Image = icon;
            }
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            if (ValidObject())
            {
                string object_name = combo_box_name.Text;

                // If the object already exists, update it. Otherwise, add it.
                if (_saved_types.ContainsKey(combo_box_name.Text))
                {
                    _form1._graph.UpdateInteractable(combo_box_name.Text,
                        text_box_primary_mesh_name.Text,
                        text_box_secondary_mesh_name.Text,
                        text_box_icon.Text);
                }
                else
                {
                    _saved_types[combo_box_name.Text] = (new Interactable(text_box_primary_mesh_name.Text,
                        text_box_secondary_mesh_name.Text,
                        new Point(0, 0),
                        text_box_icon.Text,
                        combo_box_name.Text));

                    combo_box_name.Items.Add(combo_box_name.Text);
                }

                _form1.grid_canvas.Invalidate();
            }
        }
        private bool ValidObject()
        {
            return (combo_box_name.Text.Length != 0 &&
                text_box_primary_mesh_name.Text.Length != 0 &&
                text_box_icon.Text.Length != 0);
        }

        private void combo_box_name_SelectedIndexChanged(object sender, EventArgs e)
        {
            Interactable selected_object = _saved_types[combo_box_name.Text];

            text_box_primary_mesh_name.Text = selected_object._mesh_name;
            text_box_secondary_mesh_name.Text = selected_object._secondary_mesh_name;
            text_box_icon.Text = selected_object._icon_name;
            picture_box_icon.Image = Icons._icons[text_box_icon.Text];
        }
    }
}
