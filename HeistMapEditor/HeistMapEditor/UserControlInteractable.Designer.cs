﻿namespace HeistMapEditor
{
    partial class UserControlInteractable
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.group_box_interactable = new System.Windows.Forms.GroupBox();
            this.text_box_icon = new System.Windows.Forms.TextBox();
            this.picture_box_icon = new System.Windows.Forms.PictureBox();
            this.button_browse = new System.Windows.Forms.Button();
            this.combo_box_name = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.text_box_secondary_mesh_name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.text_box_primary_mesh_name = new System.Windows.Forms.TextBox();
            this.button_save = new System.Windows.Forms.Button();
            this.group_box_interactable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_icon)).BeginInit();
            this.SuspendLayout();
            // 
            // group_box_interactable
            // 
            this.group_box_interactable.Controls.Add(this.text_box_icon);
            this.group_box_interactable.Controls.Add(this.picture_box_icon);
            this.group_box_interactable.Controls.Add(this.button_browse);
            this.group_box_interactable.Controls.Add(this.combo_box_name);
            this.group_box_interactable.Controls.Add(this.label3);
            this.group_box_interactable.Controls.Add(this.label2);
            this.group_box_interactable.Controls.Add(this.text_box_secondary_mesh_name);
            this.group_box_interactable.Controls.Add(this.label1);
            this.group_box_interactable.Controls.Add(this.text_box_primary_mesh_name);
            this.group_box_interactable.Controls.Add(this.button_save);
            this.group_box_interactable.Location = new System.Drawing.Point(6, 3);
            this.group_box_interactable.Name = "group_box_interactable";
            this.group_box_interactable.Size = new System.Drawing.Size(323, 251);
            this.group_box_interactable.TabIndex = 0;
            this.group_box_interactable.TabStop = false;
            this.group_box_interactable.Text = "Interactable";
            // 
            // text_box_icon
            // 
            this.text_box_icon.BackColor = System.Drawing.SystemColors.MenuBar;
            this.text_box_icon.Location = new System.Drawing.Point(46, 168);
            this.text_box_icon.Name = "text_box_icon";
            this.text_box_icon.ReadOnly = true;
            this.text_box_icon.Size = new System.Drawing.Size(200, 22);
            this.text_box_icon.TabIndex = 17;
            // 
            // picture_box_icon
            // 
            this.picture_box_icon.BackColor = System.Drawing.SystemColors.ControlLight;
            this.picture_box_icon.Location = new System.Drawing.Point(8, 158);
            this.picture_box_icon.Name = "picture_box_icon";
            this.picture_box_icon.Size = new System.Drawing.Size(32, 32);
            this.picture_box_icon.TabIndex = 16;
            this.picture_box_icon.TabStop = false;
            // 
            // button_browse
            // 
            this.button_browse.Location = new System.Drawing.Point(248, 168);
            this.button_browse.Name = "button_browse";
            this.button_browse.Size = new System.Drawing.Size(72, 23);
            this.button_browse.TabIndex = 15;
            this.button_browse.Text = "Browse...";
            this.button_browse.UseVisualStyleBackColor = true;
            this.button_browse.Click += new System.EventHandler(this.button_browse_Click);
            // 
            // combo_box_name
            // 
            this.combo_box_name.FormattingEnabled = true;
            this.combo_box_name.Location = new System.Drawing.Point(8, 38);
            this.combo_box_name.Name = "combo_box_name";
            this.combo_box_name.Size = new System.Drawing.Size(308, 24);
            this.combo_box_name.TabIndex = 14;
            this.combo_box_name.SelectedIndexChanged += new System.EventHandler(this.combo_box_name_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "Secondary Mesh File Name";
            // 
            // text_box_secondary_mesh_name
            // 
            this.text_box_secondary_mesh_name.Location = new System.Drawing.Point(8, 130);
            this.text_box_secondary_mesh_name.Name = "text_box_secondary_mesh_name";
            this.text_box_secondary_mesh_name.Size = new System.Drawing.Size(312, 22);
            this.text_box_secondary_mesh_name.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Primary Mesh File Name";
            // 
            // text_box_primary_mesh_name
            // 
            this.text_box_primary_mesh_name.Location = new System.Drawing.Point(8, 85);
            this.text_box_primary_mesh_name.Name = "text_box_primary_mesh_name";
            this.text_box_primary_mesh_name.Size = new System.Drawing.Size(312, 22);
            this.text_box_primary_mesh_name.TabIndex = 8;
            // 
            // button_save
            // 
            this.button_save.Location = new System.Drawing.Point(248, 222);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(75, 23);
            this.button_save.TabIndex = 9;
            this.button_save.Text = "Save";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // UserControlInteractable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.group_box_interactable);
            this.Name = "UserControlInteractable";
            this.Size = new System.Drawing.Size(329, 300);
            this.group_box_interactable.ResumeLayout(false);
            this.group_box_interactable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_icon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox group_box_interactable;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox text_box_primary_mesh_name;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox text_box_secondary_mesh_name;
        private System.Windows.Forms.TextBox text_box_icon;
        private System.Windows.Forms.PictureBox picture_box_icon;
        private System.Windows.Forms.Button button_browse;
        private System.Windows.Forms.ComboBox combo_box_name;
        private System.Windows.Forms.Label label3;
    }
}
