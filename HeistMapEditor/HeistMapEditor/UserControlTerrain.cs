﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeistMapEditor
{
    public partial class UserControlTerrain : UserControlInterface
    {
        private Form1 _form1;

        // TODO pass in ref to Form1
        public UserControlTerrain(Form1 form1)
        {
            InitializeComponent();

            _form1 = form1;

            track_bar_terrain_min_Scroll(null, null);
            track_bar_terrain_max_Scroll(null, null);
        }
        public override void OnClick(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
            {
                if (radioButtonRadius.Checked)
                {
                    bool raise = e.Button == MouseButtons.Left;

                    _form1._graph.EditTerrainRadius(new Point(e.X, e.Y), raise, (int)numeric_up_down_radius.Value, (int)track_bar_radius.Value);
                }
            }
        }
        private void track_bar_terrain_min_Scroll(object sender, EventArgs e)
        {            
            Graph._min_vert_height = track_bar_terrain_min.Value;

            label_terrain_radius_min.Text = track_bar_terrain_min.Value.ToString();

            _form1.Refresh();
        }

        private void track_bar_terrain_max_Scroll(object sender, EventArgs e)
        {
            Graph._max_vert_height = track_bar_terrain_max.Value;

            label_terrain_radius_max.Text = track_bar_terrain_max.Value.ToString();

            _form1.Refresh();
        }
    }
}
