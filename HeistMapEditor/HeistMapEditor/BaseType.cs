﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Xml.Linq;

namespace HeistMapEditor
{
    public class BaseType
    {
        public enum OBJECT_NAME { NAME_CAMERA = 0, NAME_INTERACTABLE, NAME_AESTHETIC, NAME_GROUND_CLUTTER, NAME_TERRAIN, NAME_DIRECTIONAL_LIGHT, NUM_NAMES };
        public OBJECT_NAME _object_name;       
        public string _mesh_name;
        public Point _position;
        public string _icon_name;

        public BaseType(OBJECT_NAME object_name, string mesh_name, Point position, string icon_name)
        {
            _object_name = object_name;
            _mesh_name = mesh_name;
            _position = position;
            _icon_name = icon_name;
        }
        virtual public void Draw(Graphics graphics, int scale, Point camera_position)
        {
            float width = Graph.WIDTH_OF_METER * scale;

            int start_row = (int)((Graph.GRID_WIDTH - _position.Y - 1) * width);
            int start_col = (int)(_position.X * width);

            start_col -= camera_position.X;
            start_row -= camera_position.Y;

            Rectangle rect = new Rectangle(start_col, start_row, Graph.WIDTH_OF_METER * scale, Graph.WIDTH_OF_METER * scale);

            if (_icon_name.Length != 0)
            {
                graphics.DrawImage(Icons._icons[_icon_name], rect);
            }
        }
        virtual public void GetAttributes(ref List<XAttribute> attributes)
        {
            attributes.Add(new XAttribute("ObjectName", (int)_object_name));
            attributes.Add(new XAttribute("PositionX", _position.X + 0.5f));
            attributes.Add(new XAttribute("PositionZ", _position.Y + 0.5f));
        }
        virtual public void GetMeshNames(ref List<XElement> xMeshNames)
        {
            xMeshNames.Add(new XElement("MeshName", _mesh_name));
        }
    }
}
