﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Drawing.Drawing2D;
using System.Windows.Media.Media3D;
using System.IO;

namespace HeistMapEditor
{
    struct Line
    {
        public Point start;
        public Point end;
    };

    struct Vert
    {
        public float _height;

        public void MoveHeight(float amount)
        {
            _height += amount;

            if (_height > Graph._max_vert_height)
            {
                _height = Graph._max_vert_height;
            }
            else if (_height < Graph._min_vert_height)
            {
                _height = Graph._min_vert_height;
            }
        }
    }

    public class Graph
    {
        static public int _max_vert_height = 0;
        static public int _min_vert_height = 0;

        public const int GRID_WIDTH = 50;
        public const int GRID_HEIGHT = 50;
        public const int WIDTH_OF_METER = 5;

        private Line[] _horizontal_lines = new Line[GRID_HEIGHT];
        private Line[] _vertical_lines = new Line[GRID_WIDTH];
        public Tile[,] _tiles = new Tile[GRID_WIDTH, GRID_HEIGHT];
        private Vert[,] _verts = new Vert[GRID_WIDTH + 1, GRID_HEIGHT + 1];
        public int _scale = 1;
        private int _max_scale = 10;
        private int _scale_speed = 100; // large the slower
        private float _offset_amount = 1.0f;

        public Point _camera_position = new Point(0, 0);
        public BaseType _selected_type = null;

        public Graph()
        {
            // Initialize the grid
            for (int i = 0; i < GRID_WIDTH; i++)
            {
                _vertical_lines[i].start = new Point(i, 0);
                _vertical_lines[i].end = new Point(i, GRID_HEIGHT);
            }
            for (int i = 0; i < GRID_HEIGHT; i++)
            {
                _horizontal_lines[i].start = new Point(0, i);
                _horizontal_lines[i].end = new Point(GRID_WIDTH, i);
            }
            // Initialize the lists of objects
            for (int i = 0; i < GRID_WIDTH; i++)
            {
                for (int j = 0; j < GRID_HEIGHT; j++)
                {
                    _tiles[i, j] = new Tile(i, j);
                }
            }
            // Initialize the verts
            for (int i = 0; i <= GRID_WIDTH; i++)
            {
                for (int j = 0; j <= GRID_HEIGHT; j++)
                {
                    _verts[i, j]._height = 50;
                }
            }
        }

        public void DrawGrid(Graphics graphics)
        {
            // Draw the vertical lines
            for (int i = 0; i < GRID_WIDTH; i++)
            {
                // Draw the grid at scale
                Point start = new Point((i * WIDTH_OF_METER) * _scale, 0);
                Point end = new Point((i * WIDTH_OF_METER) * _scale, _vertical_lines[i].end.Y * WIDTH_OF_METER * _scale);

                // Move the grid by the scroll bar amount
                start.X -= _camera_position.X;
                end.X -= _camera_position.X;

                start.Y -= _camera_position.Y;
                end.Y -= _camera_position.Y;

                graphics.DrawLine(Pens.Black, start, end);
            }
            // Draw the horizontal lines
            for (int i = 0; i < GRID_HEIGHT; i++)
            {
                // Draw the grid at scale
                Point start = new Point(0, (i * WIDTH_OF_METER) * _scale);
                Point end = new Point(_horizontal_lines[i].end.X * WIDTH_OF_METER * _scale, (i * WIDTH_OF_METER) * _scale);

                // Move the grid by the scroll bar amount
                start.X -= _camera_position.X;
                end.X -= _camera_position.X;

                start.Y -= _camera_position.Y;
                end.Y -= _camera_position.Y;

                graphics.DrawLine(Pens.Black, start, end);
            }
        }
        public void DrawObjects(Graphics graphics)
        {
            for (int i = 0; i < GRID_WIDTH; i++)
            {
                for (int j = 0; j < GRID_HEIGHT; j++)
                {
                    _tiles[i, j].Draw(graphics, _scale, _camera_position);
                }
            }
        }

        public void DrawTerrain(Graphics graphics)
        {
            for (int i = 0; i < GRID_WIDTH; i++)
            {
                for (int j = 0; j < GRID_HEIGHT; j++)
                {
                    // Draw Height
                    int row = (int)(_tiles[i, j]._row * Graph.WIDTH_OF_METER * _scale);
                    int col = (int)(_tiles[i, j]._col * Graph.WIDTH_OF_METER * _scale);

                    row -= _camera_position.Y;
                    col -= _camera_position.X;
                    Rectangle rect = new Rectangle(col, row, Graph.WIDTH_OF_METER * _scale, Graph.WIDTH_OF_METER * _scale);

                    // take the average of the 4 verts
                    float average_of_verts = _verts[i, GRID_HEIGHT - 1 - j]._height;
                    average_of_verts += _verts[i + 1, GRID_HEIGHT - 1 - j]._height;
                    average_of_verts += _verts[i + 1, GRID_HEIGHT - 1 - j + 1]._height;
                    average_of_verts += _verts[i, GRID_HEIGHT - 1 - j + 1]._height;

                    average_of_verts /= 4.0f;

                    float percent = -1.0f;
                    float denominator = _max_vert_height - _min_vert_height;

                    if (denominator != 0.0f)
                    {
                        percent = (average_of_verts - _min_vert_height) / (_max_vert_height - _min_vert_height);
                    }

                    SolidBrush brush = new SolidBrush(PercentToColor(percent * 100.0f));

                    graphics.FillRectangle(brush, rect);
                }
            }
        }
        public int GetScale()
        {
            return _scale;
        }
        public void ScaleGrid(int scale)
        {
            int delta_scale = scale / _scale_speed;

            if (_scale + delta_scale < 1)
            {
                _scale = 1;
            }
            else if (_scale + delta_scale > _max_scale)
            {
                _scale = _max_scale;
            }
            else
            {
                _scale += delta_scale;
            }
        }

        public int SizeOfMeter()
        {
            return WIDTH_OF_METER * _scale;
        }

        public void EditTerrainRadius(Point mouse, bool raise, int radius, int speed)
        {
            Point p = GetColRow(mouse);

            int col = p.X;
            int row = p.Y;

            float offset = raise ? _offset_amount : -_offset_amount;

            offset *= (speed / 10.0f);

            radius -= 1;

            for (int c = col - radius; c < col + 2 + radius; ++c)
            {
                for (int r = row - radius; r < row + 2 + radius; ++r)
                {
                    if (IsValidVert(new Point(c, r)))
                    {
                        _verts[c, r].MoveHeight(offset);
                    }
                }
            }
        }

        public void EditGroundClutter(Point mouse, bool add, int radius)
        {
            Point p = GetColRow(mouse);

            int col = p.X;
            int row = p.Y;

            for (int c = col - radius; c < col + 1 + radius; ++c)
            {
                for (int r = row - radius; r < row + 1 + radius; ++r)
                {
                    if (IsValidTile(c, r))
                    {
                        _tiles[c, r]._has_grass = add;
                    }
                }
            }
        }

        public void AddType(BaseType base_type)
        {
            if (base_type != null)
            {
                int col = base_type._position.X;
                int row = base_type._position.Y;

                if (IsValidTile(col, row))
                {
                    _tiles[col, row].AddObject(base_type);
                }
            }
        }

        public void RemoveType(int object_name, Point mouse)
        {
            Point point = MouseToColRow(mouse);

            if (IsValidTile(point))
            {
                _tiles[point.X, point.Y].RemoveObject(object_name);
            }
        }

        public void UpdateInteractable(
            string interactable_name,
            string primary_mesh_name,
            string secondary_mesh_name,
            string icon_name)
        {
            foreach (Tile t in _tiles)
            {
                foreach (BaseType b in t._objects)
                {
                    if (b._object_name == BaseType.OBJECT_NAME.NAME_INTERACTABLE)
                    {
                        if (((Interactable)b)._interactable_name == interactable_name)
                        {
                            ((Interactable)b)._interactable_name = interactable_name;
                            ((Interactable)b)._secondary_mesh_name = secondary_mesh_name;
                            b._mesh_name = primary_mesh_name;
                            b._icon_name = icon_name;
                        }
                    }
                }
            }
        }
        public void ExportAll()
        {
            SaveFileDialog dlg = new SaveFileDialog();

            dlg.Filter = "All Files|*.*|xml Files|*.xml";
            dlg.FilterIndex = 2;
            dlg.DefaultExt = "xml";

            if (DialogResult.OK == dlg.ShowDialog())
            {
                XElement xRoot = new XElement("Map");

                xRoot.SetAttributeValue("Cols", GRID_WIDTH);
                xRoot.SetAttributeValue("Rows", GRID_HEIGHT);

                Vector3D tri0_v0 = new Vector3D();
                Vector3D tri0_v1 = new Vector3D();
                Vector3D tri0_v2 = new Vector3D();

                Vector3D tri1_v0 = new Vector3D();
                Vector3D tri1_v1 = new Vector3D();
                Vector3D tri1_v2 = new Vector3D();

                for (int col = 0; col < GRID_WIDTH; col++)
                {
                    for (int row = 0; row < GRID_HEIGHT; row++)
                    {
                        XElement xTile = new XElement("Tile");
                        xTile.SetAttributeValue("Col", col);
                        xTile.SetAttributeValue("Row", row);
                        xTile.SetAttributeValue("Walkable", _tiles[col, row]._walkable ? 1 : 0);
                        xTile.SetAttributeValue("HasGrass", _tiles[col, row]._has_grass ? 1 : 0);

                        float tl = _verts[col, row + 1]._height;
                        float bl = _verts[col, row]._height;
                        float tr = _verts[col + 1, row + 1]._height;
                        float br = _verts[col + 1, row]._height;

                        XElement xTriangle0 = new XElement("Triangle");
                        XElement xTriangle1 = new XElement("Triangle");

                        XElement xTriangle0Vert0 = new XElement("Vertex");
                        XElement xTriangle0Vert1 = new XElement("Vertex");
                        XElement xTriangle0Vert2 = new XElement("Vertex");

                        XElement xTriangle1Vert0 = new XElement("Vertex");
                        XElement xTriangle1Vert1 = new XElement("Vertex");
                        XElement xTriangle1Vert2 = new XElement("Vertex");

                        // Determine which way the tile is split
                        if (tl == br)
                        {
                            // Left triangle
                            tri0_v0.X = col;
                            tri0_v0.Y = _verts[col, row]._height;
                            tri0_v0.Z = row;

                            tri0_v1.X = col;
                            tri0_v1.Y = _verts[col, row + 1]._height;
                            tri0_v1.Z = row + 1;

                            tri0_v2.X = col + 1;
                            tri0_v2.Y = _verts[col + 1, row]._height;
                            tri0_v2.Z = row;

                            // Right triangle
                            tri1_v0.X = col;
                            tri1_v0.Y = _verts[col, row + 1]._height;
                            tri1_v0.Z = row + 1;

                            tri1_v1.X = col + 1;
                            tri1_v1.Y = _verts[col + 1, row + 1]._height;
                            tri1_v1.Z = row + 1;

                            tri1_v2.X = col + 1;
                            tri1_v2.Y = _verts[col + 1, row]._height;
                            tri1_v2.Z = row;
                        }
                        else
                        {
                            // Left triangle
                            tri0_v0.X = col;
                            tri0_v0.Y = _verts[col, row]._height;
                            tri0_v0.Z = row;

                            tri0_v1.X = col;
                            tri0_v1.Y = _verts[col, row + 1]._height;
                            tri0_v1.Z = row + 1;

                            tri0_v2.X = col + 1;
                            tri0_v2.Y = _verts[col + 1, row + 1]._height;
                            tri0_v2.Z = row + 1;

                            // Right triangle
                            tri1_v0.X = col;
                            tri1_v0.Y = _verts[col, row]._height;
                            tri1_v0.Z = row;

                            tri1_v1.X = col + 1;
                            tri1_v1.Y = _verts[col + 1, row + 1]._height;
                            tri1_v1.Z = row + 1;

                            tri1_v2.X = col + 1;
                            tri1_v2.Y = _verts[col + 1, row]._height;
                            tri1_v2.Z = row;
                        }

                        // left triangle
                        xTriangle0Vert0.SetAttributeValue("X", tri0_v0.X);
                        xTriangle0Vert0.SetAttributeValue("Y", tri0_v0.Y);
                        xTriangle0Vert0.SetAttributeValue("Z", tri0_v0.Z);
                        xTriangle0Vert1.SetAttributeValue("X", tri0_v1.X);
                        xTriangle0Vert1.SetAttributeValue("Y", tri0_v1.Y);
                        xTriangle0Vert1.SetAttributeValue("Z", tri0_v1.Z);
                        xTriangle0Vert2.SetAttributeValue("X", tri0_v2.X);
                        xTriangle0Vert2.SetAttributeValue("Y", tri0_v2.Y);
                        xTriangle0Vert2.SetAttributeValue("Z", tri0_v2.Z);

                        xTriangle0.Add(xTriangle0Vert0);
                        xTriangle0.Add(xTriangle0Vert1);
                        xTriangle0.Add(xTriangle0Vert2);

                        // calculate normals
                        Vector3D tri0_normal = Vector3D.CrossProduct(tri0_v2 - tri0_v1, tri0_v0 - tri0_v1);
                        tri0_normal.Normalize();
                        xTriangle0.SetAttributeValue("NormalX", tri0_normal.X);
                        xTriangle0.SetAttributeValue("NormalY", tri0_normal.Y);
                        xTriangle0.SetAttributeValue("NormalZ", tri0_normal.Z);

                        // right triangle
                        xTriangle1Vert0.SetAttributeValue("X", tri1_v0.X);
                        xTriangle1Vert0.SetAttributeValue("Y", tri1_v0.Y);
                        xTriangle1Vert0.SetAttributeValue("Z", tri1_v0.Z);
                        xTriangle1Vert1.SetAttributeValue("X", tri1_v1.X);
                        xTriangle1Vert1.SetAttributeValue("Y", tri1_v1.Y);
                        xTriangle1Vert1.SetAttributeValue("Z", tri1_v1.Z);
                        xTriangle1Vert2.SetAttributeValue("X", tri1_v2.X);
                        xTriangle1Vert2.SetAttributeValue("Y", tri1_v2.Y);
                        xTriangle1Vert2.SetAttributeValue("Z", tri1_v2.Z);

                        xTriangle1.Add(xTriangle1Vert0);
                        xTriangle1.Add(xTriangle1Vert1);
                        xTriangle1.Add(xTriangle1Vert2);

                        // calculate normals
                        Vector3D tri1_normal = Vector3D.CrossProduct(tri1_v1 - tri1_v0, tri1_v2 - tri1_v0);
                        tri1_normal.Normalize();
                        xTriangle1.SetAttributeValue("NormalX", tri1_normal.X);
                        xTriangle1.SetAttributeValue("NormalY", tri1_normal.Y);
                        xTriangle1.SetAttributeValue("NormalZ", tri1_normal.Z);

                        // Add the triangles to the tile
                        xTile.Add(xTriangle0);
                        xTile.Add(xTriangle1);

                        // Export the objects on this tile
                        for (int k = 0; k < _tiles[col, row]._objects.Count; k++)
                        {
                            XElement xObject = new XElement("Object");

                            List<XAttribute> attributes = new List<XAttribute>();

                            _tiles[col, row]._objects[k].GetAttributes(ref attributes);

                            xObject.Add(attributes);

                            // Get all the mesh names associated with this object
                            List<XElement> xMeshNames = new List<XElement>();

                            _tiles[col, row]._objects[k].GetMeshNames(ref xMeshNames);

                            xObject.Add(xMeshNames);

                            xTile.Add(xObject);
                        }
                        xRoot.Add(xTile);
                    }
                }
                xRoot.Save(dlg.FileName);
            }
        }

        public void ExportAllBinary()
        {
            SaveFileDialog dlg = new SaveFileDialog();

            dlg.Filter = "All Files|*.*|bin Files|*.bin";
            dlg.FilterIndex = 2;
            dlg.DefaultExt = "bin";

            if (DialogResult.OK == dlg.ShowDialog())
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(dlg.FileName, FileMode.Create)))
                {
                    writer.Write((GRID_WIDTH + 1) * (GRID_HEIGHT + 1));
                    writer.Write(GRID_WIDTH * GRID_HEIGHT * 6);

                    for (int col = 0; col <= GRID_WIDTH; col++)
                    {
                        for (int row = 0; row <= GRID_HEIGHT; row++)
                        {
                            writer.Write((float)col);
                            writer.Write(_verts[col, row]._height);
                            writer.Write((float)row);
                        }
                    }

                    int index = 0;

                    for (int col = 0; col < GRID_WIDTH; col++)
                    {
                        for (int row = 0; row < GRID_HEIGHT; row++)
                        {
                            float tl = _verts[col, row + 1]._height;
                            float bl = _verts[col, row]._height;
                            float tr = _verts[col + 1, row + 1]._height;
                            float br = _verts[col + 1, row]._height;

                            // Determine which way the tile is split
                            if (tl == br)
                            {
                                // left triangle
                                writer.Write(index);
                                writer.Write(index+1);
                                writer.Write(index+GRID_HEIGHT+1);

                                writer.Write(0.0f);
                                writer.Write(1.0f);
                                writer.Write(0.0f);
                                writer.Write(0.0f);
                                writer.Write(0.0f);

                                // right triangle
                                writer.Write(index+1);
                                writer.Write(index+1+GRID_HEIGHT+1);
                                writer.Write(index+GRID_HEIGHT+1);
                            }
                            else
                            {
                                // left triangle
                                writer.Write(index);
                                writer.Write(index + 1);
                                writer.Write(index + 1 + GRID_HEIGHT);

                                writer.Write(0.0f);
                                writer.Write(1.0f);
                                writer.Write(0.0f);
                                writer.Write(0.0f);
                                writer.Write(0.0f);

                                // right triangle
                                writer.Write(index);
                                writer.Write(index + 1 + GRID_HEIGHT);
                                writer.Write(index + GRID_HEIGHT);
                            }

                            writer.Write(0.0f);
                            writer.Write(1.0f);
                            writer.Write(0.0f);
                            writer.Write(0.0f);
                            writer.Write(0.0f);

                            index++;
                        }
                        index++;
                    }
                    //        xTile.SetAttributeValue("Walkable", _tiles[col, row]._walkable ? 1 : 0);
                    //
                    //
                    //        XElement xTriangle0 = new XElement("Triangle");
                    //        XElement xTriangle1 = new XElement("Triangle");
                    //
                    //        XElement xTriangle0Vert0 = new XElement("Vertex");
                    //        XElement xTriangle0Vert1 = new XElement("Vertex");
                    //        XElement xTriangle0Vert2 = new XElement("Vertex");
                    //
                    //        XElement xTriangle1Vert0 = new XElement("Vertex");
                    //        XElement xTriangle1Vert1 = new XElement("Vertex");
                    //        XElement xTriangle1Vert2 = new XElement("Vertex");
                    //
                    //
                    //        // left triangle
                    //        xTriangle0Vert0.SetAttributeValue("X", tri0_v0.X);
                    //        xTriangle0Vert0.SetAttributeValue("Y", tri0_v0.Y);
                    //        xTriangle0Vert0.SetAttributeValue("Z", tri0_v0.Z);
                    //        xTriangle0Vert1.SetAttributeValue("X", tri0_v1.X);
                    //        xTriangle0Vert1.SetAttributeValue("Y", tri0_v1.Y);
                    //        xTriangle0Vert1.SetAttributeValue("Z", tri0_v1.Z);
                    //        xTriangle0Vert2.SetAttributeValue("X", tri0_v2.X);
                    //        xTriangle0Vert2.SetAttributeValue("Y", tri0_v2.Y);
                    //        xTriangle0Vert2.SetAttributeValue("Z", tri0_v2.Z);
                    //
                    //        xTriangle0.Add(xTriangle0Vert0);
                    //        xTriangle0.Add(xTriangle0Vert1);
                    //        xTriangle0.Add(xTriangle0Vert2);
                    //
                    //        // calculate normals
                    //        Vector3D tri0_normal = Vector3D.CrossProduct(tri0_v2 - tri0_v1, tri0_v0 - tri0_v1);
                    //        tri0_normal.Normalize();
                    //        xTriangle0.SetAttributeValue("NormalX", tri0_normal.X);
                    //        xTriangle0.SetAttributeValue("NormalY", tri0_normal.Y);
                    //        xTriangle0.SetAttributeValue("NormalZ", tri0_normal.Z);
                    //
                    //        // right triangle
                    //        xTriangle1Vert0.SetAttributeValue("X", tri1_v0.X);
                    //        xTriangle1Vert0.SetAttributeValue("Y", tri1_v0.Y);
                    //        xTriangle1Vert0.SetAttributeValue("Z", tri1_v0.Z);
                    //        xTriangle1Vert1.SetAttributeValue("X", tri1_v1.X);
                    //        xTriangle1Vert1.SetAttributeValue("Y", tri1_v1.Y);
                    //        xTriangle1Vert1.SetAttributeValue("Z", tri1_v1.Z);
                    //        xTriangle1Vert2.SetAttributeValue("X", tri1_v2.X);
                    //        xTriangle1Vert2.SetAttributeValue("Y", tri1_v2.Y);
                    //        xTriangle1Vert2.SetAttributeValue("Z", tri1_v2.Z);
                    //
                    //        xTriangle1.Add(xTriangle1Vert0);
                    //        xTriangle1.Add(xTriangle1Vert1);
                    //        xTriangle1.Add(xTriangle1Vert2);
                    //
                    //        // calculate normals
                    //        Vector3D tri1_normal = Vector3D.CrossProduct(tri1_v1 - tri1_v0, tri1_v2 - tri1_v0);
                    //        tri1_normal.Normalize();
                    //        xTriangle1.SetAttributeValue("NormalX", tri1_normal.X);
                    //        xTriangle1.SetAttributeValue("NormalY", tri1_normal.Y);
                    //        xTriangle1.SetAttributeValue("NormalZ", tri1_normal.Z);
                    //
                    //        // Add the triangles to the tile
                    //        xTile.Add(xTriangle0);
                    //        xTile.Add(xTriangle1);
                    //
                    //        // Export the objects on this tile
                    //        for (int k = 0; k < _tiles[col, row]._objects.Count; k++)
                    //        {
                    //            XElement xObject = new XElement("Object");
                    //
                    //            List<XAttribute> attributes = new List<XAttribute>();
                    //
                    //            _tiles[col, row]._objects[k].GetAttributes(ref attributes);
                    //
                    //            xObject.Add(attributes);
                    //
                    //            // Get all the mesh names associated with this object
                    //            List<XElement> xMeshNames = new List<XElement>();
                    //
                    //            _tiles[col, row]._objects[k].GetMeshNames(ref xMeshNames);
                    //
                    //            xObject.Add(xMeshNames);
                    //
                    //            xTile.Add(xObject);
                    //        }
                    //        xRoot.Add(xTile);
                    //    }
                    //}
                    //xRoot.Save(dlg.FileName);
                }
            }
        }

        public Point MouseToColRow(Point mouse)
        {
            mouse.X += _camera_position.X;
            mouse.Y += _camera_position.Y;

            float row = GRID_HEIGHT - (float)mouse.Y / (WIDTH_OF_METER * _scale);
            float col = (float)mouse.X / (WIDTH_OF_METER * _scale);

            return new Point((int)col, (int)row);
        }

        private Point GetColRow(Point mouse)
        {
            mouse.X += _camera_position.X;
            mouse.Y += _camera_position.Y;

            float f_row = GRID_HEIGHT - (float)mouse.Y / (WIDTH_OF_METER * _scale);
            float f_col = (float)mouse.X / (WIDTH_OF_METER * _scale);

            int row = (int)f_row;
            int col = (int)f_col;

            Point p = new Point(col, row);

            return p;
        }
        private bool IsValidVert(Point vert)
        {
            return (vert.X >= 0 && vert.X <= GRID_WIDTH &&
                 vert.Y >= 0 && vert.Y <= GRID_HEIGHT);
        }
        private bool IsValidTile(Point tile)
        {
            return (IsValidTile(tile.X, tile.Y));
        }
        private bool IsValidTile(int col, int row)
        {
            return (col >= 0 && col < GRID_WIDTH &&
                row >= 0 && row < GRID_HEIGHT);
        }
        private Color PercentToColor(float percent)
        {
            if (percent < 0)
            {
                return Color.FromArgb(204, 204, 204);
            }
            else if (percent > 100)
            {
                return Color.FromArgb(51, 51, 51);
            }
            float R = 0, B = 0, G = 0;
            if (percent < 25.0f)
            {
                R = 0;
                G = percent / 25 * 255;
                B = 255;
            }
            else if (percent < 50.0f)
            {
                R = 0;
                G = 255;
                B = (50 - percent) / 25 * 255;
            }
            else if (percent < 75.0f)
            {
                R = (percent - 50) / 25 * 255;
                G = 255;
                B = 0;
            }
            else
            {
                R = 255;
                G = (100 - percent) / 25 * 255;
                B = 0;
            }
            return Color.FromArgb((int)R, (int)G, (int)B);
        }
    }
}
