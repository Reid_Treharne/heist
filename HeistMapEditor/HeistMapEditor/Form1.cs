﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeistMapEditor
{
    public partial class Form1 : Form
    {
        public Graph _graph = new Graph();

        int _object_selected_index;

        List<UserControlInterface> _user_controls = new List<UserControlInterface>();

        public Form1()
        {
            InitializeComponent();

            InitializeUserControls();

            combo_box_object_name.SelectedIndex = 0;
            _object_selected_index = combo_box_object_name.SelectedIndex;
            this.MouseWheel += on_mouse_scroll;
            on_mouse_scroll(null, null);
        }

        private void grid_canvas_Paint(object sender, PaintEventArgs e)
        {
            // draw terrain
            _graph.DrawTerrain(e.Graphics);

            // draw objects
            _graph.DrawObjects(e.Graphics);

            // draw grid
            _graph.DrawGrid(e.Graphics);

            if (_user_controls[combo_box_object_name.SelectedIndex] != null)
            {
                _user_controls[combo_box_object_name.SelectedIndex].Draw(e.Graphics);
            }
        }

        public void on_mouse_scroll(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e != null)
            {
                _graph.ScaleGrid(e.Delta);

                HandledMouseEventArgs ee = (HandledMouseEventArgs)e;
                ee.Handled = true;
            }

            int panel_width = grid_canvas.Width - vertical_scroll_bar.Width;
            int panel_height = grid_canvas.Height - horizontal_scroll_bar.Height;

            // Determine the width of the scroll bars
            int grid_width = Graph.GRID_WIDTH * Graph.WIDTH_OF_METER * _graph.GetScale();
            int grid_height = Graph.GRID_HEIGHT * Graph.WIDTH_OF_METER * _graph.GetScale();

            int width_diff = panel_width - grid_width;
            int height_diff = panel_height - grid_height;

            horizontal_scroll_bar.Enabled = width_diff < 0;
            vertical_scroll_bar.Enabled = height_diff < 0;

            // max needs to be 1 to have full scroll bar
            if (horizontal_scroll_bar.Enabled)
            {
                horizontal_scroll_bar.Maximum = -width_diff;
            }
            else
            {
                _graph._camera_position.X = 0;
            }
            if (vertical_scroll_bar.Enabled)
            {
                vertical_scroll_bar.Maximum = -height_diff;
            }
            else
            {
                _graph._camera_position.Y = 0;
            }

            grid_canvas.Invalidate();
        }

        private void exportAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _graph.ExportAll();
        }

        private void combo_box_object_name_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Hide the old group box and show the new one
            if (_user_controls[_object_selected_index] != null)
            {
                _user_controls[_object_selected_index].Hide();
            }

            _object_selected_index = combo_box_object_name.SelectedIndex;

            if (_user_controls[_object_selected_index] != null)
            {
                _user_controls[_object_selected_index].Show();
            }
        }

        private void horizontal_scroll_bar_Scroll(object sender, ScrollEventArgs e)
        {
            _graph._camera_position.X = e.NewValue;

            grid_canvas.Invalidate();
        }
        private void vertical_scroll_bar_Scroll(object sender, ScrollEventArgs e)
        {
            _graph._camera_position.Y = e.NewValue;

            grid_canvas.Invalidate();
        }

        private void InitializeUserControls()
        {
            // Initialize the object group boxes
            _user_controls.Add(null); // camera
            _user_controls.Add(new UserControlInteractable(this));
            _user_controls.Add(null); // aesthetic
            _user_controls.Add(new UserControlGroundClutter(this));
            _user_controls.Add(new UserControlTerrain(this));
            _user_controls.Add(null); // directional light

            foreach (UserControl user_control in _user_controls)
            {
                if (user_control != null)
                {
                    user_control.Hide();
                    user_control.Parent = this;
                }
                panel1.Controls.Add(user_control);
            }
        }

        private void grid_canvas_MouseDown(object sender, MouseEventArgs e)
        {
            int index = combo_box_object_name.SelectedIndex;

            if (_user_controls[index] != null)
            {
                _user_controls[combo_box_object_name.SelectedIndex].OnClick(e);

                grid_canvas.Invalidate();
            }
        }

        private void grid_canvas_MouseMove(object sender, MouseEventArgs e)
        {
            int index = combo_box_object_name.SelectedIndex;

            if (_user_controls[index] != null)
            {
                _user_controls[combo_box_object_name.SelectedIndex].OnClick(e);

                grid_canvas.Invalidate();
            }
        }
    }
}
