﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeistMapEditor
{
    public class UserControlInterface : UserControl
    {
        public virtual void OnClick(MouseEventArgs e)
        {
        }
        public virtual void Draw(Graphics graphics)
        {
        }
    }
}
