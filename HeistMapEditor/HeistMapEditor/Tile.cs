﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace HeistMapEditor
{
    public class Tile
    {
        public List<BaseType> _objects;
        public int _row;
        public int _col;
        public bool _walkable;
        public bool _has_grass;

        public Tile(int col, int row)
        {
            _objects = new List<BaseType>();
            _row = row;
            _col = col;
            _walkable = true;
            _has_grass = false;
        }

        public void Draw(Graphics graphics, int scale, Point camera_position)
        {
            for (int i = 0; i < _objects.Count; i++)
            {
                _objects[i].Draw(graphics, scale, camera_position);
            }

            if (_has_grass)
            {
                float width = Graph.WIDTH_OF_METER * scale;

                int start_row = (int)((Graph.GRID_WIDTH - _row - 1) * width);
                int start_col = (int)(_col * width);

                start_col -= camera_position.X;
                start_row -= camera_position.Y;

                int height = Graph.WIDTH_OF_METER * scale;
                int icon_height = Graph.WIDTH_OF_METER / 2 * scale;

                Rectangle rect = new Rectangle(
                    start_col,
                    start_row + height - icon_height,
                    height,
                    icon_height);

                Brush brush = new LinearGradientBrush(rect, Color.LightGreen, Color.Black, LinearGradientMode.Vertical);

                graphics.FillRectangle(brush, rect);
            }
        }
        public void AddObject(BaseType the_object)
        {
            bool contains = false;
            for (int i = 0; i < _objects.Count; i++)
            {
                if (_objects[i]._object_name == the_object._object_name)
                {
                    contains = true;
                    break;
                }
            }
            if (contains == false)
            {
                _objects.Add(the_object);
                _walkable = false;
                _has_grass = false;
            }
        }

        public void RemoveObject(int object_name)
        {
            for (int i = 0; i < _objects.Count; ++i)
            {
                if ((int)_objects[i]._object_name == object_name)
                {
                    _objects.RemoveAt(i);
                }
            }
            if (_objects.Count == 0)
            {
                _walkable = true;
            }
        }
    }
}
