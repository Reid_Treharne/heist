﻿namespace HeistMapEditor
{
    partial class UserControlTerrain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.group_box_terrain = new System.Windows.Forms.GroupBox();
            this.label_terrain_radius_max = new System.Windows.Forms.Label();
            this.label_terrain_radius_min = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.track_bar_terrain_max = new System.Windows.Forms.TrackBar();
            this.track_bar_terrain_min = new System.Windows.Forms.TrackBar();
            this.track_bar_radius = new System.Windows.Forms.TrackBar();
            this.numeric_up_down_radius = new System.Windows.Forms.NumericUpDown();
            this.radioButtonRadius = new System.Windows.Forms.RadioButton();
            this.group_box_terrain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.track_bar_terrain_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.track_bar_terrain_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.track_bar_radius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_up_down_radius)).BeginInit();
            this.SuspendLayout();
            // 
            // group_box_terrain
            // 
            this.group_box_terrain.BackColor = System.Drawing.SystemColors.Control;
            this.group_box_terrain.Controls.Add(this.label_terrain_radius_max);
            this.group_box_terrain.Controls.Add(this.label_terrain_radius_min);
            this.group_box_terrain.Controls.Add(this.label5);
            this.group_box_terrain.Controls.Add(this.label6);
            this.group_box_terrain.Controls.Add(this.label3);
            this.group_box_terrain.Controls.Add(this.label2);
            this.group_box_terrain.Controls.Add(this.label1);
            this.group_box_terrain.Controls.Add(this.track_bar_terrain_max);
            this.group_box_terrain.Controls.Add(this.track_bar_terrain_min);
            this.group_box_terrain.Controls.Add(this.track_bar_radius);
            this.group_box_terrain.Controls.Add(this.numeric_up_down_radius);
            this.group_box_terrain.Controls.Add(this.radioButtonRadius);
            this.group_box_terrain.Location = new System.Drawing.Point(3, 3);
            this.group_box_terrain.Name = "group_box_terrain";
            this.group_box_terrain.Size = new System.Drawing.Size(323, 172);
            this.group_box_terrain.TabIndex = 12;
            this.group_box_terrain.TabStop = false;
            this.group_box_terrain.Text = "Terrain";
            // 
            // label_terrain_radius_max
            // 
            this.label_terrain_radius_max.AutoSize = true;
            this.label_terrain_radius_max.Location = new System.Drawing.Point(290, 135);
            this.label_terrain_radius_max.Name = "label_terrain_radius_max";
            this.label_terrain_radius_max.Size = new System.Drawing.Size(16, 17);
            this.label_terrain_radius_max.TabIndex = 21;
            this.label_terrain_radius_max.Text = "1";
            // 
            // label_terrain_radius_min
            // 
            this.label_terrain_radius_min.AutoSize = true;
            this.label_terrain_radius_min.Location = new System.Drawing.Point(290, 78);
            this.label_terrain_radius_min.Name = "label_terrain_radius_min";
            this.label_terrain_radius_min.Size = new System.Drawing.Size(16, 17);
            this.label_terrain_radius_min.TabIndex = 20;
            this.label_terrain_radius_min.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(262, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 17);
            this.label5.TabIndex = 19;
            this.label5.Text = "100";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(261, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 17);
            this.label6.TabIndex = 18;
            this.label6.Text = "100";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(206, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "speed";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "max";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 15;
            this.label1.Text = "min";
            // 
            // track_bar_terrain_max
            // 
            this.track_bar_terrain_max.Location = new System.Drawing.Point(7, 115);
            this.track_bar_terrain_max.Maximum = 100;
            this.track_bar_terrain_max.Name = "track_bar_terrain_max";
            this.track_bar_terrain_max.Size = new System.Drawing.Size(287, 56);
            this.track_bar_terrain_max.TabIndex = 14;
            this.track_bar_terrain_max.Value = 1;
            this.track_bar_terrain_max.Scroll += new System.EventHandler(this.track_bar_terrain_max_Scroll);
            // 
            // track_bar_terrain_min
            // 
            this.track_bar_terrain_min.Location = new System.Drawing.Point(6, 59);
            this.track_bar_terrain_min.Maximum = 100;
            this.track_bar_terrain_min.Name = "track_bar_terrain_min";
            this.track_bar_terrain_min.Size = new System.Drawing.Size(288, 56);
            this.track_bar_terrain_min.TabIndex = 12;
            this.track_bar_terrain_min.Scroll += new System.EventHandler(this.track_bar_terrain_min_Scroll);
            // 
            // track_bar_radius
            // 
            this.track_bar_radius.Location = new System.Drawing.Point(147, 19);
            this.track_bar_radius.Name = "track_bar_radius";
            this.track_bar_radius.Size = new System.Drawing.Size(170, 56);
            this.track_bar_radius.TabIndex = 13;
            // 
            // numeric_up_down_radius
            // 
            this.numeric_up_down_radius.Location = new System.Drawing.Point(84, 21);
            this.numeric_up_down_radius.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numeric_up_down_radius.Name = "numeric_up_down_radius";
            this.numeric_up_down_radius.Size = new System.Drawing.Size(61, 22);
            this.numeric_up_down_radius.TabIndex = 12;
            this.numeric_up_down_radius.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // radioButtonRadius
            // 
            this.radioButtonRadius.AutoSize = true;
            this.radioButtonRadius.Checked = true;
            this.radioButtonRadius.Location = new System.Drawing.Point(5, 21);
            this.radioButtonRadius.Name = "radioButtonRadius";
            this.radioButtonRadius.Size = new System.Drawing.Size(73, 21);
            this.radioButtonRadius.TabIndex = 11;
            this.radioButtonRadius.TabStop = true;
            this.radioButtonRadius.Text = "Radius";
            this.radioButtonRadius.UseVisualStyleBackColor = true;
            // 
            // UserControlTerrain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.group_box_terrain);
            this.Name = "UserControlTerrain";
            this.Size = new System.Drawing.Size(329, 182);
            this.group_box_terrain.ResumeLayout(false);
            this.group_box_terrain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.track_bar_terrain_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.track_bar_terrain_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.track_bar_radius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_up_down_radius)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox group_box_terrain;
        private System.Windows.Forms.Label label_terrain_radius_max;
        private System.Windows.Forms.Label label_terrain_radius_min;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar track_bar_terrain_max;
        private System.Windows.Forms.TrackBar track_bar_terrain_min;
        private System.Windows.Forms.TrackBar track_bar_radius;
        private System.Windows.Forms.NumericUpDown numeric_up_down_radius;
        private System.Windows.Forms.RadioButton radioButtonRadius;
    }
}
