﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Xml.Linq;

namespace HeistMapEditor
{
    public class Interactable : BaseType
    {
        public string _interactable_name;
        public string _secondary_mesh_name;

        public Interactable(string primary_mesh_name, string secondary_mesh_name, Point position, string icon_name, string interactable_name) :
            base(OBJECT_NAME.NAME_INTERACTABLE, primary_mesh_name, position, icon_name)
        {
            _interactable_name = interactable_name;
            _secondary_mesh_name = secondary_mesh_name;
        }

        public override void GetMeshNames(ref List<XElement> xMeshNames)
        {
            xMeshNames.Add(new XElement("MeshName", _mesh_name));

            if (_secondary_mesh_name.Length > 0)
            {
                xMeshNames.Add(new XElement("MeshName", _secondary_mesh_name));
            }
        }
    }
}
